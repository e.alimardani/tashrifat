import React, {Component} from 'react';
import {
    Text,
    TouchableOpacity,
    Dimensions,
    View,
    // AsyncStorage,
    BackHandler,
    Image, StatusBar,
    TouchableNativeFeedback, Alert, TextInput, FlatList
} from 'react-native';
import AsyncStorage from '@react-native-community/async-storage';
import {Spinner, Header, Left, Body, Right, Button, Title} from 'native-base';
import Icon from 'react-native-vector-icons/FontAwesome5';
import LinearGradient from "react-native-linear-gradient";
import {Actions} from "react-native-router-flux";
import {setUser, setUsers} from "../../Redux/Actions";
import {connect} from "react-redux";
import BottomNavigation from '../../components/Shared/bottomNavigation';
import style from './Style'
import ServerData from "../connection/connect";

const width = Dimensions.get('window').width;
const height = Dimensions.get('window').height;
const Connect = new ServerData();

class Search3 extends Component {

    constructor(props) {
        super(props);
        this.state = {
            specialityBySearch3: [],
        };
        this.renderSpecialityForSearch3 = this.renderSpecialityForSearch3.bind(this);
    };

   async componentWillMount() {
       await this.setState({specialityBySearch3: this.props.sub_subCat})
    }
    renderSpecialityForSearch3({item, index}) {
        if (item.name === 'مشخصات') return null;
        return (
            <TouchableOpacity activeOpacity={.7} style={[style.view_item_search,{marginHorizontal:10}]} onPress={() => Actions.push('Search4',{item:item})}>
                <Text style={[style.txt_item_search,{paddingVertical:width*.02}]}>{item.name}</Text>
            </TouchableOpacity>
        )
    }
    render() {
        return (
            <View style={style.view_container_user}>
                <View style={style.view_top}>
                    <LinearGradient style={[style.view_header_user]} start={{x: 0, y: 0}} end={{x: 1, y: 0}}
                                    colors={['#627efb', '#8b68fc', '#a95bfe']}>

                        <Header transparent style={style.view_top_header}>
                            <StatusBar backgroundColor="transparent" translucent={true} barStyle="light-content"/>

                            <Left style={{flex: 1,}}>
                                <Button transparent onPress={() => Actions.pop()}>
                                    <Icon name="arrow-left" color={"#ffffff"} size={width * .07}/>
                                </Button>
                            </Left>
                            <Body style={{flex: 1, alignItems: 'center'}}>
                                <Title style={style.txt_top_header}>{this.props.headerName}</Title>
                            </Body>
                            <Right style={{flex: 1}}>
                                <Button transparent onPress={() => Actions.drawerOpen()}>
                                    <Icon name="bars" color={"#fff"} size={width * .07}/>
                                </Button>
                            </Right>
                        </Header>
                    </LinearGradient>
                </View>

                <View style={style.center_page_users}>
                    <FlatList
                        // style={{display:this.state.displayFilterUser === 'flex' ? 'none' : 'flex'}}
                        data={this.state.specialityBySearch3}
                        numColumns={1}
                        renderItem={this.renderSpecialityForSearch3}
                        keyExtractor={(item, index) => index.toString()}/>
            </View>
                <View>
                    <BottomNavigation styleBottom={'other'} />
                </View>
            </View>
        );
    }
}

const mapDispatchToProps = dispatch => {
    return {
        setUsers: users => {
            dispatch(setUsers(users))
        }
    }
};
const mapStateToProps = (state) => {
    return {
        user: state.user,
    };
};
export default connect(mapStateToProps, mapDispatchToProps)(Search3)

