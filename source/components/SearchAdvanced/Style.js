import React from 'react';
import {StyleSheet, Dimensions, Platform} from 'react-native';


const width = Dimensions.get('window').width;
const height = Dimensions.get('window').height;

export default StyleSheet.create({
    view_container_user: {
        alignItems: 'center',
        flex: 1
    },
    viewNotFound:{width: width * .90, height: height * .60, justifyContent: 'center', alignItems: 'center',},
    txtNotFound:{fontSize: width / 20, color: '#000', fontFamily: 'IRANSansMobile',},
    view_item_search: {
        justifyContent: 'center',
        alignItems: 'center',
        // width: width * .80,
        flex:1,
        // paddingHorizontal:width*.10,
        // height: height * .07,
        backgroundColor: '#fff',
        elevation: 4,
        borderRadius: width * .05,
        marginVertical: height * .01,
    },
    txt_item_search: {
        fontFamily: 'IRANSansMobile',
        color: '#8b68fc',
        fontSize: width / 25,
    },
    view_top: {justifyContent: 'center', alignItems: 'center'},
    view_header_user: {
        height: height / 7,
        width: width / 1,
        // backgroundColor:'#000'
    },
    view_header_search4: {
        width: width / 1,
        // backgroundColor:'#000'
    },
    view_top_header: {
        marginTop: height * .01,
        paddingHorizontal: width * .05,
        width: width,
        alignItems:'center',
        justifyContent:'space-between'
        // flex:1
    },
    txt_top_header: {
        // flex:1,
        color: '#fff',
        fontFamily: 'IRANSansMobile',
        textAlign: 'center',
        fontSize: width / 25
    },
    center_page_users: {flex: 1,width:'100%'},
    center_page_search4: {flex: 1},
    imageCat: {width: width * .20, height: width * .20},
    view_item_search2: {
        width: width /3,
        height: height * .20,
        // marginVertical: height * .01,
        // marginHorizontal: width * .01,
        elevation:5,
        padding:5,
        justifyContent:'center',
        alignItems:'center'
    },
    txt_item_search2:{
        ...Platform.select({
            ios:{
                fontFamily: 'IRANSansMobile',
                fontWeight: 'bold'
            },
            android:{
                fontFamily: 'IRANSansMobile_Bold',
            }
        }),
        // color: '#8b68fc',
        color: '#45555f',
        fontSize: width / 28,
        marginTop: height*.01
    },
    view_user: {
        flexDirection: 'row',
        width: width * .90,
        height: height * .12,
        paddingHorizontal: width * .03,
        marginTop: height * .02,
        backgroundColor: '#fff',
        justifyContent: 'center',
        alignItems: 'center',
        borderRadius: width * .10
    },
    view_user_icon: {alignItems: 'center', flexDirection: 'row', justifyContent: 'center'},
    view_folow: {
        justifyContent: 'center',
        alignItems: 'center',
        width: width * .10,
        height: width * .10,
        borderRadius: width * .05,
        backgroundColor: '#8b68fc',
        marginRight: width * .02
    },
    txt_flow: {
        color: '#7e7e7e',
        fontFamily: 'IRANSansMobile',
        fontSize: width / 30,
    },
    txt_name_user: {fontSize: width / 27, fontFamily: 'IRANSansMobile',},
    view_user_txt: {flex: 1, justifyContent: 'center', alignItems: 'flex-end', marginRight: width * .02},
    view_img: {
        justifyContent: 'flex-end',
        alignItems: 'center',
        borderWidth: width * .01,
        borderRadius: width * .07,
        borderColor: '#d1d1d1'
    },
    pic_user: {
        width: width * .12,
        height: width * .12,
        borderRadius: width * .06,
        // marginTop: height * .08
    },
    view_dropDown:{
        padding:10,
        backgroundColor:'#fff',
        flexDirection:'row-reverse',
        // width:width*.70,
        // height:height*.07,
        // backgroundColor:'#000',
        borderRadius:width*.07,
        justifyContent:'center',
        alignItems:'center',
        marginTop:-25,
        marginBottom:10
    },
    dropdownStyleTextSkill: {
        color: '#fff',
        fontSize: width / 27,
        justifyContent: 'center',
        width: width * .70, 
        backgroundColor: '#8b68fc',
        alignSelf: 'center',
        textAlign: 'center'
    },
    modalTextStyleSkill: {color: '#8b68fc', fontFamily: 'IRANSansMobile', fontSize: width / 28, textAlign: 'center'},
    dropdownContainerStyle: {
        justifyContent: 'center',
        // height: height * .07,
        // width: width * .70,
        paddingVertical:width*.03,
        flex:1,
        marginHorizontal:width*.01,
        paddingRight: width * .02,
        backgroundColor: '#fff',
        borderRadius: width * .10,
        // marginTop: height * .01,
        alignSelf: 'center'
    },
    dropdownStyleFilter:{
        backgroundColor:'transparent',
        height:width*.60,borderWidth:0,
        width:width*.30,
        marginTop:-12
    }

});