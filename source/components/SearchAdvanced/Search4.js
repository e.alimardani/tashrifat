import React, {Component} from 'react';
import {
    Text,
    TouchableOpacity,
    Dimensions,
    View,
    // AsyncStorage,
    BackHandler,
    ActivityIndicator,
    Image, StatusBar,
    TouchableNativeFeedback, Alert, TextInput, FlatList
} from 'react-native';
import AsyncStorage from '@react-native-community/async-storage';
import {Spinner, Header, Left, Body, Right, Button, Title} from 'native-base';
import Icon from 'react-native-vector-icons/FontAwesome5';
import LinearGradient from "react-native-linear-gradient";
import {Actions} from "react-native-router-flux";
import {setUsers} from "../../Redux/Actions";
import {connect} from "react-redux";
import BottomNavigation from '../Shared/bottomNavigation';
import style from './Style'
import ServerData from "../connection/connect";
import ModalDropdown from "react-native-modal-dropdown";
import Modal from "react-native-modal";


const width = Dimensions.get('window').width;
const height = Dimensions.get('window').height;
const Connect = new ServerData();

class Search4 extends Component {

    constructor(props) {
        super(props);
        this.state = {
            ListFilterUsersLoading : true,
            ListFilterUsers: [],
            ListFilterUsers1Loading : true,
            ListFilterUsers1: [],
            page: 1,
            hasMore:true,
            loading:true,
            refreshing:false,
            optionDropDown: [],
            ProvinceList1:[],
            ProvinceList:[],
            CityList:[],
            selectedProvince:'',
            selectedCity:'',
            workInList:[],
            FilterWorkIn:[],
            Genres:[],
            isModalWorkIn:false,
            isModalCity:false,
            isModalProvince:false,
            LoadData:false,
            isModalGenre:false,
            WorkInSelected:'زمینه کاری',
            Genres:[],
            GenreSelected:'ژانر',
            CitySelected:'شهر',
            ProvinceSelected:'استان',
            arrayholder:[]
        };
        this.GetDataFilterUsers = this.GetDataFilterUsers.bind(this);
        this.renderItemListUsers = this.renderItemListUsers.bind(this);
        this.EmptyComponent = this.EmptyComponent.bind(this);
        this.handleLoadMore = this.handleLoadMore.bind(this);
        this.renderFooter = this.renderFooter.bind(this);
        this.onRefresh = this.onRefresh.bind(this);
        this.renderWorkIn = this.renderWorkIn.bind(this);
        this.ListEmpty = this.ListEmpty.bind(this);
        this.renderGenre = this.renderGenre.bind(this);
        this.renderProvince = this.renderProvince.bind(this);
        this.renderCity = this.renderCity.bind(this);


    };

    async componentWillMount() {
       this.GetDataFilterUsers();
       console.log(this.props)
    }
    renderWorkIn({item}){
        return (
                <TouchableOpacity onPress={()=>this.setState({WorkInSelected:item,isModalWorkIn:false,loading:true},()=>this.GetDataFilterUsers())} style={{padding:5}}>
                    <Text style={{fontFamily: 'IRANSansMobile',textAlign:'center',fontSize:18}}>{item}</Text>
                </TouchableOpacity>
        )
    }
    renderGenre({item}){
        return (
            <TouchableOpacity onPress={()=>this.setState({GenreSelected:item.name,isModalGenre:false},()=>this.GetDataFilterUsers())} style={{padding:5}}>
                <Text style={{fontFamily: 'IRANSansMobile',textAlign:'center',fontSize:18}}>{item.name}</Text>
            </TouchableOpacity>
        )   
    }
    renderProvince({item}){
        return (
            <TouchableOpacity onPress={()=>this.setState({ProvinceSelected:item.name,ProvinceSelectedId:item.id,isModalProvince:false},()=>this.GetDataFilterUsers())} style={{padding:5}}>
                <Text style={{fontFamily: 'IRANSansMobile',textAlign:'center',fontSize:18}}>{item.name}</Text>
            </TouchableOpacity>
        )   
    }
    renderCity({item}){
        return (
            <TouchableOpacity onPress={()=>this.setState({CitySelected:item.name,isModalCity:false,loading:true},()=>this.GetDataFilterUsers())} style={{padding:5}}>
                <Text style={{fontFamily: 'IRANSansMobile',textAlign:'center',fontSize:18}}>{item.name}</Text>
            </TouchableOpacity>
        )   
    }
    ListEmpty() {
        if(this.state.LoadData)
        {
            return(
                <ActivityIndicator style={{margin:20}} color={this.props.global.grColorTwo} size='large' />
            );
        }
        else
        {
            return(
             <View style={style.viewNotFound}>
                <Text style={style.txtNotFound}>اطلاعاتی وجود ندارد</Text>
            </View>
            );
        }   
    }
    async getGenre(){

        try {
            const details = {
                "admintoken": this.props.global.adminToken,
            };
            let result = await Connect.sendPost(this.props.global.baseApiUrl + '/user/get_genre', details);
            console.log(result)

            if (result.success) {
                    this.setState({Genres:result.data,LoadData:false})
            } else {
                this.setState({LoadData:false});
            }
           } catch (error) {
            this.setState({LoadData:false});
           }
    }

    async GetDataFilterUsers() {
        page = this.state.page
        try{
        const details = {
            "admintoken": this.props.global.adminToken,
            "genre": (this.state.GenreSelected == 'ژانر') ? '' : this.state.GenreSelected,
            "speciality": (this.state.WorkInSelected == 'زمینه کاری') ? '' : this.state.WorkInSelected,
            "city": (this.state.CitySelected == 'شهر') ? '' : this.state.CitySelected,
            "province": (this.state.ProvinceSelected == 'استان') ? '' : this.state.ProvinceSelected,
            'skil' : this.props.item.name ? this.props.item.name :''

        };
        let result = await Connect.sendPost(this.props.global.baseApiUrl+ '/user/Serach_User_With_Specialty', details);
        console.log(details)
        console.log(result)
        if (result.success) {
            console.log('sdfsdfsdf')
                this.setState({
                        ListFilterUsers: result.data,
                        arrayholder:result.data,
                        loading: false,
                        refreshing:false,
                        displayFilterUser: 'flex'
                });
                console.log(this.state.ListFilterUsers)


            //let sss = [];
         
            // for (let i = 0; i < ListFilterUsers.length; i++) {
            //     if(ListFilterUsers[i].full_name !== this.props.user.full_name){
            //         if (ListFilterUsers[i].work_in !== '') {
            //             sss.push(ListFilterUsers[i].work_in);
            //         }
            //         else {
            //             await this.setState({
            //                 loading:false,
            //                 hasMore:false,
            //                 refreshing:false
            //             });
            //         }
            // }
            // }
            // await this.setState({optionDropDown: sss});
          
        } else {
            await this.setState({
                loading:false,
                hasMore:false,
                refreshing:false,
                ListFilterUsers: result.data,
            });
        }

        }catch(error){
            await this.setState({
                loading: false,
                refreshing:false,
                hasMore:false,

            })
        };
        
    }
    async GetDataProvinceList() {
        try{
        const details = {
            "admintoken": this.props.global.adminToken,  
        };
        let result = await Connect.sendPost(this.props.global.baseApiUrl+ '/user/get_province_list', details);
            if (result.success) {
                this.setState({ProvinceList:result.data})    
            } else { }
        }catch(error){};
    }
    async GetCity(){
        try{
            const details = {
                "admintoken": this.props.global.adminToken,  
                "province_id": this.state.ProvinceSelectedId
            };
            let result = await Connect.sendPost(this.props.global.baseApiUrl+ '/user/get_city_by_province_id', details);
                if (result.success) {
                    this.setState({CityList:result.data,LoadData:false})    
                } else {}
            }catch(error){}; 
    }
    async GetDataSkillAndUpdate() {
        const details = {
            "admintoken": this.props.global.adminToken,
        };
       let result = await Connect.sendPost(this.props.global.baseApiUrl + '/post/getProfileFields', details)
                if (result.success === true) {
                    const findWorkIn = result.data.find( a => a.name === "زمینه کاری" );
                    await this.setState({work_in:findWorkIn,LoadData:false}) 
                    let a =[]
                    for(let name of findWorkIn.option){
                        a.push(name.name)
                    }
                    this.setState({FilterWorkIn:a})
                       
                } else {
                    this.setState({LoadData:false})
                }
    }
    async SortUsersWithWorkIn(index, item) {
        await this.setState({ListFilterUsers1: ''});
        let search = this.state.ListFilterUsers.filter(a => a.work_in === item)
        await this.setState({ListFilterUsers1: search, refreshing: false, loading: false,});
    }
    renderItemListUsers =({item, index})=>{
     
            
                return (
                    <TouchableOpacity activeOpacity={.5} style={style.view_user} onPress={()=>Actions.push('Profile_user',{ListUsers:item})}>
             
                    {this.props.user.apiToken !== null && item.Frindship_Status_With_Current_User == 'nothing' &&
                    <View style={style.view_user_icon}>
                        <TouchableOpacity activeOpacity={.5} onPress={() => this.FollowRequest(index)}>
                            <LinearGradient style={style.view_folow} start={{x: 0, y: 0}} end={{x: 1, y: 0}}
                                            colors={['#5e38c2', '#4c26b3']}>
                                <Icon size={width * .05} color={'#fff'} name={'user-plus'}/>
                            </LinearGradient>
                        </TouchableOpacity>
                        <Text style={style.txt_flow}>دنبال کردن</Text>
                    </View>
                    }
                    {this.props.user.apiToken !== null && item.Frindship_Status_With_Current_User == 'friend' || item.Frindship_Status_With_Current_User == 'Requested' &&
                    <View style={style.view_user_icon}>
                        <TouchableOpacity activeOpacity={.5} onPress={() => this.UnFollowRequest(index)}>
                            <LinearGradient style={style.view_folow} start={{x: 0, y: 0}} end={{x: 1, y: 0}}
                                            colors={['#ff6f75', '#fe4f4b']}>
                                <Icon size={width * .05} color={'#fff'} name={'user-minus'}/>
                            </LinearGradient>
                        </TouchableOpacity>
                        <Text style={style.txt_flow}>لغو دنبال کردن</Text>
                    </View>
                    }
                    <View style={style.view_user_txt}>
                        <Text
                            style={style.txt_name_user}>{item.full_name} {`( ${item.work_in} )`}</Text>
                    </View>
                    <View style={style.view_img}>
                        {item.avatar=='' &&  <Image source={require('../../assets/images/userIconLogin.png')} style={style.pic_user} resizeMode={'cover'}/>}
                        {item.avatar!='' && <Image source={{uri: item.avatar}} style={style.pic_user}/>}
                    </View>
              
               </TouchableOpacity>
                )
    };

    EmptyComponent() {
        if(this.state.loading)
        {
            return(
                <ActivityIndicator  color={this.props.global.grColorTwo} size='large' />
            );
        }
        else 
        {
            return(
             <View style={style.viewNotFound}>
                <Text style={style.txtNotFound}>کاربری وجود
                    ندارد</Text>
            </View>
            );
        }   
        
    }

    onRefresh() {
        this.setState({page: 1, refreshing : true }, () => {
            this.GetDataFilterUsers();
        })
    }

   async handleLoadMore() {

        if (this.state.ListFilterUsers.length > 0) {
            await this.setState({page : this.state.page + 1 , loading : true}, async () => {
                this.GetDataFilterUsers();
            })
        }
    }

    renderFooter() {
        if(this.state.loading && this.state.ListFilterUsers.length > 0)
        {
            return <ActivityIndicator style={{alignSelf: 'center',marginBottom: 20}} color={this.props.global.grColorTwo} />
        }
        else {
            return null;
        }
    }
    searchMember(text){
        const newData = this.state.arrayholder.filter(item => {      
            const itemData = `${item.full_name.toUpperCase()}`;
            
             const textData = text.toUpperCase();
              
             return itemData.indexOf(textData) > -1;    
          });
          
          this.setState({ ListFilterUsers: newData });  
    }

    render() {
        return (
            <View style={style.view_container_user}>
                <View style={style.view_top}>
                    <LinearGradient style={[style.view_header_search4]} start={{x: 0, y: 0}} end={{x: 1, y: 0}}
                                    colors={['#627efb', '#8b68fc', '#a95bfe']}>

                        <Header transparent style={style.view_top_header}>
                            <StatusBar backgroundColor="transparent" translucent={true} barStyle="light-content"/>

                            <Left style={{flex: 1,}}>
                                <Button transparent onPress={() => Actions.pop()}>
                                    <Icon name="arrow-left" color={"#ffffff"} size={width * .07}/>
                                </Button>
                            </Left>
                            <Body style={{flex: 1, alignItems: 'center'}}>
                                <Title style={style.txt_top_header}>{'فیلتر کاربران'}</Title>
                            </Body>
                            <Right style={{flex: 1}}>
                                <Button transparent onPress={() => Actions.drawerOpen()}>
                                    <Icon name="bars" color={"#fff"} size={width * .07}/>
                                </Button>
                            </Right>
                        </Header>
                        <TextInput
                        onChangeText={text => this.searchMember(text)}
                        placeholder='جستجو کنید ...'
                        style={{borderRadius:25,paddingHorizontal:15,textAlign:'right',fontFamily:'IRANSansMobile',marginHorizontal:30,backgroundColor:'white',height:50,marginBottom:40}}/>

                    </LinearGradient>
                    
                    <View style={style.view_dropDown}>
                    <View style={{alignItems:'center'}}>
                        {this.state.WorkInSelected != 'زمینه کاری' &&
                        <TouchableOpacity onPress={()=>this.setState({WorkInSelected:'زمینه کاری'},()=>this.GetDataFilterUsers())}>
                            <Icon name="times-circle" color="red" style={{fontSize:18}}/>
                        </TouchableOpacity>
                        }
                        <TouchableOpacity onPress={()=>this.setState({isModalWorkIn:true,LoadData:true},()=>this.GetDataSkillAndUpdate())} style={{paddingHorizontal:10,borderLeftWidth:.8,borderLeftColor:'#662e9d'}}>
                            <Text style={style.modalTextStyleSkill}>{this.state.WorkInSelected}</Text>
                        </TouchableOpacity>
                    </View>
                    <View  style={{alignItems:'center'}}>
                        {this.state.GenreSelected != 'ژانر' &&
                        <TouchableOpacity onPress={()=>this.setState({GenreSelected:'ژانر'},()=>this.GetDataFilterUsers())}>
                            <Icon name="times-circle" color="red" style={{fontSize:18}}/>
                        </TouchableOpacity>
                        }
                        <TouchableOpacity onPress={()=>this.setState({isModalGenre:true,LoadData:true},()=>this.getGenre())}  style={{paddingHorizontal:10,borderLeftWidth:.8,borderLeftColor:'#662e9d'}}>
                            <Text style={style.modalTextStyleSkill}>{this.state.GenreSelected}</Text>
                        </TouchableOpacity>
                    </View>
                    <View  style={{alignItems:'center'}}>
                        {this.state.ProvinceSelected != 'استان' &&
                        <TouchableOpacity onPress={()=>this.setState({ProvinceSelected:'استان'},()=>this.GetDataFilterUsers())}>
                            <Icon name="times-circle" color="red" style={{fontSize:18}}/>
                        </TouchableOpacity>
                        }
                        <TouchableOpacity onPress={()=>this.setState({isModalProvince:true,LoadData:true},()=>this.GetDataProvinceList())} style={{paddingHorizontal:10,borderLeftWidth:.8,borderLeftColor:'#662e9d'}}>
                            <Text style={style.modalTextStyleSkill}>{this.state.ProvinceSelected}</Text>
                        </TouchableOpacity>
                    </View>
                    <View  style={{alignItems:'center'}}>
                        {this.state.CitySelected != 'شهر' &&
                        <TouchableOpacity onPress={()=>this.setState({CitySelected:'شهر'},()=>this.GetDataFilterUsers())}>
                            <Icon name="times-circle" color="red" style={{fontSize:18}}/>
                        </TouchableOpacity>
                        }
                        <TouchableOpacity onPress={()=>this.setState({isModalCity:true,LoadData:true},()=>this.GetCity())} style={{paddingHorizontal:10}}>
                            <Text style={style.modalTextStyleSkill}>{this.state.CitySelected}</Text>
                        </TouchableOpacity>
                    </View>
                   
                    
                    
                        {/* <ModalDropdown
                            defaultValue={this.state.selectedCity ===''?'فیلتر شهرستان':this.state.selectedCity}
                            animated={true}
                            dropdownTextStyle={style.dropdownStyleTextSkill}
                            textStyle={style.modalTextStyleSkill}
                            dropdownStyle={style.dropdownStyleFilter} 
                            style={style.dropdownContainerStyle}
                            options={this.state.CityList}
                            onSelect={this.SortUsersWithCity}/>
                        <ModalDropdown
                            defaultValue={this.state.selectedProvince === ''?'فیلتر ژانر':this.state.selectedProvince}
                            animated={true}
                            dropdownTextStyle={[style.dropdownStyleTextSkill,]}
                            textStyle={style.modalTextStyleSkill}
                            dropdownStyle={[style.dropdownStyleFilter,{position:'absolute',top:0,right:width*.04,left:0}]} 
                            style={[style.dropdownContainerStyle,{borderRadius:0,borderLeftWidth:1,borderLeftWidth:1,borderColor:'#00000020'}]}
                            options={this.state.ProvinceList}
                            onSelect={this.SortUsersWithProvince}/>
                        <ModalDropdown
                            defaultValue={this.state.selectedProvince === ''?'فیلتر استان':this.state.selectedProvince}
                            animated={true}
                            dropdownTextStyle={[style.dropdownStyleTextSkill,]}
                            textStyle={style.modalTextStyleSkill}
                            dropdownStyle={[style.dropdownStyleFilter,{position:'absolute',top:0,right:width*.04,left:0}]} 
                            style={[style.dropdownContainerStyle,{borderRadius:0,borderRightWidth:1,borderLeftWidth:1,borderColor:'#00000020'}]}
                            options={this.state.ProvinceList}
                            onSelect={this.SortUsersWithProvince}/>
                            */}
                        {/* <ModalDropdown
                            defaultValue={'فیلتر زمینه کاری'}
                            animated={true}
                            dropdownTextStyle={[style.dropdownStyleTextSkill,{marginRight:0}]}
                            textStyle={style.modalTextStyleSkill}
                            dropdownStyle={style.dropdownStyleFilter} 
                            style={[style.dropdownContainerStyle,]}
                            options={this.state.FilterWorkIn}
                            onSelect={this.SortUsersWithWorkIn}/>  */}
                    </View>
                </View>

                <View style={style.center_page_search4}>

               {/* {(this.state.loading) &&
                     <View style={{ height : Dimensions.get('window').height * .2  , justifyContent:'center',alignItems:'center' }}>
                        <ActivityIndicator color={this.props.global.grColorTwo} size='large'/>
                    </View>
                    } 
                        {(!this.state.loading) &&  */}
          
                        <FlatList
                            keyExtractor={(item, index) => index.toString()}
                            data={this.state.ListFilterUsers}
                            renderItem={this.renderItemListUsers}
                            ListEmptyComponent={this.EmptyComponent}
                            showsVerticalScrollIndicator={false}
                            
                            refreshing={this.state.refreshing}
                            onRefresh={this.onRefresh}
                            ListFooterComponent={this.renderFooter}
                        />
                    {/* } */}
                   

                </View>
                <View>
                    <BottomNavigation styleBottom={'other'}/>
                </View>
                <Modal isVisible={this.state.isModalWorkIn}
                       backdropColor={'#000000'}
                       backdropOpacity={0.70}
                       onBackButtonPress={() => this.setState({isModalWorkIn: !this.state.isModalWorkIn})}
                       onBackdropPress={() => this.setState({isModalWorkIn: !this.state.isModalWorkIn})}>
                    <View style={{backgroundColor:'#fff',borderRadius:15,marginHorizontal:15}}>
                    <FlatList
                        keyExtractor={(item, index) => index.toString()}
                        data={this.state.FilterWorkIn}
                        renderItem={this.renderWorkIn}
                        ListEmptyComponent={this.ListEmpty}/>
                    </View>
                </Modal>
                <Modal isVisible={this.state.isModalGenre}
                       backdropColor={'#000000'}
                       backdropOpacity={0.70}
                       onBackButtonPress={() => this.setState({isModalGenre: !this.state.isModalGenre})}
                       onBackdropPress={() => this.setState({isModalGenre: !this.state.isModalGenre})}>
                    <View style={{backgroundColor:'#fff',borderRadius:15,marginHorizontal:15}}>
                    <FlatList
                        keyExtractor={(item, index) => index.toString()}
                        data={this.state.Genres}
                        renderItem={this.renderGenre}
                        ListEmptyComponent={this.ListEmpty}/>
                    </View>
                </Modal>
                <Modal isVisible={this.state.isModalProvince}
                       backdropColor={'#000000'}
                       backdropOpacity={0.70}
                       onBackButtonPress={() => this.setState({isModalProvince: !this.state.isModalProvince})}
                       onBackdropPress={() => this.setState({isModalProvince: !this.state.isModalProvince})}>
                    <View style={{backgroundColor:'#fff',borderRadius:15,marginHorizontal:15}}>
                    <FlatList
                        keyExtractor={(item, index) => index.toString()}
                        data={this.state.ProvinceList}
                        renderItem={this.renderProvince}
                        ListEmptyComponent={this.ListEmpty}/>
                    </View>
                </Modal>
                <Modal isVisible={this.state.isModalCity}
                       backdropColor={'#000000'}
                       backdropOpacity={0.70}
                       onBackButtonPress={() => this.setState({isModalCity: !this.state.isModalCity})}
                       onBackdropPress={() => this.setState({isModalCity: !this.state.isModalCity})}>
                    <View style={{backgroundColor:'#fff',borderRadius:15,marginHorizontal:15}}>
                    {this.state.ProvinceSelected == 'استان' && <Text style={{paddingVertical:40,textAlign:'center',fontFamily:'IRANSansMobile',color:'black',fontSize:16}}>ابتدا یک استان انتخاب کنید</Text>}
                    {this.state.ProvinceSelected != 'استان' && 
                    <FlatList
                        keyExtractor={(item, index) => index.toString()}
                        data={this.state.CityList}
                        renderItem={this.renderCity}
                        ListEmptyComponent={this.ListEmpty}/>
                    }
                    </View>
                </Modal>
            </View>
        );
    }

   
}

const mapDispatchToProps = dispatch => {
    return {
        setUsers: users => {
            dispatch(setUsers(users))
        }
    }
};
const mapStateToProps = (state) => {
    return {
        user: state.user,
        global: state.global
    };
};
export default connect(mapStateToProps, mapDispatchToProps)(Search4)

