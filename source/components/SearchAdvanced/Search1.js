import React, {Component} from 'react';
import {
    Text,
    TouchableOpacity,
    Dimensions,
    View,
    // AsyncStorage,
    BackHandler,
    Image, StatusBar,
    TouchableNativeFeedback, Alert, TextInput, FlatList
} from 'react-native';
import AsyncStorage from '@react-native-community/async-storage';
import {Spinner, Header, Left, Body, Right, Button, Title} from 'native-base';
import Icon from 'react-native-vector-icons/FontAwesome5';
import LinearGradient from "react-native-linear-gradient";
import {Actions} from "react-native-router-flux";
import {setUser, setUsers} from "../../Redux/Actions";
import {connect} from "react-redux";
import BottomNavigation from '../../components/Shared/bottomNavigation';
import style from './Style'
import ServerData from "../connection/connect";

const width = Dimensions.get('window').width;
const height = Dimensions.get('window').height;
const Connect = new ServerData();

class Search1 extends Component {

    constructor(props) {
        super(props);
        this.state = {
            specialityBySearch: []
        };
        this.renderSpecialityForSearch = this.renderSpecialityForSearch.bind(this);
        this.GetSpecialityBySearch = this.GetSpecialityBySearch.bind(this);
        this.subCat = this.subCat.bind(this);
        this.EmptyComponent = this.EmptyComponent.bind(this);
    };

    async componentWillMount() {
        await this.GetSpecialityBySearch();
    }
    async GetSpecialityBySearch() {
        const details = {
            "admintoken": this.props.global.adminToken,
        };
        let result = await Connect.sendPost(this.props.global.baseApiUrl + '/user/getSpeciality_By_Category', details);
        if (result.success === true) {
            this.setState({
                specialityBySearch: result.data
            });
        } else {
        }
    }
    EmptyComponent() {
        if (this.state.specialityBySearch.length > 0) return null;
        return <Spinner color="#8b68fc"/>
    }
    subCat(item) {
        let subCat = [];
        for (let i = 0; i < item.SubCategory.length; i++) {
            subCat.push(item.SubCategory[i])
        }
        Actions.push('Search2', {subCat: subCat, headerName: item.name})
    }

    renderSpecialityForSearch({item, index}) {
        if (item.name === 'مشخصات' || item.name === 'اطلاعات کاربری' || item.name === 'حیطه کاری') return null;
        return (
            <TouchableOpacity activeOpacity={.7} style={[style.view_item_search,{margin:5,marginHorizontal:15}]} onPress={() => this.subCat(item)}>
                <Text style={[style.txt_item_search,{paddingHorizontal:15,paddingVertical:5}]}>{item.name}</Text>
            </TouchableOpacity>
        )
    }
    render() {
        return (
            <View style={style.view_container_user}>
                <View style={style.view_top}>
                    <LinearGradient style={[style.view_header_user]} start={{x: 0, y: 0}} end={{x: 1, y: 0}}
                                    colors={['#627efb', '#8b68fc', '#a95bfe']}>

                        <Header transparent style={style.view_top_header}>
                            <StatusBar backgroundColor="transparent" translucent={true} barStyle="light-content"/>

                            <Left style={{flex: 1,}}>
                                <Button transparent onPress={() => Actions.pop()}>
                                    <Icon name="arrow-left" color={"#ffffff"} size={width * .07}/>
                                </Button>
                            </Left>
                            <Body style={{flex: 1, alignItems: 'center'}}>
                                <Title style={style.txt_top_header}>درخواست خدمت</Title>
                            </Body>
                            <Right style={{flex: 1}}>
                                <Button transparent onPress={() => Actions.drawerOpen()}>
                                    <Icon name="bars" color={"#fff"} size={width * .07}/>
                                </Button>
                            </Right>
                        </Header>
                    </LinearGradient>
                </View>

                <View style={style.center_page_users}>
                    <FlatList
                        data={this.state.specialityBySearch}
                        numColumns={1}
                        renderItem={this.renderSpecialityForSearch}
                        ListEmptyComponent={this.EmptyComponent}
                        keyExtractor={(item, index) => index.toString()}/>
                </View>
                <View>
                    <BottomNavigation styleBottom={'other'}/>
                </View>
            </View>
        );

    }
}

const mapDispatchToProps = dispatch => {
    return {
        setUsers: users => {
            dispatch(setUsers(users))
        }
    }
};
const mapStateToProps = (state) => {
    return {
        user: state.user,
        global: state.global
    };
};
export default connect(mapStateToProps, mapDispatchToProps)(Search1)

