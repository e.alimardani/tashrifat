import React, {Component} from 'react';
import {
    Text,
    TouchableOpacity,
    Dimensions,
    View,
    // AsyncStorage,
    BackHandler,
    Image, StatusBar,
    TouchableNativeFeedback, Alert, TextInput, FlatList
} from 'react-native';
import AsyncStorage from '@react-native-community/async-storage';
import {Spinner, Header, Left, Body, Right, Button, Title} from 'native-base';
import Icon from 'react-native-vector-icons/FontAwesome5';
import LinearGradient from "react-native-linear-gradient";
import {Actions} from "react-native-router-flux";
import {setUser, setUsers} from "../../Redux/Actions";
import {connect} from "react-redux";
import BottomNavigation from '../../components/Shared/bottomNavigation';
import style from './Style'
import ServerData from "../connection/connect";

const width = Dimensions.get('window').width;
const height = Dimensions.get('window').height;
const Connect = new ServerData();

class Search2 extends Component {

    constructor(props) {
        super(props);
        this.state = {
            specialityBySearch2: []
        };
        this.renderSpecialityForSearch2 = this.renderSpecialityForSearch2.bind(this);
        this.sub_subCat = this.sub_subCat.bind(this);
    };

   async componentWillMount() {

      await  this.setState({specialityBySearch2: this.props.subCat})
    }

    sub_subCat(item) {
        // this.setState({ModalSubSubCat: true, headerText1: item.name})
        let sub_subCat = [];
        for (let i = 0; i < item.Sub_sub_Category.length; i++) {
            sub_subCat.push(item.Sub_sub_Category[i])
        }
        // this.setState({sub_subCat: sub_subCat})
        Actions.push('Search3',{sub_subCat:sub_subCat,headerName: item.name})
    }

    renderSpecialityForSearch2({item, index}) {
        if (item.name === 'مشخصات') return null;
        const backgroundColor = index%2 === 0 ? '#86a5b0' : '#dee9ed';
        return (
            <TouchableOpacity activeOpacity={.7} style={[style.view_item_search2,{backgroundColor:backgroundColor}]} onPress={() => this.sub_subCat(item)}>
                {item.subcategory_image !="none" && 
                    <Image source={{uri:item.subcategory_image}} style={style.imageCat} resizeMode={'contain'}/>
                }
                <Text style={[style.txt_item_search2,{textAlign:'center'}]}>{item.name}</Text>
            </TouchableOpacity>
        )
    }



    render() {
        return (
            <View style={style.view_container_user}>
                <View style={style.view_top}>
                    <LinearGradient style={[style.view_header_user]} start={{x: 0, y: 0}} end={{x: 1, y: 0}}
                                    colors={['#627efb', '#8b68fc', '#a95bfe']}>

                        <Header transparent style={style.view_top_header}>
                            <StatusBar backgroundColor="transparent" translucent={true} barStyle="light-content"/>
                            <View>
                                <Button transparent onPress={() => Actions.pop()}>
                                    <Icon name="arrow-left" color={"#ffffff"} size={width * .07}/>
                                </Button>
                            </View>
                            <View>
                                <Title style={style.txt_top_header}>{this.props.headerName}</Title>
                            </View>
                            <View >
                                <Button transparent onPress={() => Actions.drawerOpen()}>
                                    <Icon name="bars" color={"#fff"} size={width * .07}/>
                                </Button>
                            </View>
                        </Header>
                    </LinearGradient>
                </View>

                <View style={style.center_page_users}>
                    <FlatList
                        data={this.state.specialityBySearch2}
                        numColumns={3}
                        showsVerticalScrollIndicator={false}
                        renderItem={this.renderSpecialityForSearch2}
                        keyExtractor={(item, index) => index.toString()}/>
                </View>
                <View>
                    <BottomNavigation styleBottom={'other'} />
                </View>
            </View>
        );

    }
}

const mapDispatchToProps = dispatch => {
    return {
        setUsers: users => {
            dispatch(setUsers(users))
        }
    }
};
const mapStateToProps = (state) => {
    return {
        user: state.user,
    };
};
export default connect(mapStateToProps, mapDispatchToProps)(Search2)

