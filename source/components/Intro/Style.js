import React from 'react';
import {StyleSheet, Dimensions, Platform} from 'react-native';


const width = Dimensions.get('window').width;
const height = Dimensions.get('window').height;

export default StyleSheet.create({
    viewKoliLogin:{flex:1, backgroundColor: '#fff',justifyContent:'flex-start',alignItems:'center'},
    swiperContainer:{
        backgroundColor:'transparent',
        ...Platform.select({
            android: {
                width: width
            }
        }),
        justifyContent:'center',
        alignItems:'center',
        paddingTop: width*.50,
        height:height ,
    },
    view_container_login: {
        backgroundColor:'#fff',
        padding: width*.10,
        marginHorizontal: width*.10,
        justifyContent:'center',
        alignItems:'center',
        borderRadius:20,
        ...Platform.select({
            ios: {
                shadowColor: 'rgb(0, 0, 0)',
                shadowOffset: { width: 0, height: 0 },
                shadowOpacity: 0.3,
                shadowRadius: 6,
            },
            android: {
                elevation: 5
            }
        }),
        marginTop:width*.60,
        marginBottom:width*.05,
    },
    dot:{
        backgroundColor:'#393939',
        width: 8,
        height: 8,
        borderRadius: 4,
        marginLeft: 3,
        marginRight: 3,
        marginTop: 3,
        marginBottom: 3
    },
    activeDot:{
        backgroundColor: 'rgba(0,0,0,0.3)',
        width: 8,
        height: 8,
        borderRadius: 4,
        marginLeft: 3,
        marginRight: 3,
        marginTop: 3,
        marginBottom: 3
    },
    
    back_login:{
        position: 'absolute',
    width:width,
    height:height * .40,
    top:0
},
    userIcon_login:{width:width*.15,height:width*.15,},
    txt_login_header:{fontFamily: 'IRANSansMobile',fontSize:width/28,color:"#5b3862"},
    view_container_form_login: {
        width: width * .60,
        backgroundColor: '#ff513e'
    },
    
    view_input_Item: {
        marginTop: 10,
        marginBottom:-10
    },
    username_input: {
        color: '#000', fontFamily: 'IRANSansMobile',
        backgroundColor:'rgb(250,239,253)',
        fontSize: width / 28,
        padding:0,
        width: width * .60,
        height:height*.07,
        borderRadius: width * .10,
        textAlign:'center',
        alignItems:'center',
        justifyContent:'center',
    },
    wrong_userName: {
        color: '#ff2e2e', fontFamily: 'IRANSansMobile',
        fontSize: width / 25,
    },
    txt_exit_dialog: {lineHeight: height / 30, color: '#222222', fontSize: width / 25, fontFamily: 'IRANSansMobile'},
    view_btn_login: {
        justifyContent: 'center',
        alignItems: 'center',
        width: "60%",
        height:50,
        borderRadius: width * .10,
        marginTop: height * .04,
        backgroundColor: '#5b3862',
    },
    txt_btn_login: {
        color: '#fff',
        fontFamily: 'IRANSansMobile',
        fontSize: width / 25,
    },
    view_without_username: {
        flexDirection: 'row',
        marginTop: height * .01,
    },
    view_without_username1: {
        flexDirection: 'row',

        marginTop: height * .02,
    },
    txt_signup: {
        color: '#5689ff', fontFamily: 'IRANSansMobile',
        fontSize: width / 28,
    },
        view_container_signup:{flex: 1},
    
        view_container_form_signUp: {
            flex: 1,
            // height:height*.80,
            justifyContent: 'center',
            alignItems: 'center',
            // marginTop: height *.05,
            // backgroundColor: "#ff8b17",
            paddingTop:height*.02
    
        },
    
        wrong_userName:{color:'#f01a1a',fontFamily: 'IRANSansMobile', fontSize: width / 25,},
        wrong_userName_email:{color:'#f01a1a',fontFamily: 'IRANSansMobile', fontSize: width / 25},
        view_btn_signUp: {
            justifyContent: 'center',
            alignItems: 'center',
            width: width * .30,
            height: height * .07,
            borderRadius: width * .02,
            marginTop: height *.03,
            backgroundColor: '#fff',
        },
        txt_btn_signUp: {
            color: '#1c8bd9',
            fontFamily: 'IRANSansMobile',
            fontSize: width / 25,
        },
        view_without_username: {
            flexDirection: 'row',
            marginTop: height * .02,
        },
        view_without_username1: {
            flexDirection: 'row',
    
            marginTop: height * .01,
        },
        txt_signup: {
            color: '#fff', fontFamily: 'IRANSansMobile',
            fontSize: width / 28,
        },
        bottom_signUp:{height:height*.20,width:width,justifyContent:'center',alignItems:'center',paddingBottom:height*.02,marginTop:height*.01},
        ModalEditSkillStyle:{
            flex: 1,
            alignItems: 'center',
            justifyContent: 'flex-end',
            margin: 0,
            padding: 0,
            // width:width,
            height:height*70,
            // backgroundColor:'#000',
            borderTopLeftRadius: width * .15,
            borderTopRightRadius: width * .15,
        },
        viewModalEditSkillState:{
            backgroundColor:'#8b68fc',
            height: height * .64,
            width: width * .75,
            paddingVertical:height*.02,
            borderTopLeftRadius: width * .12,
            borderTopRightRadius: width * .12,
            justifyContent: 'flex-start', alignItems: 'center',
        },
        itemOptionModal:{width:width*.80,height:height*.06,marginTop:height*.003,justifyContent:'center',alignItems:'center'},
        txt_itemOptionModal:{fontSize:width/25,fontFamily:'IRANSansMobile',color:'#fff'},
        btnExitModal:{backgroundColor:'#fff',width:width*.10,height:width*.10,position:'absolute',borderRadius:width*.05,top:width* -(.05),justifyContent:'center',alignItems:'center'}
    
});
