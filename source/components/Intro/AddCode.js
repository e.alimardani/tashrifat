import React, {Component} from 'react';
import {
    Text,
    TouchableOpacity,
    Dimensions,
    View,
    KeyboardAvoidingView,
    TextInput,
    BackHandler, StatusBar, Alert, Image,Linking
} from 'react-native';
import AsyncStorage from '@react-native-community/async-storage';
import {Form, Item, Label, Input, Button, Content} from 'native-base';
import ServerData from '../connection/connect';
import style from './Style';
import {Actions} from 'react-native-router-flux';
import {ProgressDialog} from 'react-native-simple-dialogs';
import {KeyboardAwareScrollView} from "react-native-keyboard-aware-scroll-view";
import Swiper from 'react-native-swiper';
import {setUser} from "../../Redux/Actions";
import {connect} from "react-redux";
import CodeInput from 'react-native-confirmation-code-input';

const width = Dimensions.get('window').width;
const height = Dimensions.get('window').height;
const Connect = new ServerData();

class AddCode extends Component {
    constructor(props) {
        super(props);
        this.state = {
            code:'',
            curTime:'',
            canGetNewCode:'',
            message:''

        }
    };
    componentWillMount(){
        this.startTimer();
    }
    toPersian(context) {
		const arrayMap = ["۰", "۱", "۲", "۳", "۴", "۵", "۶", "۷", "۸", "۹"];
		const newContext = context.toString().replace(/\d/g, (index) => arrayMap[index]);
		return newContext;
	}
    startTimer() {
        clearInterval(this.codInterval)
		let timeLeft = 59;
		this.setState({ canGetNewCode: false });

		this.setState({ curTime: "۰ : " + this.toPersian(timeLeft) });

		this.codInterval = setInterval(() => {
			timeLeft > 10 ? this.setState({ curTime: "۰ : " + this.toPersian(--timeLeft) }) : this.setState({ curTime: "۰ : ۰" + this.toPersian(--timeLeft) });

			if (timeLeft === 0) {
				clearInterval(this.codInterval)
                this.setState({ curTime: "" });
                this.setState({ canGetNewCode: true });
			}
		}, 1000);
    }
    showRecode() {
		if (this.state.canGetNewCode) {
			return (
				<Button onPress={() => this.sendAgain()} style={{ flex: 1, justifyContent: "center",paddingHorizontal:10, borderRadius: 5, flexGrow: 1,marginHorizontal:20,backgroundColor:'#5b3862' }}>
					<Text style={{ fontSize: 14,textAlign:'center',fontFamily:'IRANSansMobile',color:'white' }}>ارسال مجدد کد فعال سازی</Text>
				</Button>
			);
		}
		if (!this.state.canGetNewCode) {
			return (
				<Button disabled style={{ borderRadius: 5, opacity: 0.5 ,backgroundColor:'#5b3862'}}>
					<Text style={{ paddingHorizontal:10,fontSize: 14,textAlign:'center',fontFamily:'IRANSansMobile',color:'white' }}>ارسال مجدد کد فعال سازی</Text>
				</Button>
			);
		}
    }
    showTimer() {
		if (this.state.canGetNewCode) {
			return null;
		}
		if (!this.state.canGetNewCode) {
			return (
				<View style={{ backgroundColor:'#ed5d81',opacity:.9,width: 55, height: 45,marginRight:10, alignItems: "center",justifyContent:'center', borderRadius: 5 }}>
					<Text style={{fontSize: 14, color: "white",fontFamily:'IRANSansMobile' }}>{this.state.curTime}</Text>
				</View>
			);
		}
    }
    async ResendSms(){
        try {
            let formData = new FormData();
            formData.append("admintoken", this.props.global.adminToken);
            formData.append("user_id",  this.props.userId);

            console.log(formData)

            let response = await fetch(this.props.global.baseApiUrl + '/user/resend_sms',
                {
                    method: "POST",
                    body: formData,
                });
                let json = await response.json();
                console.log(json)

                if (json.success === true) {
                    this.setState({message:''});
                } else {
                    this.setState({progressVisible: false,message:json.message});
                }

        } catch (error) {
            console.log(error)         
        }
    }

    sendAgain() {
        this.setState({ canGetNewCode: false ,message:''});
        this.startTimer();
        this.ResendSms()
    }


   async btn_login() {
        this.setState({flex_wrongUseName: 'none', flex_wrongPassword: 'none'});
        let error = false;
        if (this.state.username === '') {
            error = true;

            this.setState({
                flex_wrongUseName: 'flex',
                txt_wrongUserName: ' شماره تماس خود را وارد نمایید'
            });
        }
        if (error == false) {
            this.setState({progressVisible: true,message:''});

            const details = {
                "admintoken": this.props.global.adminToken,
                "username": this.state.username,
                //"password": this.state.password.toString(),
            };

          let result = await Connect.sendPost(this.props.global.baseApiUrl + '/user/login', details)
            console.log(result)
                    if (result.success) {
                        this.setState({progressVisible: false});
                        // this.props.setUser(result.data);
                       
                    //    await this.props.setUser({
                    //         id: result.data.id,
                    //         username: result.data.username,
                    //         email: result.data.email,
                    //         full_name: result.data.full_name,
                    //         work_in: result.data.work_in,
                    //         phone_number: result.data.phone_number,
                    //         friends_count: result.data.friends_count,
                    //         Groups_count: result.data.Groups_count,
                    //         Avatar: result.data.Avatar,
                    //         apiToken: result.data.token,
                    //         instegram: result.data.instegram,
                    //         telegram: result.data.telegram,
                    //         whatsapp: result.data.whatsapp
                    //     })
                    //   await  AsyncStorage.setItem('Token', result.data.token);
                    //     Actions.replace('addCode');

                    } else {
                        this.setState({
                            message: result.messages,
                            progressVisible: false
                        });
                    }
        }
    }
    async VerifyCode(code){

        this.setState({progressVisible: true,message:''});
        try {
            let formData = new FormData();
            formData.append("admintoken", this.props.global.adminToken);
            formData.append("user_id", this.props.userId);
            formData.append("code", code);
            console.log(formData)

            let response = await fetch(this.props.global.baseApiUrl + '/user/verify_user_with_sms',
                {
                    method: "POST",
                    body: formData,
                });
                let result = await response.json();
                console.log(result)

                if (result.success) {
                    await  AsyncStorage.setItem('Token', result.data.token);
                    Actions.replace('Home_Screen',{complateProfile:1});
                    this.setState({progressVisible: false, message:'',curTime: "" });
                    await this.props.setUser({
                            id: result.data.id,
                            username: result.data.username,
                            email: result.data.email,
                            full_name: result.data.full_name,
                            work_in: result.data.work_in,
                            phone_number: result.data.phone_number,
                            friends_count: result.data.friends_count,
                            Groups_count: result.data.Groups_count,
                            Avatar: result.data.Avatar,
                            apiToken: result.data.token,
                            instegram: result.data.instegram,
                            telegram: result.data.telegram,
                            whatsapp: result.data.whatsapp,
                            Genre: result.data.genre
                        })
                } else {
                    this.setState({progressVisible: false , message:result.message,curTime: "" });
                }

        } catch (error) {
            console.log(error)         
        }
    }

    render() {
        const dot = <View style={style.dot} />;
        const activeDot = <View style={style.activeDot} />;
        return (

            <View style={style.viewKoliLogin}>

                <ProgressDialog visible={this.state.progressVisible} message="لطفا منتظر بمانید..."/>
                <StatusBar backgroundColor="transparent" translucent={true} barStyle="light-content"/>
            
                    <Image resizeMode="cover" source={require('../../assets/images/back_login.png')} style={style.back_login}/>
        
                <KeyboardAwareScrollView style={{flex:1}} keyboardShouldPersistTaps={'handled'}>
                <Swiper style={style.swiperContainer} showsButtons={false} loop={false} activeDot={activeDot} dot={dot}
                            paginationStyle={{flexDirection: 'row-reverse'}} onIndexChanged={this.changeIndex} ref={ref => this.swiper = ref}>
         
                <View style={[style.view_container_login]}>
                    <Image source={require('../../assets/images/userIconLogin.png')} style={style.userIcon_login}/>
                    <Text style={[style.txt_login_header,{textAlign:'center'}]}>کد امنیتی به شماره تماس شما ارسال شد</Text>
                    <Text style={[style.txt_login_header,{textAlign:'center'}]}>لطفا آنرا در کادر زیر وارد نمایید</Text>
                    <View style={[style.view_input_Item,{height:185}]}>
                    
                    <CodeInput
                                ref="codeInputRef1"
                                codeLength={4}
                                space={4}
                                size={50}
                                keyboardType="numeric"
                                inputPosition='center'
                                style={{color:'black',height:50,borderBottomColor:'#5b3862',fontSize:26,borderBottomWidth:1,marginHorizontal:10,textAlign:'center',paddingBottom:5}}
                                onFulfill={(code) => this.VerifyCode(code)}
                            />
                            {(this.state.message !='' )&&
                                        <Text style={{fontFamily:'IRANSansMobile',fontSize:16,color:'red',textAlign:'center',marginTop:10,marginBottom:-5}}>{this.state.message}</Text>
                                    }
                            <View style={{flexDirection:'row-reverse',justifyContent:'center',alignItems:'center',marginTop:15}}>
                                {this.showRecode()}
                                {this.showTimer()}
                            </View>
                            
                        </View>
                        
                </View>
                      
                </Swiper>
                </KeyboardAwareScrollView>

            </View>


        ) ;

    }
}

const mapDispatchToProps = dispatch => {
    return {
        setUser: user => {
            dispatch(setUser(user))
        }
    }
};
const mapStateToProps = (state) => {
    return {
        user: state.user,
        global:state.global
    };
};
export default connect(mapStateToProps, mapDispatchToProps)(AddCode)
