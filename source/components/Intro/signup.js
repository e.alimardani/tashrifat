import React, {Component} from 'react';
import {
    Text,
    TouchableOpacity,
    Dimensions,
    View,
    Alert,
    FlatList, StatusBar,
    TextInput,
    // AsyncStorage,
    Image
} from 'react-native';
import AsyncStorage from '@react-native-community/async-storage';
import {Form, Item, Label, Input, Button, Content,Accordion } from 'native-base';
import ServerData from '../connection/connect';
import style from './Style';
import {KeyboardAwareScrollView} from "react-native-keyboard-aware-scroll-view";
import Swiper from 'react-native-swiper';
import Icon from "react-native-vector-icons/FontAwesome5";
import Modal from "react-native-modal";
import {ProgressDialog} from 'react-native-simple-dialogs';
import {Actions} from 'react-native-router-flux';
import {setUser} from "../../Redux/Actions";
import {connect} from "react-redux";

const width = Dimensions.get('window').width;
const height = Dimensions.get('window').height;
const Connect = new ServerData();

const dataArray = [
    { title: "مشاهده قوانین", content: "لورم ایپسوم متن ساختگی با تولید سادگی نامفهوم از صنعت چاپ و با استفاده از طراحان گرافیک است. چاپگرها و متون بلکه روزنامه و مجله در ستون و سطرآنچنان که لازم است و برای شرایط فعلی تکنولوژی مورد نیاز و کاربردهای متنوع با هدف بهبود ابزارهای کاربردی می باشد. کتابهای زیادی در شصت و سه درصد گذشته" },

  ];

 class SignUp extends Component {

    constructor(props) {
        super(props);
        this.state = {
            name: '',
            lastName: '',
            Phone: '',
            userName: '',
            password: '',
            Email: '',
            work_in:'',
            Accept:false,
            Reference_number:'',
            ModalSkillState:false,
            optionWork_in:[],
            selectedWorkIn:'زمینه کاری',
            txt_wrongName: '',
            flex_wrongName: 'none',
            txt_wrong_lastName: '',
            flex_wrong_lastName: 'none',
            txt_wrongPhone: '',
            flex_wrongPhone: 'none',
            txt_wrongUserName: '',
            flex_wrongUserName: 'none',
            txt_wrongPassword: '',
            flex_wrongPassword: 'none',
            txt_wrongEmail: '',
            flex_wrongEmail: 'none',
            txt_wrongEmail_userName: '',
            flex_wrongEmail_UserName: 'none',
            flex_WrongAccept:'none',
            txt_wrongAccept:'',
            txt_wrong_Genre:'',
            flex_wrong_Genre:'none',
            open:false,

            // color_name: '#c0c0c0',
            // color_lastName: '#c0c0c0',
            // color_phone: '#c0c0c0',
            // color_password: '#c0c0c0',
            // color_userName: '#c0c0c0',
            // color_email: '#c0c0c0',
            progressVisible: false,
            ModalGenre:false,
            selectedGenre:'ژانر',
            Genres:[]

        }
        this.renderOptionSkill = this.renderOptionSkill.bind(this);
    };
 

    componentWillMount(){
        this.GetDataSkillAndUpdate();
        this.getGenre()
    }
    async GetDataSkillAndUpdate() {
        const details = {
            "admintoken": this.props.global.adminToken,
        };
       let result = await Connect.sendPost(this.props.global.baseApiUrl + '/post/getProfileFields', details)
                if (result.success == true) {

                    this.setState({SkillAndUpdate: result.data});
    
                    const findWorkIn = result.data.find( a => a.name === "زمینه کاری" );
                    await this.setState({work_in:findWorkIn})
                    await this.setState({optionWork_in:findWorkIn.option})
                   
                } else {
    
                }
    }
    async getGenre(){

        try {
            const details = {
                "admintoken": this.props.global.adminToken,
            };
            let result = await Connect.sendPost(this.props.global.baseApiUrl + '/user/get_genre', details);
            console.log(result)

            if (result.success == true) {
                this.setState({Genres: result.data});
            } else {
                this.setState({loading:false});
            }
           } catch (error) {
            this.setState({loading:false});
           }
    }
  async  btn_signUp() {
        this.setState({
            flex_wrongName: 'none',
            flex_wrong_lastName: 'none',
            flex_wrongEmail: 'none',
            flex_wrongPassword: 'none',
            flex_wrongUserName: 'none',
            flex_wrongPhone: 'none',
            flex_WrongAccept: 'none',
            flex_wrong_Genre:'none'
        });
        let error = false;
        if (this.state.name === '') {
            error = true;

            this.setState({
                txt_wrongName: 'نام و نام خانوادگی نباید خالی باشد',
                flex_wrongName: 'flex',
            });
        }
        if (this.state.selectedWorkIn === 'زمینه کاری') {
            error = true;

            this.setState({
                txt_wrong_lastName: 'زمینه کاری نباید خالی باشد',
                flex_wrong_lastName: 'flex',
            });
        }
        if (this.state.selectedGenre === 'ژانر') {
            error = true;

            this.setState({
                txt_wrong_Genre: 'ژانر نباید خالی باشد',
                flex_wrong_Genre: 'flex',
            });
        }
        if (this.state.userName.length < 11) {
            error = true;

            this.setState({
                flex_wrongUserName: 'flex',
                txt_wrongUserName: 'شماره همراه نادرست است'
            });
        }
        if (this.state.password === '') {
            error = true;

            this.setState({
                txt_wrongPassword: 'پسورد نباید خالی باشد',
                flex_wrongPassword: 'flex',
            });
        }
        // if (this.state.Email === '') {
        //     error = true;

        //     this.setState({
        //         txt_wrongEmail: 'ایمیل نباید خالی باشد',
        //         flex_wrongEmail: 'flex',
        //     });
        // }
        if (!this.state.Accept) {
            error = true;

            this.setState({
                txt_wrongAccept: 'پذیرش قوانین الزامیست',
                flex_WrongAccept: 'flex',
            });
        }
        if (error == false) {
            this.setState({progressVisible: true});

            const details = {
                "admintoken": this.props.global.adminToken,
                "username": this.state.userName,
                "email": this.state.Email,
                "name": this.state.name,
                "work_in": this.state.selectedWorkIn,
                "password": this.state.password,
                "moaref_phone": this.state.Reference_number,
                "genre" : this.state.selectedGenre
            };
          let result = await Connect.sendPost(this.props.global.baseApiUrl + '/user/adduser', details)  
          console.log(details)
                    this.setState({progressVisible: false});
                    console.log(result)
                    if (result.success) {
                        Actions.replace('addCode',{userId:result.data.id});                
                    //    await this.props.setUser(result.data);
                    } else {
                        this.setState({
                            flex_wrongEmail_UserName: 'flex',
                            txt_wrongEmail_userName: result.messages
                        });
                    }
        }
    }

    renderOptionSkill({item,index}){
  
        return(
            <View>
            <TouchableOpacity style={style.itemOptionModal} onPress={()=> this.setState({selectedWorkIn:item.name,ModalSkillState:false})}>
                <Text style={style.txt_itemOptionModal}>{item.name}</Text>
            </TouchableOpacity>
                <View style={{borderBottomWidth:width*.001,borderBottomColor:'#c5c5c5'}}/>
            </View>
        )
    }
    renderGenre({item,index}){
  
        return(
            <View>
            <TouchableOpacity style={style.itemOptionModal} onPress={()=> this.setState({selectedGenre:item.name,ModalGenre:false})}>
                <Text style={style.txt_itemOptionModal}>{item.name}</Text>
            </TouchableOpacity>
                <View style={{borderBottomWidth:width*.001,borderBottomColor:'#c5c5c5'}}/>
            </View>
        )
    }
    render() {
        const dot = <View style={style.dot} />;
        const activeDot = <View style={style.activeDot} />;
        return (
            
            <View style={style.viewKoliLogin}>

            <ProgressDialog visible={this.state.progressVisible} message="لطفا منتظر بمانید..."/>
            <StatusBar backgroundColor="transparent" translucent={true} barStyle="light-content"/>
        
                <Image resizeMode="cover" source={require('../../assets/images/back_login.png')} style={style.back_login}/>
    
            <KeyboardAwareScrollView style={{flex:1}} keyboardShouldPersistTaps={'handled'}>
            <Swiper style={style.swiperContainer} showsButtons={false} loop={false} activeDot={activeDot} dot={dot}
                        paginationStyle={{flexDirection: 'row-reverse'}} onIndexChanged={this.changeIndex} ref={ref => this.swiper = ref}>
     <Content>
 
            <View style={[style.view_container_login,{marginTop:width*.35, padding: width*.04}]}>
                <Image source={require('../../assets/images/userIconLogin.png')} style={style.userIcon_login}/>
                <Text style={style.txt_login_header}>ثبت نام</Text>
                            <View style={[style.view_input_Item,{marginBottom:0}]}>
                                <TextInput
                                  placeholder='نام و نام خانوادگی'
                                  placeholderTextColor={'#5b3862'}
                                    style={style.username_input}
                                 onChangeText={(text) => this.setState({name: text})}

                                />
                            </View>
                            <Text
                                style={[style.wrong_userName, {display: this.state.flex_wrongName}]}>{this.state.txt_wrongName}</Text>
        
                              <View style={[style.view_input_Item,{marginBottom:0}]}>
                            
                              <TouchableOpacity style={style.username_input} onPress={()=>this.setState({ModalSkillState:true,isFocused:false})}>
                                <Text style={[style.wrong_userName,{color:'#5b3862'}]}>{this.state.selectedWorkIn}</Text>
                              </TouchableOpacity>

                              <Modal isVisible={this.state.ModalSkillState}
                                    deviceWidth={width*.80}
                                    deviceHeight={height * .50}
                                    animationType={"slide"}
                                    hasBackdrop={false}
                                    // coverScreen={false}
                                    avoidKeyboard={false}
                                    backdropColor={'#203fff'}
                                    backdropOpacity={0.80}
                                    transparent={true}
                                    // onShow={() => { this.textInput.focus(); }}
                                    onRequestClose={() => {
                                        alert("Modal has been closed.")
                                    }}
                                    onBackdropPress={() => this.setState({ModalSkillState: !this.state.ModalSkillState})}
                                    style={style.ModalEditSkillStyle}
                                >
                                <View style={[style.viewModalEditSkillState]}>
                                    <TouchableOpacity activeOpacity={.7} style={style.btnExitModal} onPress={()=>this.setState({ModalSkillState:false})}>
                                        <Icon name="times-circle" color={'#f01a1a'} size={width * .06}/>
                                    </TouchableOpacity>
                                    <FlatList
                                        data={this.state.optionWork_in}
                                        renderItem={this.renderOptionSkill.bind(this)}
                                        keyExtractor={(item, index) => index.toString()}/>
                                </View>
                            </Modal>
                            </View>
                            <Text
                                style={[style.wrong_userName, {display: this.state.flex_wrong_lastName}]}>{this.state.txt_wrong_lastName}</Text>
                            {/* {(!this.state.ModalSkillState) && */}
                          <View>
                          <TouchableOpacity style={[style.username_input,{marginTop:10}]} onPress={()=>this.setState({ModalGenre:true})}>
                                <Text style={[style.wrong_userName,{color:'#5b3862'}]}>{this.state.selectedGenre}</Text>
                              </TouchableOpacity>

                              <Modal isVisible={this.state.ModalGenre}
                                    deviceWidth={width*.80}
                                    deviceHeight={height * .50}
                                    animationType={"slide"}
                                    hasBackdrop={false}
                                    avoidKeyboard={false}
                                    backdropColor={'#203fff'}
                                    backdropOpacity={0.80}
                                    transparent={true}
                                    onBackdropPress={() => this.setState({ModalSkillState: !this.state.ModalGenre})}
                                    style={style.ModalEditSkillStyle}
                                >
                                <View style={[style.viewModalEditSkillState]}>
                                    <TouchableOpacity activeOpacity={.7} style={style.btnExitModal} onPress={()=>this.setState({ModalGenre:false})}>
                                        <Icon name="times-circle" color={'#f01a1a'} size={width * .06}/>
                                    </TouchableOpacity>
                                    <FlatList
                                        data={this.state.Genres}
                                        renderItem={this.renderGenre.bind(this)}
                                        keyExtractor={(item, index) => index.toString()}/>
                                </View>
                            </Modal>

                            <Text
                                style={[style.wrong_userName, {display: this.state.flex_wrong_Genre}]}>{this.state.txt_wrong_Genre}</Text>
                            <Text
                                style={[style.wrong_userName, {display: this.state.flex_wrongPhone,}]}>{this.state.txt_wrongPhone}</Text>
                           <View style={[style.view_input_Item,{marginBottom:0}]}>
                                <TextInput
                                    placeholder='شماره همراه'
                                    placeholderTextColor={'#5b3862'}
                                    style={style.username_input}
                                    onChangeText={(text) => this.setState({userName: text})}
                                    keyboardType='numeric'
                                    maxLength={11}

                                />
                            </View>
                            <Text
                                style={[style.wrong_userName, {display: this.state.flex_wrongUserName}]}>{this.state.txt_wrongUserName}</Text>

                            <View style={[style.view_input_Item,{marginBottom:0}]}>
                                <TextInput
                                placeholder='رمز عبور'
                                placeholderTextColor={'#5b3862'}
                                
                                    secureTextEntry={true}
                                    style={style.username_input}
                                    // onFocus={() => {
                                    //     this.setState({color_userName: '#ffffff'})
                                    // }}
                                    // onEndEditing={() => this.setState({color_userName: '#c0c0c0'})}
                                    onChangeText={(text) => this.setState({password: text})}

                                />
                            </View>
                            <Text
                                style={[style.wrong_userName, {display: this.state.flex_wrongPassword}]}>{this.state.txt_wrongPassword}</Text>

                            <View style={[style.view_input_Item,{marginBottom:0}]}>
                                <TextInput
                                    placeholder='شماره معرف'
                                    placeholderTextColor={'#5b3862'}
                                   
                                    keyboardType={'numeric'}
                                    maxLength={11}
                                    style={style.username_input}
                                    onChangeText={(text) => this.setState({Reference_number: text})}
                                />
                            </View>
                            <View style={[style.view_input_Item,{marginBottom:0}]}>
                                <TextInput
                                placeholder='ایمیل'
                                placeholderTextColor={'#5b3862'}
                               
                                    style={style.username_input}
                                    // onFocus={() => {
                                    //     this.setState({color_email: '#ffffff'})
                                    // }}
                                    // onEndEditing={() => this.setState({color_email: '#c0c0c0'})}
                                    onChangeText={(text) => this.setState({Email: text})}

                                />
                            </View>
                                <Text style={[style.wrong_userName, {display: this.state.flex_wrongEmail}]}>{this.state.txt_wrongEmail}</Text>
                                <Text style={[style.wrong_userName_email, {display: this.state.flex_wrongEmail_UserName}]}>{this.state.txt_wrongEmail_userName}</Text>
                            </View>
                            
                                <TouchableOpacity onPress={()=> this.state.open ? this.setState({open:false}) : this.setState({open:true})} style={{flex:1,width:'100%',flexDirection:'row-reverse',alignItems:'center',justifyContent:'space-between',padding:5,paddingHorizontal:30}}>
                                    <Text style={{fontFamily:'IRANSansMobile',fontSize:16,color:'#5b3862'}}>مشاهده قوانین</Text>
                                    {!this.state.open &&
                                        <Icon name='sort-down' size={20} color="#5b3862"/>
                                    }
                                    {this.state.open &&
                                        <Icon name='sort-up' size={20} color="#5b3862"/>
                                    }
                                </TouchableOpacity>
                                {this.state.open &&
                                <View style={{paddingHorizontal:30}}>
                                    <Text style={{fontFamily:'IRANSansMobile',fontSize:14,color:'#5b3862',textAlign:'right'}}>
                                    لورم ایپسوم متن ساختگی با تولید سادگی نامفهوم از صنعت چاپ و با استفاده از طراحان گرافیک است. چاپگرها و متون بلکه روزنامه و مجله در ستون و سطرآنچنان که لازم است و برای شرایط فعلی تکنولوژی مورد نیاز و کاربردهای متنوع با هدف بهبود ابزارهای کاربردی می باشد. 
                                    </Text>
                                </View>
                                }
                                
                            
                            <View style={{flexDirection:'row',alignItems:'center',flex:1,width:"100%",paddingHorizontal:width*.08,paddingTop:10,justifyContent:'flex-end'}}>
                                <Text style={{fontFamily:'IRANSansMobile',fontSize:16,color:'#5b3862'}}>پذیرش قوانین</Text>
                                {!this.state.Accept &&
                                <TouchableOpacity onPress={()=>this.setState({Accept:true})} style={{marginLeft:10,borderColor:'#5b3862',borderWidth:1, padding:2,borderRadius:3,width:22,height:22}}/>
                                }
                                {this.state.Accept &&
                                <TouchableOpacity onPress={()=>this.setState({Accept:false})} style={{marginLeft:10,borderColor:'#5b3862',borderWidth:1, padding:2,borderRadius:3}}>
                                    <Icon size={16} name="check"/>
                                </TouchableOpacity>
                                }
                               
                                
                            </View>
                            <Text
                                style={[style.wrong_userName, {display: this.state.flex_WrongAccept,width:'100%',paddingRight:10}]}>{this.state.txt_wrongAccept}</Text>
                        <TouchableOpacity onPress={() => this.btn_signUp()} style={[style.view_btn_login,{marginTop: height * .02,paddingHorizontal:10}]}>
                            <Text style={style.txt_btn_login}> ثبت نام </Text>
                        </TouchableOpacity>

                <View style={style.view_without_username}>
                    <TouchableOpacity activeOpacity={.5} onPress={() => Actions.push('login')}>
                        <Text style={[style.txt_signup,{color:'#000'}]}>ورود</Text>
                    </TouchableOpacity>
                    <Text style={[style.txt_signup,{color:'#000'}]}>دارای حساب کاربری هستم! </Text>
                </View>
                {/*<TouchableOpacity activeOpacity={.5}>*/}
                {/*    <View style={style.view_without_username1}>*/}
                {/*        <Text style={style.txt_signup}>رمز عبور خود را فراموش کردم</Text>*/}
                {/*    </View>*/}
                {/*</TouchableOpacity>*/}
            </View>
        
            </Content>    
            </Swiper>
            </KeyboardAwareScrollView>

        </View>

        );

    }
}

const mapDispatchToProps = dispatch => {
    return {
        setUser: user => {
            dispatch(setUser(user))
        }
    }
};
const mapStateToProps = (state) => {
    return {
        user: state.user,
        global:state.global
    };
};
export default connect(mapStateToProps, mapDispatchToProps)(SignUp)
