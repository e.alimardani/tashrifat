import React, {Component} from 'react';
import {
    Text,
    TouchableOpacity,
    Dimensions,
    View,
    KeyboardAvoidingView,
    TextInput,
    BackHandler, StatusBar, Alert, Image,Linking
} from 'react-native';
import AsyncStorage from '@react-native-community/async-storage';
import {Form, Item, Label, Input, Button, Content} from 'native-base';
import ServerData from '../connection/connect';
import style from './Style';
import {Actions} from 'react-native-router-flux';
import {ProgressDialog} from 'react-native-simple-dialogs';
import {KeyboardAwareScrollView} from "react-native-keyboard-aware-scroll-view";
import Swiper from 'react-native-swiper';
import {setUser} from "../../Redux/Actions";
import {connect} from "react-redux";

const width = Dimensions.get('window').width;
const height = Dimensions.get('window').height;
const Connect = new ServerData();

class login extends Component {
    constructor(props) {
        super(props);
        this.state = {
            username: '',
            password: '',
            color_username: '#c0c0c0',
            color_password: '#c0c0c0',
            txt_wrongUserName: '',
            txt_wrongPassword: '',
            flex_wrongUseName: 'none',
            flex_wrongPassword: 'none',
            flex_wrongPassword_Username: 'none',
            progressVisible: false,
            dialogVisible: false,
            message:''
        }
    };


   async btn_login() {
        this.setState({flex_wrongUseName: 'none', flex_wrongPassword: 'none'});
        let error = false;
        if (this.state.username === '') {
            error = true;

            this.setState({
                flex_wrongUseName: 'flex',
                txt_wrongUserName: ' شماره تماس خود را وارد نمایید'
            });
        }
        if (error == false) {
            this.setState({progressVisible: true});

            const details = {
                "admintoken": this.props.global.adminToken,
                "username": this.state.username,
                //"password": this.state.password.toString(),
            };

          let result = await Connect.sendPost(this.props.global.baseApiUrl + '/user/login', details)
            console.log(result)
                    if (result.success) {
                        this.setState({progressVisible: false});
                        Actions.replace('addCode',{userId:result.user_id});

                        // this.props.setUser(result.data);
                       
                    //    await this.props.setUser({
                    //         id: result.data.id,
                    //         username: result.data.username,
                    //         email: result.data.email,
                    //         full_name: result.data.full_name,
                    //         work_in: result.data.work_in,
                    //         phone_number: result.data.phone_number,
                    //         friends_count: result.data.friends_count,
                    //         Groups_count: result.data.Groups_count,
                    //         Avatar: result.data.Avatar,
                    //         apiToken: result.data.token,
                    //         instegram: result.data.instegram,
                    //         telegram: result.data.telegram,
                    //         whatsapp: result.data.whatsapp
                    //     })
                    //   await  AsyncStorage.setItem('Token', result.data.token);
                    //     Actions.replace('Home_Screen',{complateProfile:1});

                    } else {
                        this.setState({
                            flex_wrongPassword_Username: 'flex',
                            progressVisible: false,
                            message: result.messages,
                        });
                    }
        }
    }

    render() {
        const dot = <View style={style.dot} />;
        const activeDot = <View style={style.activeDot} />;
        return (

            <View style={style.viewKoliLogin}>

                <ProgressDialog visible={this.state.progressVisible} message="لطفا منتظر بمانید..."/>
                <StatusBar backgroundColor="transparent" translucent={true} barStyle="light-content"/>
            
                    <Image resizeMode="cover" source={require('../../assets/images/back_login.png')} style={style.back_login}/>
        
                <KeyboardAwareScrollView style={{flex:1}} keyboardShouldPersistTaps={'handled'}>
                <Swiper style={style.swiperContainer} showsButtons={false} loop={false} activeDot={activeDot} dot={dot}
                            paginationStyle={{flexDirection: 'row-reverse'}} onIndexChanged={this.changeIndex} ref={ref => this.swiper = ref}>
         
                <View style={[style.view_container_login]}>
                    <Image source={require('../../assets/images/userIconLogin.png')} style={style.userIcon_login}/>
                    <Text style={style.txt_login_header}>ورود به حساب کاربری</Text>

                    <View style={[style.view_input_Item]}>
                                <TextInput
                                keyboardType="numeric"
                                maxLength={11}
                                style={[style.username_input,{color: '#000000'}]}
                                placeholder='شماره تماس خود را وارد کنید'
                                placeholderTextColor={'#5b3862'}
                                value={this.state.username}
                                onChangeText={(text) => this.setState({username: text})}
                            />
                        </View>
                        <Text
                            style={[style.wrong_userName, {display: this.state.flex_wrongUseName}]}>{this.state.txt_wrongUserName}</Text>
                      {/* <View style={[style.view_input_Item,]}>
                                <TextInput
                                secureTextEntry={true}
                                placeholder='رمز عبور'
                                placeholderTextColor={'#5b3862'}
                                value={this.state.password}
                                style={[style.username_input,{color: '#000000'}]}
                                onChangeText={(text) => this.setState({password: text})}

                            />
                        </View>
                        <Text
                            style={[style.wrong_userName, {display: this.state.flex_wrongPassword}]}>{this.state.txt_wrongPassword}</Text> */}
                        <Text
                            style={[style.wrong_userName, {display: this.state.flex_wrongPassword_Username}]}>{this.state.message}</Text>

                            <TouchableOpacity onPress={() => this.btn_login()} style={style.view_btn_login}>
                                <Text style={style.txt_btn_login}>تایید</Text>
                            </TouchableOpacity>

                    <View style={style.view_without_username}>
                        <TouchableOpacity activeOpacity={.5} onPress={() => Actions.push('signUp')}>
                            <Text style={[style.txt_signup,{color:'red',fontSize:16}]}>ثبت نام کنید</Text>
                        </TouchableOpacity>
                        <Text style={[style.txt_signup,{color:'#000'}]}>آیا حساب کاربری ندارید ؟ </Text>
                        
                    </View>
                    <View style={style.view_without_username}>
                        <TouchableOpacity onPress={()=>Linking.openURL('http://tashrifat.wpfix.ir/my-account/lost-password/')} style={{flex:1,paddingRight:10}}>
                            <Text style={[style.txt_signup,{color:'#000',textAlign:'right'}]}>فراموشی رمز عبور</Text>
                        </TouchableOpacity>
                    </View>
                    {/*<TouchableOpacity activeOpacity={.5}>*/}
                    {/*    <View style={style.view_without_username1}>*/}
                    {/*        <Text style={style.txt_signup}>رمز عبور خود را فراموش کردم</Text>*/}
                    {/*    </View>*/}
                    {/*</TouchableOpacity>*/}
                </View>
                      
                </Swiper>
                </KeyboardAwareScrollView>

            </View>


        ) ;

    }
}

const mapDispatchToProps = dispatch => {
    return {
        setUser: user => {
            dispatch(setUser(user))
        }
    }
};
const mapStateToProps = (state) => {
    return {
        user: state.user,
        global:state.global
    };
};
export default connect(mapStateToProps, mapDispatchToProps)(login)
