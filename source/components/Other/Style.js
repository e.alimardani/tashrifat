import React from 'react';
import {StyleSheet, Dimensions, Platform} from 'react-native';


const width = Dimensions.get('window').width;
const height = Dimensions.get('window').height;

export default StyleSheet.create({
    view_container_messageRecive: {
        flex: 1,
        // backgroundColor: '#000'
    },
 
    view_header_prof: {
        height: height / 2,
        width: width / 1,
        // backgroundColor:'#000'
    },
    view_top: {justifyContent: 'center', alignItems: 'center'},
    view_header_user: {
        height: height / 7,
        width: width / 1,
        // backgroundColor:'#000'
    },
    view_top_header: {
        marginTop: height * .01,
        paddingHorizontal: width * .05,
        width: width,
    },
    txt_top_header: {
        color: '#fff',
        fontFamily: 'IRANSansMobile',
        textAlign: 'center',
        fontSize: width / 25
    },
    centerContainer: {flex: 1},dropdownStyleTextSkill: {
        color: '#fff',
        // width: width * .25,
        // marginTop:width* (.05),
        fontSize: width / 25,
        justifyContent: 'center',
        // height: height * .07,
        width: width * .70,
        // paddingRight: width * .02,
        backgroundColor: '#8b68fc',
        // borderRadius: width * .10,
        alignSelf: 'center',
        textAlign: 'center'
    },
    modalTextStyleSkill: {color: '#8b68fc', fontFamily: 'IRANSansMobile', fontSize: width / 25, textAlign: 'center'},
    dropdownContainerStyle: {
        justifyContent: 'center',
        height: height * .07,
        width: width * .70,
        paddingRight: width * .02,
        backgroundColor: '#fff',
        borderRadius: width * .10,
        // marginTop: height * .01,
        alignSelf: 'center'
    },
    dropdownStyleFilter:{backgroundColor:'transparent',height:0,borderWidth:0,marginTop:-9}


});

