import React, {Component} from 'react';
import {
    Dimensions,
    View,
     Platform, Alert,
     Text, StatusBar, TouchableOpacity, StyleSheet, PermissionsAndroid, 
} from 'react-native';
import AsyncStorage from '@react-native-community/async-storage';
import {Container, Header, Left, Body, Right, Button, Title, Fab} from 'native-base';
import ServerData from '../connection/connect';
import style from './Style';
import LinearGradient from "react-native-linear-gradient";
import Icon from "react-native-vector-icons/FontAwesome5";
import MapView,{ Marker, ProviderPropType } from 'react-native-maps';
import {connect} from "react-redux";
import {Actions} from "react-native-router-flux";
import {ProgressDialog} from 'react-native-simple-dialogs';
import ModalDropdown from "react-native-modal-dropdown";
import { identifier } from '@babel/types';

const width = Dimensions.get('window').width;
const height = Dimensions.get('window').height;
const Connect = new ServerData();

class Maps extends Component {

    constructor(props) {
        super(props);
        this.state = {
            progressVisible:false,
            region:{
                latitude: 35.69876465083631,
                longitude: 51.385422684252255,
                latitudeDelta: 0.0988,
                longitudeDelta: 0.0421,
            },
            markers: [],
            ProvinceList1:[],
            ProvinceList:[],
            CityList:[],
            selectedProvince:'',
            selectedCity:''

        }
    };
    componentDidMount(){
        this.GetDataProvinceList()
        this.getLocation()
    }
    async getLocation(){
        formData = new FormData();
        formData.append("admintoken", this.props.global.adminToken);
        formData.append("user_id", this.props.user.id);
    try {
        let response = await fetch(this.props.global.baseApiUrl + '/user/get_User_Location', {
            method: 'post',
            headers: {
                'Content-Type': 'multipart/form-data',
            },
            body: formData
        });
        let result = await response.json();
        if(result.success === true){
                await this.setState({
                    region:{
                    latitude: parseInt(result.location_ip1),
                    longitude: parseInt(result.location_ip2),
                    latitudeDelta: 0.0988,
                    longitudeDelta: 0.0421,
                },
                selectedProvince:result.province,
                selectedCity:result.city    
                });
        }else{ }
        }
    catch (error) {}
}
    async GetDataProvinceList() {
        try{
        const details = {
            "admintoken": this.props.global.adminToken,  
        };
        let result = await Connect.sendPost(this.props.global.baseApiUrl+ '/user/get_province_list', details);
            if (result.success === true) {
                let a =[]
                for(let name of result.data){
                    a.push(name.name)
                }
                this.setState({ProvinceList:a,ProvinceList1:result.data})    
            } else { }
        }catch(error){};
    }
   async SelectProvince(index,item){
        await this.setState({selectedProvince:item});
        try{
            const details = {
                "admintoken": this.props.global.adminToken,  
                "province_id": this.state.ProvinceList1[index].id
            };
            let result = await Connect.sendPost(this.props.global.baseApiUrl+ '/user/get_city_by_province_id', details);
                if (result.success === true) {
                    let a =[]
                    for(let name of result.data){
                        a.push(name.name)
                    }
                    this.setState({CityList:a})    
                } else { }
            }catch(error){};
       
    }
   async SelectCity(index,item){
        await this.setState({selectedCity:item});
    }
      makeMarker({nativeEvent}){
          const pos = nativeEvent.coordinate;
            this.setState(prevState =>{
                return{
                    markers:[
                        // ...prevState.markers,
                        {
                            latitude: pos.latitude,
                            longitude: pos.longitude,
                        }

                    ]
                }
            })
      }
      renderMarker(marker,index) {
        return <Marker key={index} coordinate={marker}/>
      }
     async setLocation(position){
     
      }
     async getCurrentPosition1(){
       
        navigator.geolocation.getCurrentPosition(
            (position)  =>  { 
                this.setState({
                          region:{
                            latitude: position.coords.latitude,
                            longitude: position.coords.longitude,
                            latitudeDelta: 0.0920,
                            longitudeDelta: 0.0400,
                          }
                      })

              },
              error => console.log(error),
              {enableHighAccuracy:false, timeout:10000, maximumAge:1000, distanceFilter:10}
              )
             
      }
      onRegionChange(region){
// console.warn(region)
        this.setState({region})

      }
     async updateLocation(){
        //  if(this.state.region.latitude !== 35.69876465083631 && this.state.region.longitude !== 51.385422684252255){

        await this.setState({progressVisible:true});
          try{
            const details = {
                "admintoken": this.props.global.adminToken,
                "usertoken": this.props.user.apiToken,
                "location_ip1": this.state.region.latitude,
                "location_ip2": this.state.region.longitude,
                "province": this.state.selectedProvince,
                "city": this.state.selectedCity
            };
            let result = await Connect.sendPost(this.props.global.baseApiUrl+ '/user/Location', details);
            if (result.success === true) {
                    await this.setState({progressVisible:false});
                    Alert.alert("", 'موقعیت مکانی بروز شد', [
                        {text: 'باشه'}
                    ]);
                }else {
                    await this.setState({progressVisible:false});
                    Alert.alert("خطا", 'بروزرسانی بامشکل مواجه شد', [
                        {text: 'باشه'}
                    ]);
            }
            }catch(err){
                await this.setState({progressVisible:false});
            }
        //  }
        //  else{
        //     Alert.alert("خطا", 'لطفا موقعیت مکانی را انتخاب کنید', [
        //         {text: 'باشه'}
        //     ]);
        //  }
    }
    render() {

        return (
            <View style={style.view_container_messageRecive}>
            <ProgressDialog visible={this.state.progressVisible} message="لطفا منتظر بمانید..."/>
                <View style={style.view_top}>
                    <LinearGradient style={[style.view_header_user]} start={{x: 0, y: 0}} end={{x: 1, y: 0}}
                                    colors={['#627efb', '#8b68fc', '#a95bfe']}>

                        <Header transparent style={style.view_top_header}>
                            <StatusBar backgroundColor="transparent" translucent={true} barStyle="light-content"/>

                            <Left style={{flex: 1,}}>
                                <Button transparent onPress={()=> Actions.pop()}>
                                       <Icon name="arrow-left" color={"#ffffff"} size={width * .07}/>
                                </Button>
                            </Left>
                            <Body style={{flex: 1, alignItems: 'center'}}>
                                <Title style={style.txt_top_header}>نقشه</Title>
                            </Body>
                            <Right style={{flex: 1}} >
                                    <Button transparent onPress={()=> Actions.drawerOpen()}>
                                        <Icon name="bars" color={"#fff"} size={width * .07}/>
                                    </Button>
                            </Right>
                        </Header>
                    </LinearGradient>
                </View>
                
                <View style={{flex:1,alignItems:'center'}}>
                    <ModalDropdown
                        defaultValue={this.state.selectedProvince === '' || null ?'انتخاب استان':this.state.selectedProvince}
                        animated={true}
                        dropdownTextStyle={style.dropdownStyleTextSkill}
                        textStyle={style.modalTextStyleSkill}
                        dropdownStyle={style.dropdownStyleFilter} 
                        style={[style.dropdownContainerStyle,{marginTop: height * .01,}]}
                        options={this.state.ProvinceList}
                        onSelect={(index,item)=>this.SelectProvince(index,item)}/>
                    <ModalDropdown
                        defaultValue={this.state.selectedCity ==='' || null?'انتخاب شهرستان':this.state.selectedCity}
                        animated={true}
                        dropdownTextStyle={style.dropdownStyleTextSkill}
                        textStyle={style.modalTextStyleSkill}
                        dropdownStyle={style.dropdownStyleFilter} 
                        style={[style.dropdownContainerStyle,{marginTop: height * .01,}]}
                        options={this.state.CityList}
                        onSelect={(index,item)=>this.SelectCity(index,item)}/>
                
                    <TouchableOpacity onPress={this.updateLocation.bind(this)} style={{backgroundColor:'#a95bfe',marginHorizontal:width*.10,borderRadius:width*.02,marginTop:20}}>
                        <Text style={{paddingHorizontal:width*.03,paddingVertical:width*.03,fontSize:14,color:'#fff'}}>بروزرسانی موقعیت مکانی</Text>
                    </TouchableOpacity>
                </View>

            </View>
        );

    }
}
const styles = StyleSheet.create({
    container: {
      ...StyleSheet.absoluteFillObject,
      justifyContent: 'flex-end',
      alignItems: 'center',
    },
    map: {
      ...StyleSheet.absoluteFillObject,
    },
    bubble: {
      backgroundColor: 'rgba(255,255,255,0.7)',
      paddingHorizontal: 18,
      paddingVertical: 12,
      borderRadius: 20,
    },
    latlng: {
      width: 200,
      alignItems: 'stretch',
    },
    button: {
      width: 80,
      paddingHorizontal: 12,
      alignItems: 'center',
      marginHorizontal: 10,
    },
    buttonContainer: {
      flexDirection: 'row',
      marginVertical: 20,
      backgroundColor: 'transparent',
    },
  });
const mapStateToProps = (state) => {
    return {
        user: state.user,
        global: state.global
    };
};
export default connect(mapStateToProps, null)(Maps)
