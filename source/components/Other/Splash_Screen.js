import React, {Component} from 'react';
import {
    Text,
    StyleSheet,
    TouchableOpacity,
    // NetInfo,
    Image,
    View,
    Dimensions,
    // AsyncStorage,
    Alert,
    StatusBar
} from 'react-native';
import AsyncStorage from '@react-native-community/async-storage';
import NetInfo from "@react-native-community/netinfo";
import {Spinner} from 'native-base'
import ServerData from "../connection/connect";
import {Actions} from 'react-native-router-flux';
import {setUser} from "../../Redux/Actions";
import {connect} from "react-redux";

const width = Dimensions.get('window').width;
const height = Dimensions.get('window').height;

const Connect = new ServerData();

class Splash_Screen extends Component {
    constructor(props) {
        super(props);
        this.state = {
            progressVisible: false,
            SkillUser: [],
            hideMenu: 'none',
            displaySpinner: 'none',
        };
        this.checkUserLogin = this.checkUserLogin.bind(this);
        this.clickButton2 = this.clickButton2.bind(this);
    }

   async componentWillMount() {
        setTimeout(() => {
            this.getConnect();
        },);
    }

    getConnect = () => {
        NetInfo.isConnected.fetch().then(isConnected => {
            if (isConnected) {
                this.checkUserLogin();
            } else {
                Alert.alert("اتصال به اینترنت", 'اینترنت خود را متصل و روی تلاش مجدد کلیک نمایید', [
                    {text: "تلاش مجدد", onPress: () => this.getConnect()}, {
                        text: 'خروج',
                        onPress: () => this.exitApp()
                    }
                ]);
            }
        })
    };

    async checkUserLogin() {
        this.setState({displaySpinner: 'flex'});
        try {
            let apiToken = await AsyncStorage.getItem('Token');
            return apiToken === null
                ? this.setState({hideMenu: 'flex', displaySpinner: 'none'})
                : await this.checkUserLoginFromApi(apiToken)
        } catch (error) {
            // console.log(error);
        }
    }

    async checkUserLoginFromApi(apiToken) {
        try {
            const details = {
                "admintoken": this.props.global.adminToken,
                "usertoken": apiToken,
            };
            let result = await Connect.sendPost(this.props.global.baseApiUrl + '/user/get', details);
            if (result.success === true) {
                this.setState({displaySpinner: 'none'});        
            //    await this.props.setUser(response.data);
            console.log(result)
           await this.props.setUser({
                id: result.data.id,
                username: result.data.username,
                email: result.data.email,
                full_name: result.data.full_name,
                work_in: result.data.work_in,
                phone_number: result.data.phone_number,
                friends_count: result.data.friends_count,
                Groups_count: result.data.Groups_count,
                Avatar: result.data.Avatar,
                apiToken: result.data.token,
                instegram: result.data.instegram,
                telegram: result.data.telegram,
                whatsapp: result.data.whatsapp,
                Genre:result.data.genre,
                Is_vip:result.data.is_vip
            })

            Actions.replace('Home_Screen',{complateProfile:1});
            } else {
                this.setState({hideMenu: 'flex', displaySpinner: 'none'})
            }
        } catch (error) {
            // console.log(error)
        }
    }



    clickButton2() {
        Actions.push('drawer');
    }



    render() {
        return (
            <View style={style.container}>
                <StatusBar backgroundColor="transparent" translucent={true} barStyle="light-content"/>
                <Image source={require('../../assets/images/splash.png')} style={style.image_load} resizeMode={'stretch'}/>
                <View style={[style.viewSpinner, {display: this.state.displaySpinner}]}>
                    <Spinner color={'#6f419e'}/>
                </View>
                <View style={{display: this.state.hideMenu}}>
                    <TouchableOpacity activeOpacity={.7} style={style.ViewBtn} onPress={() => Actions.push('login')}>
                        <Text style={style.txtBtn}>{'میخواهم دیده شوم!'}</Text>
                        <Image source={require('../../assets/images/oscar-academy-award.png')} style={style.imgBtn}
                               resizeMode={'contain'}/>
                    </TouchableOpacity>
                    <TouchableOpacity activeOpacity={.7} style={[style.ViewBtn,{backgroundColor: '#fb9c00'}]} onPress={()=>  Actions.drawer_root('home')}>
                        <Text style={style.txtBtn}>{'میخواهم ببینم!'}</Text>
                        <Image source={require('../../assets/images/video.png')} style={style.imgBtn}
                               resizeMode={'contain'}/>
                    </TouchableOpacity>
                </View>
            </View>
        );
    }
}

const mapDispatchToProps = dispatch => {
    return {
        setUser: user => {
            dispatch(setUser(user))
        }
    }
};
const mapStateToProps = (state) => {
    return {
        user: state.user,
        global: state.global,
        countMessage:state.messages
    };
};
export default connect(mapStateToProps, mapDispatchToProps)(Splash_Screen)

const style = StyleSheet.create({
    container: {
        width: width,
        height: height,
        alignSelf: 'center',
        justifyContent: 'flex-start',
        backgroundColor: '#fff'
    },
    image_load: {width: width, height: height / 1.5},
    ViewBtn: {
        width: width * .70,
        height: width * .16,
        // flex:1,
        borderRadius: width * .05,
        backgroundColor: '#6f419e',
        alignSelf: 'center',
        marginTop: height * .05,
        flexDirection: 'row',
        justifyContent: 'flex-end',
        alignItems: 'center'
    },
   
    viewSpinner: {width: width, height: height * .33, alignItems: 'center', justifyContent: 'center'},
    txtBtn: {fontSize:16, color: '#fff', fontFamily: 'IRANSansMobile',},
    imgBtn: {width: width * .10, height: height * .05, marginHorizontal: width * .05}
});
