import React from 'react';
import {Image, Dimensions, StatusBar, View, TouchableOpacity,Platform,ActivityIndicator} from 'react-native';
import {Body,Button, Container, Icon, Header,Title, Left, Right} from "native-base";
import style from './Style';
// import BackButton from "../Shared/BackButton";
import Video from 'react-native-video';
import LinearGradient from "react-native-linear-gradient";
import IconF from 'react-native-vector-icons/FontAwesome';
// import HeaderRightLabel from "../Shared/HeaderRightLabel";
import {Actions} from "react-native-router-flux";
import Orientation from 'react-native-orientation';
const width = Dimensions.get('window').width;
const height = Dimensions.get('window').height;

export default class MainVideoPlayer extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            width: Dimensions.get('window').width,
            height: Dimensions.get('window').width * .58,
            side:'p',
            loading:true,
            fullScreen:false
        };

        this.back = this.back.bind(this);
        this.fullScreen = this.fullScreen.bind(this);
        this.exitFullScreen = this.exitFullScreen.bind(this);

    }

    async fullScreen(){
        await this.setState({
            width: '100%',
            height:'100%',
            side: 'l',
            fullScreen:true
        });
        StatusBar.setHidden(true);
        Orientation.lockToLandscape();

    }

    async exitFullScreen(){
        console.log('asdfasdf')
        await this.setState({
            width: '100%',
            height: '100%',
            side: 'p',
            fullScreen:false
        });
        StatusBar.setHidden(false);
        StatusBar.setBarStyle('dark-content');
        Orientation.lockToPortrait();
    }

    back(){
        console.log('asdfasdf')
    }

    render() {
        return (
            <Container>
              {!this.state.fullScreen && 
                <LinearGradient style={[style.view_header_user]} start={{x: 0, y: 0}} end={{x: 1, y: 0}}
                                    colors={['#627efb', '#8b68fc', '#a95bfe']}>

                        <Header transparent style={style.view_top_header}>
                            <StatusBar backgroundColor="transparent" translucent={true} barStyle="light-content"/>

                            <Left style={{flex: 1,}}>
                                <Button transparent onPress={() => Actions.pop()}>
                                    <IconF name="arrow-left" color={"#ffffff"} size={width * .07}/>
                                </Button>
                            </Left>
                            <Body style={{flex: 1, alignItems: 'center'}}>
                                <Title style={style.txt_top_header}>{this.props.headerName}</Title>
                            </Body>
                            <Right style={{flex: 1}}>
                                <Button transparent onPress={() => Actions.drawerOpen()}>
                                    <IconF name="bars" color={"#fff"} size={width * .07}/>
                                </Button>
                            </Right>
                        </Header>
                </LinearGradient>
              }
              <View style={style.view_top}>
                    
                </View>
               
                <View style={{flex: 1, justifyContent: 'center', alignItems: 'center', backgroundColor: 'black'}}>
                    {this.state.loading && <View style={{position:'absolute',width:40,height:40,zIndex:9999,alignSelf:'center',backgroundColor:'rgba(0,0,0,.5)',borderRadius:20,justifyContent:'center',alignItems:'center'}}>
                        <ActivityIndicator color='#fff' size='small'/>
                    </View>}
                    <Video
                        ref={p => { this.videoPlayer = p; }}
                        source={{uri:this.props.urlVideo}}
                        style={{width: this.state.width , height: this.state.height ,backgroundColor:'#000'}}
                        controls={true}
                        //disableBack={true}
                        
                        onBack={()=>console.log('sadfasksdjbflasddblajksenf')}
                        resizeMode='contain'
                        // poster={this.props.poster}
                        posterResizeMode='cover'
                        onFullscreenPlayerWillPresent={this.fullScreen}
                        onFullscreenPlayerWillDismiss={this.exitFullScreen}
                        onReadyForDisplay={async () => await this.setState({loading:false})}
                    />
                    {this.state.side ==='p' && Platform.OS === 'android' && <TouchableOpacity style={{position:'absolute',top:10,right:10,backgroundColor:'rgba(0,0,0,.5)',borderRadius:20,width:40,height:40,justifyContent:'center',alignItems:'center'}} onPress={() => this.videoPlayer.presentFullscreenPlayer()}>
                        <Icon name='ios-expand' style={{color:'#fac917'}} />
                    </TouchableOpacity>}
                    {this.state.side ==='l' && Platform.OS === 'android' && <TouchableOpacity style={{position:'absolute',top:10,right:10,backgroundColor:'rgba(0,0,0,.5)',borderRadius:20,width:40,height:40,justifyContent:'center',alignItems:'center'}} onPress={() => this.videoPlayer.dismissFullscreenPlayer()}>
                        <Icon name='ios-contract' style={{color:'#fac917'}} />
                    </TouchableOpacity>}
                </View>
            </Container>
        )
    }
}