import React, {Component} from 'react';
import {
    Text,
    TouchableOpacity,
    Dimensions, View, Alert,
    TextInput,
    Image,
    StatusBar
} from 'react-native';
import {Container,Form, Header, Left, Body, Right, Button, Title, Content, Item, Label, Input} from 'native-base';

import ServerData from '../connection/connect';
import style from './Style';
import Icon from "react-native-vector-icons/FontAwesome5";
import {Actions} from "react-native-router-flux";
import {ProgressDialog} from "react-native-simple-dialogs";
import Modal from "react-native-modal";
import LinearGradient from "react-native-linear-gradient";
import {connect} from "react-redux";
import mapStateToProps from "react-redux/es/connect/mapStateToProps";
import {setUser} from "../../Redux/Actions";


const width = Dimensions.get('window').width;
const height = Dimensions.get('window').height;
const Connect = new ServerData();

class InsertSocial extends Component {

    constructor(props) {
        super(props);
        this.state = {
            Telegram:'',
            Instagram:'',
            whatsApp:'', 
        }
    this.btn_updateSocial = this.btn_updateSocial.bind(this);
    };

  
    componentDidMount(){
        this.setState({
            Telegram: this.props.user.telegram,
            Instagram: this.props.user.instegram,
            whatsApp: this.props.user.whatsapp,
        })
    }
   async btn_updateSocial() {
        this.setState({progressVisible: true});
        const details = {
            "admintoken": this.props.global.adminToken,
            "usertoken": this.props.user.apiToken,
            "telegram": this.state.Telegram,
            "instegram": this.state.Instagram,
            "whatsapp": this.state.whatsApp,
        };
       let result = await Connect.sendPost(this.props.global.baseApiUrl + '/user/Insert_User_SocialMedia', details)
        
                this.setState({progressVisible: false});
                console.log(result)
                if (result.success === true) {
                    await this.props.setUser({
                        
                        instegram: this.state.Instagram,
                        telegram: this.state.Telegram,
                        whatsapp: this.state.whatsApp,
                        "Groups_count":this.props.user.Groups_count,
                        "apiToken":this.props.user.apiToken,
                        "email":this.props.user.email,
                        "friends_count":this.props.user.friends_count,
                        "full_name":this.props.user.full_name,
                        "id":this.props.user.id,
                        "phone_number":this.props.user.phone_number,
                        "username":this.props.user.username,
                        "work_in":this.props.user.work_in,
                        "Avatar":this.props.user.Avatar,
                        "Genre":this.props.user.genre
                    });
                    
                    Alert.alert("بروزرسانی شد", 'شما با موفقیت اطلاعات خود را بروزرسانی کردید', [
                        {text: 'باشه'}
                    ]);
                } else {
                    Alert.alert("خطا", 'بروزرسانی با مشکل مواجه شد لطفا دوباره تلاش کنید', [
                        {text: 'باشه'}
                    ]);
                }
    }
  
    render() {
        const user = this.props.user;
        return (
            <View style={style.view_container_prof}>
 <ProgressDialog
                    visible={this.state.progressVisible}
                    message="لطفا منتظر بمانید..."
                />
            <LinearGradient style={[style.view_header_prof,{height: height / 6,}]} colors={['#627efb', '#8b68fc', '#a95bfe']}>

                <Header transparent >
                    <StatusBar backgroundColor="transparent" translucent={true} barStyle="light-content"/>
                    <Left style={{flex:1}}>
                        <Button transparent onPress={() => Actions.pop()}>
                            <Icon name="arrow-left" color={"#ffffff"} size={width * .07}/>
                        </Button>
                    </Left>
                    <Body>
                        <Title style={style.txt_top_header}>{'شبکه های اجتماعی'}</Title>
                    </Body>
                    <Right >
                        <Button transparent onPress={() => Actions.drawerOpen()}>
                            <Icon name="bars" color={"#fff"} size={width * .07}/>
                        </Button>
                    </Right>
                </Header>
            </LinearGradient>
     
               
             <View style={{backgroundColor:'#fff',marginTop:width*.10,alignSelf:'center',elevation:5,width:width*.90,alignItems:'center',justifyContent:'space-around',borderRadius:width*.05,paddingVertical:width*.10}}>
                <View style={[style.view_input_Item]}>
                                <TextInput
                                
                                placeholder='آیدی تلگرام'
                                placeholderTextColor={'#5b3862'}
                                value={this.state.Telegram}
                                style={[style.username_input,{color: '#000000'}]}
                                onChangeText={(text) => this.setState({Telegram: text})}

                            />
                        </View>
                        <View style={[style.view_input_Item,]}>
                                <TextInput
                                placeholder='شماره واتساپ'
                                placeholderTextColor={'#5b3862'}
                                maxLength={11}
                                value={this.state.whatsApp}
                                style={[style.username_input,{color: '#000000'}]}
                                onChangeText={(text) => this.setState({whatsApp: text})}

                            />
                        </View>
                        <View style={[style.view_input_Item,]}>
                                <TextInput
                                placeholder='آیدی اینستگرام'
                                placeholderTextColor={'#5b3862'}
                                value={this.state.Instagram}
                                style={[style.username_input,{color: '#000000'}]}
                                onChangeText={(text) => this.setState({Instagram: text})}

                            />
                        </View>

                        <TouchableOpacity activeOpacity={.5} onPress={() => this.btn_updateSocial()}>
                    <View style={style.view_btn_EditSocial}>
                        <Text style={style.txt_btn_update}>بروزرسانی</Text>
                    </View>
                </TouchableOpacity>
                    </View>       
            </View>
        );

    }
}
const mapDispatchToProps = dispatch => {
    return {
        setUser: user => {
            dispatch(setUser(user))
        }
    }
};
const mapStateToPropsEditUser = (state) => {
    return {
        user: state.user,
        global: state.global
    };
};
export default connect(mapStateToPropsEditUser,mapDispatchToProps)(InsertSocial)
