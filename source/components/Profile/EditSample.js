import React, {Component} from 'react';
import {
    Text,
    TouchableOpacity,
    Dimensions, View, Alert,
    TextInput,
    Image,
    Platform
} from 'react-native';

import ServerData from '../connection/connect';
import style from './Style';
import {Form, Input, Item, Label, Content, Body} from "native-base";
import {Actions} from "react-native-router-flux";
import {ProgressDialog} from "react-native-simple-dialogs";
import {connect} from "react-redux";
import {setUser} from "../../Redux/Actions";
import {showImagePicker} from 'react-native-image-picker';
import DocumentPicker from 'react-native-document-picker';
import Helpers from '../Shared/Helper';
const width = Dimensions.get('window').width;
const height = Dimensions.get('window').height;
const Connect = new ServerData();
const options = {
    quality: 1.0,
    maxWidth: width * .80,
    maxHeight: height * .30,
    title: 'انتخاب عکس',
    takePhotoButtonTitle: 'دوربین',
    chooseFromLibraryButtonTitle: 'انتخاب عکس از گالری',
    cancelButtonTitle: 'لغو کردن',
    storageOptions: {
        skipBackup: true,
        path: 'image',
    }
};
class EditSample extends Component {
    constructor(props) {
        super(props);
        this.state = {
            subject:'',
            link:'',
            imageSample: '',
            imagePath:'',
            imageFileName:'',

            file_attach_url: '',
            file_attach_type: '',
            file_attach_filename: '',
            file_attach_fileSize: '',
            progressVisible:false,
            File_size:0 
        }
      
     
    };
    showImagePicker(){
        showImagePicker(options, (response) => {
        if (response.didCancel) {
            console.log('User cancelled image picker');
        } else if (response.error) {
            console.log('ImagePicker Error: ', response.error);
        } else {
            let im1 = new Date().getTime() + '.png';
    
            const source = {uri: response.uri};
    
            this.setState({
                imageSample: response.uri,
                imagePath: response.path,
                imageFileName: response.fileName
            
            });
           
        }
    
    })
}
 async openDocumentPicker() {
    try {
        const res = await DocumentPicker.pick({
          type: [DocumentPicker.types.allFiles],
        });
        console.log(res)
       await this.setState({
        file_attach_url: res.uri,
        file_attach_type: res.type,
        file_attach_filename: res.name,
        file_attach_fileSize: res.size,
        File_size:res.size
        
    });
   
      } catch (err) {
        if (DocumentPicker.isCancel(err)) {
          // User cancelled the picker, exit any dialogs or menus and move on
        } else {
          throw err;
        }
      } 
}

async btn_updateSample() {
    if(await Helpers.CheckNetInfo()){
        return;
    }
    if(this.state.imageSample == ''){
        Alert.alert('','ابتدا یک عکس انتخاب کنید',[{text:'باشه'}])
    }
    else if(this.state.File_size >15000000){
        Alert.alert('','حجم فایل انتخابی باید کمتر از 15 مگابایت باشد',[{text:'باشه'}])
    }
    else{
        
    try {
        this.setState({progressVisible:true})
    
        let formData = new FormData();
            formData.append("image", {
                name: this.state.imageFileName,
                uri: "file://"+this.state.imagePath,
                type: 'image/jpg'
            });
            if(this.state.file_attach_url!=''){
                formData.append("file", {
                    name: this.state.file_attach_filename,
                    uri: this.state.file_attach_url,
                    type: 'video/mp4'
                });
            }else{
                formData.append("file", '');
            }
            formData.append("admintoken", this.props.global.adminToken);
            formData.append("usertoken", this.props.user.apiToken);
            formData.append("title", this.state.subject);
            formData.append("link", this.state.link);
      
    console.log(formData)
    
        let response = await fetch(this.props.global.baseApiUrl + '/user/Insert_Sample', {
            method: 'post',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'multipart/form-data'
            },
            body: formData
        })
        
        let result = await response.json();
        console.log(result)
    
            if (result.success) {
                this.setState({ progressVisible: false,imageSample:'',file_attach_url:'',File_size:0,link:'',subject:'' });
                Alert.alert('','نمونه کار اضاف شد',[{text:'باشه'}])
                }
                else {
                    this.setState({ progressVisible: false });
                    Alert.alert('','اضافه کردن نمونه کار با مشکل مواجه شد',[{text:'باشه'}])
                } 
       } catch (error) {
        this.setState({ progressVisible: false });
        Alert.alert('','اضافه کردن نمونه کار با مشکل مواجه شد',[{text:'باشه'}])
           console.log(error)
       }

    }

}


    render() {
        
        return (
           <View style={style.viewContainerEditSample}>
               <ProgressDialog
                    visible={this.state.progressVisible}
                    message="لطفا منتظر بمانید..."
                />
                <Content>
                <TouchableOpacity activeOpacity={0.7} style={style.view_center_headerEditProfile}
                                      onPress={()=>this.showImagePicker()}>
                        <View style={style.img_user_profEditProfile}>
                        {this.state.imageSample === '' &&
                            <Image source={require('../../assets/images/UploadImage.png')} style={{ width:width*.60,height:width*.25,}}
                                   resizeMode={'contain'}/>
                                }
                        {this.state.imageSample !== '' &&
                    <Image source={{uri:this.state.imageSample}} style={{width:'100%',height:'100%'}}
                            resizeMode='cover'/>
                        }
                        </View>
                </TouchableOpacity>
                    <TouchableOpacity activeOpacity={0.7} style={[style.username_input,{marginTop:10}]} onPress={()=>this.openDocumentPicker()}>
                        <View style={style.img_user_profEditProfile}>
                        {this.state.file_attach_url === '' &&
                        <Text style={{color: '#5b3862',fontSize: width / 28,fontFamily: 'IRANSansMobile',}}> انتخاب فایل ( اختیاری )</Text>
                                }
                        {this.state.file_attach_url !== '' &&
                         <Text style={{color: '#5b3862',fontSize: width / 28,fontFamily: 'IRANSansMobile',}}>فایل انتخاب شد</Text>
                        }
                        </View>
                    </TouchableOpacity>
                    {this.state.File_size >0 &&
                        <Text style={{fontFamily:'IRANSansMobile',textAlign:'center',padding:5}}>حجم فایل  : {Helpers.ToPersianNumber(String(this.state.File_size/1000000))} مگابایت</Text>
                    }
                    <View style={[style.view_input_Item,]}>
                        <TextInput
                            placeholder='عنوان ( اختیاری )'
                            placeholderTextColor={'#5b3862'}
                            value={this.state.subject}
                            style={[style.username_input,{color: '#000000'}]}
                            onChangeText={(text) => this.setState({subject: text})}
                        />
                    </View>
                    <View style={[style.view_input_Item,]}>
                        <TextInput
                            placeholder='لینک ( اختیاری )'
                            placeholderTextColor={'#5b3862'}
                            value={this.state.link}
                            style={[style.username_input,{color: '#000000'}]}
                            onChangeText={(text) => this.setState({link: text})}
                        />
                    </View>
                    <TouchableOpacity activeOpacity={.5} onPress={this.btn_updateSample.bind(this)}>
                    <View style={[style.view_btn_update,{marginTop: 10,alignSelf:'center'}]}>
                        <Text style={style.txt_btn_update}>ثبت</Text>
                    </View>
                </TouchableOpacity>
                </Content>
           </View>
        );

    }
}
const mapDispatchToProps = dispatch => {
    return {
        setUser: user => {
            dispatch(setUser(user))
        }
    }
};
const mapStateToPropsEditUser = (state) => {
    return {
        user: state.user,
        global: state.global
    };
};
export default connect(mapStateToPropsEditUser,mapDispatchToProps)(EditSample)
