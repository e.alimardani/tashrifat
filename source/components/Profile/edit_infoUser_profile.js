import React, {Component} from 'react';
import {
    Text,
    TouchableOpacity,
    Dimensions, View, Alert,
    FlatList,TextInput,
    ScrollView
} from 'react-native';

import ServerData from '../connection/connect';
import style from '../../assets/styles/editProfile_css';
import {Form, Input, Item, Label, Content, Container} from "native-base";
import Icon from "react-native-vector-icons/FontAwesome5";
import {Actions} from "react-native-router-flux";
import {ProgressDialog} from "react-native-simple-dialogs";
import Modal from "react-native-modal";
import LinearGradient from "react-native-linear-gradient";
import {connect} from "react-redux";
import mapStateToProps from "react-redux/es/connect/mapStateToProps";
import {setUser} from "../../Redux/Actions";

const width = Dimensions.get('window').width;
const height = Dimensions.get('window').height;
const Connect = new ServerData();

class edit_infoUser_profile extends Component {

    constructor(props) {
        super(props);
        this.state = {
            firstname:'',
            lastname:'',
            phone_number:'',
            username:'',
            Password:'',
            email:'',
            work_in:'',
           optionWork_in:[],
           selectedWorkIn:'زمینه کاری',
            progressVisible: false,
            SkillAndUpdate:'',
            ModalEditSkillState: false,
            selectedGenre:'ژانر',
            ModalGenre:false,
            Genres:[]
        }
        this.renderOptionSkill = this.renderOptionSkill.bind(this);
        this.GetDataSkillAndUpdate = this.GetDataSkillAndUpdate.bind(this);
        this.renderGenre = this.renderGenre.bind(this);

    };

  

  async componentWillMount() {
   await this.GetDataSkillAndUpdate();
   this.getGenre()
       const user = this.props.user;
       this.setState({
           username: user.username,
           email: user.email,
           firstname: user.firstname,
           lastname: user.full_name,
           phone_number: user.phone_number,
           selectedWorkIn:user.work_in,
           selectedGenre:user.Genre
       })
   };
  async GetDataSkillAndUpdate() {
    const details = {
        "admintoken": this.props.global.adminToken,
    };
   let result = await Connect.sendPost(this.props.global.baseApiUrl + '/post/getProfileFields', details)
            if (result.success == true) {

                this.setState({SkillAndUpdate: result.data});

                const findWorkIn = result.data.find( a => a.name === "زمینه کاری" );
                await this.setState({work_in:findWorkIn})
                await this.setState({optionWork_in:findWorkIn.option})
             
               
            } else {

            }
}
renderGenre({item,index}){
  
    return(
        <View>
        <TouchableOpacity style={style.itemOptionModal} onPress={()=> this.setState({selectedGenre:item.name,ModalGenre:false})}>
            <Text style={style.txt_itemOptionModal}>{item.name}</Text>
        </TouchableOpacity>
            <View style={{borderBottomWidth:width*.001,borderBottomColor:'#c5c5c5'}}/>
        </View>
    )
}
async getGenre(){

    try {
        const details = {
            "admintoken": this.props.global.adminToken,
        };
        let result = await Connect.sendPost(this.props.global.baseApiUrl + '/user/get_genre', details);
        console.log(result)

        if (result.success == true) {
            this.setState({Genres: result.data});
        }
       } catch (error) {
        console.log(error)
       }
}

   async btn_updateInfo() {
        this.setState({progressVisible: true});
        const details = {
            "admintoken": this.props.global.adminToken,
            "usertoken": this.props.user.apiToken,
            "email": this.state.email,
            "work_in": this.state.selectedWorkIn,
            "name": this.state.lastname,
            "username":this.props.user.username,
            "password": this.state.Password,
            "genre":this.state.selectedGenre,
        };
       let result = await Connect.sendPost(this.props.global.baseApiUrl + '/user/update', details)
        console.log(result)
                this.setState({progressVisible: false});
                if (result.success === true) {
                    // console.log(result)
                    // this.setState({progressVisible: false});
                   await this.props.setUser({
                        username : this.state.username,
                        email: this.state.email,
                        work_in: this.state.selectedWorkIn,
                        full_name: this.state.lastname,
                        phone_number:this.props.user.phone_number,
                        work_in:this.state.selectedWorkIn,
                        Genre:this.state.selectedGenre,
                        whatsapp:this.props.user.whatsapp,
                        telegram:this.props.user.telegram,
                        instegram:this.props.user.instegram,
                        id:this.props.user.id,
                        apiToken:this.props.user.apiToken,
                        Groups_count:this.props.user.Groups_count,
                        friends_count:this.props.user.friends_count,
                        Avatar:this.props.user.Avatar

                    })
                    Alert.alert("بروزرسانی شد", 'شما با موفقیت اطلاعات خود را بروزرسانی کردید', [
                        {text: 'باشه'}
                    ]);
                } else {
                    // console.log(result)
                    Alert.alert("خطا", 'بروزرسانی با مشکل مواجه شد لطفا دوباره تلاش کنید', [
                        {text: 'باشه'}
                    ]);
                    // alert(result.messages)
                }
    }
    renderOptionSkill({item,index}){
        return(
            <View>
            <TouchableOpacity style={style.itemOptionModal} onPress={()=> this.setState({selectedWorkIn:item.name,ModalEditSkillState:false})}>
                <Text style={style.txt_itemOptionModal}>{item.name}</Text>
            </TouchableOpacity>
                <View style={{borderBottomWidth:width*.001,borderBottomColor:'#c5c5c5'}}/>
            </View>
        )
    }
    render() {
        const user = this.props.user;
        return (

                <ScrollView style={style.view_container_form_signUp}>
                    <Modal isVisible={this.state.ModalGenre}
                        
                        onBackdropPress={() => this.setState({ModalGenre: !this.state.ModalGenre})}
                        style={style.ModalEditSkillStyle}
                    >
                        <View style={[style.viewModalEditSkillState]}>
                            <TouchableOpacity activeOpacity={.7} style={style.btnExitModal} onPress={()=>this.setState({ModalGenre:false})}>
                                <Icon name="times-circle" color={'#fff'} size={width * .06}/>
                            </TouchableOpacity>
                            <FlatList
                                data={this.state.Genres}
                                renderItem={this.renderGenre}
                                keyExtractor={(item, index) => index.toString()}/>
                        </View>
                    </Modal>
                <Modal isVisible={this.state.ModalEditSkillState}
                        
                        onRequestClose={() => {
                            alert("Modal has been closed.")
                        }}
                        onBackdropPress={() => this.setState({ModalEditSkillState: !this.state.ModalEditSkillState})}
                        style={style.ModalEditSkillStyle}
                    >
                        <View style={[style.viewModalEditSkillState]}>
                            <TouchableOpacity activeOpacity={.7} style={style.btnExitModal} onPress={()=>this.setState({ModalEditSkillState:false})}>
                                <Icon name="times-circle" color={'#fff'} size={width * .06}/>
                            </TouchableOpacity>
                            <FlatList
                                data={this.state.optionWork_in}
                                renderItem={this.renderOptionSkill}
                                keyExtractor={(item, index) => index.toString()}/>
                        </View>
                    </Modal>
                    <ProgressDialog
                        visible={this.state.progressVisible}
                        message="لطفا منتظر بمانید..."
                    />
                    
                        <View style={[style.view_input_Item,]}>
                                    <TextInput
                                
                                    placeholder='نام و نام خانوادگی'
                                    placeholderTextColor={'#5b3862'}
                                    value={this.state.lastname}
                                    style={[style.username_input,{color: '#000000'}]}
                                    onChangeText={(text) => this.setState({lastname: text})}

                                />
                            </View>
                    
                        {/* <View style={[style.view_input_Item,]}>
                                    <TextInput
                                maxLength={11}
                                    placeholder='شماره تماس'
                                    placeholderTextColor={'#5b3862'}
                                    value={this.state.phone_number}
                                    style={[style.username_input,{color: '#000000'}]}
                                    onChangeText={(text) => this.setState({phone_number: text})}

                                />
                            </View>
                    
                        <View style={[style.view_input_Item,]}>
                                    <TextInput
                                
                                    placeholder='نام کاربری'
                                    placeholderTextColor={'#5b3862'}
                                    value={this.state.username}
                                    style={[style.username_input,{color: '#000000'}]}
                                    onChangeText={(text) => this.setState({username: text})}

                                />
                            </View> */}
                    
                        <View style={[style.view_input_Item,]}>
                                    <TextInput
                                
                                    placeholder='رمز عبور'
                                    placeholderTextColor={'#5b3862'}
                                    value={this.state.Password}
                                    style={[style.username_input,{color: '#000000'}]}
                                    onChangeText={(text) => this.setState({Password: text})}

                                />
                            </View>
                    
                        <View style={[style.view_input_Item,]}>
                                    <TextInput
                                
                                    placeholder='ایمیل'
                                    placeholderTextColor={'#5b3862'}
                                    value={this.state.email}
                                    style={[style.username_input,{color: '#000000'}]}
                                    onChangeText={(text) => this.setState({email: text})}

                                />
                            </View>
                        <View style={[style.view_input_Item,]}>
                            <TouchableOpacity style={style.username_input} onPress={()=>this.setState({ModalEditSkillState:true})}>
                                <Text style={[style.username_inputSkill,{color:"#5b3862",textAlign:'right'}]}>{this.state.selectedWorkIn}</Text>
                            </TouchableOpacity>
                        </View>
                        <View style={[style.view_input_Item,]}>
                            <TouchableOpacity style={style.username_input} onPress={()=>this.setState({ModalGenre:true})}>
                                <Text style={[style.username_inputSkill,{color:"#5b3862",textAlign:'right'}]}>{this.state.selectedGenre}</Text>
                            </TouchableOpacity>
                        </View>
                
                    <TouchableOpacity activeOpacity={.5} onPress={() => this.btn_updateInfo(user)}>
                        <View style={style.view_btn_signUp}>
                            <Text style={style.txt_btn_signUp}>بروزرسانی</Text>
                        </View>
                    </TouchableOpacity>
                    
                </ScrollView>
            
        );

    }
}
const mapDispatchToProps = dispatch => {
    return {
        setUser: user => {
            dispatch(setUser(user))
        }
    }
};
const mapStateToPropsEditUser = (state) => {
    return {
        user: state.user,
        global: state.global
    };
};
export default connect(mapStateToPropsEditUser,mapDispatchToProps)(edit_infoUser_profile)
