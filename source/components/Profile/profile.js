import React, {Component} from 'react';
import {
    Text,
    TouchableOpacity,
    Dimensions,
    View,
    ScrollView,
    BackHandler,
    Image, StatusBar,
    TouchableNativeFeedback, Alert, FlatList,Share
} from 'react-native';
import {Container, Header, Left, Body, Right, Button, Title,Content} from 'native-base';
import ServerData from '../connection/connect';
import style from '../../assets/styles/profile_css';
import Menu2_profile from '../User/show_Takhasos_profile';
import Menu3_profile from '../User/show_infoUser_profile';
import Icon from 'react-native-vector-icons/FontAwesome';
import LinearGradient from "react-native-linear-gradient";
import {Actions} from 'react-native-router-flux';
import {connect} from "react-redux";
import mapDispatchToProps from "react-redux/es/connect/mapDispatchToProps";
import { setUser} from "../../Redux/Actions";
import Helpers from '../Shared/Helper';


const width = Dimensions.get('window').width;
const height = Dimensions.get('window').height;
const Connect = new ServerData();

class profile extends Component {
    constructor(props) {
        super(props);
        this.state = {
            menu2_display: 'none',
            menu3_display: 'flex',
            background_menu2: '#b2b2b2',
            background_menu3: '#b2b2b2',
            setColor: '1',
            SkillUser: [],
            loading:true
        };
        this.share = this.share.bind(this);
    };

    menu2() {
        this.setState({setColor: '0', menu2_display: 'flex', menu3_display: 'none'})
    }

    menu3() {
        this.setState({setColor: '1', menu2_display: 'none', menu3_display: 'flex'})
    }

    async componentWillMount() {
        await this.GetDataSkillUser();
        console.log(this.props.user)
    }

    async GetDataSkillUser() {
        try {
            const details1 = {
                "admintoken": this.props.global.adminToken,
                "usertoken": this.props.user.apiToken,
            };
            let result = await Connect.sendPost(this.props.global.baseApiUrl + '/user/getUser_Specialty', details1);
            if (result.success === true) {
                this.setState({SkillUser: result.data,loading:false})
            } else {
                this.setState({loading:false})
            }
        } catch (error) {
            this.setState({loading:false})
        }
     
    }
    async share() {
        
        if (await Helpers.CheckNetInfo()) {
            return;
        }

        let inviteText = 'سلام\nاپلیکیشن "' + this.props.global.appName + '" رو از لینک زیر دانلود کن \n\n' + this.props.global.website;

        Share.share({
            message: inviteText,
            title: "به اشتراک گذاری " + this.props.global.appName
        }, {dialogTitle: "به اشتراک گذاری " + this.props.global.appName});

    }
    render() {
        const user = this.props.user;

        return (
            <Container style={style.view_container_prof}>
                <LinearGradient style={style.view_header_prof} colors={['#627efb', '#8b68fc', '#a95bfe']}>
                    <Header transparent style={style.view_top_header}>
                        <StatusBar backgroundColor="transparent" translucent={true} barStyle="light-content"/>
                        <Left style={{flex: 1,}}>
                            <Button transparent onPress={() => Actions.pop()}>
                                <Icon name="arrow-left" color={"#ffffff"} size={width * .07}/>
                            </Button>
                        </Left>
                        <Body style={{flex: 1, alignItems: 'center'}}>
                            <Title style={style.txt_top_header}>پروفایل من</Title>
                        </Body>
                        <Right style={{flex: 1}}>
                            <Button transparent onPress={() => Actions.drawerOpen()}>
                                <Icon name="bars" color={"#fff"} size={width * .07}/>
                            </Button>
                        </Right>
                    </Header>
                    <View style={{flexDirection:'row',width:width,paddingHorizontal:width*.03,justifyContent:'space-between',alignItems:'center'}}>
                        <TouchableOpacity activeOpacity={.8} style={style.viewContainerLocation} onPress={()=>Actions.push('maps')}>
                            <Text style={[style.txt_nameUser_prof_header,{marginRight:5}]}>موقعیت</Text>
                            <Image source={require('../../assets/images/location.png')} style={style.img_location}/>
                        </TouchableOpacity>
                        <TouchableOpacity onPress={this.share} style={[style.viewContainerLocation]}>
                        <Text style={[style.txt_nameUser_prof_header,{marginRight:5}]}>دعوت از دوستان</Text>
                        <Image source={require('../../assets/images/create-group-button.png')} style={style.img_location}/>
                         </TouchableOpacity>
                    </View>
                       
                    <View style={style.view_center_header}>
                    <View style={style.img_user_prof}>
                            {user.Avatar == "" && <Image source={require('../../assets/images/userIconLogin.png')} style={style.pic_member} resizeMode={'cover'}/>}
                            {user.Avatar != "" && <Image source={{uri: user.Avatar}} style={style.pic_member} resizeMode={'cover'}/>}
                        </View>
                        <View style={style.view_name_side_prof}>
                            <Text style={style.txt_post_prof_header}>{`( ${user.work_in === null ? 'زمینه کاری انتخاب نشده':user.work_in} )`}</Text>
                            <Text style={style.txt_nameUser_prof_header}>{user.full_name}</Text>
                        </View>
                        {user.Genre != undefined &&
                            <Text style={style.txt_nameUser_prof_header}>{user.Genre}</Text>
                        }
                    </View>
                    <View style={style.view_count_group_users}>
                        {/* <TouchableOpacity activeOpacity={.7} style={style.view_bottom_header}
                                          onPress={() => Actions.push('User_Friend')}>
                            <View style={style.view_count_txt}>
                                <Text style={style.txt_header}>{Helpers.ToPersianNumber(String(user.friends_count))}</Text>
                            </View>
                            <Text style={style.txt_header1}>تعداد دوستان</Text>
                        </TouchableOpacity> */}
                        {/* <View style={style.view_bottom_header}>
                            <Text style={style.txt_header1}>تعداد گروه ها</Text>
                            <View style={style.view_count_txt}>
                                <Text style={style.txt_header}>{Helpers.ToPersianNumber(String(user.Groups_count))}</Text>
                            </View>
                        </View> */}
                    </View>
                    <View style={style.view_count_group_users}>
                    
                    {/* <TouchableOpacity onPress={this.share} style={style.view_shareLink}>
                        <Text style={[style.txt_tabMenu,{fontSize:width/30}]}>ویرایش شبکه های اجتماعی</Text>
                    </TouchableOpacity> */}
                    </View>
                </LinearGradient>
                <Content style={[style.container_menu_profile]}>
                    <View style={style.view_menu_top}>
                        <TouchableNativeFeedback background={TouchableNativeFeedback.Ripple('#8b68fc70')}
                                                 onPress={() => this.menu2()}>
                            <View style={this.state.setColor == 0 ? style.tabMenu_focus : style.tabMenu}>
                                <Text
                                    style={[style.txt_tabMenu, {color: this.state.setColor == 0 ? '#8b68fc' : '#CAC9CC'}]}>تخصص
                                    ها</Text>
                            </View>
                        </TouchableNativeFeedback>

                        <TouchableNativeFeedback background={TouchableNativeFeedback.Ripple('#8b68fc70')}
                                                 onPress={() => this.menu3()}>
                            <View style={this.state.setColor == 1 ? style.tabMenu1_focus : style.tabMenu1}>
                                <Text
                                    style={[style.txt_tabMenu, {color: this.state.setColor == 1 ? '#8b68fc' : '#CAC9CC'}]}>مشخصات</Text>
                            </View>
                        </TouchableNativeFeedback>
                    </View>
                        <View style={[style.view_menu2, {display: this.state.menu2_display}]}>
                            <Menu2_profile skill={this.state.SkillUser}/>
                        </View>
                        <View style={{display: this.state.menu3_display, justifyContent: 'space-around'}}>
                            <Menu3_profile color_prof={'#fff'}/>
                        </View>
                    </Content>
            </Container>
        );
    }
}

const mapDispatchToPropsProfile = dispatch => {
    return {
        setUser: user => {
            dispatch(setUser(user))
        }
    }
};
const mapStateToProps = (state) => {
    return {
        user: state.user,
        global: state.global
    };
};
export default connect(mapStateToProps, mapDispatchToPropsProfile)(profile)

