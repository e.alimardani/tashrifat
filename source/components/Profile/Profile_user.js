import React, {Component} from 'react';
import {
    Text,
    TouchableOpacity,
    Dimensions,
    View,
    Linking,
    BackHandler,
    Image, StatusBar,
    TouchableNativeFeedback, Alert, FlatList
} from 'react-native';
import AsyncStorage from '@react-native-community/async-storage';
import {Container, Header, Left, Body, Right, Button, Title,Content} from 'native-base';
import ServerData from '../connection/connect';
import style from '../../assets/styles/profile_css';
import SkillSkillUser from '../../components/User/show_Skill_profileUser';
import InfoProfileUser from '../../components/User/show_infoUser_profileUser';
import Location_User from '../../components/User/locationUser';
import Icon from 'react-native-vector-icons/FontAwesome';
import LinearGradient from "react-native-linear-gradient";
import IconF from 'react-native-vector-icons/AntDesign'
import {Actions} from 'react-native-router-flux';
import {connect} from "react-redux";
import mapDispatchToProps from "react-redux/es/connect/mapDispatchToProps";
import {setSkillUser, setUser} from "../../Redux/Actions";


const width = Dimensions.get('window').width;
const height = Dimensions.get('window').height;
const Connect = new ServerData();

class Profile_user extends Component {

    constructor(props) {
        super(props);
        this.state = {
            menu2_display: 'none',
            menu1_display:'none',
            menu3_display: 'flex',
            background_menu2: '#b2b2b2',
            background_menu3: '#b2b2b2',
            setColor: 1,
            apiToken: '',
            socialMedia:[]
           
        }

    this.unFollowRequest = this.unFollowRequest.bind(this);
    this.FollowRequest = this.FollowRequest.bind(this);
    };


    componentDidMount(){
        console.log(this.props)
    }

    menu1() {
        this.setState({setColor: 2,menu1_display:'flex', menu2_display: 'none', menu3_display: 'none'})
    }
    menu2() {
        this.setState({setColor: 0,menu1_display:'none', menu2_display: 'flex', menu3_display: 'none'})
    }

    menu3() {
        this.setState({setColor: 1,menu1_display:'none', menu2_display: 'none', menu3_display: 'flex'})
    }

   async unFollowRequest(ListUsers) {
        const details = {
            "admintoken": this.props.global.adminToken,
            "usertoken": this.props.user.apiToken,
            "target_id": this.props.ListUsers.user_id,
        };
        let result = await Connect.sendPost(this.props.global.baseApiUrl + '/user/CancelOrIngnore_FollowRequest', details)
         
                if (result.success == true) {
                    ListUsers.Frindship_Status_With_Current_User = 'nothing'
                    Alert.alert('', result.data, [{text: 'باشه'}])

                } else {
                    Alert.alert('', result.messages, [{text: 'باشه'}])
                }          
    }

  async FollowRequest(ListUsers) {
        const details = {
            "admintoken": this.props.global.adminToken,
            "usertoken": this.props.user.apiToken,
            "target_user": this.props.ListUsers.user_id,
        };
       let result = await Connect.sendPost(this.props.global.baseApiUrl + '/user/Follow_Request', details)
        if (result.success == true) {
            ListUsers.Frindship_Status_With_Current_User = 'Requested'
            Alert.alert('', result.data, [{text: 'باشه'}])
        } else {
            Alert.alert('', result.messages, [{text: 'باشه'}])
        }  
    }

    render() {
        const ListUsers = this.props.ListUsers;
        return (
            <Container style={style.view_container_prof}>
            
                <LinearGradient style={[style.view_header_prof]} colors={['#627efb', '#8b68fc', '#a95bfe']}>
                    <Header transparent style={style.view_top_header}>
                        <StatusBar backgroundColor="transparent" translucent={true} barStyle="light-content"/>
                        <Left style={{flex: 1,}}>
                            <Button transparent onPress={() => Actions.pop()}>
                                <Icon name="arrow-left" color={"#ffffff"} size={width * .07}/>
                            </Button>
                        </Left>
                        <Body style={{flex: 1, alignItems: 'center'}}>
                            <Title style={style.txt_top_header}>پروفایل {ListUsers.full_name}</Title>
                        </Body>
                        <Right style={{flex: 1}}>
                            <Button transparent onPress={() => Actions.drawerOpen()}>
                                <Icon name="bars" color={"#fff"} size={width * .07}/>
                            </Button>
                        </Right>
                    </Header>
                    <View style={style.view_center_header}>
                        <View style={style.img_user_prof}>
                            {(ListUsers.avatar == "") && <Image source={require('../../assets/images/userIconLogin.png')} style={style.pic_member} resizeMode={'cover'}/>}
                            {(ListUsers.avatar != "")&& <Image source={{uri: ListUsers.avatar ? ListUsers.avatar:ListUsers.Avatar}} style={style.pic_member} resizeMode={'cover'}/>}
                        </View>
                        <View style={style.view_name_side_prof}>
                            <Text style={style.txt_post_prof_header}>{`( ${ListUsers.work_in == null ?'زمینه کاری انتخاب نشده':ListUsers.work_in} )`}</Text>
                            <Text
                                style={style.txt_nameUser_prof_header}>{ListUsers.full_name}</Text>
                        </View>
                        {ListUsers.genre != undefined &&
                            <Text style={style.txt_nameUser_prof_header}>{ListUsers.genre}</Text>
                        }
                    </View>
                   
                     <View style={style.view_count_group_users}>
                        {/* {this.props.user.apiToken !== null && 
                        <TouchableOpacity style={[style.view_bottom_header_follow,]}
                                          onPress={() => this.FollowRequest(ListUsers)}>
                            <IconF name="adduser" color={'#fff'} size={width * .05}/>
                            <Text style={style.txt_header1}>دنبال کردن</Text>
                        </TouchableOpacity>
                        } */}
                        {/* {this.props.user.apiToken !== null && ListUsers.Frindship_Status_With_Current_User === 'friend' || ListUsers.Frindship_Status_With_Current_User === 'Requested' &&
                         <TouchableOpacity
                            style={[style.view_bottom_header_follow,]}
                            onPress={() => this.unFollowRequest(ListUsers)}>
                            <View style={style.view_count_txt}>
                            <Text style={style.txt_header}>{user.friends_count}</Text>
                            <IconF name="adduser" color={'#fff'} size={width * .05}/>
                            </View>
                            <Text style={style.txt_header1}>لغو دنبال کردن</Text>
                        </TouchableOpacity>
                        } */}
                        {this.props.user.apiToken !== null &&
                        <TouchableOpacity style={[style.view_bottom_header_follow,]}
                                          onPress={() => Actions.push('contact_user', {'ListUserSendMessage': ListUsers})}>
                            <IconF name="adduser" color={'#fff'} size={width * .05}/>
                            <Text style={style.txt_header1}>ارسال پیام</Text>
                        </TouchableOpacity>
                        }
                    </View>
                {/* <View style={{flexDirection:'row',justifyContent:'space-around',marginTop:'6%',width:width*.60,alignSelf:'center'}}>
                    <TouchableOpacity activeOpacity={.7} onPress={this.LinkToWhatsApp.bind(this)}>
                        <Image source={require('../../assets/images/whatsapp.png')} style={{width:width*.05,height:width*.05}} resizeMode={"contain"}/>
                    </TouchableOpacity>
                    <TouchableOpacity activeOpacity={.7} onPress={this.LinkToTelegram.bind(this)}>
                        <Image source={require('../../assets/images/telegram.png')} style={{width:width*.05,height:width*.05}} resizeMode={"contain"}/>
                    </TouchableOpacity>
                    <TouchableOpacity activeOpacity={.7} onPress={this.LinkToInstagram.bind(this)}>
                        <Image source={require('../../assets/images/instagram.png')} style={{width:width*.05,height:width*.05}} resizeMode={"contain"}/>
                    </TouchableOpacity>
                </View> */}
                </LinearGradient>
                <Content style={style.container_menu_profile}>
                
                    <View style={{flexDirection:'row',elevation:5}}>
                    <TouchableNativeFeedback background={TouchableNativeFeedback.Ripple('#8b68fc70')}
                                                 onPress={() => this.menu1()}>
                            <View style={this.state.setColor === 2 ? style.tabMenu_focus : style.tabMenu}>
                                <Text
                                    style={[style.txt_tabMenu, {color: this.state.setColor === 2 ? '#8b68fc' : '#CAC9CC'}]}>موقعیت</Text>
                            </View>
                        </TouchableNativeFeedback>
                        <TouchableNativeFeedback background={TouchableNativeFeedback.Ripple('#8b68fc70')}
                                                 onPress={() => this.menu2()}>
                            <View style={this.state.setColor === 0 ? style.tabMenu_focus : style.tabMenu}>
                                <Text
                                    style={[style.txt_tabMenu, {color: this.state.setColor === 0 ? '#8b68fc' : '#CAC9CC'}]}>تخصص
                                    ها</Text>
                            </View>
                        </TouchableNativeFeedback>

                        <TouchableNativeFeedback background={TouchableNativeFeedback.Ripple('#8b68fc70')}
                                                 onPress={() => this.menu3()}>
                            <View style={this.state.setColor === 1 ? style.tabMenu1_focus : style.tabMenu1}>
                                <Text
                                    style={[style.txt_tabMenu, {color: this.state.setColor === 1 ? '#8b68fc' : '#CAC9CC'}]}>مشخصات</Text>
                            </View>
                        </TouchableNativeFeedback>
                    </View>
                    
                    <View style={[style.view_menu2, {display: this.state.menu1_display}]}>
                        <Location_User IDForGetLocation={ListUsers} color_prof={'#fff'}/>
                    </View>
                    <View style={[style.view_menu2, {display: this.state.menu2_display}]}>
                        <SkillSkillUser Specialty={this.props.ListUsers.User_Specialty} color_prof={'#fff'}/>
                    </View>
                    <View style={{display: this.state.menu3_display, justifyContent: 'space-around'}}>
                        <InfoProfileUser ListUser={this.props.ListUsers} SocialMedia={this.state.socialMedia}/>
                    </View>
                </Content>
                
            </Container>
        );

    }
}

// const mapDispatchToPropsProfile = dispatch => {
//     return {
//         setUser: user => {
//             dispatch(setUser(user))
//         }
//     }
// };
const mapStateToProps = (state) => {
    return {
        user: state.user,
        global: state.global
    };
};
export default connect(mapStateToProps, null)(Profile_user)

