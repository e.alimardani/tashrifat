import React from 'react';
import {StyleSheet, Dimensions, Platform} from 'react-native';


const width = Dimensions.get('window').width;
const height = Dimensions.get('window').height;

export default StyleSheet.create({
    viewContainerEditSample: {
        width: width * .80,
        justifyContent:'center',
        alignItems:'center',
        // backgroundColor:'#000'
    },
   
   
    view_btn_EditSocial:{width:width*.30,height:width*.10,justifyContent:'center',
alignItems:'center',backgroundColor:'#5b3862',marginTop:width*.05,borderRadius:width*.02},
    view_input_Item: {
        marginTop: height * .01,
    },
    username_input: {
        color: '#000',
        fontSize: width / 28,
        fontFamily: 'IRANSansMobile',
        backgroundColor:'rgb(250,239,253)',
       
        padding:0,
        width: width * .70,
        height:height*.07,
        borderRadius: width * .10,
        // marginHorizontal:width*.05,
        textAlign:'center',
        alignItems:'center',
        justifyContent:'center',

    },
    view_center_headerEditProfile: {
        marginTop: width * (.02),
        backgroundColor:'#00000010',
        justifyContent: 'center',
        width:width*.50,
        height:width*.40,
        alignSelf:'center',
        alignItems: 'center'
    },
    img_user_profEditProfile: {
        width:width*.50,
        height:width*.40,
        // borderRadius: width * .10,
        // borderWidth: width * .01,
        // borderColor: '#fff',
        justifyContent:'center',
        alignItems:'center'
    },
    pic_member: {
        width:width*.60,
        height:width*.25,
    },
    message_container: {
        flex: 1,
        flexDirection: 'row',
        alignItems: 'flex-end',

    },
    center_messageRecive:{
        // flex: 1,
        flexDirection: 'row',
        // alignItems: 'flex-end',
    },
    view_name:{paddingBottom:height*.02},
   
   
   
    
    pic_member: {
        width: width * .15,
        height: width * .15,
        borderRadius: width * .07,
        marginHorizontal:width*.02,
        marginVertical: height*.01

    },
    view_btn_update: {
        justifyContent: 'center',
        alignItems: 'center',
        width: width * .59,
        height:height*.08,
        borderRadius: width * .10,
        marginBottom: 20,
        backgroundColor: '#5b3862',
    },
    txt_btn_update: {
        color: '#fff',
        fontFamily: 'IRANSansMobile',
        fontSize: width / 25,
    },
});

