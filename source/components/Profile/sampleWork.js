import React, {Component} from 'react';
import {
    Dimensions,
    View,
    Text, Platform, FlatList,
    BackHandler, StatusBar, TouchableOpacity, ActivityIndicator, KeyboardAvoidingView, Image,Alert,Linking
} from 'react-native';
import AsyncStorage from '@react-native-community/async-storage';
import {Container, Header, Left, Body, Right, Button, Title, Spinner} from 'native-base';
import ServerData from '../connection/connect';
import style from '../../assets/styles/sampleWork_css';
import LinearGradient from "react-native-linear-gradient";
import Icon from "react-native-vector-icons/FontAwesome5";
import {ProgressDialog} from "react-native-simple-dialogs";
import {connect} from "react-redux";
import {Actions} from "react-native-router-flux";
import ComponentSampleWork from "../../components/Shared/componentSampleWork";
import Modal from "react-native-modal";
import Helper from '../Shared/Helper';
import RNFetchBlob from 'rn-fetch-blob'
import * as Progress from 'react-native-progress';

const width = Dimensions.get('window').width;
const height = Dimensions.get('window').height;
const Connect = new ServerData();

class sampleWork extends Component {

    constructor(props) {
        super(props);
        this.state = {
            ListSampleWork: [],
            ModalShowPicture: false,
            loading:true,
            imgOriginal: '',
            idImg:'',
            progressVisible:false,
            titleSelect:'',
            LinkSelect:'',
            file_url:'',
            downloadPercent:0,
            downloading:false,
            totalSize:0,
            downloadedSize:0,
            canceled:false,
            source:'',
            showFileModal:false,
            hasLocale:false
        };
        this.selectedFile = null;
        this.renderItemListSampleWork = this.renderItemListSampleWork.bind(this);
        this.setAddressImage = this.setAddressImage.bind(this);
        this.GetSampleWork = this.GetSampleWork.bind(this);
        this.ListEmptyComponentSample = this.ListEmptyComponentSample.bind(this);
    };
    componentWillMount() {
        this.GetSampleWork();
    }
   async GetSampleWork() {

       try {
        const details = {
            "admintoken": this.props.global.adminToken,
            "user_id": this.props.user.id,
        };
      let result = await Connect.sendPost(this.props.global.baseApiUrl + '/user/get_sample_by_userId', details)
               
      
                if (result.success) {
                    console.log(result)
                    this.setState({ListSampleWork: result.data,loading:false});
                } else {
                    this.setState({loading:false,ListSampleWork: []});
                }
       } catch (error) {
        this.setState({loading:false});
       }
    }
    setAddressImage(item) {
        this.setState({imgOriginal: item, ModalShowPicture: true})
    }
    renderItemListSampleWork({item, index}) {
        console.log(item.image_url)
        return (
            <View style={style.SampleWork_container}>
                <TouchableOpacity style={style.view_name}
                                  onPress={()=>{this.setAddressImage(item.image_url) ,console.log(item), this.setState({idImg:item.id,titleSelect:item.title, LinkSelect:item.link , file_url:item.file_url})}}>
                    <Image source={{uri: item.image_url}} style={style.img_sampleWork} resizeMode={'contain'}/>
                </TouchableOpacity>
            </View>
        )
    }
    ListEmptyComponentSample(){
        if(this.state.loading)
        {
            return(
                <ActivityIndicator  color={this.props.global.grColorTwo} size='large' />
            );
        }
        else
        {
            return(
             <View style={style.viewNotFound}>
                <Text style={style.txtNotFound}>نمونه کاری وجود
                    ندارد</Text>
            </View>
            );
        }   
    }
    async deleteSample(){
    this.setState({progressVisible: true});
    console.log('asdfadff')
        try {
            const details = {
                "admintoken": this.props.global.adminToken,
                "sample_id": this.state.idImg,
               
            };
        let result = await Connect.sendPost(this.props.global.baseApiUrl + '/user/delete_sample_by_id', details)
        console.log(details)
        console.log(result)

        if (result.success == true) {
                        this.setState({progressVisible: false,ModalShowPicture:false});
                        console.log(result)
                        this.GetSampleWork();
                        Alert.alert("حذف نمونه کار", 'نمونه کار حذف شد', [
                            {text: 'باشه'}
                        ]);
                    } else {
                        this.setState({progressVisible: false});
                        Alert.alert("حذف نمونه کار", 'خطایی رخ داد', [
                            {text: 'باشه'}
                        ]);
                    }
        } catch (error) {
            this.setState({progressVisible: false});
        }
    }
    render() {
        const user = this.props.user;
        return (
            <View style={style.view_container_messageRecive}>
                <Modal isVisible={this.state.ModalShowPicture}
                       deviceWidth={width}
                       deviceHeight={height}
                       animationType={"slide"}
                       
                       backdropColor={'#000000'}
                       backdropOpacity={0.70}
                       onBackButtonPress={() => this.setState({ModalShowPicture: !this.state.ModalShowPicture})}
                       onBackdropPress={() => this.setState({ModalShowPicture: !this.state.ModalShowPicture})}
                       style={style.ModalSearchDriver}
                >
                    <View style={style.viewContainerSupport}>
                    {this.state.titleSelect !='' && <Text style={[style.txt_header,{marginBottom:5}]}>{this.state.titleSelect}</Text>}
                           
                            <Image source={{uri: this.state.imgOriginal}} style={style.imgModalSampleWork} resizeMode={'cover'}/>
                            {/* <TouchableOpacity style={style.btnSupport1} activeOpacity={.7}
                                              onPress={() => this.setState({ModalShowPicture: false})}>
                                    <Image style={style.Image_close} resizeMode={'cover'} source={require('../../assets/images/close.png')}/>
                            </TouchableOpacity> */}
                            {this.state.LinkSelect !='' &&
                            <TouchableOpacity onPress={()=>Linking.openURL('https://'+this.state.LinkSelect)}>
                                <Text style={[style.txt_header,{marginTop:5}]}>{this.state.LinkSelect}</Text>
                            </TouchableOpacity>
                            }
                            {this.state.file_url !='http://tashrifat.wpfix.ir/wp-content/uploads/sample-file/83/' &&
                            <TouchableOpacity onPress={()=>Linking.openURL(this.state.file_url)} style={{justifyContent: 'center', alignItems: 'center'}}>
                                <Text  style={[style.txt_header,{marginTop:5}]}>مشاهده فایل</Text>
                            </TouchableOpacity>
                            }
                            <TouchableOpacity style={{
                            width:width*.30,height:width*.10,
                            justifyContent:'center',alignItems:'center',
                            borderRadius:10,
                            marginTop:10,
                            backgroundColor:'#a95bfe'}}
                            onPress={()=>this.deleteSample()}
                            >
                                <Text style={[style.txt_header,{color:'#fff'}]}>حذف</Text>
                            </TouchableOpacity>
                    </View>
                </Modal>
                <ProgressDialog
                        visible={this.state.progressVisible}
                        message="لطفا منتظر بمانید..."
                    />
                <LinearGradient style={[style.view_header_prof]} colors={['#627efb', '#8b68fc', '#a95bfe']}>
                    <Header transparent style={style.view_top_header}>
                        <StatusBar backgroundColor="transparent" translucent={true} barStyle="light-content"/>
                        <Left style={{flex: 1,}}>
                            <Button transparent onPress={() => Actions.pop()}>
                                <Icon name="arrow-left" color={"#ffffff"} size={width * .07}/>
                            </Button>
                        </Left>
                        <Body style={{flex: 1, alignItems: 'center'}}>
                            <Title style={style.txt_top_header}>نمونه کارها</Title>
                        </Body>
                        <Right style={{flex: 1}}>
                            <Button transparent onPress={() => Actions.drawerOpen()}>
                                <Icon name="bars" color={"#fff"} size={width * .07}/>
                            </Button>
                        </Right>
                    </Header>
                    <View style={[style.view_center_header,{marginBottom:0}]}>
                        <View style={style.img_user_prof}>
                            <Image source={{uri: user.Avatar}} style={style.pic_member} resizeMode={'cover'}/>
                        </View>
                        <View style={style.view_name_side_prof}>
                            <Text style={style.txt_post_prof_header}>{`( ${user.work_in === null ? 'زمینه کاری ثبت نشده': user.work_in} )`}</Text>
                            <Text style={style.txt_nameUser_prof_header}>{user.full_name}</Text>
                        </View>
                    </View>
                    
                    <View style={[style.view_count_group_users,{marginBottom:15}]}>
                    {user.Genre != undefined &&
                            <Text style={style.txt_nameUser_prof_header}>{user.Genre}</Text>
                        }
                        {/* <TouchableOpacity activeOpacity={.8} style={style.view_bottom_header}  onPress={() => Actions.push('User_Friend')}>
                            <View style={style.view_count_txt}>
                                <Text style={style.txt_header}>{Helper.ToPersianNumber(String(user.friends_count))}</Text>
                            </View>
                            <Text style={style.txt_header1}>تعداد دوستان</Text>
                        </TouchableOpacity>
                        <View style={style.view_bottom_header}>
                            <Text style={style.txt_header1}>تعداد گروه ها</Text>
                            <View style={style.view_count_txt}>
                                <Text style={style.txt_header}>{user.Groups_count}</Text>
                            </View>
                        </View> */}
                    </View>
                </LinearGradient>
                <View style={style.center_messageRecive}>
                {(!this.state.loading) &&
                    <FlatList
                        numColumns={3}
                        data={this.state.ListSampleWork}
                        renderItem={this.renderItemListSampleWork}
                        keyExtractor={(item, index) => index.toString()}
                        ListEmptyComponent={this.ListEmptyComponentSample}
                    />
                }
                {(this.state.loading) &&
                <View style={style.viewNotFound}>
                    <ActivityIndicator  color={this.props.global.grColorTwo} size='large' />
                </View>
                }
                </View>

            </View>
        );
    }
}

const mapStateToProps = (state) => {
    return {
        user: state.user,
        global: state.global
    };
};
export default connect(mapStateToProps, null)(sampleWork)
