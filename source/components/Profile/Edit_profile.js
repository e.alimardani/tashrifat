import React, {Component} from 'react';
import {
    Text,
    TouchableOpacity,
    Dimensions,
    View,
    Platform,
    BackHandler,
    Image, StatusBar,
    TouchableNativeFeedback, Alert,
} from 'react-native';
import {Container, Header, Left, Body, Right, Button, Title, Content, Item, Label, Input} from 'native-base';
import ServerData from '../connection/connect';
import style from '../../assets/styles/editProfile_css';
import Icon from 'react-native-vector-icons/FontAwesome5';
import LinearGradient from "react-native-linear-gradient";
import EditUserInfo from './edit_infoUser_profile'
import EditSkillProfile from '../User/Edit_skill_profile'
import {connect} from 'react-redux'
import {Actions} from "react-native-router-flux";
import {showImagePicker} from 'react-native-image-picker';
import {setUser} from "../../Redux/Actions";
import EditSample from './EditSample';

const width = Dimensions.get('window').width;
const height = Dimensions.get('window').height;
const Connect = new ServerData();

const options = {
    quality: 1.0,
    maxWidth: width * .80,
    maxHeight: height * .30,
    title: 'انتخاب عکس',
    takePhotoButtonTitle: 'دوربین',
    chooseFromLibraryButtonTitle: 'انتخاب عکس از گالری',
    cancelButtonTitle: 'لغو کردن',
    storageOptions: {
        skipBackup: true,
        path: 'image',
    }
};

class Edit_profile extends Component {

    constructor(props) {
        super(props);
        this.state = {
            setColor: 0,
            txt_wrongUserName: '',
            flex_wrongUserName: 'none',
            txt_wrongPhone: '',
            flex_wrongPhone: 'none',
            EditUserInfo_display: 'flex',
            EditUserSkill_display: 'none',
            EditSampleUser_display:'none',
            imageAvatar: this.props.user.Avatar,
            dataAvatar: '',
            nameImageAvatar: '',
            test:''
        };

        this.openImagePicker = this.openImagePicker.bind(this);
        this.uploadAvatar = this.uploadAvatar.bind(this);
    };

  

    infoUser() {
        this.setState({setColor: 0, EditUserInfo_display: 'flex', EditUserSkill_display: 'none',EditSampleUser_display:'none'});
    }

    skillUser() {
        this.setState({setColor: 1, EditUserInfo_display: 'none', EditUserSkill_display: 'flex',EditSampleUser_display:'none'});
    }
   sampleUser() {
        this.setState({setColor: 2, EditUserInfo_display: 'none', EditUserSkill_display: 'none',EditSampleUser_display:'flex'});
    }

    render() {
        const user = this.props.user;
        return (
            <Container style={{alignItems:'center'}}>

                <LinearGradient style={[style.view_header_prof]} colors={['#627efb', '#8b68fc', '#a95bfe']}>

                    <Header transparent style={style.view_top_header}>
                        <StatusBar backgroundColor="transparent" translucent={true} barStyle="light-content"/>
                        <Left style={{flex: 1,}}>
                            <Button transparent onPress={() => Actions.pop()}>
                                <Icon name="arrow-left" color={"#ffffff"} size={width * .07}/>
                            </Button>
                        </Left>
                        <Body style={{flex: 1, alignItems: 'center'}}>
                            <Title style={style.txt_top_header}>{'ویرایش پروفایل'}</Title>
                        </Body>
                        <Right style={{flex: 1}}>
                            <Button transparent onPress={() => Actions.drawerOpen()}>
                                <Icon name="bars" color={"#fff"} size={width * .07}/>
                            </Button>
                        </Right>
                    </Header>
                    <TouchableOpacity activeOpacity={0.7} style={style.view_center_headerEditProfile}
                                      onPress={this.openImagePicker}>
                        <View style={style.img_user_profEditProfile}>
                            <Image source={{uri: this.state.imageAvatar}} style={style.pic_member}
                                   resizeMode={'cover'}/>
                        </View>
                    </TouchableOpacity>
                </LinearGradient>
                <Content style={{width:"100%"}}>
                <View style={style.container_menu_profile}>
                    <View style={style.view_menu_top}>
                    <TouchableNativeFeedback background={TouchableNativeFeedback.Ripple('#8b68fc70')}
                                                 onPress={() => this.sampleUser()}>
                            <View style={this.state.setColor === 2 ? style.tabMenu_focus : style.tabMenu}>
                                <Text
                                    style={[style.txt_tabMenu, {color: this.state.setColor === 2 ? '#8b68fc' : '#CAC9CC'}]}>
                                    نمونه کار</Text>
                            </View>
                        </TouchableNativeFeedback>
                        <TouchableNativeFeedback background={TouchableNativeFeedback.Ripple('#8b68fc70')}
                                                 onPress={() => this.skillUser()}>
                            <View style={this.state.setColor === 1 ? style.tabMenu_focus : style.tabMenu}>
                                <Text
                                    style={[style.txt_tabMenu, {color: this.state.setColor === 1 ? '#8b68fc' : '#CAC9CC'}]}>تخصص
                                    ها</Text>
                            </View>
                        </TouchableNativeFeedback>
                        <TouchableNativeFeedback background={TouchableNativeFeedback.Ripple('#8b68fc70')}
                                                 onPress={() => this.infoUser()}>
                            <View style={this.state.setColor === 0 ? style.tabMenu1_focus : style.tabMenu1}>
                                <Text
                                    style={[style.txt_tabMenu, {color: this.state.setColor === 0 ? '#8b68fc' : '#CAC9CC'}]}>مشخصات</Text>
                            </View>
                        </TouchableNativeFeedback>
                    </View>

                    <View style={{display: this.state.EditUserInfo_display}}>
                        <EditUserInfo/>
                    </View>
                    <View style={{display: this.state.EditUserSkill_display}}>
                        <EditSkillProfile/>
                    </View>
                    <View style={{display: this.state.EditSampleUser_display}}>
                        <EditSample/>
                    </View>
                </View>
                </Content>
            </Container>
        );

    }

    async openImagePicker() {
        showImagePicker(options, (response) => {
                if (response.didCancel) {
                    console.log('User cancelled image picker');
                } else if (response.error) {
                    console.log('ImagePicker Error: ', response.error);
                } else {
                    let im1 = new Date().getTime() + '.png';

                    const source = {uri: response.uri};

                    this.setState({
                        imageAvatar: response.uri,
                        dataAvatar: response.data,
                        nameImageAvatar: im1,
                    });
                   
                    this.uploadAvatar(response.uri, response.path, response.fileName)
                }
            }
        );
    }

    async uploadAvatar(source, path, fileName) {
     
        let formData = new FormData();
        formData.append("admintoken", this.props.global.adminToken);
        formData.append("usertoken", this.props.user.apiToken);
        formData.append("image", source);
        if (source !== null) {
            if (Platform.OS === 'ios') {
                formData.append("image", {uri: "file://" + source, name: fileName, type: 'image/jpg'});
            } else {
                formData.append("image", {uri: "file://" + path, name: '1.jpg', type: 'image/jpg'});
            }
        }

        let response = await fetch(this.props.global.baseApiUrl + '/user/upload_user_avatar',
            {
                headers: {
                    'Accept': 'application/json',
                    'Content-Type': 'multipart/form-data'
                },
                method: "POST",
                body: formData
            });
        let json = await  response.json();
        console.log(json)
       await this.props.setUser({

           "Avatar":json.url,
           "instegram": this.props.user.Instagram,
           "telegram": this.props.user.Telegram,
           "whatsapp": this.props.user.whatsApp,
           "Groups_count":this.props.user.Groups_count,
           "apiToken":this.props.user.apiToken,
           "email":this.props.user.email,
           "friends_count":this.props.user.friends_count,
           "full_name":this.props.user.full_name,
           "id":this.props.user.id,
           "phone_number":this.props.user.phone_number,
           "username":this.props.user.username,
           "work_in":this.props.user.work_in,
           "Genre":this.props.user.Genre
           
        })
    }
}

const mapDispatchToProps = dispatch => {
    return {
        setUser: user => {
            dispatch(setUser(user))
        }
    }
};
const mapStateToProps = (state) => {
    return {
        user: state.user,
        global: state.global
    };
};
export default connect(mapStateToProps, mapDispatchToProps)(Edit_profile)


