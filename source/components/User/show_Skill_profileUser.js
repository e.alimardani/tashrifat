import React, {Component} from 'react';
import {
    Text,
    TouchableOpacity,
    Dimensions,
    View,
    ActivityIndicator,
    ScrollView, FlatList
} from 'react-native';
import Icon from 'react-native-vector-icons/FontAwesome';
import style from '../../assets/styles/profile_css'
import {Spinner} from "native-base";
import { connect } from 'react-redux';

const width = Dimensions.get('window').width;
const height = Dimensions.get('window').height;

class show_Skill_profileUser extends Component {

    constructor(props) {
        super(props);
        this.state = {
            loading: true,
            skill:''
        };


        this.renderItemSkill = this.renderItemSkill.bind(this);
        this.EmptySkill = this.EmptySkill.bind(this);

    };
    async componentWillMount() {
        setTimeout(()=>{
             this.setState({skill: this.props.Specialty,loading:false})
        },1500)
    }

    renderItemSkill = ({item, index}) => {
        if(item.length !== 0) {
            return (
                <View>
                    <View style={style.container}>
                        <Text style={style.txt_takhasos}>{item.value}</Text>
                        <Icon name="check-square-o" color={"#636363"} size={width * .05}
                              style={{marginRight: width * .02}}/>
                    </View>

                </View>
            )
        } 
    };
    EmptySkill(){
        if(this.state.loading)
        {
            return(
                <ActivityIndicator  color={this.props.global.grColorTwo} size='large' />
            );
        }
        else
        {
            return(
             <View style={[style.viewNotFound,{width: width*.80,height: height * .30}]}>
                <Text style={style.txtNotFound}>تخصصی وجود
                    ندارد</Text>
            </View>
            );
        } 
    }
    render() {
        const Specialty = this.state.skill;

        return (
            <View style={style.view_bottom_screen1}>
                <View style={[style.view_menu3, {flexDirection: 'row'}]}>
                {(!this.state.loading) &&
                    <FlatList
                        data={Specialty}
                        renderItem={this.renderItemSkill}
                        ListEmptyComponent={this.EmptySkill}
                        keyExtractor={(item, index) => index.toString()}
                    />
                       }
                {(this.state.loading) &&
                <View style={style.viewNotFound}>
                    <ActivityIndicator  color={this.props.global.grColorTwo} size='large' />
                </View>
                }
                </View>
            </View>
        );

    }

}

// const mapDispatchToPropsProfile = dispatch => {
//     return {
//         setSkillUser:
//             skillUser => {
//                 dispatch(setSkillUser(skillUser))
//             },
//     }
// };
const mapStateToPropsSkill = state => {
    return {
        user: state.user,
        global: state.global
    }
};
export default connect(mapStateToPropsSkill,null)(show_Skill_profileUser)



