import React, {Component} from 'react';
import {
    Text,
    TouchableOpacity,
    Dimensions,
    View,
    ActivityIndicator,
    ScrollView, FlatList,
    Alert
} from 'react-native';
import Icon from 'react-native-vector-icons/FontAwesome';
import ServerData from '../connection/connect';
import style from '../../assets/styles/profile_css'
import {connect} from "react-redux";
import {setSkillUser, setUser} from "../../Redux/Actions";
import {ProgressDialog} from "react-native-simple-dialogs";
import {Actions} from 'react-native-router-flux'
const width = Dimensions.get('window').width;
const height = Dimensions.get('window').height;
const Connect = new ServerData();

class show_Skill_profile extends Component {

    constructor(props) {
        super(props);
        this.state = {
            loading: false,
            skill:'',
            progressVisible:false
        };
        this.renderItem = this.renderItem.bind(this);
    };
    async GetDataSkillUser() {
        this.setState({Loading:true})
        console.log('sfsdfsd')
        try {
            const details1 = {
                "admintoken": this.props.global.adminToken,
                "usertoken": this.props.user.apiToken,
            };
            let result = await Connect.sendPost(this.props.global.baseApiUrl + '/user/getUser_Specialty', details1);
            console.log(result)
            if (result.success) {
                this.setState({skill: result.data,Loading:false})
            } else {
                this.setState({skill: result.data,progressVisible: false,Loading:false});
            }
        } catch (error) {

        }
     
    }
    async componentDidMount() {
       this.GetDataSkillUser()
    }
    async RemoveSkill(item){
        this.setState({progressVisible: true});
        const details = {
            "admintoken": this.props.global.adminToken,
            "usertoken": this.props.user.apiToken,
            "field_id": parseInt(item.field_id),
        };
       let result = await Connect.sendPost(this.props.global.baseApiUrl + '/user/Delete_user_speciality', details)
        
                console.log(result)
                if (result.success === true) {
                    this.GetDataSkillUser()
                    this.setState({progressVisible: false});
                    Alert.alert("بروزرسانی شد", 'تخصص مورد نظر حدف شد', [
                        {text: 'باشه'}
                    ]);
                } else {
                    this.setState({progressVisible: false});
                    Alert.alert("خطا", 'خطایی رخ داد', [
                        {text: 'باشه'}
                    ]);

                }
    }


    renderItem = ({item, index}) => {
            return (
                <View style={[style.container]}>
                    <Text style={[style.txt_takhasos,{flex:1}]}>{item.value}</Text>
                    <TouchableOpacity onPress={()=>this.RemoveSkill(item)} >
                        <Icon name="trash" color={"#636363"} size={width * .06} style={{marginRight: width * .02, color:'red'}}/>
                    </TouchableOpacity>
                </View>
            );
    };
    render() {
        return (
            <View style={style.view_bottom_screen1}>
            <ProgressDialog
                    visible={this.state.progressVisible}
                    message="لطفا منتظر بمانید..."
                />
                <View style={[style.view_menu3, {flexDirection: 'row'}]}>
                {(!this.state.loading && this.state.skill.length == 0) &&
                <View style={[style.viewNotFound,{width: width*.80,height:width*.40}]}>
                   <Text style={style.txtNotFound}>تخصصی وجود
                       ندارد</Text>
               </View>
                }
                {(!this.state.loading) &&
                    <FlatList
                        data={this.state.skill}
                        renderItem={this.renderItem}
                        keyExtractor={(item, index) => index.toString()}
                    />
                  }
                {(this.state.loading) &&
                <View style={[style.viewNotFound,{width: width*.80}]}>
                    <ActivityIndicator  color={this.props.global.grColorTwo} size='large' />
                </View>
                }
                </View>
            </View>
        );

    }


}

const mapDispatchToPropsProfile = dispatch => {
    return {
        setSkillUser:
            skillUser => {
                dispatch(setSkillUser(skillUser))
            },
    }
};
const mapStateToPropsSkill = state => {
    return {
        user: state.user,
        global: state.global
    }
};
export default connect(mapStateToPropsSkill, mapDispatchToPropsProfile)(show_Skill_profile)



