import React, {Component} from 'react';
import {
    Dimensions,
    View,
    Text, Platform, FlatList,
    Image, StatusBar, TouchableOpacity, TextInput, KeyboardAvoidingView,ActivityIndicator,Alert
} from 'react-native';
import AsyncStorage from '@react-native-community/async-storage';
import {Container, Header, Left, Body, Right, Button, Title, Spinner} from 'native-base';
import ServerData from '../../connection/connect';
import style from './Style';
import LinearGradient from "react-native-linear-gradient";
import Icon from "react-native-vector-icons/FontAwesome5";
import {connect} from "react-redux";
import {Actions} from "react-native-router-flux";
import Helpers from '../../Shared/Helper';
import Modal from "react-native-modal";
import {ProgressDialog} from 'react-native-simple-dialogs';

const width = Dimensions.get('window').width;
const height = Dimensions.get('window').height;
const Connect = new ServerData();

class MessageInBox extends Component {

    constructor(props) {
        super(props);
        this.state = {
            messageInbox: [],
            ModalDeleteMessage:false,
            idMessage:'',
            loading:true,
            progressVisible:false,
        }
        this.renderItem = this.renderItem.bind(this)
        this.Empty = this.Empty.bind(this)

    };


   async GetMessage() {
        const details = {
            "admintoken": this.props.global.adminToken,
            "usertoken": this.props.user.apiToken,
        };
       let result = await Connect.sendPost(this.props.global.baseApiUrl + '/user/Inbox', details)
       console.log(result)
                if (result.success == true) {
                    this.setState({messageInbox: result.data,loading:false});
                } else {
                    this.setState({messageInbox: [],loading:false});
                }
    }

    componentWillMount() {
        this.GetMessage();
    }
    async deleteMessage(){
        this.setState({progressVisible:true})
            try {
                const details = {
                    "admintoken": this.props.global.adminToken,
                    "message_id": this.state.idMessage,
                };
                console.log(result)
               let result = await Connect.sendPost(this.props.global.baseApiUrl + '/user/Delete_Single_Message', details)
               if (result.success) {
                this.GetMessage()
                            this.setState({progressVisible:false,ModalDeleteMessage:false})
                            Alert.alert("", 'پیام حذف شد', [
                                {text: 'باشه'}
                            ]);
                        } else {
                            this.setState({progressVisible:false,ModalDeleteMessage:false})
                            Alert.alert("", 'مشکلی درحذف پیام رخ داد', [
                                {text: 'باشه'}
                            ]);
                        }
                        // console.warn(this.state.ListUsersFriend)
               } catch (error) {
                this.setState({progressVisible:false})
               }
        }
    renderItem({item, index}) {
        return (
        <TouchableOpacity onPress={()=>{this.setState({idMessage:item.message_id,dataMessage:item},()=>this.ReadMessage())}} style={style.message_container}>
            <View style={[style.txt_container,{flex:1,flexDirection:'row-reverse',alignItems:'center',justifyContent:'space-between'}]}>       
                    <Text style={[style.txt_message,{flex:1}]}>موضوع : {item.message}</Text>
                    <TouchableOpacity onPress={()=>this.setState({ModalDeleteMessage:true,idMessage:item.message_id})}>
                        <Icon style={{paddingRight:10}} name="trash" color='red' size={18}/>
                    </TouchableOpacity>
            </View>
            {item.sender_avatar =="" && <Image source={require('../../../assets/images/userIconLogin.png')} style={style.pic_member} resizeMode={'cover'}/>}
            {item.sender_avatar !="" && <Image source={{uri: item.sender_avatar}} style={style.pic_member} resizeMode={'cover'}/>}
            
        </TouchableOpacity>
            )
    }
    async ReadMessage(){
        Actions.push('ListUserInBox',{data:this.state.dataMessage})
        try {
            const details = {
                "admintoken": this.props.global.adminToken,
                "message_id": this.state.idMessage,
            };
           let result = await Connect.sendPost(this.props.global.baseApiUrl + '/user/read_message', details)
           console.log(result)
           } catch (error) {
               console.log(error)
           }
    }
    Empty(){
        if(this.state.loading)
        {
            return(
                <ActivityIndicator  color={this.props.global.grColorTwo} size='large' />
            );
        }
        else
        {
            return(
             <View style={{width:'100%',position:'absolute',top:5}}>
                <Text style={[style.txtNotFound,{textAlign:'center'}]}> پیامی وجود
                    ندارد</Text>
            </View>
            );
        }   
    }

    render() {
        let behavior = '';
        if (Platform.OS == 'ios') {
            behavior = 'padding'
        }
        return (
            <View style={style.view_container_messageRecive}>
            <Modal isVisible={this.state.ModalDeleteMessage}
                       backdropColor={'#000000'}
                       backdropOpacity={0.70}
                       onBackdropPress={() => this.setState({ModalDeleteMessage: !this.state.ModalDeleteMessage})}
                       onBackButtonPress={() => this.setState({ModalDeleteMessage: !this.state.ModalDeleteMessage})}
                       style={style.ModalSearchDriver}
                >
                    <View style={style.viewContainerSupport}>
                        <View style={[style.view_top_support, {height: height * .08}]}>
                            <Text style={[style.txt_top_support,{fontFamily:'IRANSansMobile'}]}>حذف پیام</Text>
                        </View>
                        <View style={[style.view_top_support, {height: height * .09}]}>
                            <Text style={[style.txt_center_support,{fontFamily:'IRANSansMobile'}]}>پیام حذف شود ؟</Text>
                        </View>
                        <View style={[style.view_bottom_support]}>
                            <TouchableOpacity activeOpacity={.7} onPress={() => this.setState({ModalDeleteMessage: false})}>
                                <View style={style.btnSupport1}>
                                    <Text style={[style.txt_bottom_support1,{fontFamily:'IRANSansMobile'}]}>خیر</Text>
                                </View>
                            </TouchableOpacity>
                            <TouchableOpacity activeOpacity={.7} onPress={() => this.deleteMessage()}>
                                <View style={style.btnSupport}>
                                    <Text style={[style.txt_bottom_support,{fontFamily:'IRANSansMobile'}]}>بله</Text>
                                </View>
                            </TouchableOpacity>
                        </View>
                    </View>
                </Modal>
                <View style={style.view_top}>
                    <ProgressDialog visible={this.state.progressVisible} message="لطفا منتظر بمانید..."/>
                    <LinearGradient style={[style.view_header_user]} start={{x: 0, y: 0}} end={{x: 1, y: 0}}
                                    colors={['#627efb', '#8b68fc', '#a95bfe']}>

                        <Header transparent style={style.view_top_header}>
                            <StatusBar backgroundColor="transparent" translucent={true} barStyle="light-content"/>

                            <Left style={{flex: 1,}}>
                                <Button transparent>
                                    <TouchableOpacity activeOpacity={.5} onPress={() => Actions.pop()}>
                                        <Icon name="arrow-left" color={"#ffffff"} size={width * .07}/>
                                    </TouchableOpacity>

                                </Button>
                            </Left>
                            <Body style={{flex: 1, alignItems: 'center'}}>
                                <Title style={style.txt_top_header}>پیام های دریافتی</Title>
                            </Body>
                            <Right style={{flex: 1}}>
                                    <Button transparent onPress={()=> Actions.drawerOpen()}>
                                        <Icon name="bars" color={"#fff"} size={width * .07}/>
                                    </Button>
                            </Right>
                        </Header>
                    </LinearGradient>
                </View>
                <View style={style.center_messageRecive}>
                    <FlatList
                        data={this.state.messageInbox}
                        contentContainerStyle={{flexGrow: 1}}
                        renderItem={this.renderItem}
                        keyExtractor={(item, index) => index.toString()}
                        ListEmptyComponent={this.Empty}
                    />
                </View>
            </View>
        );

    }
}
const mapStateToProps = (state) => {
    return {
        user: state.user,
        global: state.global
    };
};
export default connect(mapStateToProps, null)(MessageInBox)

