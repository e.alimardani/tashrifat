import React from 'react';
import {StyleSheet, Dimensions, Platform} from 'react-native';


const width = Dimensions.get('window').width;
const height = Dimensions.get('window').height;

export default StyleSheet.create({
view_container_messageRecive: {
    flex: 1,
    // backgroundColor: '#000'
},
view_top: {justifyContent: 'center', alignItems: 'center'},
view_header_user: {
    height: height / 7,
    width: width / 1,
    // backgroundColor:'#000'
},
view_top_header: {
    marginTop: height * .01,
    paddingHorizontal: width * .05,
    width: width,
},
txt_top_header: {
    color: '#fff',
    fontFamily: 'IRANSansMobile',
    textAlign: 'center',
    fontSize: width / 25
},
center_messageRecive: {flex: 1,},
message_container: {
    flex: 1,
    flexDirection: 'row',
    marginLeft:10,
    marginVertical:5
},
txt_message:{fontFamily: 'IRANSansMobile',
fontSize: width / 28,},
txt_container: {
    flex:1,
    backgroundColor: '#fff',
    alignSelf:'center',
    borderRadius: width * .03,
    padding:10,
    marginLeft:10,
},
view_txt_message:{paddingVertical: height*.03},
date_txt:{
    alignSelf:'flex-start'
},
pic_member: {
    width: width * .15,
    height: width * .15,
    borderRadius: width * .07,
    marginHorizontal:width*.02,
    marginVertical: height*.01

},
ModalSearchDriver: {
    alignItems: 'center',
    justifyContent: 'flex-end',
    margin: 0,
    padding: 0,
    borderTopLeftRadius: width * .15,
    borderTopRightRadius: width * .15,
},
viewContainerSupport: {
    backgroundColor: "#fff",
    borderRadius: width * .10,
    width: width * .80,
    height: height * .27,
    marginBottom: width * .60,
    alignItems: 'center'
},
view_top_support: {
    width: width * .80,
    justifyContent: 'center',
    alignItems: 'center'
},
txt_top_support: {fontSize: width / 25, color: '#2089ff', fontFamily: 'IRANSansMobile'},
txt_center_support: {fontSize: width / 22, color: '#000', fontFamily: 'IRANSansMobile'},
txt_bottom_support: {fontSize: width / 22, color: '#fff', fontFamily: 'IRANSansMobile'},
txt_bottom_support1: {fontSize: width / 22, color: '#2089ff', fontFamily: 'IRANSansMobile'},
view_bottom_support: {
    width: width * .60,
    justifyContent: 'space-around',
    alignItems: 'center',
    height: height * .10,
    flexDirection: 'row'
},
btnSupport1: {
    justifyContent: 'center',
    alignItems: 'center',
    width: width * .25,
    height: height * .05,
    backgroundColor: '#f0f6fc',
    borderRadius: width * .05,
    elevation: 1
},
btnSupport: {
    justifyContent: 'center',
    alignItems: 'center',
    width: width * .25,
    height: height * .05,
    backgroundColor: '#2089ff',
    borderRadius: width * .05,
    elevation: 1
},
view_top: {justifyContent: 'center', alignItems: 'center'},
view_header_user: {
    height: height / 6,
    width: width / 1,
},
view_top_header: {
    marginTop: height * .01,
    paddingHorizontal: width * .05,
    width: width,
},
view_user: {
    flexDirection: 'row',
    width: width * .90,
    height: height * .12,
    paddingHorizontal:width*.03,
    marginTop: height * .02,
    backgroundColor:'#fff',
    justifyContent:'center',
    alignItems:'center',
    borderRadius:width*.10
},
view_user_txt: {flex: 1, justifyContent: 'center', alignItems: 'flex-end', marginRight: width * .02},
    txt_name_user: {fontSize: width / 27, fontFamily: 'IRANSansMobile',},
    view_img: {justifyContent: 'flex-end', alignItems: 'center',borderWidth: width*.01,borderRadius:width*.07,borderColor: '#d1d1d1'},
    pic_user: {
        width: width * .12,
        height: width * .12,
        borderRadius: width * .06,
        // marginTop: height * .08
    },
    viewNotFound:{width: width * .90, height: height * .60, justifyContent: 'center', alignItems: 'center',},
    txtNotFound:{fontSize: width / 20, color: '#000', fontFamily: 'IRANSansMobile',textAlign:'center'},
    view_container_user: {
        alignItems: 'center',
        flex: 1
    },
    view_top: {justifyContent: 'center', alignItems: 'center'},
    view_header_user: {
        height: height / 6,
        width: width / 1,
    },
    view_top_header: {
        marginTop: height * .01,
        paddingHorizontal: width * .05,
        width: width,
    },
   txt_top_header: {
        // flex:1,
        color: '#fff',
        fontFamily: 'IRANSansMobile',
        textAlign: 'center',
        fontSize: width / 25
    },
    center_page_users: {
        marginTop:10,
        flexDirection:'row',
        alignItems:'center'
    },
   
});