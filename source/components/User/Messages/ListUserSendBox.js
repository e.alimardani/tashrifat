import React, {Component} from 'react';
import {
    Text,
    TouchableOpacity,
    Dimensions,
    View,
    ScrollView,
    BackHandler,
    Image, StatusBar,
    ActivityIndicator, Alert, TextInput, FlatList
} from 'react-native';
import {Spinner, Header, Left, Body, Right, Button, Title} from 'native-base';
import ServerData from '../../connection/connect';
import style from './Style';
import BottomNavigation from '../../Shared/bottomNavigation';
import Icon from 'react-native-vector-icons/FontAwesome5';
import LinearGradient from "react-native-linear-gradient";
import ModalDropdown from 'react-native-modal-dropdown';
import {Actions} from "react-native-router-flux";
import {setUser, setUsers} from "../../../Redux/Actions";
import {connect} from "react-redux";
import Helper from '../../Shared/Helper'
import moment from 'jalali-moment';

const width = Dimensions.get('window').width;
const height = Dimensions.get('window').height;
const Connect = new ServerData();

class ListUserSendBox extends Component {

    constructor(props) {
        super(props);
        this.state = {
            userMessageInBox: [],
            loading:true
        };
    };
    componentWillMount() {
        console.log(this.props.data)
    }


    render() {
        return (
            <View style={style.view_container_user}>
                <View style={style.view_top}>
                    <LinearGradient style={[style.view_header_user]} start={{x: 0, y: 0}} end={{x: 1, y: 0}}
                                    colors={['#627efb', '#8b68fc', '#a95bfe']}>

                        <Header transparent style={style.view_top_header}>
                            <StatusBar backgroundColor="transparent" translucent={true} barStyle="light-content"/>

                            <Left style={{flex: 1,}}>
                                <Button transparent onPress={()=>Actions.pop()}>
                                    <Icon name="arrow-left" color={"#ffffff"} size={width * .07}/>
                                </Button>
                            </Left>
                            <Body style={{flex: 1, alignItems: 'center'}}>
                                <Title style={style.txt_top_header}>پیام</Title>
                            </Body>
                            <Right style={{flex: 1}}>
                                    <Button transparent onPress={()=>Actions.drawerOpen()}>
                                        <Icon name="bars" color={"#fff"} size={width * .07}/>
                                    </Button>
                            </Right>
                        </Header>
                    </LinearGradient>

                </View>

                <View style={style.center_page_users}>
                    <View style={[style.txt_container,{flex:1}]}>       
                        <View>
                            <Text style={style.txt_message}>موضوع : {this.props.data.subject}</Text>
                        </View>
                        <View style={style.view_txt_message}>
                            <Text style={style.txt_message}>متن پیام : {this.props.data.message}</Text>
                        </View>
                        
                    </View>
                    {this.props.data.reciver_avatar =="" &&
                    <TouchableOpacity  onPress={()=>Actions.push('Profile_user',{ListUsers:this.props.data.user})}>
                        <Image source={require('../../../assets/images/userIconLogin.png')} style={style.pic_member} resizeMode={'cover'}/>
                    </TouchableOpacity>
                    }
                    {this.props.data.reciver_avatar !="" &&
                    <TouchableOpacity  onPress={()=>Actions.push('Profile_user',{ListUsers:this.props.data.user})}>
                        <Image source={{uri: this.props.data.reciver_avatar}} style={style.pic_member} resizeMode={'contain'}/>
                    </TouchableOpacity>}
                </View>
                <View>
                    <BottomNavigation navigation={this.props.navigation} color_members={'#fff'}/>
                </View>
            </View>
        );

    }
}
const mapDispatchToProps = dispatch => {
    return {
        setUsers: users => {
            dispatch(setUsers(users))
        }
    }
};
const mapStateToProps = (state) => {
    return {
        user: state.user,
        global: state.global
    };
};
export default connect(mapStateToProps, mapDispatchToProps)(ListUserSendBox)


