import React, {Component} from 'react';
import {
    Text,
    TouchableOpacity,
    Dimensions,
    View,
    // AsyncStorage,
    BackHandler,
    Image, StatusBar,
    ActivityIndicator,
    TouchableNativeFeedback, Alert, TextInput, FlatList
} from 'react-native';
import AsyncStorage from '@react-native-community/async-storage';
import {Spinner, Header, Left, Body, Right, Button, Title} from 'native-base';
import ServerData from '../connection/connect';
import style from '../../assets/styles/users_css';
import BottomNavigation from '../Shared/bottomNavigation';
import Icon from 'react-native-vector-icons/FontAwesome5';
import LinearGradient from "react-native-linear-gradient";
import {Actions} from "react-native-router-flux";
import {setUser} from "../../Redux/Actions";
import {connect} from "react-redux";


const width = Dimensions.get('window').width;
const height = Dimensions.get('window').height;
const Connect = new ServerData();

class RequestsFollowList extends Component {

    constructor(props) {
        super(props);
        this.state = {
            search_users: '',
            loading: true,
            ListUsers: [],
       
        };
        this.renderItemListUsers = this.renderItemListUsers.bind(this);
        this.EmptyComponent = this.EmptyComponent.bind(this);
        this.GetDataRequestsFollowList = this.GetDataRequestsFollowList.bind(this)
    };

    async componentWillMount() {
      
            this.GetDataRequestsFollowList()
    }

  async GetDataRequestsFollowList() {
        try {
        const details = {
            "admintoken": this.props.global.adminToken,
            "usertoken": this.props.user.apiToken,
        };
      let result = await Connect.sendPost(this.props.global.baseApiUrl + '/user/get_FollowRequest_list', details)
            
                if (result.success == true) {
                        this.setState( {ListUsers:result.data ,
                                loading: false,
                            });
                    }else{
                        this.setState({loading: false,});
                    }       
        } catch (error) {
            this.setState({loading: false,});
        }
        
    }

    GoToProfileUser(index) {
        // Actions.push('Profile_user', {ListUsers: this.state.ListUsers[index]});
    }

   async FollowRequest(index,item) {
       alert(item.initiator_user_id)
        const details = {
            "admintoken": this.props.global.adminToken,
            "usertoken": this.props.user.apiToken,
            "target_user": item.initiator_user_id,
        };
       let result = await Connect.sendPost(this.props.global.baseApiUrl + '/user/Accept_FollowRequest', details)     
       if (result.success == true) {
                    // Alert.alert('', 'درخواست دوستی پذیرفته شد', [{text: 'باشه', onPress: () => this.GetDataRequestsFollowList()}]);
                    this.GetDataRequestsFollowList()
                } else {
                    this.GetDataRequestsFollowList()
                    // Alert.alert('', result.messages, [{text: 'باشه'}])
                }
    }

   async UnFollowRequest(index,item) {
        const details = {
            "admintoken": this.props.global.adminToken,
            "usertoken": this.props.user.apiToken,
            "target_id": item.initiator_user_id,
        };
       let result = await Connect.sendPost(this.props.global.baseApiUrl + '/user/CancelOrIngnore_FollowRequest', details)
                if (result.success == true) {
                    // Alert.alert('', result.data, [{text: 'باشه', onPress: () => this.GetDataRequestsFollowList()}])
                    this.GetDataRequestsFollowList()
                } else {
                    this.GetDataRequestsFollowList()
                    // Alert.alert('', result.messages, [{text: 'باشه'}])
                }
    }

    renderItemListUsers = ({item, index}) => {
            
                return (
                    <TouchableOpacity style={style.view_user} onPress={() => this.GoToProfileUser(index)}>
                        {this.props.user.apiToken !== null && 
                        <View style={style.view_user_icon}>
                            <TouchableOpacity activeOpacity={.5} onPress={() => this.FollowRequest(index,item)}>
                                <LinearGradient style={style.view_folow} start={{x: 0, y: 0}} end={{x: 1, y: 0}}
                                                colors={['#5e38c2', '#4c26b3']}>
                                    <Icon size={width * .05} color={'#fff'} name={'user-plus'}/>
                                </LinearGradient>
                            </TouchableOpacity>
                            {/* <Text style={style.txt_flow}>دنبال کردن</Text> */}
                        </View>
                        }
                        {this.props.user.apiToken !== null && 
                        <View style={style.view_user_icon}>
                            <TouchableOpacity activeOpacity={.5} onPress={() => this.UnFollowRequest(index,item)}>
                                <LinearGradient style={style.view_folow} start={{x: 0, y: 0}} end={{x: 1, y: 0}}
                                                colors={['#ff6f75', '#fe4f4b']}>
                                    <Icon size={width * .05} color={'#fff'} name={'user-minus'}/>
                                </LinearGradient>
                            </TouchableOpacity>
                            {/* <Text style={style.txt_flow}>لغو دنبال کردن</Text> */}
                        </View>
                        }

                        <View style={style.view_user_txt}>
                            <Text style={style.txt_name_user}>{item.initiator_firstname} </Text>
                        </View>
                        <View style={style.view_img}>
                            <Image source={{uri: item.initiator_avatar}} style={style.pic_user}/>
                        </View>

                    </TouchableOpacity>
                );
    };

    EmptyComponent() {
        if(this.state.loading)
        {
            return(
                <ActivityIndicator color={this.props.global.grColorTwo} size='large' />
            );
        }
        else
        {
            return(
             <View style={style.viewNotFound}>
                <Text style={style.txtNotFound}>درخواستی وجود
                    ندارد</Text>
            </View>
            );
        }   
    }

    render() {
        return (
            <View style={style.view_container_user}>

                <View style={style.view_top}>
                    <LinearGradient style={[style.view_header_user]} start={{x: 0, y: 0}} end={{x: 1, y: 0}}
                                    colors={['#627efb', '#8b68fc', '#a95bfe']}>

                        <Header transparent style={style.view_top_header}>
                            <StatusBar backgroundColor="transparent" translucent={true} barStyle="light-content"/>

                            <Left style={{flex: 1,}}>
                                <Button transparent onPress={() => Actions.pop()}>
                                    <Icon name="arrow-left" color={"#ffffff"} size={width * .07}/>
                                </Button>
                            </Left>
                            <Body style={{flex: 1, alignItems: 'center'}}>
                                <Title style={style.txt_top_header}>درخواست ها</Title>
                            </Body>
                            <Right style={{flex: 1}}>
                                <Button transparent onPress={() => Actions.drawerOpen()}>
                                    <Icon name="bars" color={"#fff"} size={width * .07}/>
                                </Button>
                            </Right>
                        </Header>
                        <View style={[style.view_search]}>
                            <View style={style.icon_search}>
                                <Icon name="search" color={'#e8e8e8'} size={width * .05}/>
                            </View>
                            <View style={style.input_search}>
                                <TextInput
                                    textAlign={'right'}
                                    placeholder="جستجو اعضا..."
                                    placeholderTextColor={"#e8e8e8"}
                                    style={style.textinput_search}
                                    onChangeText={(text) => this.setState({search_users: text})}
                                    // onChangeText={(text) => this.FilterUserBySearch(text)}
                                />
                            </View>
                            <TouchableOpacity style={style.modalFilter}
                                              onPress={() => Actions.push('Search1')}>
                                <Text style={style.modalTextStyle}>جستجو پیشرفته</Text>
                            </TouchableOpacity>
                        </View>
                    </LinearGradient>

                </View>

                <View style={style.center_page_users}>
                  
                 {(!this.state.loading)  && 
                    <FlatList keyExtractor={(item, index) => index.toString()}
                              data={this.state.ListUsers
                                  // .filter(item => item.first_name.includes(this.state.search_users))
                              }
                              renderItem={this.renderItemListUsers}
                              ListEmptyComponent={this.EmptyComponent}
                    />
                 }
                 {(this.state.loading) &&
                     <View style={{ height : Dimensions.get('window').height * .2  , justifyContent:'center',alignItems:'center' }}>
                        <ActivityIndicator color={this.props.global.grColorTwo} size='large'/>
                    </View>}
                </View>
                <View>
                    <BottomNavigation styleBottom={'other'} color_members={'#fff'}/>
                </View>
            </View>
        );

    }
}

const mapDispatchToProps = dispatch => {
    return {
        setUser: user => {
            dispatch(setUser(user))
        }
    }
};
const mapStateToProps = (state) => {
    return {
        user: state.user,
        global: state.global
    };
};
export default connect(mapStateToProps, mapDispatchToProps)(RequestsFollowList)

