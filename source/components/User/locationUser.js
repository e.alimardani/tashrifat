    import React, {Component} from 'react';
    import {
        Dimensions,
        View,
         Platform, Alert,
         Text, StatusBar, TouchableOpacity, StyleSheet, KeyboardAvoidingView,
         ActivityIndicator
    } from 'react-native';
    import AsyncStorage from '@react-native-community/async-storage';
    import {Container, Header, Left, Body, Right, Button, Title, Fab} from 'native-base';
    import ServerData from '../connection/connect';
    import style from './Style';
    import LinearGradient from "react-native-linear-gradient";
    import Icon from "react-native-vector-icons/FontAwesome5";
    import MapView,{ Marker, ProviderPropType } from 'react-native-maps';
    import {connect} from "react-redux";
    import {Actions} from "react-native-router-flux";
    import {ProgressDialog} from 'react-native-simple-dialogs';
    
    const width = Dimensions.get('window').width;
    const height = Dimensions.get('window').height;
    const Connect = new ServerData();
    
    class locationUser extends Component {
    
        constructor(props) {
            super(props);
            this.state = {
                progressVisible:false,
                latitude: 1,
                longitude: 1,
                latitudeDelta: 0.0988,
                longitudeDelta: 0.0421,
                markers: [],
                Loading:true,
                message:'',
                Done:false
                
    
            }
        };
      async componentWillMount(){  
        console.log(this.props.IDForGetLocation)
       await this.getLocation()
       }
       async getLocation(){
        formData = new FormData();
        formData.append("admintoken", this.props.global.adminToken);
        formData.append("user_id", this.props.user.id);
    try {
        let response = await fetch(this.props.global.baseApiUrl + '/user/get_User_Location', {
            method: 'post',
            headers: {
                'Content-Type': 'multipart/form-data',
            },
            body: formData
        });
        let result = await response.json();
        if(result.success){
                await this.setState({
                    Loading:false,
                    Done:true,
                    latitude: result.location_ip1,
                    longitude: result.location_ip2,
                    latitudeDelta: 0.0988,
                    longitudeDelta: 0.0421,
                    city:result.city,
                    province:result.province
                });
                console.log(this.state.latitude + '   ' + this.state.longitude)
        }else{ 
            this.setState({Loading:false,message:result.message,Done:false})
        }
        }
    catch (error) {}
}
     
        render() {
           
            return (
                <View style={style.view_container_messageRecive}>
                <ProgressDialog visible={this.state.progressVisible} message="لطفا منتظر بمانید..."/>
                    {(this.props.IDForGetLocation.city != null && this.props.IDForGetLocation.province != null) &&
                    <View style={{flex:1,width: width * .80,
                            flexDirection: 'row',
                            alignItems: 'center',
                            justifyContent: 'center'}}>
                            <View style={{ height: height * .39,
                            width: width * .80,
                            marginTop:width*(-.06),
                            borderBottomRightRadius: width * .02,
                            borderBottomLeftRadius: width * .02,
                            justifyContent: 'center',
                            alignItems: 'center'}}>
                            <View style={{borderRadius:15,borderWidth:1,padding:10,width:'70%',marginBottom:10,borderColor:'#8b68fc',alignItems:'center',justifyContent:'center'}}>
                                <Text style={{fontFamily:'IRANSansMobile',color:'black',fontSize:14,textAlign:'center'}}>{"شهر : "}{this.props.IDForGetLocation.city}</Text>
                            </View>
                            <View style={{borderRadius:15,borderWidth:1,padding:10,width:'70%',borderColor:'#8b68fc',alignItems:'center',justifyContent:'center'}}>
                                <Text style={{fontFamily:'IRANSansMobile',color:'black',fontSize:14,textAlign:'center'}}>{"استان : "}{this.props.IDForGetLocation.province}</Text>
                            </View>
                            {/* <MapView
                                provider={this.props.provider}
                                style={styles.map}
                                initialRegion={{
                                    latitude:parseFloat(this.state.latitude),
                                    longitude: parseFloat(this.state.longitude),
                                    latitudeDelta: 0.0988,
                                     longitudeDelta: 0.0421,
                                }}>
                                <Marker  coordinate={{
                                    latitude:parseFloat(this.state.latitude),
                                    longitude: parseFloat(this.state.longitude),
                                }}/>
                            </MapView> */}
                        </View>
                    </View>
                    }
                     {/* {this.state.Loading && <ActivityIndicator size="large" />
                    
                    } */}
                    {(this.props.IDForGetLocation.city == null && this.props.IDForGetLocation.province == null) &&
                    
                     <Text style={{fontFamily:'IRANSansMobile',color:'black',marginTop:10,fontSize:14}}>موقعیتی ثبت نشده</Text>
                     } 
                </View>
            );
    
        }
    }
    const styles = StyleSheet.create({
        container: {
          ...StyleSheet.absoluteFillObject,
          justifyContent: 'flex-end',
          alignItems: 'center',
        },
        map: {
          ...StyleSheet.absoluteFillObject,
        },
        bubble: {
          backgroundColor: 'rgba(255,255,255,0.7)',
          paddingHorizontal: 18,
          paddingVertical: 12,
          borderRadius: 20,
        },
        latlng: {
          width: 200,
          alignItems: 'stretch',
        },
        button: {
          width: 80,
          paddingHorizontal: 12,
          alignItems: 'center',
          marginHorizontal: 10,
        },
        buttonContainer: {
          flexDirection: 'row',
          marginVertical: 20,
          backgroundColor: 'transparent',
        },
      });
    const mapStateToProps = (state) => {
        return {
            user: state.user,
            global: state.global
        };
    };
    export default connect(mapStateToProps, null)(locationUser)
    