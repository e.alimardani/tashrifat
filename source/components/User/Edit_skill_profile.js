import React, {Component} from 'react';
import {
    Text,
    TouchableOpacity,
    Dimensions, View, FlatList, Alert,
} from 'react-native';
import ServerData from '../connection/connect';
import style from '../../assets/styles/editProfile_css';
import {Content, Form, Input, Item, Label, Spinner} from "native-base";
import Icon from "react-native-vector-icons/FontAwesome5";
import {ProgressDialog} from "react-native-simple-dialogs";
import {connect} from "react-redux";
import Modal from "react-native-modal";

const width = Dimensions.get('window').width;
const height = Dimensions.get('window').height
const Connect = new ServerData();

class Edit_skill_profile extends Component {

    constructor(props) {
        super(props);
        this.state = {
            SkillAndUpdate: '',
            OptionArray: [],
            workSkill:'',
            idText:'',
            progressVisible: false,
            ModalEditSkillState: false,
            OptionArrayItem:[],
            ModalEditSkillStateItem:false
        };

        this.UpdateSkill = this.UpdateSkill.bind(this);
        this.renderOptionSkill123Item = this.renderOptionSkill123Item.bind(this);
        this.renderOptionSkill = this.renderOptionSkill.bind(this);
        this.renderUpdateSkill = this.renderUpdateSkill.bind(this);
        this.GetDataSkillAndUpdate = this.GetDataSkillAndUpdate.bind(this);
        this.renderOptionSkill123 = this.renderOptionSkill123.bind(this);

    };

 


    componentWillMount() {
        this.GetDataSkillAndUpdate();
    }

    GetDataSkillAndUpdate() {
        const details = {
            "admintoken": this.props.global.adminToken,
        };
        Connect.sendPost(this.props.global.baseApiUrl + '/user/getSpeciality_By_Category', details)
            .then((result) => {
                console.log(result.data)
                if (result.success == true) {

                    this.setState({SkillAndUpdate: result.data});

                } else {

                }
            });
    }

    btn_updateSkill() {
        this.setState({progressVisible: true});
        const details = {
            "admintoken": this.props.global.adminToken,
            "usertoken": this.props.user.apiToken,
            "field_id": this.state.idText,
            "value": this.state.workSkill,
        };
        Connect.sendPost(this.props.global.baseApiUrl + '/user/Insert_User_Profile_Data', details)
            .then((result) => {
                this.setState({progressVisible: false});

                if (result.success == true) {

                    Alert.alert('', result.data, [{text: 'باشه'}])

                } else {
                    Alert.alert('', result.messages, [{text: 'باشه'}])

                }
            });

    };

   async UpdateSkill({item,index}){
    this.setState({progressVisible: true});
        const details = {
            "admintoken": this.props.global.adminToken,
            "usertoken": this.props.user.apiToken,
            "field_id":item.Sub_sub_Category[index].id,
            "value": item.Sub_sub_Category[index].name,
        };
        Connect.sendPost(this.props.global.baseApiUrl + '/user/Insert_User_Profile_Data', details)
            .then((result) => {
                this.setState({progressVisible: false});
                console.log(result)
                if (result.success) {

                    Alert.alert('', result.data, [{text: 'باشه'}])

                } else {
                    Alert.alert('', result.message, [{text: 'باشه'}])

                }
            });
        // for (let i = 0; i < this.state.SkillAndUpdate.length; i++) {
        //     if (this.state.SkillAndUpdate[i].option !== null) {
        //         for (let j = 0; j < this.state.SkillAndUpdate[i].option.length; j++) {
        //             if (this.state.SkillAndUpdate[i].id === this.state.SkillAndUpdate[i].option[j].parent_id) {
        //                 if (this.state.SkillAndUpdate[i].option[j].name === item) {
        //                     // alert(this.state.SkillAndUpdate[i].option[j].id)
        //                     this.setState({progressVisible: true});
        //                     const details = {
        //                         "admintoken": this.props.global.adminToken,
        //                         "usertoken": this.props.user.apiToken,
        //                         "field_id": this.state.SkillAndUpdate[i].id,
        //                         "value": item,
        //                     };
        //                   let result = await Connect.sendPost(this.props.global.baseApiUrl + '/user/Insert_User_Profile_Data', details)
        //                     this.setState({progressVisible: false});
        //                     if (result.success === true) {
        //                         this.setState({ModalEditSkillState:false});
        //                         Alert.alert('', result.data, [{text: 'باشه'}])

        //                     } else {
        //                         this.setState({ModalEditSkillState:false});

        //                         Alert.alert('', result.messages, [{text: 'باشه'}])

        //                     }               
        //                 }
        //             }
        //         }
        //     }else {
        //         this.setState({idText:this.state.SkillAndUpdate.id})
        //     }
        // }
    };

    renderOptionSkill123({item,index}){
        
        console.log(item.SubCategory[index].name)
        return(
            <View>
            <TouchableOpacity style={style.itemOptionModal}
            // onPress={()=> index === index123? this.UpdateSkill(item):null}
            onPress={()=>this.renderOptionSkillItem(item.SubCategory[index])}
            >
                <Text style={style.txt_itemOptionModal}>{item.SubCategory[index].name}</Text>
            </TouchableOpacity>
                <View style={{borderBottomWidth:width*.001,borderBottomColor:'#c5c5c5'}}/>
            </View>
        )
    }

    renderOptionSkill123Item({item,index}){
        
        return(
            <View>
            <TouchableOpacity style={style.itemOptionModal}
            onPress={()=>this.UpdateSkill({item,index})}
            //onPress={()=>this.renderOptionSkillItem(item.Sub_sub_Category[index])}
            >
                <Text style={style.txt_itemOptionModal}>{item.Sub_sub_Category[index].name}</Text>
            </TouchableOpacity>
                <View style={{borderBottomWidth:width*.001,borderBottomColor:'#c5c5c5'}}/>
            </View>
        )
    }

    renderOptionSkill(item){
        this.setState({ModalEditSkillState:true});
        let optionItem =[];
        for (let j = 0; j < item.SubCategory.length; j++) {
            if (item.group_order == item.SubCategory[j].parent_id) {
                optionItem.push(item);
                console.log(item.SubCategory[j].name)
            }
    }
         this.setState({OptionArray:optionItem});
    }
    renderOptionSkillItem(item){
        this.setState({ModalEditSkillStateItem:true,ModalEditSkillState:false});
        console.log(item)
        let optionItem =[];
        for (let j = 0; j < item.Sub_sub_Category.length; j++) {
            if (item.group_id == item.Sub_sub_Category[j].group_id) {
                optionItem.push(item);
                console.log(item.Sub_sub_Category[j].name)
            }
    }
         this.setState({OptionArrayItem:optionItem});
    }

    renderUpdateSkill = ({item, index}) => {
        return (
            <View>
                <TouchableOpacity style={style.view_input_ItemEditSkill} onPress={()=>this.renderOptionSkill(item)}>
                    <Text style={style.username_inputSkill}>{item.name}</Text>
                </TouchableOpacity>
            </View>
        );
    }
    render() {
        return (
            <View>
                <Modal isVisible={this.state.ModalEditSkillState}
                    //    deviceWidth={width*.80}
                    //    deviceHeight={height * .64}
                    //    animationType={"slide"}
                    //    hasBackdrop={false}
                    //    coverScreen={false}
                    //    backdropColor={'#203fff'}
                       //backdropOpacity={0.80}
                    //    transparent={false}
                       onBackButtonPress={() => this.setState({ModalEditSkillState: !this.state.ModalEditSkillState})}
                       onBackdropPress={() => this.setState({ModalEditSkillState: !this.state.ModalEditSkillState})}
                       style={style.ModalEditSkillStyle}
                >
                    <View style={[style.viewModalEditSkillState]}>
                        <TouchableOpacity activeOpacity={.7} style={style.btnExitModal} onPress={()=>this.setState({ModalEditSkillState:false})}>
                            <Icon name="times-circle" color={'#fff'} size={width * .06}/>
                        </TouchableOpacity>
                        <FlatList
                            data={this.state.OptionArray}
                            renderItem={this.renderOptionSkill123}
                            keyExtractor={(item, index) => index.toString()}/>
                    </View>
                </Modal>
                <Modal isVisible={this.state.ModalEditSkillStateItem}
                       onBackButtonPress={() => this.setState({ModalEditSkillStateItem: !this.state.ModalEditSkillStateItem})}
                       onBackdropPress={() => this.setState({ModalEditSkillStateItem: !this.state.ModalEditSkillStateItem})}
                       style={style.ModalEditSkillStyle}
                >
                    <View style={[style.viewModalEditSkillState]}>
                        <TouchableOpacity activeOpacity={.7} style={style.btnExitModal} onPress={()=>this.setState({ModalEditSkillStateItem:false})}>
                            <Icon name="times-circle" color={'#fff'} size={width * .06}/>
                        </TouchableOpacity>
                        <FlatList
                            data={this.state.OptionArrayItem}
                            renderItem={this.renderOptionSkill123Item}
                            keyExtractor={(item, index) => index.toString()}/>
                    </View>
                </Modal>
            <Form style={style.view_container_form_signUp}>
                <ProgressDialog
                    visible={this.state.progressVisible}
                    message="لطفا منتظر بمانید..."
                />
                <Content>
                    <FlatList keyExtractor={(item) => item.id.toString()}
                              inverted
                              data={this.state.SkillAndUpdate}
                              renderItem={this.renderUpdateSkill}
                              ListEmptyComponent={() => <Spinner/>}
                    />
                </Content>
                {/*<TouchableOpacity activeOpacity={.5} onPress={() => this.btn_updateSkill()}>*/}
                {/*    <View style={style.view_btn_signUp}>*/}
                {/*        <Text style={style.txt_btn_signUp}>بروزرسانی</Text>*/}
                {/*    </View>*/}
                {/*</TouchableOpacity>*/}
            </Form>
            </View>
        );
    }
}

const mapStateToPropsEditUser = (state) => {
        return {
            user: state.user,
            global: state.global
        };
    };

export default connect(mapStateToPropsEditUser, null)(Edit_skill_profile)


// {/*<ModalDropdown*/}
// {/*    defaultValue={item.name}*/}
// {/*    dropdownTextStyle={style.dropdownStyleTextSkill}*/}
// {/*    textStyle={style.modalTextStyleSkill}*/}
// {/*    dropdownStyle={{backgroundColor: 'transparent'}}*/}
// {/*    style={style.modalFilterSkill}*/}
// {/*    options={this.optionModalDropdown(item)}*/}
// {/*    onSelect={this.UpdateSkill}/>*/}