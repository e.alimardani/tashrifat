import React, {Component} from 'react';
import {
    Text,
    TouchableOpacity,
    Dimensions,
    View,
    // AsyncStorage,
    BackHandler,
    Image, StatusBar,
    ActivityIndicator, Alert, TextInput, FlatList
} from 'react-native';
import AsyncStorage from '@react-native-community/async-storage';
import {Spinner, Header, Left, Body, Right, Button, Title} from 'native-base';
import ServerData from '../connection/connect';
import style from '../../assets/styles/user_friend_css';
import BottomNavigation from '../Shared/bottomNavigation';
import Icon from 'react-native-vector-icons/FontAwesome5';
import LinearGradient from "react-native-linear-gradient";
import {Actions} from "react-native-router-flux";
import {setUser} from "../../Redux/Actions";
import {connect} from "react-redux";

const width = Dimensions.get('window').width;
const height = Dimensions.get('window').height;
const Connect = new ServerData();

class User_Friend extends Component {

    constructor(props) {
        super(props);
        this.state = {
            ListUsersFriend: [],
            ListUsersFriend1:[],
            search_users:'',
            loading:false
        };
        this.GetDataItemListUserFriend = this.GetDataItemListUserFriend.bind(this);
        this.renderItemListUserFriend = this.renderItemListUserFriend.bind(this);
        this.EmptyComponent = this.EmptyComponent.bind(this);
    };

    async componentDidMount() {
       await this.GetDataItemListUserFriend()
    }

    GoToProfileUser(index){
        // console.warn(this.state.ListUsersFriend[index])
        Actions.push('Profile_user',{"ListUsers" : this.state.ListUsersFriend[index]})
    }
   async GetDataItemListUserFriend() {
       try {
        this.setState({loading:true})
        const details = {
            "admintoken": this.props.global.adminToken,
            "usertoken": this.props.user.apiToken,
        };
       let result = await Connect.sendPost(this.props.global.baseApiUrl + '/user/getUser_Friends', details)
                if (result.success == true) {
                   await this.setState({ListUsersFriend: result.data,ListUsersFriend1:result.data,loading:false})
                } else {
                    this.setState({loading:false})
                }
                // console.warn(this.state.ListUsersFriend)
       } catch (error) {
        this.setState({loading:false})
       }
    }

    renderItemListUserFriend = ({item, index}) => {
        return (
            <TouchableOpacity style={style.view_user} onPress={() => this.GoToProfileUser(index)}>
                {item.Frindship_Status_With_Current_User == 'friend' || item.Frindship_Status_With_Current_User == 'Requested' &&
                <View style={style.view_user_icon}>
                    <TouchableOpacity activeOpacity={.5} onPress={() => this.UnFollowRequest(index)}>
                        <LinearGradient style={style.view_folow} start={{x: 0, y: 0}} end={{x: 1, y: 0}}
                                        colors={['#ff6f75', '#fe4f4b']}>
                            <Icon size={width * .05} color={'#fff'} name={'user-minus'}/>
                        </LinearGradient>
                    </TouchableOpacity>
                    <Text style={style.txt_flow}>لغو دنبال کردن</Text>
                </View>
                }
                <View style={style.view_user_txt}>
                    <Text style={style.txt_name_user}>{item.full_name}</Text>
                </View>
                <View style={style.view_img}>
                    <Image source={{uri: item.avatar}} style={style.pic_user}/>
                </View>
            </TouchableOpacity>
        );
    };

    EmptyComponent() {
        if(this.state.loading)
        {
            return(
                <ActivityIndicator  color={this.props.global.grColorTwo} size='large' />
            );
        }
        else
        {
            return(
             <View style={style.viewNotFound}>
                <Text style={style.txtNotFound}>کاربری وجود
                    ندارد</Text>
            </View>
            );
        }   
    }
    searchFilterFunction (text)  {    
        const newData = this.state.ListUsersFriend.filter(item => {      
        const itemData = `${item.full_name.toUpperCase()}`;
        
        const textData = text.toUpperCase();
            
        return itemData.indexOf(textData) > -1;

        });
        this.setState({ ListUsersFriend1: newData });
       
    };
    render() {
        return (
            <View style={style.view_container_user}>

                <View style={style.view_top}>
                    <LinearGradient style={[style.view_header_user]} start={{x: 0, y: 0}} end={{x: 1, y: 0}}
                                    colors={['#627efb', '#8b68fc', '#a95bfe']}>

                        <Header transparent style={style.view_top_header}>
                            <StatusBar backgroundColor="transparent" translucent={true} barStyle="light-content"/>

                            <Left style={{flex: 1,}}>
                                <Button transparent onPress={() => Actions.pop()}>
                                    <Icon name="arrow-left" color={"#ffffff"} size={width * .07}/>
                                </Button>
                            </Left>
                            <Body style={{flex: 1, alignItems: 'center'}}>
                                <Title style={style.txt_top_header}>دوستان</Title>
                            </Body>
                            <Right style={{flex: 1}}>
                                <Button transparent onPress={() => Actions.drawerOpen()}>
                                    <Icon name="bars" color={"#fff"} size={width * .07}/>
                                </Button>
                            </Right>
                        </Header>
                        <View style={[style.view_search]}>
                            <View style={style.icon_search}>
                                <Icon name="search" color={'#e8e8e8'} size={width * .05}/>
                            </View>
                            <View style={style.input_search}>
                                <TextInput
                                    textAlign={'right'}
                                    placeholder="جستجو اعضا..."
                                    placeholderTextColor={"#e8e8e8"}
                                    style={style.textinput_search}
                                    onChangeText={text => this.searchFilterFunction(text)}
                                    // onChangeText={(text) => this.FilterUserBySearch(text)}
                                />
                            </View>
                        </View>
                    </LinearGradient>

                </View>

                <View style={style.center_page_users}>
                {(!this.state.loading) && this.state.ListUsersFriend1.length > 0 &&
                    <FlatList
                        keyExtractor={(item, index) => index.toString()}
                              data={this.state.ListUsersFriend1
                                  // .filter(item => item.first_name.includes(this.state.search_users))
                              }
                              renderItem={this.renderItemListUserFriend}
                              ListEmptyComponent={this.EmptyComponent}
                    />
                     }
                {(this.state.loading) &&
                <View style={style.viewNotFound}>
                    <ActivityIndicator  color={this.props.global.grColorTwo} size='large' />
                </View>
                }
                </View>
                <View>
                    <BottomNavigation styleBottom={'other'}/>
                </View>
            </View>
        );

    }
}


const mapStateToProps = (state) => {
    return {
        user: state.user,
        global: state.global
    };
};
export default connect(mapStateToProps, null)(User_Friend)

