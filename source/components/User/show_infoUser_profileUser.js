import React, {Component} from 'react';
import {
    Text,
    TouchableOpacity,
    Dimensions,
    View,
    TouchableNativeFeedback,
    Image, Linking
} from 'react-native';
import Icon from 'react-native-vector-icons/FontAwesome5';
import ServerData from '../connection/connect';
import style from '../../assets/styles/profile_css';
import {Actions} from 'react-native-router-flux';
import {connect} from "react-redux";
// import Toast, {DURATION} from 'react-native-easy-toast';
import Toast from 'react-native-simple-toast';
import Helper from '../Shared/Helper'
const width = Dimensions.get('window').width;
const Connect = new ServerData();

class show_infoUser_profileUser extends Component {

    constructor(props) {
        super(props);
        this.state = {
            socialMedia:[]
        }

    };
componentWillMount(){
    this.getDataSocialMedia()
}

async getDataSocialMedia(){
    console.log(this.props)
    formData = new FormData();
    formData.append("admintoken", this.props.global.adminToken);
    formData.append("user_id", this.props.ListUser.user_id ? this.props.ListUser.user_id : this.props.ListUser.id);
try {
    let response = await fetch(this.props.global.baseApiUrl + '/user/get_userSocialMedia_By_Userid', {
        method: 'post',
        headers: {
            'Content-Type': 'multipart/form-data',
        },
        body: formData
    });
    
    let result = await response.json();
    console.log(response)
    console.log(result)
    if(result.success === true){
            await this.setState({
                socialMedia: result.data,
            });
    }else{ }
    }
catch (error) {}
}

LinkToWhatsApp(){
    if(this.state.socialMedia.whatsapp !== '' ){
     Linking.openURL(`whatsapp://send?&phone=+98${this.state.socialMedia.whatsapp}`)
    }else{
        Toast.show('کاربر شماره ای برای واتساپ ثبت نکرده است!',Toast.Long)
        // this.refs.toast.show('کاربر شماره ای برای واتساپ ثبت نکرده است!',2500);
    } 
}
LinkToTelegram(){
    if(this.state.socialMedia.telegram !== '' ){
        Linking.openURL(`https://t.me/${this.state.socialMedia.telegram}`)
       }else{
           Toast.show('کاربر آیدی خود را برای تلگرام ثبت نکرده است!',Toast.Long)
        //    this.refs.toast.show('کاربر آیدی خود را برای تلگرام ثبت نکرده است!',2500);
       } 
}
LinkToInstagram(){
    if(this.state.socialMedia.instegram !== '' ){
        Linking.openURL(`instagram://user?username=${this.state.socialMedia.instegram}`)
       }else{
        Toast.show('کاربر آیدی خود را برای اینستگرام ثبت نکرده است!', Toast.LONG);
        //    this.refs.toast.show('کاربر آیدی خود را برای اینستگرام ثبت نکرده است!',2500);
       } 
}
    render() {
        const ListUser = this.props.ListUser;
       
        const user = this.props.user;
        return (
            <View style={style.view_bottom_screen}>
                <View style={style.left_bottom_screen}>
                <TouchableOpacity style={style.view_icon_name} onPress={()=>Actions.push('sampleWorkUser',{'ListUser':ListUser})}>
                        <Icon name="file-signature" color={"#26ffdd"} size={width * .07}/>
                        <Text numberOfLines={1} style={style.txt_icon_bottom}>نمونه کارها</Text>
                    </TouchableOpacity>
                    <TouchableOpacity style={style.view_icon_name} onPress={()=> Linking.openURL(`mailto:${ListUser.user_email ? ListUser.user_email : ListUser.email}`)}>
                        <Icon name="envelope-open-text" color={"#ff31fe"} size={width * .07}/>
                        <Text numberOfLines={1} style={style.txt_icon_bottom}>{ListUser.user_email ? ListUser.user_email : ListUser.email}</Text>
                    </TouchableOpacity> 
                    <TouchableOpacity numberOfLines={1} style={style.view_icon_name} onPress={()=> Linking.openURL(`tel:${ListUser.username}`)}>
                        <Icon name="mobile-alt" color={"#ff8b17"} size={width * .07}/>
                        <Text style={style.txt_icon_bottom}>{Helper.ToPersianNumber(ListUser.username)}</Text>
                    </TouchableOpacity> 
                   
                </View>
                <View style={style.center_bottom_screen}>

                <TouchableOpacity activeOpacity={.7} style={style.view_icon_name} onPress={this.LinkToWhatsApp.bind(this)}>
                        <Image source={require('../../assets/images/whatsapp.png')} style={{width:30,height:30}} resizeMode={"contain"}/>
                        <Text numberOfLines={1} style={style.txt_icon_bottom}>{'واتساپ'}</Text>
                </TouchableOpacity>
                    <TouchableOpacity activeOpacity={.7} style={style.view_icon_name} onPress={this.LinkToTelegram.bind(this)}>
                        <Image source={require('../../assets/images/telegram.png')} style={{width:30,height:30}} resizeMode={"contain"}/>
                        <Text numberOfLines={1} style={style.txt_icon_bottom}>{'تلگرام'}</Text>
                    </TouchableOpacity>
                    <TouchableOpacity activeOpacity={.7} style={style.view_icon_name} onPress={this.LinkToInstagram.bind(this)}>
                        <Image source={require('../../assets/images/instagram.png')} style={{width:30,height:30}} resizeMode={"contain"}/>
                        <Text numberOfLines={1} style={style.txt_icon_bottom}>{'اینستگرام'}</Text>
                    </TouchableOpacity>
                    {/* <Toast
                    ref="toast"
                    // style={{backgroundColor:'red'}}
                    defaultCloseDelay={2000}
                    position='bottom'
                    positionValue={200}
                    fadeInDuration={750}
                    fadeOutDuration={1000}
                    opacity={0.8}
                    // textStyle={{color:'red'}}
                /> */}
                </View>
              
            </View>
        );

    }
}

const mapStateToProps = (state) => {
    return {
        user: state.user,
        global: state.global

    };
};
export default connect(mapStateToProps, null)(show_infoUser_profileUser)
