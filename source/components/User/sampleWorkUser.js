import React, {Component} from 'react';
import {
    Dimensions,
    View,
    Text, Platform, FlatList,Alert,
    ActivityIndicator, StatusBar, TouchableOpacity, TextInput, KeyboardAvoidingView, Image,Linking
} from 'react-native';
import AsyncStorage from '@react-native-community/async-storage';
import {Container, Header, Left, Body, Right, Button, Title, Spinner} from 'native-base';
import ServerData from '../connection/connect';
import style from '../../assets/styles/sampleWorkUser_css';
import styles from '../../assets/styles/sampleWork_css';
import LinearGradient from "react-native-linear-gradient";
import Icon from "react-native-vector-icons/FontAwesome5";

import {connect} from "react-redux";
import {Actions} from "react-native-router-flux";
import ComponentSampleWork from "../Shared/componentSampleWork";
import IconF from "react-native-vector-icons/AntDesign";
import Modal from "react-native-modal";
import Video from 'react-native-video';
import Orientation from 'react-native-orientation';


const width = Dimensions.get('window').width;
const height = Dimensions.get('window').height;
const Connect = new ServerData();

class sampleWorkUser extends Component {

    constructor(props) {
        super(props);
        this.state = {
            ListSampleWork: [],
            unFollowDisplay: 'none',
            followDisplay: 'flex',
            ModalShowPicture: false,
            imgOriginal: '',
            urlVideo:'',
            loading:true,
            idImg:'',
            titleSelect:'',
            LinkSelect:'',
            file_url:''
        };
       
        this.renderItemSampleUser = this.renderItemSampleUser.bind(this);
        this.GetMessage = this.GetMessage.bind(this);
        this.ListEmptyComponentSampleUser = this.ListEmptyComponentSampleUser.bind(this);
    };

   async GetMessage() {
       try {
        
        const details = {
            "admintoken": this.props.global.adminToken,
            "user_id": this.props.ListUser.user_id ? this.props.ListUser.user_id :this.props.ListUser.id,
        };
       let result = await Connect.sendPost(this.props.global.baseApiUrl+ '/user/get_sample_by_userId', details)
       console.log(details)
       console.log(result)         
       if (result.success == true) {
             
                  await this.setState({loading:false,ListSampleWork: result.data});
                 
                } else {
                  await this.setState({loading:false})
                }
       } catch (error) {
          await this.setState({loading:false})
       }
        
           
    }

   async componentWillMount() {
         this.GetMessage();
    }

    setAddressImage(item) {
        this.setState({imgOriginal: item, ModalShowPicture: true})
    }
    renderItemSampleUser({item, index}) {
        let desc = item.original;
        if(item.length !== 0){
                return (
                    <View style={style.SampleWork_container}>
                    <TouchableOpacity style={style.view_name}
                                    onPress={()=>{this.setAddressImage(item.image_url) ,console.log(item), this.setState({idImg:item.id,titleSelect:item.title, LinkSelect:item.link , file_url:item.file_url})}}>
                    <Image source={{uri: item.image_url}} style={style.img_sampleWork} resizeMode={'contain'}/>
                    </TouchableOpacity>
                    </View> 
                )
            
            
        }
    }

    ListEmptyComponentSampleUser() {
   
        if(this.state.loading)
        {
           
            return(
            <View style={style.viewNotFound}>
                <ActivityIndicator color={this.props.global.grColorTwo} size='large' />
           </View>
            );
        }
        else
        {
            
            return(
             <View style={style.viewNotFound}>
                <Text style={style.txtNotFound}>نمونه کاری وجود
                    ندارد</Text>
            </View>
            );
        }   
    }
   async FollowRequest() {
        
        const details = {
            "admintoken": this.props.global.adminToken,
            "usertoken": this.props.user.apiToken,
            "target_user": this.props.ListUser.ID,
        };
       let result = await Connect.sendPost(this.props.global.baseApiUrl + '/user/Follow_Request', details)
            
                // console.warn(result);
                if (result.success == true) {
                    Alert.alert('', result.data, [{text: 'باشه'}]);

                } else {
                    Alert.alert('', result.messages, [{text: 'باشه'}])
                }

           
    }
   async UnFollowRequest() {
        const details = {
            "admintoken": this.props.global.adminToken,
            "usertoken": this.props.user.apiToken,
            "target_id": this.props.ListUser.ID,
        };
        let result = await Connect.sendPost(this.props.global.baseApiUrl + '/user/CancelOrIngnore_FollowRequest', details)
            
                // console.warn(result)
                if (result.success == true) {
                    Alert.alert('', result.data, [{text: 'باشه'}])

                } else {
                    Alert.alert('', result.messages, [{text: 'باشه'}])
                }          
    }
    
   
    render() {
        const ListUsers = this.props.ListUser;
        let numColumns = 3;
        return (
            <View style={style.view_container_messageRecive}>
                <Modal isVisible={this.state.ModalShowPicture}
                       deviceWidth={width}
                       deviceHeight={height}
                       animationType={"slide"}
                       backdropColor={'#000000'}
                       backdropOpacity={0.70}
                       onBackButtonPress={() => this.setState({ModalShowPicture: !this.state.ModalShowPicture})}
                       onBackdropPress={() => this.setState({ModalShowPicture: !this.state.ModalShowPicture})}
                       style={styles.ModalSearchDriver}
                >
                    <View style={styles.viewContainerSupport}>
                    {this.state.titleSelect !='' && <Text style={[styles.txt_header,{marginBottom:5}]}>{this.state.titleSelect}</Text>}
                           
                            <Image source={{uri: this.state.imgOriginal}} style={styles.imgModalSampleWork} resizeMode={'cover'}/>
                            {/* <TouchableOpacity style={style.btnSupport1} activeOpacity={.7}
                                              onPress={() => this.setState({ModalShowPicture: false})}>
                                    <Image style={style.Image_close} resizeMode={'cover'} source={require('../../assets/images/close.png')}/>
                            </TouchableOpacity> */}
                            {this.state.LinkSelect !='' &&
                            <TouchableOpacity onPress={()=>Linking.openURL('https://'+this.state.LinkSelect)}>
                                <Text style={[styles.txt_header,{marginTop:5}]}>{this.state.LinkSelect}</Text>
                            </TouchableOpacity>
                            }
                            {this.state.file_url !='http://tashrifat.wpfix.ir/wp-content/uploads/sample-file/83/' &&
                            <TouchableOpacity onPress={()=>Linking.openURL(this.state.file_url)} style={{justifyContent: 'center', alignItems: 'center'}}>
                                <Text  style={[styles.txt_header,{marginTop:5}]}>مشاهده فایل</Text>
                            </TouchableOpacity>
                            }
                    </View>
                </Modal>
                <View style={style.view_top}>
                    <LinearGradient style={[style.view_header_prof]} colors={['#627efb', '#8b68fc', '#a95bfe']}>
                        <Header transparent style={style.view_top_header}>
                            <StatusBar backgroundColor="transparent" translucent={true} barStyle="light-content"/>
                            <Left style={{flex: 1,}}>
                                <Button transparent onPress={() => Actions.pop()}>
                                    <Icon name="arrow-left" color={"#ffffff"} size={width * .07}/>
                                </Button>
                            </Left>
                            <Body style={{flex: 1, alignItems: 'center'}}>
                                <Title
                                    style={style.txt_top_header}>پروفایل {ListUsers.full_name}</Title>
                            </Body>
                            <Right style={{flex: 1}}>
                                <Button transparent onPress={() => Actions.drawerOpen()}>
                                    <Icon name="bars" color={"#fff"} size={width * .07}/>
                                </Button>
                            </Right>
                        </Header>
                        <View style={style.view_center_header}>
                            <View style={style.img_user_prof}>
                                <Image source={{uri: ListUsers.avatar? ListUsers.avatar : ListUsers.Avatar}} style={style.pic_member} resizeMode={'cover'}/>
                            </View>
                            <View style={style.view_name_side_prof}>
                                <Text style={style.txt_post_prof_header}>{`( ${ListUsers.work_in === '' ?'زمینه کاری انتخاب نشده':ListUsers.work_in} )`}</Text>
                                <Text
                                    style={style.txt_nameUser_prof_header}>{ListUsers.full_name}</Text>
                            </View>
                        </View>
                       
                        <View style={style.view_count_group_users}>
                        {this.props.user.apiToken !== null && ListUsers.Frindship_Status_With_Current_User === 'nothing' &&
                            <TouchableOpacity
                                style={[style.view_bottom_header_follow, {display: this.state.followDisplay}]}
                                onPress={() => this.FollowRequest()}>
                                <IconF name="adduser" color={'#fff'} size={width * .05}/>
                                <Text style={style.txt_header1}>دنبال کردن</Text>
                            </TouchableOpacity>
                        }
                        {this.props.user.apiToken !== null && ListUsers.Frindship_Status_With_Current_User === 'friend' || ListUsers.Frindship_Status_With_Current_User === 'Requested' &&
                            <TouchableOpacity
                                style={[style.view_bottom_header_follow,]}
                                onPress={() => this.unFollowRequest()}>
                                {/*<View style={style.view_count_txt}>*/}
                                {/*<Text style={style.txt_header}>{user.friends_count}</Text>*/}
                                <IconF name="adduser" color={'#fff'} size={width * .05}/>
                                {/*</View>*/}
                                <Text style={style.txt_header1}>لغو دنبال کردن</Text>
                            </TouchableOpacity>
                            }
                            {this.props.user.apiToken !== null &&
                            <TouchableOpacity
                                style={[style.view_bottom_header_follow, {display: this.state.followDisplay}]}
                                onPress={() => Actions.push('contact_user', {'ListUserSendMessage': ListUsers})}>
                                <IconF name="adduser" color={'#fff'} size={width * .05}/>
                                <Text style={style.txt_header1}>ارسال پیام</Text>
                            </TouchableOpacity>
                            }
                        </View>
                    
                    </LinearGradient>
                </View>
                <View style={style.center_messageRecive}>
                {(!this.state.loading) &&
                    <FlatList
                        numColumns={numColumns}
                        data={this.state.ListSampleWork}
                        renderItem={this.renderItemSampleUser}
                        keyExtractor={(item, index) => index.toString()}
                        ListEmptyComponent={this.ListEmptyComponentSampleUser}
                    />
            }
            {(this.state.loading) &&
                <View style={style.viewNotFound}>
                <ActivityIndicator color={this.props.global.grColorTwo} size='large' />
                </View>
            }
                </View>

            </View>
        );

    }
}


const mapStateToProps = (state) => {
    return {
        user: state.user,
        global: state.global
    };
};
export default connect(mapStateToProps, null)(sampleWorkUser)
