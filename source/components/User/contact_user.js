import React, {Component} from 'react';
import {
    Text,
    TouchableOpacity,
    Dimensions,
    View,
    ScrollView,
    BackHandler,
    Image, StatusBar,
    TouchableNativeFeedback, Alert, TextInput, FlatList
} from 'react-native';
import {Spinner, Header, Left, Body, Right, Button, Title, Content} from 'native-base';
import ServerData from '../connection/connect';
import style from '../../assets/styles/contact_user_css';
import BottomNavigation from '../Shared/bottomNavigation';
import Icon from 'react-native-vector-icons/FontAwesome5';
import LinearGradient from "react-native-linear-gradient";
import ModalDropdown from 'react-native-modal-dropdown';
import {Actions} from "react-native-router-flux";
import {connect} from "react-redux";
import {ProgressDialog} from 'react-native-simple-dialogs';


const width = Dimensions.get('window').width;
const height = Dimensions.get('window').height;
const Connect = new ServerData();

class UselessTextInput extends Component {
    render() {
        return (
            <TextInput
                {...this.props} // Inherit any props passed to it; e.g., multiline, numberOfLines below
                editable = {true}
                // maxLength = {40}
            />
        );
    }
}

class contact_user extends Component {

    constructor(props) {
        super(props);
        this.state = {
            subjectUser: '',
            messageUser: '',
            progressVisible:false
        };

        this.sendMessageUser = this.sendMessageUser.bind(this);

    };

   

   async sendMessageUser() {
       if(this.state.subjectUser == ''){
           this.setState({message:'موضوع نمیتواند خالی باشد'})
       }
       else if(this.state.messageUser == ''){
        this.setState({message:'پیام نمیتواند خالی باشد'})
       }
       else{
        this.setState({progressVisible:true,message:''})

        const details = {
            "admintoken": this.props.global.adminToken,
            "usertoken": this.props.user.apiToken,
            "reciver_id": this.props.ListUserSendMessage.user_id,
            "subject": this.state.subjectUser,
            "message": this.state.messageUser,
        };
       let result = await Connect.sendPost(this.props.global.baseApiUrl + '/user/Send_Message', details)
       console.log(this.props.ListUserSendMessage)
       console.log(result)
                if (result.success) {
                    this.setState({messageUser:'',subjectUser:'',progressVisible:false,message:''})
                    Alert.alert('','پیام ارسال شد',[{text:'باشه'}])

                } else {
                    this.setState({progressVisible:false,message:''})
                    Alert.alert('','ارسال پیام انجام نشد',[{text:'باشه'}])
                }
       }
    };

    render() {
        const user = this.props.ListUserSendMessage;
        return (
            <View style={style.view_container_user}>
                <ProgressDialog visible={this.state.progressVisible} message="لطفا منتظر بمانید..."/>
                <View style={style.view_top}>
                    <LinearGradient style={[style.view_header_user]} start={{x: 0, y: 0}} end={{x: 1, y: 0}}
                                    colors={['#627efb', '#8b68fc', '#a95bfe']}>

                        <Header transparent style={style.view_top_header}>
                            <StatusBar backgroundColor="transparent" translucent={true} barStyle="light-content"/>

                            <Left style={{flex: 1,}}>
                                <Button transparent onPress={() => Actions.pop()}>
                                    <Icon name="arrow-left" color={"#ffffff"} size={width * .07}/>
                                </Button>
                            </Left>
                            <Body style={{flex: 1, alignItems: 'center'}}>
                                <Title style={style.txt_top_header}>ارسال پیام</Title>
                            </Body>
                            <Right style={{flex: 1}}>
                                <Button transparent onPress={() => Actions.drawerOpen()}>
                                    <Icon name="bars" color={"#fff"} size={width * .07}/>
                                </Button>
                            </Right>
                        </Header>

                    </LinearGradient>

                </View>

                <Content style={style.center_page_users}>
                    <View style={style.view_send_container}>
                        <Text style={style.txt_message_user2}>ارسال پیام به {user.full_name}</Text>
                        <View style={style.view_subject_send_message}>
                            <TextInput style={style.textinput_subject}
                                       onChangeText={(text) => this.setState({subjectUser:text})}
                                       value={this.state.subjectUser}
                            />
                            <Text style={style.txt_message_user2}>موضوع : </Text>
                        </View>
                        <View style={{flexDirection:'row-reverse',marginTop:10}}>
                            <Text style={[style.txt_message_user]}>پیام : </Text>

                            <UselessTextInput
                                multiline = {true}
                                numberOfLines = {4}
                                style={style.textInputMessage}
                                onChangeText={(text) => this.setState({messageUser:text})}
                                value={this.state.messageUser}
                            />
                        </View>
                        {this.state.message != '' && 
                        <Text style={[style.txt_message_user,{color:'red',textAlign:'center'}]}>{this.state.message}</Text>
                        }
                    </View>
                    <TouchableOpacity activeOpacity={.7} style={[style.btnSendMessage,{alignSelf:'center'}]} onPress={this.sendMessageUser}>
                        <Text style={[style.txt_message_user1,{color:'white'}]}>ارسال</Text>
                    </TouchableOpacity>
                </Content>
            </View>
        );

    }
}


const mapStateToProps = (state) => {
    return {
        user: state.user,
        global: state.global
    };
};
export default connect(mapStateToProps, null)(contact_user)

