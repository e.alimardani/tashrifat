import React, {Component} from 'react';
import {
    Text,
    TouchableOpacity,
    Dimensions,
    View,
    Platform,
    BackHandler,
    Image, StatusBar,Linking,
    TouchableNativeFeedback, Alert, TextInput, FlatList,ActivityIndicator
} from 'react-native';
import {Spinner, Header, Left, Body, Right, Button, Title, Content} from 'native-base';
import style from './Ticket/Style';
import Icon from 'react-native-vector-icons/FontAwesome5';
import LinearGradient from "react-native-linear-gradient";
import {Actions} from "react-native-router-flux";
import {connect} from "react-redux";
import {ProgressDialog} from 'react-native-simple-dialogs';
import Helper from '../Shared/Helper';



const width = Dimensions.get('window').width;
const height = Dimensions.get('window').height;

class Subscribe extends Component {
    constructor(props) {
        super(props);
        this.state = {
            data:[],
            loading:true,
            loadingbuy:false,
        };
        this.renderData = this.renderData.bind(this);
        this.EmptyComponent = this.EmptyComponent.bind(this);

    }
    componentWillMount(){
        this.GetList()
    }
    EmptyComponent() {    
        return(
         <View style={{marginTop:10}}>
            <Text style={{textAlign:'center',fontFamily:'IRANSansMobile'}}>پکیجی وجود ندارد</Text>
        </View>
        );
}
    async GetList(){

        try {
            let formData = new FormData();
            formData.append("admintoken", this.props.global.adminToken);

            let response = await fetch(this.props.global.baseApiUrl + '/view/get_all_plans',
                {
                    method: "POST",
                    body: formData
                });

            if (response.status !== 200) {
                await this.setState({
                    loading:false,
                });
            }
            else {

                let json = await response.json();

                console.log(json);

                if(json.success){

                    await this.setState({
                        data:json.data,
                        loading:false
                    });;
                }
                else {
                    await this.setState({
                        loading:false
                    });
                }
            }
        } catch (error) {
            await this.setState({
                loading:false
            });
        }
    }
    async buy(id){
        if(this.props.user.apiToken === null){
            Alert.alert("خطا", 'برای خرید پکیج ابتدا وارد شوید', [
                {text: 'باشه'}
            ]);
        }
        else{
            console.log(id)
            this.setState({loadingbuy:true})
            try {
                let formData = new FormData();
                formData.append("admintoken", this.props.global.adminToken);
                formData.append("usertoken", this.props.user.apiToken);
                formData.append("plan_id", id);
                let response = await fetch(this.props.global.baseApiUrl + '/view/buy_plan',
                    {
                        method: "POST",
                        body: formData
                    });
    
                if (response.status !== 200) {
                    await this.setState({
                        loadingbuy:false
                    });
                    Alert.alert("خطا", 'خطایی رخ داده', [
                        {text: 'باشه'}
                    ]);
                }
                else {
    
                    let json = await response.json();
                    console.log(formData)
                    console.log(json);
    
                    if(json.success){
    
                        await this.setState({
                            loadingbuy:false
                        });
                        Linking.openURL(json.data)
                    }
                    else {
                        await this.setState({
                            loadingbuy:false
                        });
                        Alert.alert("خطا", 'خطایی رخ داده', [
                            {text: 'باشه'}
                        ]);
                    }
                }
            } catch (error) {
                await this.setState({
                    loadingbuy:false
                });
                Alert.alert("خطا", 'خطایی رخ داده', [
                    {text: 'باشه'}
                ]);
            }
        }
        
    }
    renderData(item){
        console.log(item)
        return(
            <View style={{backgroundColor:'#fff',borderRadius:10,margin:10,padding:10}}>
                <View style={{flexDirection:'row-reverse',justifyContent:'space-around',alignItems:'center'}}>
                    <View style={{alignItems:'center',flex:1}}>
                        <Text style={style.TextStyle}>نام پکیج</Text>
                        <Text style={[style.TextStyle,{textAlign:'center',paddingTop:5}]}>{item.item.title}</Text>
                    </View>
                    <View style={{alignItems:'center',flex:1}}>
                        <Text style={style.TextStyle}>قیمت</Text>
                        <Text style={[style.TextStyle,{textAlign:'center',paddingTop:5}]}> {Helper.ToPersianNumber(item.item.price)} تومان</Text>
                    </View>
                    <View style={{alignItems:'center',flex:1}}>
                        <Text style={style.TextStyle}>مدت اعتبار</Text>
                        <Text style={[style.TextStyle,{textAlign:'center',paddingTop:5}]}>{Helper.ToPersianNumber(item.item.credit)} روز</Text>
                    </View>
                </View>
                <TouchableOpacity onPress={()=>this.buy(item.item.id)} style={{justifyContent:'center',alignItems:'center',backgroundColor:'#8b68fc',marginTop:10,width:'30%',alignSelf:'center',borderRadius:5,padding:5}}>
                    <Text style={[style.TextStyle,{color:'#fff'}]}>خرید</Text>
                </TouchableOpacity>
            </View>
        )
    }
    render() {
        return (
            <View style={style.view_container_user}>
             <ProgressDialog visible={this.state.progressVisible} message="لطفا منتظر بمانید..."/>
                <View style={style.view_top}>
                    <LinearGradient style={[style.view_header_user,{height:100}]} start={{x: 0, y: 0}} end={{x: 1, y: 0}}
                                    colors={['#627efb', '#8b68fc', '#a95bfe']}>

                        <Header transparent style={style.view_top_header}>
                            <StatusBar backgroundColor="transparent" translucent={true} barStyle="light-content"/>

                            <Left style={{flex: 1,}}>
                                <Button transparent onPress={() => Actions.pop()}>
                                    <Icon name="arrow-left" color={"#ffffff"} size={width * .07}/>
                                </Button>
                            </Left>
                            <Body style={{flex: 1, alignItems: 'center'}}>
                                <Title style={style.txt_top_header}>اشتراک</Title>
                            </Body>
                            <Right style={{flex: 1}}>
                                <Button transparent onPress={() => Actions.drawerOpen()}>
                                    <Icon name="bars" color={"#fff"} size={width * .07}/>
                                </Button>
                            </Right>
                        </Header>
                    </LinearGradient>
                </View>
                <Content style={{flex:1,width:"100%"}}>
                    {this.state.loading && <ActivityIndicator size="large" color='#a95bfe'/>}
                    {!this.state.loading &&
                        <FlatList
                            keyExtractor={(item, index) => index.toString()}
                            data={this.state.data}
                            renderItem={this.renderData}
                            ListEmptyComponent={this.EmptyComponent}
                        />
                    }
                    
                </Content>
                <ProgressDialog visible={this.state.loadingbuy} message="لطفا منتظر بمانید..."/>
            </View>
        );

    }
}


const mapStateToProps = (state) => {
    return {
        user: state.user,
        global: state.global
    };
};
export default connect(mapStateToProps, null)(Subscribe)

