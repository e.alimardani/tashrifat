import React from 'react';
import {
    ActivityIndicator,
    Text,
    View,
    StatusBar,
    Image,
    TouchableOpacity,
    Linking,
    PixelRatio,
    TextInput,
    Dimensions
} from 'react-native';
import IconFont from 'react-native-vector-icons/FontAwesome5';
import {Container, Header, Left, Body, Right, Thumbnail,Icon,Button} from 'native-base';
import style from './Style';
import Helpers from "../../Shared/Helper";
import {setUser} from "../../../Redux/Actions/index";
import {connect} from "react-redux";
import {KeyboardAwareScrollView} from "react-native-keyboard-aware-scroll-view";
import LinearGradient from "react-native-linear-gradient";
import {Actions} from "react-native-router-flux";

const width = Dimensions.get('window').width;
const height = Dimensions.get('window').height;

class ContactUs extends React.Component {

    constructor(props)
    {
        super(props);

        this.state = {
            info:null,
            loading: true,
            body:'',
            subject:''
        };

        this.getContactRequest = this.getContactRequest.bind(this);
        this.onSubjectChanged = this.onSubjectChanged.bind(this);
        this.onBodyChanged = this.onBodyChanged.bind(this);
    }

    onSubjectChanged(text) {
        this.setState({
            subject:text
        });
    }
    onBodyChanged(text) {
        this.setState({
            body:text
        });
    }

    async componentDidMount(){
        await this.getContactRequest();
    }

    async getContactRequest(){
        if (await Helpers.CheckNetInfo()) {
            await this.setState({
                loading:false
            });
        }

        try {

            let formData = new FormData();
            formData.append("admintoken", this.props.global.adminToken);

            let response = await fetch(this.props.global.baseApiUrl + '/user/contact_us_data',
                {
                    method: "POST",
                    body: formData
                });

            //console.log(response);

            if (response.status !== 200) {
                await this.setState({
                    loading:false
                });
            }
            else {

                let json = await response.json();

                console.log(json);

                if(json.success){

                    await this.setState({
                        info:json.data,
                        loading:false
                    });
                }
                else {
                    await this.setState({
                        loading:false
                    });
                }
            }
        } catch (error) {
            await this.setState({
                loading:false
            });
        }
    }

    render() {


        return (
            <Container>
                <LinearGradient style={[style.view_header_user,{height:100}]} start={{x: 0, y: 0}} end={{x: 1, y: 0}}
                                    colors={['#627efb', '#8b68fc', '#a95bfe']}>
                    <Header style={[style.header]} >

                        <Left style={{flex: .2,justifyContent:'center',alignItems:'flex-start'}}>
                            <Button transparent onPress={() => Actions.pop()}>
                                <IconFont name="arrow-left" color={"#ffffff"} size={width * .07}/>
                            </Button>
                        </Left>
                        <Body style={{flex: 1,justifyContent:'center',alignItems:'center'}}>
                            <Text style={[style.notFoundText,{marginTop:0,color:'#fff'}]}>تماس با ما</Text>
                        </Body>
                        <Right style={{flex: .2,justifyContent:'center',alignItems:'flex-end'}} >

                        </Right>
                    </Header>
                </LinearGradient>

                <StatusBar backgroundColor="transparent" translucent={true} barStyle="light-content" />

                {!this.state.loading && this.state.info !== null && <KeyboardAwareScrollView style={{flex:1,backgroundColor:'#fafbff'}} keyboardShouldPersistTaps={'handled'}>
                    <View style={{width:'100%',justifyContent:'center',alignItems:'center',paddingVertical:20}}>
                        <Thumbnail source={{uri : this.state.info.profile_Image}} large />
                        <Text style={style.contactName}>{this.state.info.name}</Text>
                    </View>
                    <View style={{width:'100%',flexDirection:'row',justifyContent:'flex-end',paddingHorizontal:10,alignItems:'center',paddingVertical:5}}>
                        <Text style={[style.contactValues,{width:'90%',textAlign: 'right',marginRight:20}]}>{this.state.info.address}</Text>
                        <Icon name='md-pin' style={{fontSize:20}} />
                    </View>
                    <View style={{width:'100%',flexDirection:'row',justifyContent:'flex-end',paddingHorizontal:10,alignItems:'center',paddingVertical:5}}>
                        <Text style={[style.contactValues,{width:'40%',textAlign: 'left',marginRight:20}]}>{Helpers.ToPersianNumber(this.state.info.workingTime)}</Text>
                        <TouchableOpacity onPress={()=>Linking.openURL(`tel:${this.state.info.phone}`)} style={{width:'40%'}}>
                            <Text style={[style.contactValues,{textAlign: 'right',marginRight:20}]}>{Helpers.ToPersianNumber(String(this.state.info.phone))}</Text>
                        </TouchableOpacity>
                        <Icon name='md-call' style={{fontSize:20}} />
                    </View>
                    <View style={{width:'100%',flexDirection:'row',justifyContent:'space-between',paddingHorizontal:20,alignItems:'center',paddingVertical:10}}>
                        <TouchableOpacity activeOpacity={.8} style={{flexDirection:'row',backgroundColor:'#fff',padding:10,borderRadius:10}} onPress={() => Linking.openURL(this.state.info.telegram)}>
                            {/* <Image style={{width:20,height:20,resizeMode:'contain',marginRight:10}} source={require('./../../assets/images/shared/telegram.png')} /> */}
                            <Text style={[style.contactValues,{fontSize:12}]}>صفحه تلگرام ما</Text>
                        </TouchableOpacity>
                        <TouchableOpacity activeOpacity={.8} style={{flexDirection:'row',backgroundColor:'#fff',padding:10,borderRadius:10}} onPress={() => Linking.openURL(this.state.info.instegram)}>
                            {/* <Image style={{width:20,height:20,resizeMode:'contain',marginRight:10}} source={require('./../../assets/images/shared/instagram.png')} /> */}
                            <Text style={[style.contactValues,{fontSize:12}]}>صفحه اینستاگرام ما</Text>
                        </TouchableOpacity>
                    </View>
                    <View style={{width:'100%',flexDirection:'row',justifyContent:'space-between',paddingHorizontal:20,alignItems:'center',paddingVertical:10}}>
                        <TouchableOpacity activeOpacity={.8} style={{flexDirection:'row',backgroundColor:'#fff',borderRadius:10,overflow:'hidden',width:'100%',height:150,borderWidth:5/PixelRatio.get(),borderColor:'#efefef'}} onPress={() => Linking.openURL(`geo:${this.state.info.lat_ip},${this.state.info.long_ip}`)}>
                            <Image style={{width:'100%',height:'100%',resizeMode:'cover'}} source={{uri:this.state.info.map_Image}} />
                        </TouchableOpacity>
                    </View>

                    {/* <View style={{width:'100%',justifyContent:'center',paddingHorizontal:20,alignItems:'flex-end',paddingVertical:10}}>
                        <Text style={[style.contactName,{fontSize:12,textAlign:'right'}]}>درخواست خود را برای ما ارسال کنید!</Text>

                        <TextInput underlineColorAndroid="rgba(0,0,0,0)" style={style.textInput}
                                   placeholderTextColor="#cccccc" placeholder='عنوان پیام' value={this.state.subject}
                                   onChangeText={this.onSubjectChanged}/>

                        <TextInput underlineColorAndroid="rgba(0,0,0,0)" style={[style.textInput,{height:150}]}
                                   placeholderTextColor="#cccccc" placeholder='متن پیام' value={this.state.body} multiline={true}
                                   onChangeText={this.onBodyChanged}/>

                        <TouchableOpacity activeOpacity={.8} style={{justifyContent:'center',alignItems:'center',marginTop:10,backgroundColor:'#27ae60',borderRadius:10,overflow:'hidden',width:'100%',height:50}}
                                          onPress={() => Linking.openURL(`mailto:${this.state.info.email}?subject=${this.state.subject}&body=${this.state.body}`)}>
                            <Text style={[style.contactValues,{fontSize:12,color:'#fff'}]}>ارسال پیام</Text>
                        </TouchableOpacity>
                    </View> */}
                </KeyboardAwareScrollView>}

                {/* {this.state.loading && <View style={{flex:1,justifyContent:'center',alignItems:'center',backgroundColor:'#fafbff'}}>
                    <ActivityIndicator color="#0082f0" size='large'/>
                </View>} */}

            </Container>
        )
    }
}


const mapDispatchToProps = dispatch => {
    return {
        setUser : user => {
            dispatch(setUser(user))
        }
    }
};

const mapStateToProps =(state) =>  {
    return{
        user: state.user,
        global:state.global
    }
};

export default connect(mapStateToProps,mapDispatchToProps)(ContactUs);
