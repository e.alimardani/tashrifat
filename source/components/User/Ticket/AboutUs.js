import React, {Component} from 'react';
import {
    Text,
    TouchableOpacity,
    Dimensions,
    View,
    Platform,
    BackHandler,
    Image, StatusBar,
    TouchableNativeFeedback, Alert, TextInput, FlatList,ActivityIndicator
} from 'react-native';
import {Spinner, Header, Left, Body, Right, Button, Title, Content} from 'native-base';
import ServerData from '../../connection/connect';
import style from './Style';
import DocumentPicker from 'react-native-document-picker';
import Icon from 'react-native-vector-icons/FontAwesome5';
import LinearGradient from "react-native-linear-gradient";
import ModalDropdown from 'react-native-modal-dropdown';
import {Actions} from "react-native-router-flux";
import {connect} from "react-redux";
import {ProgressDialog} from 'react-native-simple-dialogs';


const width = Dimensions.get('window').width;
const height = Dimensions.get('window').height;

class AboutUs extends Component {
    constructor(props) {
        super(props);
        this.state = {
            info:'',
            loading:true
        };
    }
    async componentDidMount(){
        await this.getContactRequest();
    }

    async getContactRequest(){

        try {
            let formData = new FormData();
            formData.append("admintoken", this.props.global.adminToken);

            let response = await fetch(this.props.global.baseApiUrl + '/user/contact_us_data',
                {
                    method: "POST",
                    body: formData
                });

            //console.log(response);

            if (response.status !== 200) {
                await this.setState({
                    loading:false
                });
            }
            else {

                let json = await response.json();

                console.log(json);

                if(json.success){

                    await this.setState({
                        info:json.data,
                        loading:false
                    });
                }
                else {
                    await this.setState({
                        loading:false
                    });
                }
            }
        } catch (error) {
            await this.setState({
                loading:false
            });
        }
    }
  
    render() {
        return (
            <View style={style.view_container_user}>
             <ProgressDialog visible={this.state.progressVisible} message="لطفا منتظر بمانید..."/>
                <View style={style.view_top}>
                    <LinearGradient style={[style.view_header_user,{height:100}]} start={{x: 0, y: 0}} end={{x: 1, y: 0}}
                                    colors={['#627efb', '#8b68fc', '#a95bfe']}>

                        <Header transparent style={style.view_top_header}>
                            <StatusBar backgroundColor="transparent" translucent={true} barStyle="light-content"/>

                            <Left style={{flex: 1,}}>
                                <Button transparent onPress={() => Actions.pop()}>
                                    <Icon name="arrow-left" color={"#ffffff"} size={width * .07}/>
                                </Button>
                            </Left>
                            <Body style={{flex: 1, alignItems: 'center'}}>
                                <Title style={style.txt_top_header}>معرفی سامانه</Title>
                            </Body>
                            <Right style={{flex: 1}}>
                                <Button transparent onPress={() => Actions.drawerOpen()}>
                                    <Icon name="bars" color={"#fff"} size={width * .07}/>
                                </Button>
                            </Right>
                        </Header>
                    </LinearGradient>
                </View>
                <Content style={{flex:1,width:"100%"}}>
                    <View style={{flex:1,padding:20,backgroundColor:'#fff',margin:20,borderRadius:10}}>
                        {this.state.loading && <ActivityIndicator size="large" color='#a95bfe'/>}
                        {!this.state.loading &&
                            <Text style={{fontFamily:'IRANSansMobile',letterSpacing:2,lineHeight:25,textAlign:'center'}}>{this.state.info.guide_des}</Text>
                        }
                    </View>
                </Content>
            </View>
        );

    }
}


const mapStateToProps = (state) => {
    return {
        user: state.user,
        global: state.global
    };
};
export default connect(mapStateToProps, null)(AboutUs)

