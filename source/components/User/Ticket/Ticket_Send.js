import React, {Component} from 'react';
import {
    Text,
    TouchableOpacity,
    Dimensions,
    View,
    Platform,
    BackHandler,
    Image, StatusBar,
    TouchableNativeFeedback, Alert, TextInput, FlatList
} from 'react-native';
import {Spinner, Header, Left, Body, Right, Button, Title, Content} from 'native-base';
import ServerData from '../../connection/connect';
import style from './Style';
import DocumentPicker from 'react-native-document-picker';
import Icon from 'react-native-vector-icons/FontAwesome5';
import LinearGradient from "react-native-linear-gradient";
import ModalDropdown from 'react-native-modal-dropdown';
import {Actions} from "react-native-router-flux";
import {connect} from "react-redux";
import {ProgressDialog} from 'react-native-simple-dialogs';
import Modal from "react-native-modal";
import Helpers from '../../Shared/Helper'


const width = Dimensions.get('window').width;
const height = Dimensions.get('window').height;
const Connect = new ServerData();
const fileAttach = {
    title: 'فایل خود را انتخاب کنید',
    takePhotoButtonTitle: 'گرفتن عکس',
    chooseFromLibraryButtonTitle: 'انتخاب فایل',
    cancelButtonTitle: 'لغو کردن',
    // customButtons: [{ name: 'fb', title: 'Choose Photo from Facebook' }],
    storageOptions: {
        skipBackup: true,
        path: 'images',
    },
};
class UselessTextInput extends Component {
    render() {
        return (
            <TextInput
                {...this.props} // Inherit any props passed to it; e.g., multiline, numberOfLines below
                editable = {true}
                // maxLength = {40}
            />
        );
    }
}

class Ticket_Send extends Component {
    constructor(props) {
        super(props);
        this.state = {
            subjectUser: '',
            messageUser: '',
            phoneUser: '',
            priorityId:'',
            priorityList:[],
            departmentId:'',
            departmentList:[],
            file_attach_url: '',
            file_attach_type: '',
            file_attach_filename: '',
            file_attach_fileSize: '',
            progressVisible:false,
            modalSubject:false
        }
        // this.btn_sendTicket = this.btn_sendTicket.bind(this);
        this.optionModalDropDownDepartment = this.optionModalDropDownDepartment.bind(this)
        this.optionModalDropDownPriority = this.optionModalDropDownPriority.bind(this);
        this.selectDepartment = this.selectDepartment.bind(this);
        this.selectPriority = this.selectPriority.bind(this);
    };
    componentDidMount(){
        // console.warn(this.props.user.apiToken)
    }
    componentWillMount(){
       
        this.GetDataPrioritiesList();
        this.GetDataDepartmentList();
    }
   async GetDataPrioritiesList(){
        try {
            const details = {
                "admintoken": this.props.global.adminToken,
            };
            let result = await Connect.sendPost(this.props.global.baseApiUrl + '/user/ticket_priorities_List', details);
            if (result.success == true) {
              
            await this.setState({priorityList: result.data})
            } else {
                
            }
           } catch (error) {
            
           }
    }
   async GetDataDepartmentList(){
        try {
            const details = {
                "admintoken": this.props.global.adminToken,
            };
            let result = await Connect.sendPost(this.props.global.baseApiUrl + '/user/ticket_departments_List', details);
            if (result.success == true) {
              
                let item1 = result.data.filter(a => a.parent === 0 && a.category_name !== 'دسته بندی نشده');
                await this.setState({departmentList: result.data,})
            } else {
                
            }
           } catch (error) {
            
           }
    }
   async fileAttach() {
    try {
        const res = await DocumentPicker.pick({
          type: [DocumentPicker.types.allFiles],
        });
       await this.setState({
        file_attach_url: res.uri,
        file_attach_type: res.type,
        file_attach_filename: res.name,
        file_attach_fileSize: res.size
    });
   
      } catch (err) {
        if (DocumentPicker.isCancel(err)) {
          // User cancelled the picker, exit any dialogs or menus and move on
        } else {
          throw err;
        }
      } 
    }
    async btn_sendTicket() {
        let formData = new FormData();
        if(this.props.user.apiToken == null){
            Alert.alert('','برای ارسال تیکت ابتدا ثبت نام کنید',[{text:'باشه'}])
        }
        else if(this.state.subjectUser == ''){
            Alert.alert('','یک موضوع انتخاب کنید',[{text:'باشه'}])
        }
        else if(this.state.messageUser == ''){
            Alert.alert('','پیام خود را وارد نمایید',[{text:'باشه'}])
        }
        else{
            this.setState({progressVisible:true})
            if(this.state.file_attach_filename !== ''){
                if(this.state.file_attach_fileSize > 15000000){
                    Alert.alert('','فایل انتخابی حداکثر باید 15 مگابایت باشد',[{text:'باشه'}])
                }else{
                    formData.append("file", {
                        name: this.state.file_attach_filename,
                        uri: this.state.file_attach_url,
                        type: this.state.file_attach_type.toString()
                    });
                    formData.append("admintoken", this.props.global.adminToken);
                    formData.append("usertoken", this.props.user.apiToken);
                    formData.append("departmentid", "1");
                    formData.append("priorityid", '4');
                    formData.append("subject", this.state.subjectUser);
                    formData.append("message", this.state.messageUser);
                    formData.append("phone", this.state.phoneUser);

                }
                
            }
            else{
                formData.append("admintoken", this.props.global.adminToken);
                formData.append("usertoken", this.props.user.apiToken);
                formData.append("departmentid", "1");
                formData.append("priorityid", '4');
                formData.append("subject", this.state.subjectUser);
                formData.append("message", this.state.messageUser);
                formData.append("phone", this.state.phoneUser);
            }
            console.log(formData)
            try {
                let response = await fetch(this.props.global.baseApiUrl + '/user/Add_New_Ticket', {
                    method: 'post',
                    headers: {
                        'Content-Type': 'multipart/form-data',
                    },
                    body: formData
                });
                let result = await response.json();
                console.log(result)
                if (result.success) {
                    // console.log(result)
                    this.setState({file_attach_url:'', progressVisible: false ,messageUser:'',subjectUser:'',phoneUser:''});
                    Alert.alert('','تیکت با موفقیت ارسال شد',[{text:'باشه'}])
                    Actions.push('myTicketList')            
                }
                    else {
                        // console.log(result)
                        this.setState({ progressVisible: false });
                        Alert.alert('خطا','ارسال تیکت با مشکل مواجه شد',[{text:'باشه'}])
                    }
                }
            catch (error) {
                this.setState({progressVisible:false})
                console.log('error : ' + error);
                return error;
            }
        }
}
    //     this.setState({progressVisible:true})
    //     const details = {
    //         "admintoken": this.props.global.adminToken,
    //         "usertoken":  this.props.user.apiToken,
    //         "departmentid": this.state.departmentId,
    //         "priorityid": this.state.priorityId,
    //         "subject": this.state.subjectUser,
    //         "message": this.state.messageUser,
    //         "phone": this.state.phoneUser,
    //         "file": this.state.file_attach_url
    //     };
    //   let result = await Connect.sendPost(this.props.global.baseApiUrl + '/user/Add_New_Ticket', details)
    
    //             if (result.success === true) {
    //                 this.setState({progressVisible:false})
    //                 Alert.alert('','تیکت با موفقیت ارسال شد',[{text:'باشه'}])
    //             }else{
    //                 this.setState({progressVisible:false})
    //                 Alert.alert('خطا','ارسال تیکت با مشکل مواجه شد',[{text:'باشه'}])
    //             }
            // this.setState({progressVisible:true})
            // let formData = new FormData();
            // formData.append("admintoken", this.props.global.adminToken);
            // formData.append("usertoken", this.props.user.apiToken);
            // formData.append("departmentid", this.state.departmentId);
            // formData.append("priorityid", this.state.priorityId);
            // formData.append("subject", this.state.subjectUser);
            // formData.append("message", this.state.messageUser);
            // formData.append("phone", this.state.phoneUser);
            // formData.append("file", this.state.file_attach_url);
            // if (this.state.file_attach_url != null) {
            //     if (Platform.OS === 'ios') {
            //         formData.append("file", {uri: "file://" + this.state.file_attach_url, name: this.state.file_attach_filename, type: 'file/*'});
            //     } else {
            //         formData.append("file", {uri: "file://" + this.state.file_attach_url, name: '1.jpg', type: 'file/*'});
            //     }
            // }
    
            //  await fetch(this.props.global.baseApiUrl + '/user/Add_New_Ticket',
            //     {
            //         headers: {
            //             'Accept': 'application/json',
            //             'Content-Type': 'multipart/form-data'
            //         },
            //         method: "POST",
            //         body: formData
            //     })
                // this.setState({progressVisible:false})
                // console.log(result.json())
                // .then( (response) => { 
                //     this.setState({progressVisible:false})
                // console.log(JSON.stringify(response))
                        // if (data.success === true) {
                        //     console.log(data)
                        //     this.setState({ progressVisible: false,imageSample:'' });
                        //     Alert.alert('','نمونه کار اضاف شد',[{text:'باشه'}])
                        // }
                        // else {
                        //     console.log(data)
                        //     this.setState({ progressVisible: false });
                        //     Alert.alert('','اضافه کردن نمونه کار با مشکل مواجه شد',[{text:'باشه'}])
                        // }
                // })
  
    optionModalDropDownDepartment(){
        const department = this.state.departmentList;
        let aaa = []
        department.forEach(function(element){
           aaa.push(element.departmentname)
        })
        return aaa
    }
    selectDepartment(item){
        let aaa ='';
        const department = this.state.departmentList;
        department.forEach(function(element){
            if(element.departmentname === item){
                aaa = element.id
            }
        })
        this.setState({departmentId:aaa})
    }
    optionModalDropDownPriority(){
        const priority = this.state.priorityList;
        let aaa = []
        priority.forEach(function(element){
           aaa.push(element.priority)
        })
        return aaa
    }
    selectPriority(item){
        let aaa ='';
        const priority = this.state.priorityList;
        priority.forEach(function(element){
            if(element.priority === item){
                // this.setState({priorityId:element.id})
                aaa = element.id
            }
        })
        this.setState({priorityId:aaa})
    }

    render() {
        const user = this.props.ListUserSendMessage;
        return (
            <View style={[style.view_container_user]}>
             <ProgressDialog visible={this.state.progressVisible} message="لطفا منتظر بمانید..."/>
                <View style={style.view_top}>
                    <LinearGradient style={[style.view_header_user]} start={{x: 0, y: 0}} end={{x: 1, y: 0}}
                                    colors={['#627efb', '#8b68fc', '#a95bfe']}>

                        <Header transparent style={style.view_top_header}>
                            <StatusBar backgroundColor="transparent" translucent={true} barStyle="light-content"/>

                            <Left style={{flex: 1,}}>
                                <Button transparent onPress={() => Actions.pop()}>
                                    <Icon name="arrow-left" color={"#ffffff"} size={width * .07}/>
                                </Button>
                            </Left>
                            <Body style={{flex: 1, alignItems: 'center'}}>
                                <Title style={style.txt_top_header}>ارسال تیکت</Title>
                            </Body>
                            <Right style={{flex: 1}}>
                                <Button transparent onPress={() => Actions.drawerOpen()}>
                                    <Icon name="bars" color={"#fff"} size={width * .07}/>
                                </Button>
                            </Right>
                        </Header>
                    </LinearGradient>
                    {/* <TouchableOpacity activeOpacity={.7} style={style.ViewTicketList}
                    onPress={()=>Actions.push('myTicketList')}
                    >
                        <Text style={[style.txt_message_user1,{color:'#000',fontSize: width / 28}]}>لیست تیکت های من</Text>
                    </TouchableOpacity> */}
                </View>
                <Content style={{width:'100%',padding:20}}>
                    <View style={style.view_send_container}>
                        {/* <View style={[style.view_subject_send_message,{paddingHorizontal:20}]}>
                               <TextInput style={[style.textinput_subject,{fontFamily:'IRANSansMobile'}]}
                                placeholderTextColor={'#000'}
                                onChangeText={(text) => this.setState({subjectUser:text})}
                                 />
                            <Text style={{color:'#8b68fc',fontSize:15,fontFamily:'IRANSansMobile'}}>عنوان :</Text>
                        </View> */}
                        <TouchableOpacity onPress={()=>this.setState({modalSubject:true})} style={[style.view_subject_send_message,{paddingHorizontal:20,alignItems:'flex-end'}]}>
                               <Text style={[style.textinput_subject,{fontFamily:'IRANSansMobile',paddingTop:5,color:'black',width:'70%'}]}
                                 >{this.state.subjectUser}</Text>
                            <Text style={{color:'#8b68fc',fontSize:15,fontFamily:'IRANSansMobile'}}>موضوع :</Text>
                        </TouchableOpacity>
                        {/* <View style={[style.view_subject_send_message,{paddingHorizontal:20}]}>
                            <ModalDropdown
                                defaultValue={'پشتیبانی'}
                                dropdownTextStyle={[style.dropdownStyleTextSkill,]}
                                textStyle={style.modalTextStyleSkill}
                                dropdownStyle={[style.dropDownStyle1,]}
                                style={[style.modalFilterSkill,]}
                                options={this.optionModalDropDownDepartment()}
                                onSelect={(index,item)=> this.selectDepartment(item)}/>
                            <Text style={{color:'#8b68fc',fontSize:15,fontFamily:'IRANSansMobile'}}>دپارتمان :</Text>
                        </View>
                        
                           
                            <ModalDropdown
                            defaultValue={'اولویت'}
                            dropdownTextStyle={style.dropdownStyleTextSkill}
                            textStyle={style.modalTextStyleSkill}
                            dropdownStyle={style.dropDownStyle1}
                            style={style.modalFilterSkill}
                            options={this.optionModalDropDownPriority()}
                            onSelect={(index,item)=> this.selectPriority(item)}/> */}
                       
                        <View style={[style.view_subject_send_message,{paddingHorizontal:20,alignItems:'flex-end'}]}>
                               <TextInput style={[style.textinput_subject,{fontFamily:'IRANSansMobile',width:'70%'}]}
                                placeholderTextColor={'#000'}
                                onChangeText={(text) => this.setState({phoneUser:text})}
                                keyboardType='numeric'
                                maxLength={11}
                                 />
                            <Text style={{color:'#8b68fc',fontSize:15,fontFamily:'IRANSansMobile'}}>شماره تماس ( اختیاری ) :</Text>
                        </View>
                        <View style={[style.view_send_message,{paddingHorizontal:20,marginBottom:20,marginTop:10}]}>
                            <Text style={[style.txt_message_user,{fontSize:15,fontFamily:'IRANSansMobile'}]}>پیام :</Text>

                            <UselessTextInput
                                multiline = {true}
                                numberOfLines = {4}
                                placeholder={'متن پیام  ...'}
                                placeholderTextColor={'#000'}
                                style={[style.textInputMessage,{fontFamily:'IRANSansMobile',flex:1}]}
                                onChangeText={(text) => this.setState({messageUser:text})}
                                value={this.state.text}
                            />
                        </View>
                        <View style={[{height:width*.10,flex:1,paddingHorizontal:20,paddingBottom:5,flexDirection:'row',alignItems:'center',justifyContent:'space-around'}]}>
                            <TouchableOpacity activeOpacity={.7} style={{borderColor:'#00000010',borderWidth:1,borderRadius:width*.02,flexDirection:'row',width:width*.40,height:width*.10,alignItems:'center',justifyContent:'space-around'}}  onPress={()=>this.fileAttach()}>
                                <Text style={[style.txt_message_user,{marginBottom:0,fontFamily:'IRANSansMobile'}]}>فایل ضمیمه</Text>
                                {this.state.file_attach_url !== '' &&
                                    <Image source={require('../../../assets/images/checked.png')} style={{width:width*.05,height:width*.05}} resizeMode="contain"/>
                                }
                                </TouchableOpacity>

                            <TouchableOpacity activeOpacity={.7} style={[style.btnSendMessage,{marginLeft:10}]} onPress={()=> this.btn_sendTicket()}>
                            <Text style={style.txt_message_user1}>ارسال</Text>
                            </TouchableOpacity>
                                                   
                        </View>
                        {this.state.file_attach_url !=='' &&
                                <Text style={{fontFamily:'IRANSansMobile',textAlign:'center',padding:5}}>حجم فایل  : {Helpers.ToPersianNumber(String(this.state.file_attach_fileSize/1000000))} مگابایت</Text>
                            } 
                    </View>
                </Content>
                <Modal isVisible={this.state.modalSubject}
                       onBackButtonPress={() => this.setState({modalSubject: !this.state.modalSubject})}
                       onBackdropPress={() => this.setState({modalSubject: !this.state.modalSubject})}
                       >
                    <View style={{
                        backgroundColor:'#8b68fc',
                        borderRadius:20,                        
                        }}>
                        <TouchableOpacity activeOpacity={.7}
                        style={{
                            backgroundColor:'#00000080',width:width*.10,height:width*.10,position:'absolute',borderRadius:width*.05,zIndex:10,left:0,top:0,justifyContent:'center',alignItems:'center'
                        }}
                        onPress={()=>this.setState({modalSubject:false})}>
                            <Icon name="times-circle" color={'#fff'} size={width * .06}/>
                        </TouchableOpacity>
                        <View>
                            <TouchableOpacity onPress={()=>this.setState({modalSubject:false,subjectUser:'مشکل فنی'})} style={style.view_input_ItemEditSkill}>
                                <Text style={style.username_inputSkill}>مشکل فنی</Text>
                            </TouchableOpacity>
                            <TouchableOpacity onPress={()=>this.setState({modalSubject:false,subjectUser:'تبلیغات'})} style={style.view_input_ItemEditSkill}>
                                <Text style={style.username_inputSkill}>تبلیغات</Text>
                            </TouchableOpacity>
                            <TouchableOpacity onPress={()=>this.setState({modalSubject:false,subjectUser:'ارسال محتوا(فیلم و عکس)'})} style={style.view_input_ItemEditSkill}>
                                <Text style={style.username_inputSkill}>ارسال محتوا(فیلم و عکس)</Text>
                            </TouchableOpacity>
                            <TouchableOpacity onPress={()=>this.setState({modalSubject:false,subjectUser:'درخواست مشاوره و ارتباط'})} style={style.view_input_ItemEditSkill}>
                                <Text style={style.username_inputSkill}>درخواست مشاوره و ارتباط</Text>
                            </TouchableOpacity>
                        </View>
                    </View>
                </Modal>
            </View>
        );

    }
}


const mapStateToProps = (state) => {
    return {
        user: state.user,
        global: state.global
    };
};
export default connect(mapStateToProps, null)(Ticket_Send)

