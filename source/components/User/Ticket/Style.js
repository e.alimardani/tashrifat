import React from 'react';
import {StyleSheet,Platform, PixelRatio, Dimensions, StatusBar} from 'react-native';


const width = Dimensions.get('window').width;
const height = Dimensions.get('window').height;

export default StyleSheet.create({
    header : {
        backgroundColor : 'transparent',
        borderBottomWidth:0,
        elevation: 0,
        ...Platform.select({
            ios: {

            },
            android: {
                marginTop:StatusBar.currentHeight
            }
        }),
    },
    TextStyle:{
        fontFamily:'IRANSansMobile'
    },
    username_inputSkill: {
        color: 'white',
        fontSize: width / 25,
        marginBottom: height * .01,
        textAlign:'center',
        fontFamily:'IRANSansMobile'
    },
    view_input_ItemEditSkill: {marginTop: height * .01, height: height * .07 + 2,
        width: width * .70,alignItems:'center',justifyContent:'center',alignSelf:'center',borderBottomColor:'white',borderBottomWidth:.5},
    backButton:{
        color:'#ffffff',
        fontSize:30,
    },
    backButtonText:{
        textAlign:'right',
        writingDirection:'rtl',
        fontSize:12,
        color:'#ffffff',
        ...Platform.select({
            ios: {
                fontFamily: 'IRANSansMobile',
                fontWeight: 'bold'
            },
            android: {
                fontFamily: 'IRANSansMobile'
            }
        }),
        width:50,
        marginTop:-20,
        backgroundColor:'transparent'
    },
    basketButtonBadge:{
        position: 'absolute',
        width:18,
        height:18,
        borderRadius:9,
        backgroundColor:'#ffb941',
        justifyContent:'center',
        alignItems:'center',
        bottom:0,
        right:-5,
        ...Platform.select({
            ios: {
                paddingTop: 2.5
            },
        }),
    },
    basketButtonBadgeText:{
        textAlign:'center',
        writingDirection:'rtl',
        fontSize:10,
        //lineHeight:12,
        color:'#ffffff',
        ...Platform.select({
            ios: {
                fontFamily: 'IRANSansMobile',
                fontWeight: 'normal'
            },
            android: {
                fontFamily: 'IRANSansMobile',
            }
        }),
    },
    headerRightText:{
        textAlign:'right',
        writingDirection:'rtl',
        fontSize:16,
        color:'#f5f4e8',
        ...Platform.select({
            ios: {
                fontFamily: 'IRANSansMobile',
                fontWeight: 'bold',
                //paddingTop:3
            },
            android: {
                fontFamily: 'IRANSansMobile'
            }
        }),
        backgroundColor:'transparent'
    },
    mainContainer : {
        backgroundColor:'#fff',
        flex: 1
    },
    mainBackground:{
        position: 'absolute',
        width:Dimensions.get('window').width,
        height:Dimensions.get('window').height,
        //opacity:0.7,
    },
    drawerHeader:{
        backgroundColor:'transparent',
        paddingVertical: 56.5,
        paddingRight: 20
    },
    drawerHeaderText:{
        textAlign:'right',
        writingDirection:'rtl',
        fontSize:20,
        color:'#f5f4e8',
        ...Platform.select({
            ios: {
                fontFamily: 'IRANSansMobile',
                fontWeight: 'bold',
                //paddingTop:3
            },
            android: {
                fontFamily: 'IRANSansMobile'
            }
        }),
        backgroundColor:'transparent'
    },
    drawerTitleRow:{
        flexDirection: 'row-reverse',
        justifyContent: 'space-between',
        alignItems: 'center',
        paddingVertical: 15,
        paddingRight: 20,
        borderBottomWidth: 5/PixelRatio.get(),
        borderBottomColor:'#f4f7f9',
        marginLeft: 15
    },
    drawerTitleRowText:{
        textAlign:'right',
        writingDirection:'rtl',
        fontSize:14,
        color:'#393939',
        ...Platform.select({
            ios: {
                fontFamily: 'IRANSansMobile',
                fontWeight: 'bold',
                //paddingTop:3
            },
            android: {
                fontFamily: 'IRANSansMobile'
            }
        }),
        backgroundColor:'transparent'
    },
    drawerTitleRowTextExpanded:{
        textAlign:'right',
        writingDirection:'rtl',
        fontSize:14,
        color:'black',
        ...Platform.select({
            ios: {
                fontFamily: 'IRANSansMobile',
                fontWeight: 'bold',
                //paddingTop:3
            },
            android: {
                fontFamily: 'IRANSansMobile'
            }
        }),
        backgroundColor:'transparent'
    },
    drawerTitleRowExpandedIcon:{
        fontSize:22,
        color:'black'
    },
    drawerTitleRowUnexpandedIcon:{
        fontSize:22,
        color:'#dadadb'
    },
    drawerContent:{
        padding: 10,
        borderWidth: 5/PixelRatio.get(),
        borderColor:'#f4f7f9',
        //backgroundColor:'#cccccc',
        margin: 15,
        marginTop: 0,
        borderRadius: 15
    },
    drawerContentRow:{
        flexDirection: 'row-reverse',
        alignItems:'center',
        borderBottomWidth: 5/PixelRatio.get(),
        borderBottomColor:'#f4f7f9',
    },
    drawerContentIcon:{
        fontSize:18,
        color:'black'
    },
    drawerContentText:{
        textAlign:'right',
        writingDirection:'rtl',
        fontSize:12,
        color:'#393939',
        ...Platform.select({
            ios: {
                fontFamily: 'IRANSansMobile',
                fontWeight: 'normal',
                //paddingTop:3
            },
            android: {
                fontFamily: 'IRANSansMobile'
            }
        }),
        backgroundColor:'transparent',
        marginRight: 10,
        paddingVertical:10,
    },
    requestModalContainer:{
        width:'80%',
        minHeight:200,
        backgroundColor:'#ffffff',
        alignSelf:'center',
        borderRadius:5,
        justifyContent:'center',
        alignItems:'center',
        paddingVertical: 10
    },
    requestModalText:{
        textAlign:'center',
        writingDirection:'rtl',
        fontSize:12,
        color:'#333333',
        ...Platform.select({
            ios: {
                fontFamily: 'IRANSansMobile',
                fontWeight: 'bold',
                paddingTop:2
            },
            android: {
                fontFamily: 'IRANSansMobile'
            }
        }),
        lineHeight:20,
        paddingHorizontal:10,
        marginTop:10
    },
    loadingModalContainer:{
        backgroundColor:'#ffffff',
        alignSelf:'center',
        borderRadius:5,
        justifyContent:'center',
        alignItems:'center',
        ...Platform.select({
            ios: {
                padding:10,
                paddingLeft:13,
                paddingTop:13
            },
            android: {
                padding:10
            }
        }),
    },
    notFoundText:{
        textAlign:'center',
        writingDirection:'rtl',
        fontSize:14,
        color:'#777777' ,
        ...Platform.select({
            ios: {
                fontFamily: 'IRANSansMobile',
                fontWeight: 'bold',
                //paddingTop:3
            },
            android: {
                fontFamily: 'IRANSansMobile',
            }
        }),
        marginTop:Dimensions.get('window').height * .1
    },
    notifModalContainer:{
        width:'90%',
        //height:'90%',
        backgroundColor:'#ffffff',
        alignSelf:'center',
        justifyContent:'center',
        alignItems:'center',
        borderRadius:5,
        paddingVertical:15,
        paddingHorizontal:10
    },
    modalColorGrButton:{
        alignSelf:'center',
        backgroundColor:'green',
        width:null,
        height:30,
        justifyContent:'center',
        alignItems:'center',
        borderRadius:15,
        ...Platform.select({
            ios: {
                shadowColor: 'black',
                shadowOffset: { width: 0, height: 0 },
                shadowOpacity: 0.6,
                shadowRadius: 7,
            },
            android: {
                elevation: 2
            }
        }),
    },
    modalGrButtonGradient:{
        width:null,
        height:30,
        borderRadius:15,
        paddingHorizontal:30,
        justifyContent:'center',
        alignItems:'center',
    },
    modalGrButtonText:{
        textAlign:'center',
        writingDirection:'rtl',
        fontSize:12,
        //lineHeight:14,
        color:'black',
        ...Platform.select({
            ios: {
                fontFamily: 'IRANSansMobile',
                fontWeight: 'bold'
            },
            android: {
                fontFamily: 'IRANSansMobile',
            }
        }),
    },
    contactName:{
        textAlign:'center',
        writingDirection:'rtl',
        fontSize:14,
        marginTop:10,
        color:'#393939',
        ...Platform.select({
            ios: {
                fontFamily: 'IRANSansMobile',
                fontWeight: 'bold'
            },
            android: {
                fontFamily: 'IRANSansMobile',
            }
        }),
    },
    contactValues:{
        textAlign:'center',
        writingDirection:'rtl',
        fontSize:12,
        color:'#393939',
        ...Platform.select({
            ios: {
                fontFamily: 'IRANSansMobile',
                fontWeight: 'normal'
            },
            android: {
                fontFamily: 'IRANSansMobile',
            }
        }),
    },
    textInput:{
        textAlign:'right',
        writingDirection:'rtl',
        fontSize:14,
        color:'#393939' ,
        paddingVertical:2,
        paddingHorizontal:10,
        ...Platform.select({
            ios: {
                fontFamily: 'IRANSansMobile',
                fontWeight: 'normal',
                //paddingTop:10
            },
            android: {
                fontFamily: 'IRANSansMobile'
            }
        }),
        width: '100%',
        borderRadius:10,
        marginTop:10,
        height:50,
        backgroundColor:'#fff',
    },
    view_container_user: {
        flex: 1
    },
    message_container: {
        flex: 1,
        flexDirection: 'row',
    },
    view_name:{paddingBottom:height*.02},
    txt_container: {
        flex: 1,
        // width:width*.90,
        flexDirection: 'column',
        justifyContent: 'space-between',
        alignItems: 'flex-end',
        backgroundColor: '#fff',
        borderRadius: width * .03,
        marginVertical: 10,
        paddingHorizontal: width * .04,
        paddingVertical: height * .01,
        marginHorizontal:20
    },
    txt_message:{fontFamily: 'IRANSansMobile',
        fontSize: width / 28,},
    view_txt_message:{paddingVertical: 5},
    date_txt:{
        alignSelf:'flex-start'
    },
    pic_member: {
        width: width * .15,
        height: width * .15,
        borderRadius: width * .07,
        marginHorizontal:width*.02,
        marginVertical: height*.01

    },
    view_item_search: {
        justifyContent: 'center',
        alignItems: 'center',
        width: width * .70,
        height: height * .07,
        backgroundColor: '#fff',
        marginVertical: height * .003,
        borderTopRightRadius: width * .03,
        borderBottomLeftRadius: width * .03
    },
    viewNotFound:{width: width * .90, height: height * .60, justifyContent: 'center', alignItems: 'center',},
    txtNotFound:{fontSize: width / 20, color: '#000', fontFamily: 'IRANSansMobile',textAlign:'center',paddingBottom:10},
    txt_item_search: {
        fontFamily: 'IRANSansMobile',
        color: '#8b68fc',
        fontSize: width / 25
    },
    ViewTicketList:{width:width*.45,height:width*.10,borderRadius:width*.02,position:'absolute',top:width* .25,backgroundColor:'#fff',elevation:3,justifyContent:'center',alignItems:'center'},
    view_top: {justifyContent: 'center', alignItems: 'center'},
    view_header_user: {
        height: height / 4,
        width: width / 1,
        // backgroundColor:'#000'
    },
    view_top_header: {
        marginTop: height * .01,
        paddingHorizontal: width * .05,
        width: width,
        // flex:1
    },
    txt_top_header: {
        // flex:1,
        color: '#fff',
        fontFamily: 'IRANSansMobile',
        textAlign: 'center',
        fontSize: width / 25
    },
    view_search: {
        height: height * .08,
        width: width * .85,
        marginTop: height * .01,
        flexDirection: 'row',
        borderRadius: width * .07,
        backgroundColor: 'rgba(0,0,0,0.1)',
        borderColor: '#fff',
        borderWidth: width * .001,
        alignSelf: 'center'
    },
    input_search: {
        height: height * .08,
        width: width * .50,
        justifyContent: 'center'
        // borderBottomRightRadius: width * .07,
        // borderTopRightRadius: width * .07,
        // backgroundColor:'#000'
    },
    textinput_search: {
        textAlign: 'right',
        fontSize: width / 28,
        fontFamily: 'IRANSansMobile',
        paddingHorizontal: width * .03,
        height: height * .07,
        width: width * .50,
        color: '#e8e8e8',
        // borderBottomRightRadius: width * .07,
        // borderTopRightRadius: width * .07,
        // backgroundColor: '#5b3862'
    },
    modalFilter: {
        justifyContent: 'center',
        height: height * .07 + 2,
        width: width * .25,
        paddingRight: width * .02,
        backgroundColor: '#00000050',
        borderBottomRightRadius: width * .07,
        borderTopRightRadius: width * .07,
        alignSelf: 'center'
    },
    modalTextStyle: {color: '#fff', fontFamily: 'IRANSansMobile', fontSize: width / 30, textAlign: 'center'},
    dropdownStyleText: {
        color: '#2b2b2b',
        width: width * .25,
        fontSize: width / 25,
    },
    icon_search: {
        alignItems: 'center',
        justifyContent: 'center',
        height: height * .08,
        width: width * .10,
        // backgroundColor:'#ff31fe',
        borderBottomLeftRadius: width * .07,
        borderTopLeftRadius: width * .07
    },
    center_page_users: {flex: 1,},
    ModalSearchUserStyle: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'flex-end',
        margin: 0,
        padding: 0,
        // width:width,
        height: height * 65,
        // backgroundColor:'#000',
        borderTopLeftRadius: width * .15,
        borderTopRightRadius: width * .15,
    },
    viewContainerModalSearch: {
        backgroundColor: '#8b68fc',
        height: height * .65,
        width: width * .90,
        // paddingVertical: height * .02,
        borderTopLeftRadius: width * .12,
        borderTopRightRadius: width * .12,
        justifyContent: 'flex-end', alignItems: 'center',
    },
    viewHeaderModalSearch: {
        width: width * .90,
        height: height * .06,
        elevation: 2,
        backgroundColor: '#fff',
        borderTopLeftRadius: width * .15,
        borderTopRightRadius: width * .15,
        // justifyContent: 'center',
        alignItems: 'center',
        flexDirection:'row'
    },
    txtHeaderModalSearch: {fontSize: width / 27,flex:1,textAlign:'center', fontFamily: 'IRANSansMobile',},
    imgIconModal:{width:width*.05,height:width*.05},
    btnExitModal: {
        // backgroundColor: '#00000090',
        width: width * .08,
        height: width * .08,
        // position: 'absolute',
        borderRadius: width * .04,
        justifyContent: 'center',
        alignItems: 'center',
        marginHorizontal:width*.05
    },
    btnBackModal: {
        backgroundColor: '#00000090',
        width: width * .08,
        height: width * .08,
        position: 'absolute',
        borderRadius: width * .04,
        zIndex: 10,
        left: 0,
        top: 0,
        justifyContent: 'center',
        alignItems: 'center'
    },
    view_user: {
        // flexDirection: 'row',
        width: width * .70,
        height: height * .12,
        paddingHorizontal: width * .03,
        marginTop: height * .02,
        backgroundColor: '#fff',
        justifyContent: 'center',
        alignItems: 'flex-end',
        borderRadius: width * .02
    },
    view_user_icon: {alignItems: 'center', flexDirection: 'row', justifyContent: 'center'},
    view_user_txt: {
        flex: 1,
        flexDirection: 'column',
        justifyContent: 'space-between',
        alignItems: 'flex-end',
        backgroundColor: '#fff',
        marginLeft: width * .03,
        borderRadius: width * .03,
        marginVertical: height * .01,
        paddingHorizontal: width * .04,
        paddingVertical: height * .01
    },
    txt_name_user: {fontSize: width / 27, fontFamily: 'IRANSansMobile',},
    view_img: {
        justifyContent: 'flex-end',
        alignItems: 'center',
        borderWidth: width * .01,
        borderRadius: width * .07,
        borderColor: '#d1d1d1'
    },
    pic_user: {
        width: width * .12,
        height: width * .12,
        borderRadius: width * .06,
        // marginTop: height * .08
    },
    view_folow: {
        justifyContent: 'center',
        alignItems: 'center',
        width: width * .10,
        height: width * .10,
        borderRadius: width * .05,
        backgroundColor: '#8b68fc',
        marginRight: width * .02
    },
    txt_flow: {
        color: '#7e7e7e',
        fontFamily: 'IRANSansMobile',
        fontSize: width / 30,
    },
    view_top: {justifyContent: 'center', alignItems: 'center'},
    view_header_user: {
        height: height / 6,
        width: width / 1,
        // backgroundColor:'#000'
    },
    center_page_users: {flex: 1,marginTop:width*.02, alignItems: 'center'},
    txt_top_header: {
        // flex:1,
        color: '#fff',
        fontFamily: 'IRANSansMobile',
        textAlign: 'center',
        fontSize: width / 25
    },
    view_top_header: {
        marginTop: height * .01,
        paddingHorizontal: width * .05,
        width: width,
        // flex:1
    },
    view_send_container: {
        elevation: 1,
        backgroundColor: '#fff',
        borderRadius: width * .03,
        marginBottom:40,
        marginTop:10,
        paddingBottom:10
    },
    view_subject_send_message: {
        marginTop:10,
        flex:1,
        flexDirection: 'column-reverse',
    },
    view_send_message: {flex:1},
    textinput_subject: {
        fontSize:14,
        flex:1,
        borderRadius: width * .03,
        borderWidth: 1,
        borderColor: '#00000020',
        textAlign: 'right',
        height:40,
        padding:0,
        paddingRight:10
    },
    dropdownStyleTextSkill: {
        color: '#fff',
        // width: width * .25,
        fontSize: 13,
        alignItems:'center',
        height: 40,
        backgroundColor: '#8b68fc',
        // marginTop: height * .01,
        alignSelf: 'center',
        textAlign: 'center',
        width:'100%',
        fontFamily: 'IRANSansMobile',
    },
    modalFilterSkill: {
        justifyContent: 'center',
        flex:1,
        width:'60%',
        height: 40,
        backgroundColor: '#fff',
        // elevation:4,
        borderWidth:1,
        borderColor:'#00000020',
        borderRadius: width * .02,
        alignSelf: 'flex-end'
    },
    modalTextStyleSkill: {color: 'black', fontFamily: 'IRANSansMobile', fontSize: 14, textAlign: 'center'},
    textInputMessage: {
        borderRadius: width * .03,
        borderColor: '#999',
        textAlignVertical: 'top',
        justifyContent: 'flex-start',
        borderWidth: width * .001,
        textAlign: 'right',
        height: height * .25,
        lineHeight:height*.04,
        fontFamily: 'IRANSansMobile'
    },
    txt_message_user: {fontSize: width / 25, marginBottom: height * .01,color: '#8b68fc', fontFamily: 'IRANSansMobile'},
    txt_message_user1: {fontSize: width / 25,color:'#fff', fontFamily: 'IRANSansMobile'},
    txt_message_user2: {fontSize: width / 25, marginVertical: height * .01, fontFamily: 'IRANSansMobile'},
    btnSendMessage: {
        width: width * .30, marginVertical: height * .03, backgroundColor: '#8b68fc',
    
        height: width * .10, borderRadius: width * .02, elevation: 1, justifyContent: 'center', alignItems: 'center'
    },
    dropDownStyle1:{
        justifyContent:'center',
        alignSelf:'center',
        width:'40%',
        borderRadius:10,
        borderWidth:0,
        marginTop:-10
}

});

