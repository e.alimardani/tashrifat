import React, {Component} from 'react';
import {
    Text,
    TouchableOpacity,
    Dimensions,
    View,
    // AsyncStorage,
    BackHandler,
    Image, StatusBar,
    ActivityIndicator,
    TouchableNativeFeedback, Alert, TextInput, FlatList
} from 'react-native';
import AsyncStorage from '@react-native-community/async-storage';
import {Spinner, Header, Left, Body, Right, Button, Title} from 'native-base';
import ServerData from '../../connection/connect';
import style from './Style';
import Icon from 'react-native-vector-icons/FontAwesome5';
import LinearGradient from "react-native-linear-gradient";
import {Actions} from "react-native-router-flux";
import {setUser} from "../../../Redux/Actions";
import {connect} from "react-redux";
import moment from 'jalali-moment';
import Helpers from '../../Shared/Helper';


const width = Dimensions.get('window').width;
const height = Dimensions.get('window').height;
const Connect = new ServerData();

class MyTicketList extends Component {

    constructor(props) {
        super(props);
        this.state = {
            search_users: '',
            page: 1,
            loading: false,
            refreshing: false,
            loadingTicket:false,
            TicketList: [],
    
        };
        this.onEndReachedCalledDuringMomentum = true;
        this.renderItemTicketList = this.renderItemTicketList.bind(this);
        this.EmptyComponent = this.EmptyComponent.bind(this);
        this.onRefresh = this.onRefresh.bind(this);
        this.handleLoadMore = this.handleLoadMore.bind(this);
        this.renderFooter = this.renderFooter.bind(this);
        this.GetDataMyTicketList = this.GetDataMyTicketList.bind(this);
    };
    componentWillMount(){
        this.GetDataMyTicketList()

    }

  async GetDataMyTicketList() {

    this.setState({loading:true})
    
        try {
            const {page} = this.state;
            let formData = new FormData();
            formData.append("admintoken", this.props.global.adminToken);
            formData.append("usertoken", this.props.user.apiToken);
            // formData.append("page", page);
            // formData.append("perpage", 10);

            console.log(formData)

            let response = await fetch(this.props.global.baseApiUrl + '/user/My_Ticket_list',
                {
                    method: "POST",
                    body: formData,
                });
                let json = await response.json();
                console.log(json)
                if (json.success == true) {
                    this.setState({loading: false,refreshing: false})
                    let TicketList = json.data;
                    if (TicketList.length > 0) {
                        this.setState(prevState => {
                            return {TicketList: page === 1 ? TicketList : [...prevState.TicketList, ...TicketList],      
                            }
                        });
                    }else{
                        this.setState({loading: false, refreshing: false});
                    }
                
                } else {
                    this.setState({loading: false, refreshing: false});
                }

        } catch (error) {
            this.setState({loading: false, refreshing: false});
        }
    }
    renderItemTicketList = ({item, index}) => {   
        return (
            <TouchableOpacity style={[style.view_user,{width:width*.90}]} onPress={() => Actions.push('ReplayTicket',{TicketItem:item})}>
           {/* <View style={{position:'absolute',alignSelf:'flex-start',borderWidth:1,borderColor:'#00000050',marginLeft:10}}>
            <Text style={{paddingHorizontal:20,paddingVertical:8}}>باز</Text>
            </View> */}
                <View style={style.view_user_txt}>
                    <Text style={style.txt_name_user}>{item.subject}</Text>
                    <Text style={style.txt_name_user}>{Helpers.ToPersianNumber(moment(item.created, 'YYYY-MM-DD').locale('fa').format('DD MMMM YYYY'))}</Text>
                </View>   
            </TouchableOpacity>
        );     
    };
    EmptyComponent() {    
            return(
             <View style={style.viewNotFound}>
                <Text style={style.txtNotFound}>تیکتی وجود
                    ندارد</Text>
            </View>
            );
    }
    onRefresh() {
        this.setState({page: 1, refreshing: true,}, async () => {
            this.GetDataMyTicketList();
        })
    }
   async handleLoadMore() {
        if(!this.onEndReachedCalledDuringMomentum){
           await this.setState({page: this.state.page + 1})
            this.GetDataMyTicketList();
            this.onEndReachedCalledDuringMomentum = true;
        }

    }

    renderFooter() {
        if(this.state.loading && this.state.TicketList.length > 0)
        {
            return <ActivityIndicator style={{alignSelf: 'center',marginBottom: 20}} color={this.props.global.grColorTwo} />
        }
        else {
            return null;
        }
    }

    render() {
        return (
            <View style={style.view_container_user}>
                <View style={style.view_top}>
                    <LinearGradient style={[style.view_header_user]} start={{x: 0, y: 0}} end={{x: 1, y: 0}}
                                    colors={['#627efb', '#8b68fc', '#a95bfe']}>

                        <Header transparent style={style.view_top_header}>
                            <StatusBar backgroundColor="transparent" translucent={true} barStyle="light-content"/>

                            <Left style={{flex: 1,}}>
                                <Button transparent onPress={() => Actions.pop()}>
                                    <Icon name="arrow-left" color={"#ffffff"} size={width * .07}/>
                                </Button>
                            </Left>
                            <Body style={{flex: 1, alignItems: 'center'}}>
                                <Title style={style.txt_top_header}>لیست تیکت ها</Title>
                            </Body>
                            <Right style={{flex: 1}}>
                                <Button transparent onPress={() => Actions.drawerOpen()}>
                                    <Icon name="bars" color={"#fff"} size={width * .07}/>
                                </Button>
                            </Right>
                        </Header>
                    </LinearGradient>
                </View>

                <View style={[style.center_page_users,{paddingBottom:10}]}>
                 {(!this.state.loading)  && 
                    <FlatList
                    
                     keyExtractor={(item, index) => index.toString()}
                              data={this.state.TicketList
                                  // .filter(item => item.first_name.includes(this.state.search_users))
                              }
                              renderItem={this.renderItemTicketList}
                              ListEmptyComponent={this.EmptyComponent}
                            //   onEndReached={this.handleLoadMore}
                            //   onEndReachedThreshold={0.5}
                              refreshing={this.state.refreshing}
                              onRefresh={this.onRefresh}
                            //   ListFooterComponent={this.renderFooter}
                            //   onMomentumScrollBegin={() => { this.onEndReachedCalledDuringMomentum = false; }}
                    />
                 }
                 {(this.state.loading) &&
                     <View style={{ height : Dimensions.get('window').height * .2  , justifyContent:'center',alignItems:'center' }}>
                        <ActivityIndicator color={this.props.global.grColorTwo} size='large'/>
                    </View>}
                </View>
                <TouchableOpacity onPress={()=>Actions.push('ticketSend')} style={{backgroundColor:'#8b68fc',padding:10,position:'absolute',left:30,bottom:30,borderRadius:30,alignItems:'center'}}>
                    <Text style={{color:'white',fontSize:16,textAlign:'center',fontFamily: 'IRANSansMobile'}}>
                    ارسال تیکت</Text>
                </TouchableOpacity>
            </View>
        );
    }
}

const mapDispatchToProps = dispatch => {
    return {
        setUser: user => {
            dispatch(setUser(user))
        }
    }
};
const mapStateToProps = (state) => {
    return {
        user: state.user,
        global: state.global
    };
};
export default connect(mapStateToProps, mapDispatchToProps)(MyTicketList)

