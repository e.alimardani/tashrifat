import React, {Component} from 'react';
import {
    Text,
    TouchableOpacity,
    Dimensions,
    View,
    Platform,
    StyleSheet,
    Image, StatusBar,
    ActivityIndicator, Alert, TextInput, FlatList,Linking
} from 'react-native';
import {Spinner, Header, Left, Body, Right, Button, Title, Content} from 'native-base';
import ServerData from '../../connection/connect';
import style from './Style';
import DocumentPicker from 'react-native-document-picker';
import Icon from 'react-native-vector-icons/FontAwesome5';
import LinearGradient from "react-native-linear-gradient";
import {Actions} from "react-native-router-flux";
import {connect} from "react-redux";
import {ProgressDialog} from 'react-native-simple-dialogs';
import moment from 'jalali-moment';
import Helpers from '../../Shared/Helper';

const width = Dimensions.get('window').width;
const height = Dimensions.get('window').height;
const Connect = new ServerData();
const fileAttach = {
    title: 'فایل خود را انتخاب کنید',
    takePhotoButtonTitle: 'گرفتن عکس',
    chooseFromLibraryButtonTitle: 'انتخاب فایل',
    cancelButtonTitle: 'لغو کردن',
    // customButtons: [{ name: 'fb', title: 'Choose Photo from Facebook' }],
    storageOptions: {
        skipBackup: true,
        path: 'images',
    },
};
class UselessTextInput extends Component {
    render() {
        return (
            <TextInput
                {...this.props} // Inherit any props passed to it; e.g., multiline, numberOfLines below
                editable = {true}
                // maxLength = {40}
            />
        );
    }
}

class ReplayTicket extends Component {

    constructor(props) {
        super(props);
        this.state = {
            messageUser: '',
            loading:false,
            ReplayTicketList:[],
            file_attach_url: '',
            file_attach_type: '',
            file_attach_filename: '',
            file_attach_fileSize: '',
            progressVisible:false
        };

        this.btn_ReplayTicket = this.btn_ReplayTicket.bind(this);
        this.EmptyComponent = this.EmptyComponent.bind(this);
        this.GetDataReplayTicket = this.GetDataReplayTicket.bind(this);
        this.fileAttach = this.fileAttach.bind(this);
      
    };
 
    componentWillMount(){  
        this.GetDataReplayTicket();
    }
   async GetDataReplayTicket(){
    //    alert( this.props.TicketItem.id)
       this.setState({ReplayTicketList:'',loading:true})
        try {
            const details = {
                "admintoken": this.props.global.adminToken,
                "usertoken": this.props.user.apiToken,
                "ticket_id": this.props.TicketItem.id,
            };
          let result = await Connect.sendPost(this.props.global.baseApiUrl + '/user/GetTicket_Replies_By_TicketId', details)
          console.log(result)          
          if (result.success === true) {
                        // console.warn(result.data.replies_list)
                       this.setState({loading: false,ReplayTicketList:result.data[0].replies_list});
                    } else {
                        this.setState({loading: false});
                    }
           
            } catch (error) {
                this.setState({loading: false});
            }
  }
   async fileAttach() {
    try {
        const res = await DocumentPicker.pick({
          type: [DocumentPicker.types.allFiles],
        });
       await this.setState({
        file_attach_url: res.uri,
        file_attach_type: res.type,
        file_attach_filename: res.name,
        file_attach_fileSize: res.size
    });
        // console.log(
        //   res.uri,
        //   res.type, // mime type
        //   res.name,
        //   res.size
        // );
      } catch (err) {
        if (DocumentPicker.isCancel(err)) {
          // User cancelled the picker, exit any dialogs or menus and move on
        } else {
          throw err;
        }
      } 
    }
    async btn_ReplayTicket() {
        this.setState({progressVisible:true})
        let formData = new FormData();
        if(this.state.file_attach_filename !== ''){
            if(this.state.file_attach_fileSize > 15000000){
                Alert.alert('','فایل انتخابی حداکثر باید 15 مگابایت باشد',[{text:'باشه'}])
            }else{
                formData.append("file", {
                    name: this.state.file_attach_filename,
                    uri: this.state.file_attach_url,
                    type: this.state.file_attach_type.toString()
                });
                formData.append("admintoken", this.props.global.adminToken);
                formData.append("usertoken", this.props.user.apiToken);
                formData.append("ticketid", this.props.TicketItem.id);
                formData.append("message", this.state.messageUser);
            }
    }else{
        formData.append("admintoken", this.props.global.adminToken);
        formData.append("usertoken", this.props.user.apiToken);
        formData.append("ticketid", this.props.TicketItem.id);
        formData.append("message", this.state.messageUser);
    }
    console.log(formData)
    try {
        let response = await fetch(this.props.global.baseApiUrl + '/user/Add_New_Reply', {
            method: 'post',
            headers: {
                'Content-Type': 'multipart/form-data',
            },
            body: formData
        });
        let result = await response.json();
        console.log(result)

        if (result.success === true) {
            this.setState({file_attach_url:'',progressVisible: false,file_attach_fileSize:'',messageUser:''});
            Alert.alert('','پیام با موفقیت ارسال شد',[{text:'باشه',onPress:()=> this.GetDataReplayTicket()}])
            }
            else {
                // console.log(result)
                this.setState({ progressVisible: false });
                Alert.alert('خطا','ارسال پیام با مشکل مواجه شد',[{text:'باشه'}])
            }
        }
    catch (error) {
        this.setState({progressVisible:false})
        console.log('error : ' + error);
        return error;
    }  
}
    renderItemReplayTicketList = ({item, index}) => {
        if (item.length !== 0) {
            console.log(item)
            return (
                <View style={style.message_container}>
                    <View style={style.txt_container}>
                        {/* <View>
                            <Text style={style.txt_message}>موضوع : {item.subject}</Text>
                        </View> */}
                        <View style={style.view_txt_message}>
                            <Text style={style.txt_message}>متن پیام : {item.message}</Text>
                        </View>
                        <View style={style.date_txt}>
                            <Text style={style.txt_message}>{Helpers.ToPersianNumber(moment(item.created, 'YYYY-MM-DD HH:mm').locale('fa').format('DD MMMM YYYY | HH:mm'))}</Text>
                        </View>
                        {item.attachments.length > 0 &&
                            <TouchableOpacity onPress={()=>Linking.openURL(item.attachments[0].file)} style={[style.date_txt]}>
                                <Text style={[style.txt_message]}>مشاهده فایل</Text>
                            </TouchableOpacity>
                            }
                    </View>
                
                </View>
            );
        }
    };
    EmptyComponent() {
        if(this.state.loading)
        {
            return(
                <ActivityIndicator color={this.props.global.grColorTwo} size='large' />
            );
        }
        else
        {
            return(
            //  <View style={style.viewNotFound}>
                <Text style={[style.txtNotFound]}>جوابی وجود
                    ندارد</Text>
            // </View>
            );
        }   
    }
    render() {
        const user = this.props.ListUserSendMessage;
        return (
            <View style={style.view_container_user}>
             <ProgressDialog visible={this.state.progressVisible} message="لطفا منتظر بمانید..."/>
                <View style={style.view_top}>
                    <LinearGradient style={[style.view_header_user]} start={{x: 0, y: 0}} end={{x: 1, y: 0}}
                                    colors={['#627efb', '#8b68fc', '#a95bfe']}>

                        <Header transparent style={style.view_top_header}>
                            <StatusBar backgroundColor="transparent" translucent={true} barStyle="light-content"/>

                            <Left style={{flex: 1,}}>
                                <Button transparent onPress={() => Actions.pop()}>
                                    <Icon name="arrow-left" color={"#ffffff"} size={width * .07}/>
                                </Button>
                            </Left>
                            <Body style={{flex: 1, alignItems: 'center'}}>
                                <Title style={style.txt_top_header}>پیام های تیکت</Title>
                            </Body>
                            <Right style={{flex: 1}}>
                                <Button transparent onPress={() => Actions.drawerOpen()}>
                                    <Icon name="bars" color={"#fff"} size={width * .07}/>
                                </Button>
                            </Right>
                        </Header>
                    </LinearGradient>

                </View>
            <Content>
                <View style={style.center_page_users}>
                    <View style={style.message_container}>
                        <View style={[style.txt_container,{backgroundColor:'#8b68fc'}]}>
                            <View>
                                <Text style={[style.txt_message,{color:'#fff'}]}>موضوع : {this.props.TicketItem.subject}</Text>
                            </View>
                            <View style={style.view_txt_message}>
                                <Text style={[style.txt_message,{color:'#fff'}]}>متن پیام : {this.props.TicketItem.message}</Text>
                            </View>
                            <View style={style.date_txt}>
                                <Text style={[style.txt_message,{color:'#fff'}]}>{Helpers.ToPersianNumber(moment(this.props.TicketItem.created, 'YYYY-MM-DD HH:mm').locale('fa').format('DD MMMM YYYY | HH:mm'))}</Text>
                            </View>
                        </View>
                    </View>
                <FlatList keyExtractor={(item, index) => index.toString()}
                        data={this.state.ReplayTicketList}
                        renderItem={this.renderItemReplayTicketList}
                        ListEmptyComponent={this.EmptyComponent}
                        style={{width:'100%'}}
                    />
                    <View style={[style.view_send_container,{height:width*.90,padding:10,        width:'90%',}]}>  
                        <View style={style.view_send_message}>
                            <Text style={style.txt_message_user}>پیام :</Text>
                            <UselessTextInput
                                multiline = {true}
                                numberOfLines = {4}
                                placeholder={'متن پیام  ...'}
                                placeholderTextColor={'#000'}
                                style={style.textInputMessage}
                                onChangeText={(text) => this.setState({messageUser:text})}
                                value={this.state.messageUser}
                            />
                        </View>
                        <View style={[{height:width*.10,width:width*.80,flexDirection:'row',alignItems:'center',justifyContent:'space-around'}]}>
                            <TouchableOpacity activeOpacity={.7} style={{borderColor:'#00000010',borderWidth:1,borderRadius:width*.02,flexDirection:'row',width:width*.40,height:width*.10,alignItems:'center',justifyContent:'space-around'}}  onPress={()=>this.fileAttach()}>
                                <Text style={[style.txt_message_user,{marginBottom:0,fontFamily:'IRANSansMobile'}]}>فایل ضمیمه</Text>
                                {this.state.file_attach_url !== '' &&
                                    <Image source={require('../../../assets/images/checked.png')} style={{width:width*.05,height:width*.05}} resizeMode="contain"/>
                                }
                                </TouchableOpacity>

                            <TouchableOpacity activeOpacity={.7} style={style.btnSendMessage} onPress={()=> this.btn_ReplayTicket()}>
                            <Text style={style.txt_message_user1}>ارسال</Text>
                            </TouchableOpacity>
                        </View>
                        {this.state.file_attach_url !=='' &&
                                <Text style={{fontFamily:'IRANSansMobile',textAlign:'center',padding:5}}>حجم فایل  : {Helpers.ToPersianNumber(String(this.state.file_attach_fileSize/1000000))} مگابایت</Text>
                            }
                    </View>
                   
                </View>
                </Content>
            </View>
        );

    }
}



const mapStateToProps = (state) => {
    return {
        user: state.user,
        global: state.global
    };
};
export default connect(mapStateToProps, null)(ReplayTicket)