import React, {Component} from 'react';
import {
    Text,
    TouchableOpacity,
    Dimensions,
    View,
    Platform,
    BackHandler,
    Image, StatusBar,
    TouchableNativeFeedback, Alert, TextInput, FlatList
} from 'react-native';
import {Spinner, Header, Left, Body, Right, Button, Title, Content} from 'native-base';
import ServerData from '../../connection/connect';
import style from './Style';
import DocumentPicker from 'react-native-document-picker';
import Icon from 'react-native-vector-icons/FontAwesome5';
import LinearGradient from "react-native-linear-gradient";
import ModalDropdown from 'react-native-modal-dropdown';
import {Actions} from "react-native-router-flux";
import {connect} from "react-redux";
import {ProgressDialog} from 'react-native-simple-dialogs';
import IconFont from 'react-native-vector-icons/FontAwesome5';



const width = Dimensions.get('window').width;
const height = Dimensions.get('window').height;

class Support extends Component {
    constructor(props) {
        super(props);
        this.state = {};
    }
  
    render() {
        return (
            <View style={style.view_container_user}>
             <ProgressDialog visible={this.state.progressVisible} message="لطفا منتظر بمانید..."/>
                <View style={style.view_top}>
                    <LinearGradient style={[style.view_header_user,{height:100}]} start={{x: 0, y: 0}} end={{x: 1, y: 0}}
                                    colors={['#627efb', '#8b68fc', '#a95bfe']}>

                        <Header transparent style={style.view_top_header}>
                            <StatusBar backgroundColor="transparent" translucent={true} barStyle="light-content"/>

                            <Left style={{flex: 1,}}>
                                <Button transparent onPress={() => Actions.pop()}>
                                    <Icon name="arrow-left" color={"#ffffff"} size={width * .07}/>
                                </Button>
                            </Left>
                            <Body style={{flex: 1, alignItems: 'center'}}>
                                <Title style={style.txt_top_header}>پشتیبانی</Title>
                            </Body>
                            <Right style={{flex: 1}}>
                                <Button transparent onPress={() => Actions.drawerOpen()}>
                                    <Icon name="bars" color={"#fff"} size={width * .07}/>
                                </Button>
                            </Right>
                        </Header>
                    </LinearGradient>
                </View>
                <Content style={{flex:1,width:"100%"}}>
                <View style={{flex:1}}>
                    <TouchableOpacity onPress={()=>Actions.push('myTicketList')} style={{flex:1,padding:15,backgroundColor:'#fff',borderBottomColor:'#eee',borderBottomWidth:1}}>
                        <Text style={{fontFamily:'IRANSansMobile',textAlign:'center'}}>تیکت</Text>
                    </TouchableOpacity>
                    <TouchableOpacity onPress={()=>Actions.push('contactUs')} style={{flex:1,padding:15,backgroundColor:'#fff',borderBottomColor:'#eee',borderBottomWidth:1}}>
                        <Text style={{fontFamily:'IRANSansMobile',textAlign:'center'}}>تماس با ما</Text>
                    </TouchableOpacity>
                    <TouchableOpacity onPress={()=>Actions.push('aboutUs')} style={{flex:1,padding:15,backgroundColor:'#fff',borderBottomColor:'#eee',borderBottomWidth:1}}>
                        <Text style={{fontFamily:'IRANSansMobile',textAlign:'center'}}>معرفی سامانه</Text>
                    </TouchableOpacity>
                </View>
                </Content>
            </View>
        );

    }
}


const mapStateToProps = (state) => {
    return {
        user: state.user,
        global: state.global
    };
};
export default connect(mapStateToProps, null)(Support)

