import React, {Component} from 'react';
import {
    Text,
    TouchableOpacity,
    Dimensions,
    View,
    // AsyncStorage,
    BackHandler,
    Image, StatusBar,
    ActivityIndicator,
    TouchableNativeFeedback, Alert, TextInput, FlatList
} from 'react-native';
import AsyncStorage from '@react-native-community/async-storage';
import {Spinner, Header, Left, Body, Right, Button, Title} from 'native-base';
import ServerData from '../connection/connect';
import style from '../../assets/styles/users_css';
import BottomNavigation from '../Shared/bottomNavigation';
import Icon from 'react-native-vector-icons/FontAwesome5';
import LinearGradient from "react-native-linear-gradient";
import ModalDropdown from 'react-native-modal-dropdown';
import {Actions} from "react-native-router-flux";
import {setUsers} from "../../Redux/Actions";
import {connect} from "react-redux";
import Modal from "react-native-modal";

const width = Dimensions.get('window').width;
const height = Dimensions.get('window').height;
const Connect = new ServerData();

class Users extends Component {

    constructor(props) {
        super(props);
        this.state = {
            FilterUsers: '',
            search_users: '',
            page: 1,
            pageFilter: 1,
            seed:0,
            loading: true,
            refreshing: false,
            ListUsers: [],
            ListUsers1: [],
            apiToken: ''
        };
        this.renderItemListUsers = this.renderItemListUsers.bind(this);
        this.EmptyComponent = this.EmptyComponent.bind(this);
        this.onRefresh = this.onRefresh.bind(this);
        this.handleLoadMore = this.handleLoadMore.bind(this);
        this.renderFooter = this.renderFooter.bind(this);
        this.GetDataUsersWithoutToken = this.GetDataUsersWithoutToken.bind(this);
    };

    async componentWillMount() {
        this.setState({apiToken: await AsyncStorage.getItem('Token')});
        {
            this.props.user.apiToken === null &&
            this.GetDataUsersWithoutToken()
        }
        {
            this.props.user.apiToken !== null &&
            this.GetDataUsers()
        }
    }

  async GetDataUsersWithoutToken() {
      try {
          
        const {page} = this.state;
        const details = {
            "admintoken": this.props.global.adminToken,
            "page": page,
            "perpage": '10',
        };
       let result = await Connect.sendPost(this.props.global.baseApiUrl + '/user/getAll_Users', details)
           
                if (result.success == true) {
                    let ListUsers = result.data;
                    if (ListUsers.length > 0) {
                        this.setState(prevState => {
                            return {ListUsers: page === 1 ? ListUsers : [...prevState.ListUsers, ...ListUsers],
                               loading: false, refreshing: false
                            }
                        });
                        this.setState({ListUsers1:this.state.ListUsers})
                    }else{
                        this.setState({ loading: false, refreshing: false})
                    }
              

                    let pushFilter = [];
                    for (let i = 0; i < result.data.length; i++) {
                        for (let j = 0; j < result.data[i].User_Specialty.length; j++) {
                            pushFilter.push(result.data[i].User_Specialty[j].value)
                        }
                    }
                    this.setState({FilterUsers: pushFilter})
                    // this.state.FilterUsers.push( pushFilter)
                } else {
                    this.setState({ loading: false, refreshing: false})
                }

      } catch (error) {
        this.setState({ loading: false, refreshing: false})
      }
        
            
    }

  async GetDataUsers() {
        try {
        const {page} = this.state;
        const details = {
            "admintoken": this.props.global.adminToken,
            "page": page,
            "perpage": '15',
            "usertoken": this.props.user.apiToken,
        };
      let result = await Connect.sendPost(this.props.global.baseApiUrl + '/user/getAll_Users', details)
            //console.log(result)
                if (result.success == true) {
                    let ListUsers = result.data;
                    if (ListUsers.length > 0) {
                        this.setState(prevState => {
                            return {ListUsers: page === 1 ? ListUsers : [...prevState.ListUsers, ...ListUsers],
                                loading: false, refreshing: false
                            }
                        });
                        this.setState({ListUsers1:this.state.ListUsers})
                    }else{
                        this.setState({loading: false, refreshing: false});
                    }
                    // console.log(this.state.ListUsers)
                } else {
                    this.setState({loading: false, refreshing: false});
                }
       
        } catch (error) {
            this.setState({loading: false, refreshing: false});
        }
        
    }

    GoToProfileUser(index) {
        Actions.push('Profile_user', {ListUsers: this.state.ListUsers1[index]});
    }

   async FollowRequest(index) {
        const details = {
            "admintoken": this.props.global.adminToken,
            "usertoken": this.props.user.apiToken,
            "target_user": this.state.ListUsers1[index].ID,
        };
       let result = await Connect.sendPost(this.props.global.baseApiUrl + '/user/Follow_Request', details)
                if (result.success == true) {
                    Alert.alert('', result.data, [{text: 'باشه', onPress: () => this.onRefresh()}]);

                } else {
                    Alert.alert('', result.messages, [{text: 'باشه'}])
                }
    }

   async UnFollowRequest(index) {
       
        const details = {
            "admintoken": this.props.global.adminToken,
            "usertoken": this.props.user.apiToken,
            "target_id": this.state.ListUsers1[index].ID,
        };
       let result = await Connect.sendPost(this.props.global.baseApiUrl + '/user/CancelOrIngnore_FollowRequest', details)
                if (result.success == true) {
                    Alert.alert('', result.data, [{text: 'باشه', onPress: () => this.onRefresh()}])

                } else {
                    Alert.alert('', result.messages, [{text: 'باشه'}])
                }
    }

    renderItemListUsers({item, index})  {
        if (item.length !== 0) {
            if (this.props.user.full_name !== item.full_name) { 
                return (
                    <TouchableOpacity style={style.view_user} onPress={() => this.GoToProfileUser(index)}>
                        {this.props.user.apiToken !== null && item.Frindship_Status_With_Current_User == 'nothing' &&
                        <View style={style.view_user_icon}>
                            <TouchableOpacity activeOpacity={.5} onPress={() => this.FollowRequest(index)}>
                                <LinearGradient style={style.view_folow} start={{x: 0, y: 0}} end={{x: 1, y: 0}}
                                                colors={['#5e38c2', '#4c26b3']}>
                                    <Icon size={width * .05} color={'#fff'} name={'user-plus'}/>
                                </LinearGradient>
                            </TouchableOpacity>
                            <Text style={style.txt_flow}>دنبال کردن</Text>
                        </View>
                        }
                        {this.props.user.apiToken !== null && item.Frindship_Status_With_Current_User == 'friend' || item.Frindship_Status_With_Current_User == 'Requested' &&
                        <View style={style.view_user_icon}>
                            <TouchableOpacity activeOpacity={.5} onPress={() => this.UnFollowRequest(index)}>
                                <LinearGradient style={style.view_folow} start={{x: 0, y: 0}} end={{x: 1, y: 0}}
                                                colors={['#ff6f75', '#fe4f4b']}>
                                    <Icon size={width * .05} color={'#fff'} name={'user-minus'}/>
                                </LinearGradient>
                            </TouchableOpacity>
                            <Text style={style.txt_flow}>لغو دنبال کردن</Text>
                        </View>
                        }

                     

                        <View style={style.view_user_txt}>
                            <Text style={style.txt_name_user}>{item.full_name} {item.work_in !=null ? item.work_in : ''}</Text>
                        </View>
                        <View style={style.view_img}>
                            <Image source={{uri: item.avatar}} style={style.pic_user}/>
                        </View>

                    </TouchableOpacity>
                );
            }
        }
    };

    EmptyComponent() {
        if(this.state.loading)
        {
            return(
                <ActivityIndicator color={this.props.global.grColorTwo} size='large' />
            );
        }
        else
        {
            return(
             <View style={style.viewNotFound}>
                <Text style={style.txtNotFound}>کاربری وجود
                    ندارد</Text>
            </View>
            );
        }   
    }

    onRefresh() {
        if( this.props.user.apiToken === null){
            this.setState({page: 1, refreshing: true,}, async () => {
                this.GetDataUsersWithoutToken();
            })
            
        }else {
            this.setState({page: 1, refreshing: true,}, async () => {
                this.GetDataUsers();
            }) 
        }
    }

    handleLoadMore() {
        if( this.props.user.apiToken === null){
            if (this.state.ListUsers1.length > 0) {
                this.setState({page: this.state.page + 1, loading: true}, () => {
                    this.GetDataUsersWithoutToken();
                })
            } 
    }else{
        if (this.state.ListUsers1.length > 0) {
            this.setState({page: this.state.page + 1, loading: true}, () => {
                this.GetDataUsers();
            })
        } 
    }

    }

    renderFooter() {
        if(this.state.loading && this.state.ListUsers.length > 0)
        {
            return <ActivityIndicator style={{alignSelf: 'center',marginBottom: 20}} color={this.props.global.grColorTwo} />
        }
        else {
            return null;
        }
    }
    searchFilterFunction (text)  {    
        const newData = this.state.ListUsers.filter(item => {      
        const itemData = `${item.full_name.toUpperCase()}`;
        
        const textData = text.toUpperCase();
            
        return itemData.indexOf(textData) > -1;

        });
        this.setState({ ListUsers1: newData ,loading: false, refreshing: false});
        
    };
    render() {
        return (
            <View style={style.view_container_user}>

                <View style={style.view_top}>
                    <LinearGradient style={[style.view_header_user]} start={{x: 0, y: 0}} end={{x: 1, y: 0}}
                                    colors={['#627efb', '#8b68fc', '#a95bfe']}>

                        <Header transparent style={style.view_top_header}>
                            <StatusBar backgroundColor="transparent" translucent={true} barStyle="light-content"/>

                            <Left style={{flex: 1,}}>
                                <Button transparent onPress={() => Actions.pop()}>
                                    <Icon name="arrow-left" color={"#ffffff"} size={width * .07}/>
                                </Button>
                            </Left>
                            <Body style={{flex: 1, alignItems: 'center'}}>
                                <Title style={style.txt_top_header}>اعضاء</Title>
                            </Body>
                            <Right style={{flex: 1}}>
                                <Button transparent onPress={() => Actions.drawerOpen()}>
                                    <Icon name="bars" color={"#fff"} size={width * .07}/>
                                </Button>
                            </Right>
                        </Header>
                        <View style={[style.view_search]}>
                            <View style={style.icon_search}>
                                <Icon name="search" color={'#e8e8e8'} size={width * .05}/>
                            </View>
                            <View style={style.input_search}>
                                <TextInput
                                    textAlign={'right'}
                                    placeholder="جستجو اعضا..."
                                    placeholderTextColor={"#e8e8e8"}
                                    style={style.textinput_search}
                                    onChangeText={text => this.searchFilterFunction(text)}
                                    // onChangeText={(text) => this.FilterUserBySearch(text)}
                                />
                            </View>
                            <TouchableOpacity style={style.modalFilter}
                                              onPress={() => Actions.push('Search1')}>
                                <Text style={style.modalTextStyle}>جستجو پیشرفته</Text>
                            </TouchableOpacity>
                        </View>
                    </LinearGradient>
                </View>

                <View style={style.center_page_users}>
                  
                 {/* {(!this.state.loading)  && this.state.ListUsers.length > 0 && */}
                    <FlatList keyExtractor={(item, index) => index.toString()}
                              data={this.state.ListUsers1
                            //   .filter(item => item.full_name.includes(this.state.search_users))
                              }
                              renderItem={this.renderItemListUsers}
                              ListEmptyComponent={this.EmptyComponent}
                              onEndReached={this.handleLoadMore}
                              onEndReachedThreshold={0.5}
                              refreshing={this.state.refreshing}
                              onRefresh={this.onRefresh}
                              ListFooterComponent={this.renderFooter}
                    />
                 {/* }
                 {(this.state.loading) &&
                     <View style={{ height : Dimensions.get('window').height * .2  , justifyContent:'center',alignItems:'center' }}>
                        <ActivityIndicator color={this.props.global.grColorTwo} size='large'/>
                    </View>} */}
                </View>
                <View>
                    <BottomNavigation styleBottom={'other'} color_members={'#fff'}/>
                </View>
            </View>
        );

    }
}

const mapDispatchToProps = dispatch => {
    return {
        setUsers: users => {
            dispatch(setUsers(users))
        }
    }
};
const mapStateToProps = (state) => {
    return {
        user: state.user,
        global: state.global
    };
};
export default connect(mapStateToProps, mapDispatchToProps)(Users)

