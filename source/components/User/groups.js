import React, {Component} from 'react';
import {
    Text,
    TouchableOpacity,
    Dimensions,
    View,
    FlatList,
    BackHandler,
    Image, StatusBar, TextInput
} from 'react-native';
import {Container, Header, Left, Body, Right, Button, Title, Spinner} from 'native-base';
import ServerData from '../connection/connect';
import style from '../../assets/styles/groups_css';
import BottomNavigation from '../Shared/bottomNavigation';
import Icon from 'react-native-vector-icons/FontAwesome5';
import LinearGradient from "react-native-linear-gradient";
import {Actions} from "react-native-router-flux";
import { connect } from 'react-redux';

const width = Dimensions.get('window').width;
const height = Dimensions.get('window').height;
const Connect = new ServerData();

class groups extends Component {

    constructor(props) {
        super(props);
        this.state = {
            search_groups: '',
            ListGroups: [],
            pic: 'https://www.gravatar.com/avatar/52f0fbcbedee04a121cba8dad1174462?s=200&d=mm&r=g',
            pic1: 'https://www.gravatar.com/avatar/a7bd9f996fbe4f7e3d0a5313237d58c2?s=200&d=mm&r=g',

        }
    };



   async GetDataListGroups() {
        const details = {
            "admintoken": this.props.global.adminToken,
        };
      let result = await Connect.sendPost(this.props.global.baseApiUrl + '/post/grouplist', details)
                if (result.success == true) {

                    this.setState({ListGroups: result.data})

                } else {

                }
    }

    componentWillMount() {
        this.GetDataListGroups();
    }
    renderItemListGroups = ({item, index}) => {
        return (
            <View style={style.view_user}>
                <View style={style.view_img}>
                    {/*<Image source={{uri: this.state.pic}} style={style.pic_user} resizeMode={'cover'}/>*/}
               <Text style={style.img_groups}>{item.name.charAt(0)}</Text>
                </View>
                <Text style={style.txt_name_user}>{item.name}</Text>

                <TouchableOpacity activeOpacity={.5} onPress={() => alert('asdasd')}>
                    <View style={style.view_folow}><Text style={style.txt_flow}>عضو شدن</Text></View>
                </TouchableOpacity>
            </View>
        );
    }

    render() {
        const data = this.state.data_prof;
        return (
            <View style={style.view_container_user}>
                <View style={style.view_top}>
                    <LinearGradient style={[style.view_header_group]} start={{x: 0, y: 0}} end={{x: 1, y: 0}}
                                    colors={['#627efb', '#8b68fc', '#a95bfe']}>

                        <Header transparent style={style.view_top_header}>
                            <StatusBar backgroundColor="transparent" translucent={true} barStyle="light-content"/>

                            <Left style={{flex: 1,}}>
                                <Button transparent onPress={()=>Actions.pop()}>
                                    <Icon name="arrow-left" color={"#ffffff"} size={width * .07}/>
                                </Button>
                            </Left>
                            <Body style={{flex: 1, alignItems: 'center'}}>
                                <Title style={style.txt_top_header}>گروه ها</Title>
                            </Body>
                            <Right style={{flex: 1}}>
                                <TouchableOpacity activeOpacity={.5}>
                                    <Button transparent onPress={()=>Actions.drawerOpen()}>
                                        <Icon name="bars" color={"#fff"} size={width * .07}/>
                                    </Button>
                                </TouchableOpacity>
                            </Right>
                        </Header>
                        <View style={[style.view_search]}>
                            <View style={style.icon_search}>
                                <Icon name="search" color={'#e8e8e8'} size={width * .05}/>
                            </View>
                            <View style={style.input_search}>
                                <TextInput
                                    textAlign={'right'}
                                    placeholder="جستجو گروه..."
                                    placeholderTextColor={"#e8e8e8"}
                                    style={style.textinput_search}
                                    onChangeText={(text) => this.setState({search_groups: text})}
                                />
                            </View>
                        </View>
                    </LinearGradient>
                </View>

                <View style={style.center_page_groups}>
                    <FlatList numColumns={2} keyExtractor={(item) => item.id.toString()}
                              data={this.state.ListGroups.filter(item => item.name.includes(this.state.search_groups))}
                              renderItem={this.renderItemListGroups}
                              ListEmptyComponent={() => <Spinner/>}
                    />
                </View>

                <View>
                    <BottomNavigation navigation={this.props.navigation} color_group={'#fff'}/>
                </View>
            </View>
        );

    }
}
const mapStateToProps = (state) => {
    return {
        user: state.user,
        global: state.global
    };
};
export default connect(mapStateToProps, null)(groups)

