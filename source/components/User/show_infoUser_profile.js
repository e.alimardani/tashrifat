import React, {Component} from 'react';
import {
    Text,
    TouchableOpacity,
    Dimensions,
    View,
    TouchableNativeFeedback,
    Image, Linking
} from 'react-native';
import Icon from 'react-native-vector-icons/FontAwesome5';
import ServerData from '../connection/connect';
import style from '../../assets/styles/profile_css';
import {Actions} from 'react-native-router-flux';
import {connect} from "react-redux";
import Helper from '../Shared/Helper'


const width = Dimensions.get('window').width;
const Connect = new ServerData();

class show_infoUser_profile extends Component {

    constructor(props) {
        super(props);
        this.state = {}

    };

    render() {
        const data = this.props.data;
        const user = this.props.user;
        console.log(user)
        return (
            <View style={style.view_bottom_screen}>
                <View style={[style.left_bottom_screen]}>
                    <TouchableOpacity style={style.view_icon_name} onPress={()=>Actions.push('MessageSendBox')}>
                        <Icon name="paper-plane" color={"#3b57ff"} size={width * .07}/>
                        <Text style={[style.txt_icon_bottom,{flex:1}]}>پیام های ارسالی</Text>
                    </TouchableOpacity>
                    <TouchableOpacity style={style.view_icon_name} onPress={()=>Actions.push('MessageInBox')}>
                        <Icon name="inbox" color={"#50ff34"} size={width * .07}/>
                        <Text style={[style.txt_icon_bottom,{flex:1}]}>پیام های دریافتی</Text>
                    </TouchableOpacity>
                    <TouchableOpacity onPress={()=>Linking.openURL(`tel:${user.phone_number}`)} style={style.view_icon_name}>
                        <Icon name="mobile-alt" color={"#ff8b17"} size={width * .07}/>
                        <Text style={[style.txt_icon_bottom,{flex:1}]}>{Helper.ToPersianNumber(String(user.phone_number))}</Text>
                    </TouchableOpacity>
                </View>
                <View style={style.center_bottom_screen}>
                    <TouchableOpacity style={style.view_icon_name} onPress={()=>Actions.push('sampleWork')}>
                        <Icon name="file-signature" color={"#26ffdd"} size={width * .07}/>
                        <Text style={[style.txt_icon_bottom,{flex:1}]}>نمونه کارها</Text>
                    </TouchableOpacity>
                    <TouchableOpacity onPress={()=> Linking.openURL(`mailto:${user.email}`)} style={style.view_icon_name} >
                        <Icon name="envelope-open-text" color={"#ff31fe"} size={width * .07}/>
                        <Text style={[style.txt_icon_bottom,{flex:1}]}>{user.email}</Text>
                    </TouchableOpacity>
                </View>
            </View>
        );

    }
}

const mapStateToProps = (state) => {
    return {
        user: state.user,
        global: state.global
    };
};
export default connect(mapStateToProps, null)(show_infoUser_profile)
