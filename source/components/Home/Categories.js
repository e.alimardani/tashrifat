import React, {Component} from 'react';
import {
    Text,
    TouchableOpacity,
    Dimensions,
    View,
    ActivityIndicator,
    Image, StatusBar,
    WebView, Alert, ScrollView, FlatList
} from 'react-native';
import AsyncStorage from '@react-native-community/async-storage';
import {Spinner, Header, Left, Body, Right, Button, Title} from 'native-base';
import Icon from 'react-native-vector-icons/FontAwesome5';
import LinearGradient from "react-native-linear-gradient";
import {Actions} from "react-native-router-flux";
import {setUser} from "../../Redux/Actions";
import {connect} from "react-redux";
import style from './Style';
import ServerData from "../connection/connect";
import HTML from 'react-native-render-html';
import moment from 'jalali-moment';
import Helpers from '../Shared/Helper';
const width = Dimensions.get('window').width;
const height = Dimensions.get('window').height;
const Connect = new ServerData();

class Categories extends Component {

    constructor(props) {
        super(props);
        this.state = {
            Categories:[],
            loading:false
        };
        this.renderItemCategories = this.renderItemCategories.bind(this);
        this.EmptyCategories = this.EmptyCategories.bind(this);
    };
 
   async componentWillMount(){
    let item1 = this.props.AllCat.filter(a => a.parent === this.props.ItemId );
    await this.setState({Categories:item1})
    console.log(this.state.Categories)
    }
    renderItemCategories({item, index}) {
        return (
            <TouchableOpacity activeOpacity={.7} style={style.viewSubCat}
                             onPress={()=>Actions.push('Posts', {idCat: item.id,itemName:item.category_name})}>
                {item.image !='' && <Image source={{uri: item.image}} style={style.imageIconSubCat} resizeMode={'contain'}/>}
                <Text style={[style.txt_top_header,{color:'#000'}]}>{item.category_name}</Text>
            </TouchableOpacity>
        );
    }
    EmptyCategories(){
        if(this.state.loading)
        {
            return(
                <ActivityIndicator  color={this.props.global.grColorTwo} size='large' />
            );
        }
        else
        {
            return(
             <View style={style.viewNotFound}>
                <Text style={style.txtNotFound}>دسته ای وجود
                    ندارد</Text>
            </View>
            );
        }   
    }
    render() {
        const item = this.props.itemPost;
        return (
            <View style={style.view_container_user}>
                <View style={style.view_top}>
                    <LinearGradient style={[style.view_header_user]} start={{x: 0, y: 0}} end={{x: 1, y: 0}}
                                    colors={['#627efb', '#8b68fc', '#a95bfe']}>

                        <Header transparent style={style.view_top_header}>
                            <StatusBar backgroundColor="transparent" translucent={true} barStyle="light-content"/>

                            <Left style={{flex: 1,}}>
                                <Button transparent onPress={() => Actions.pop()}>
                                    <Icon name="arrow-left" color={"#ffffff"} size={width * .07}/>
                                </Button>
                            </Left>
                            <Body style={{flex: 1, alignItems: 'center'}}>
                                <Title
                                    style={style.txt_top_header}>{this.props.itemName}</Title>
                            </Body>
                            <Right style={{flex: 1}}>
                                <Button transparent onPress={() => Actions.drawerOpen()}>
                                    <Icon name="bars" color={"#fff"} size={width * .07}/>
                                </Button>
                            </Right>
                        </Header>
                    </LinearGradient>
                </View>
              
                    <View style={[style.center_page_users]}>
                    {(!this.state.loading) &&
                    <FlatList
                        numColumns={3} 
                        keyExtractor={(item,index)=>index.toString()}
                        data={this.state.Categories}
                        ListEmptyComponent={this.EmptyCategories}
                        renderItem={this.renderItemCategories} />
                    }
                {(this.state.loading) &&
                <View style={style.viewNotFound}>
                    <ActivityIndicator  color={this.props.global.grColorTwo} size='large' />
                </View>
                }
                    </View>
                    
               
            </View>
        );
    }
}

const mapDispatchToProps = dispatch => {
    return {
        setUser: user => {
            dispatch(setUser(user))
        }
    }
};
const mapStateToProps = (state) => {
    return {
        user: state.user,
        global: state.global
    };
};
export default connect(mapStateToProps, mapDispatchToProps)(Categories)

