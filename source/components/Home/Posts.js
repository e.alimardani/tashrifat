import React, {Component} from 'react';
import {
    Text,
    TouchableOpacity,
    Dimensions,
    View,
    ActivityIndicator,
    Image, StatusBar,
    TouchableNativeFeedback, Alert, TextInput, FlatList
} from 'react-native';
import AsyncStorage from '@react-native-community/async-storage';
import {Spinner, Header, Left, Body, Right, Button, Title} from 'native-base';
import Icon from 'react-native-vector-icons/FontAwesome5';
import LinearGradient from "react-native-linear-gradient";
import {Actions} from "react-native-router-flux";
import {setUser} from "../../Redux/Actions";
import {connect} from "react-redux";
import style from './Style'
import ServerData from "../connection/connect";
import moment from 'jalali-moment';
import Helpers from '../Shared/Helper';

const width = Dimensions.get('window').width;
const height = Dimensions.get('window').height;
const Connect = new ServerData();

class Posts extends Component {

    constructor(props) {
        super(props);
        this.state = {
            Posts: [],
            loading:false
        };
        this.GetDataPosts = this.GetDataPosts.bind(this);
        this.renderItemPosts = this.renderItemPosts.bind(this);
        this.EmptyPosts = this.EmptyPosts.bind(this);
    };

    async componentWillMount() {
        await this.GetDataPosts();
    }

    async GetDataPosts() {
        this.setState({loading:true})
        const details = {
            "admintoken": this.props.global.adminToken,
            "postcatid": this.props.idCat,
        };
        let result = await Connect.sendPost(this.props.global.baseApiUrl + '/post/getpostbycat', details);
        if (result.success == true) {
            await this.setState({loading:false,Posts : result.data});

        } else {
            this.setState({loading:false})
        }
    }
    renderItemPosts({item,index}){
        return(
            <View style={style.containerPosts}>
                <View style={style.topPosts}>
                    <View style={style.containerTextHeader}>
                        <Text style={style.headerTitle}>{item.title}</Text>
                        <Text style={style.headerDate}>{Helpers.ToPersianNumber(moment(item.create_date, 'YYYY-MM-DD HH:mm').locale('fa').format('DD MMMM YYYY | HH:mm'))}</Text>
                    </View>
                    <Image source={{uri:item.image}} style={style.imageTopHeader}/>
                </View>
                <View style={{flex:1}}>
                    <Image source={{uri:item.image}} style={style.imageCenterPost} resizeMode={'cover'}/>
                </View>
                <View>
                    {/*<Text>{item.post_desc}</Text>*/}
                </View>
                <TouchableOpacity activeOpacity={.8} style={style.containerLoadMore}
                onPress={()=>Actions.push('Post',{itemPost:item})}
                >
                    <Text style={style.TextLoadMore}>نمایش بیشتر</Text>
                    <Image source={require('../../assets/images/moreshow.png')} style={style.imageLoadMore}/>
                </TouchableOpacity>
            </View>
        );
    }
    EmptyPosts(){
        if(this.state.loading)
        {
            return(
                <ActivityIndicator  color={this.props.global.grColorTwo} size='large' />
            );
        }
        else
        {
            return(
             <View style={style.viewNotFound}>
                <Text style={style.txtNotFound}>پستی وجود ندارد</Text>
            </View>
            );
        }   
    }
    render() {
        return (
            <View style={style.view_container_user}>
                <View style={style.view_top}>
                    <LinearGradient style={[style.view_header_user]} start={{x: 0, y: 0}} end={{x: 1, y: 0}}
                                    colors={['#627efb', '#8b68fc', '#a95bfe']}>

                        <Header transparent style={style.view_top_header}>
                            <StatusBar backgroundColor="transparent" translucent={true} barStyle="light-content"/>

                            <Left style={{flex: 1,}}>
                                <Button transparent onPress={() => Actions.pop()}>
                                    <Icon name="arrow-left" color={"#ffffff"} size={width * .07}/>
                                </Button>
                            </Left>
                            <Body style={{flex: 1, alignItems: 'center'}}>
                                <Title
                                    style={style.txt_top_header}>{this.props.itemName}</Title>
                            </Body>
                            <Right style={{flex: 1}}>
                                <Button transparent onPress={() => Actions.drawerOpen()}>
                                    <Icon name="bars" color={"#fff"} size={width * .07}/>
                                </Button>
                            </Right>
                        </Header>
                    </LinearGradient>
                </View>

                <View style={style.center_page_users}>
                {(!this.state.loading) &&
                    <FlatList
                        keyExtractor={(item)=>item.id.toString()}
                        data={this.state.Posts}
                        ListEmptyComponent={this.EmptyPosts}
                        renderItem={this.renderItemPosts} />
                     }
                {(this.state.loading) &&
                <View style={style.viewNotFound}>
                    <ActivityIndicator  color={this.props.global.grColorTwo} size='large' />
                </View>
                }
                </View>
            </View>
        );
    }
   
}

const mapDispatchToProps = dispatch => {
    return {
        setUser: user => {
            dispatch(setUser(user))
        }
    }
};
const mapStateToProps = (state) => {
    return {
        user: state.user,
        global: state.global
    };
};
export default connect(mapStateToProps, mapDispatchToProps)(Posts)

