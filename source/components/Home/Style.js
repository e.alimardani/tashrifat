import React from 'react';
import {StyleSheet, Dimensions, Platform} from 'react-native';


const width = Dimensions.get('window').width;
const height = Dimensions.get('window').height;

export default StyleSheet.create({
    view_container_user: {
        alignItems: 'center',
        backgroundColor:'#fff',
        flex: 1
    },
    view_top: {justifyContent: 'center', alignItems: 'center'},
    view_header_user: {
        height: height / 7,
        width: width / 1,
        // backgroundColor:'#000'
    },
    viewSubCat: {
        marginTop:10,
        width: width *.27,
        height: height * .17,
        borderRadius: width * .03,
        marginBottom: height * .01,
        marginHorizontal: width * .01,
        backgroundColor: '#f6f0fc',
        elevation: 1,
        alignSelf: 'flex-start',
        justifyContent: 'center',
        alignItems: 'center'
    },

    imageIconSubCat: {width: width * .15, height: width * .15},
    ContainerCategory:{
        width:width*.20,
        height:width*.20,
        borderRadius:width*.10,
        backgroundColor:'#fff',
        elevation:4,
        justifyContent:'center',
        alignItems:'center',
        margin:10
    },
    viewNotFound:{width: width * .90, height: height * .60, justifyContent: 'center', alignItems: 'center',},
    txtNotFound:{fontSize: width / 20, color: '#000', fontFamily: 'IRANSansMobile',},
    
    view_top_header: {
        marginTop: height * .01,
        paddingHorizontal: width * .05,
        width: width,
        alignItems: 'center',
        justifyContent: 'space-between'
        // flex:1
    },
    txt_top_header: {
        // flex:1,
        color: '#fff',
        fontFamily: 'IRANSansMobile',
        textAlign: 'center',
        fontSize: width / 25
    },
    center_page_users: {flex: 1,},
    containerPosts: {
        width: width,
        height: height * .45,
        // backgroundColor: '#99ff7e',
        justifyContent: 'center',
        elevation:3,
        marginBottom: height * .01
    },
    containerPost: {
        width: width,
        // height: height * .45,
        // backgroundColor: '#99ff7e',
        justifyContent: 'center',
        marginBottom: height * .01
    },
    topPosts: {
        width: width,
        height: height * .10,
        // backgroundColor: '#77a4ff',
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'flex-end'
    },
    containerTextHeader: {alignItems: 'flex-end'},
    headerTitle: {
        fontFamily: 'IRANSansMobile',
        fontSize: width / 28
    },
    headerDate: {
        fontFamily: 'IRANSansMobile',
        fontSize: width / 30, color: '#00000080'
    },
    imageTopHeader: {width: width * .14, height: width * .14, borderRadius: width * .07, marginHorizontal: width * .05},
    imageCenterPost: {width: width, height: height * .29,},
    containerLoadMore: {
        height: height * .05,
        backgroundColor: '#fff',
        borderRadius: width * .05,
        marginTop: height * .002,
        marginRight: width * .05,
        elevation:2,
        alignSelf: 'flex-end',
        width: width * .38,
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'flex-end'
    },
    TextLoadMore: {
        fontFamily: 'IRANSansMobile',
        fontSize: width / 30, color: '#00000080'
    },
    imageLoadMore: {width: width * .06, height: width * .06, borderRadius: width * .03, marginHorizontal: width * .04}

});