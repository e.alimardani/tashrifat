import React, {Component} from 'react';
import {
    Text,
    TouchableOpacity,
    Dimensions,
    View,
    BackHandler,
    Image, StatusBar,
    WebView, Alert, ScrollView, FlatList
} from 'react-native';
import AsyncStorage from '@react-native-community/async-storage';
import {Spinner, Header, Left, Body, Right, Button, Title} from 'native-base';
import Icon from 'react-native-vector-icons/FontAwesome5';
import LinearGradient from "react-native-linear-gradient";
import {Actions} from "react-native-router-flux";
import {setUser} from "../../Redux/Actions";
import {connect} from "react-redux";
import style from './Style'
import ServerData from "../connection/connect";
import HTML from 'react-native-render-html';
import moment from 'jalali-moment';
import Helpers from '../Shared/Helper';

const width = Dimensions.get('window').width;
const height = Dimensions.get('window').height;
const Connect = new ServerData();

class Post extends Component {

    constructor(props) {
        super(props);
        this.state = {};
    };

    


    render() {
        const item = this.props.itemPost;
        // console.log(this.props)
        return (
            <View style={style.view_container_user}>
                <View style={style.view_top}>
                    <LinearGradient style={[style.view_header_user]} start={{x: 0, y: 0}} end={{x: 1, y: 0}}
                                    colors={['#627efb', '#8b68fc', '#a95bfe']}>

                        <Header transparent style={style.view_top_header}>
                            <StatusBar backgroundColor="transparent" translucent={true} barStyle="light-content"/>

                            <Left style={{flex: 1,}}>
                                <Button transparent onPress={() => Actions.pop()}>
                                    <Icon name="arrow-left" color={"#ffffff"} size={width * .07}/>
                                </Button>
                            </Left>
                            <Body style={{flex: 1, alignItems: 'center'}}>
                                <Title
                                    style={style.txt_top_header}>{'پست'}</Title>
                            </Body>
                            <Right style={{flex: 1}}>
                                <Button transparent onPress={() => Actions.drawerOpen()}>
                                    <Icon name="bars" color={"#fff"} size={width * .07}/>
                                </Button>
                            </Right>
                        </Header>
                    </LinearGradient>
                </View>
                <ScrollView>
                    <View style={style.center_page_users}>
                        <View style={style.containerPost}>
                            <View style={style.topPosts}>
                                <View style={style.containerTextHeader}>
                                    <Text style={style.headerTitle}>{item.title}</Text>
                                    <Text style={style.headerDate}>{Helpers.ToPersianNumber(moment(item.create_date, 'YYYY-MM-DD HH:mm').locale('fa').format('DD MMMM YYYY | HH:mm'))}</Text>
                                </View>
                                <Image source={{uri: item.image}} style={style.imageTopHeader}/>
                            </View>
                            <View style={{flex: 1}}>
                                <Image source={{uri: item.image}} style={style.imageCenterPost} resizeMode={'cover'}/>
                            </View>
                            <View>
                                <HTML html={item.post_desc}/>
                            </View>
                        </View>
                    </View>
                </ScrollView>
            </View>
        );
    }
}

const mapDispatchToProps = dispatch => {
    return {
        setUser: user => {
            dispatch(setUser(user))
        }
    }
};
const mapStateToProps = (state) => {
    return {
        user: state.user,
    };
};
export default connect(mapStateToProps, mapDispatchToProps)(Post)

