import React, {Component} from 'react';
import {
    Text,
    TouchableOpacity,
    Dimensions,
    View,
    ScrollView,
    BackHandler,
    Image, StatusBar,
    ActivityIndicator, Alert, TextInput, FlatList
} from 'react-native';
import AsyncStorage from '@react-native-community/async-storage';
import {Spinner, Header, Left, Body, Right, Button, Title} from 'native-base';
import ServerData from '../connection/connect';
import style from '../../assets/styles/home_css';
import {Actions} from "react-native-router-flux";
import {setUser} from "../../Redux/Actions";
import {connect} from "react-redux";
import BottomNavigation from "../../components/Shared/bottomNavigation";
import LinearGradient from "react-native-linear-gradient";
import Icon from "react-native-vector-icons/FontAwesome5";
import Modal from "react-native-modal";
// import MasonryList from "react-native-masonry-list";

const width = Dimensions.get('window').width;
const height = Dimensions.get('window').height;
const Connect = new ServerData();
let TextCat = '';

class Home_Screen extends Component {
    constructor(props) {
        super(props);
        this.state = {
            ListCat: [],
            ListCat1:[],
            ListSubCat1: [],
            ListSubCat: [],
            ListSubCatImage: [],
            loading:true,
            setColor: 0,
            changed: 1,
            selectedItem: true,
            visibleComplateProfile:false,
            Videos:[],
            loadingVideo:false
        };
        this.renderListCat = this.renderListCat.bind(this);
        this.ListCatEmpty = this.ListCatEmpty.bind(this);
        this.subCat = this.subCat.bind(this);
        this.renderItemSubCat = this.renderItemSubCat.bind(this);
    };
    async componentWillMount() {
           setInterval(() => {
                this.countMessages()
            }, 9000);
        
       await this.GetDataVideo();
        let aaa = await AsyncStorage.getItem('complateProfile');
        if(aaa !== 'on' && this.props.complateProfile === 1){
           await this.setState({visibleComplateProfile:true});
            let aaa= await AsyncStorage.setItem('complateProfile','on');
        }else{
          await  this.setState({visibleComplateProfile:false});
        }
       
        await this.GetDataCategory(); 
        let item = this.state.ListCat.filter(a => a.parent === 0 && a.category_name !== 'دسته بندی نشده')[0];

        await this.subCat(item, 0);
        let index = this.state.ListCat.findIndex(a => a.id === item.id);
        await this.setState({setColor: index});
    }

    async countMessages(){
        console.log('start')
        console.log(this.props.user.apiToken)
        if(this.props.user.apiToken !=null){
            const details = {
                "admintoken": this.props.global.adminToken,
                "usertoken": this.props.user.apiToken,
            };
           let result = await Connect.sendPost(this.props.global.baseApiUrl + '/user/unread_message_count', details)
           console.log(details)
           console.log(result)
                    if (result.success == true) {
                        await this.props.setUser({
                            "Avatar":this.props.user.Avatar,
                            "instegram": this.props.user.Instagram,
                            "telegram": this.props.user.Telegram,
                            "whatsapp": this.props.user.whatsApp,
                            "Groups_count":this.props.user.Groups_count,
                            "apiToken":this.props.user.apiToken,
                            "email":this.props.user.email,
                            "friends_count":this.props.user.friends_count,
                            "full_name":this.props.user.full_name,
                            "id":this.props.user.id,
                            "phone_number":this.props.user.phone_number,
                            "username":this.props.user.username,
                            "work_in":this.props.user.work_in,
                            "Genre":this.props.user.Genre,
                            "messageCount":result.data,
                            "Is_vip":this.props.user.Is_vip
                         })
                    } else {
                        await this.props.setUser({
                            "Avatar":this.props.user.Avatar,
                            "instegram": this.props.user.Instagram,
                            "telegram": this.props.user.Telegram,
                            "whatsapp": this.props.user.whatsApp,
                            "Groups_count":this.props.user.Groups_count,
                            "apiToken":this.props.user.apiToken,
                            "email":this.props.user.email,
                            "friends_count":this.props.user.friends_count,
                            "full_name":this.props.user.full_name,
                            "id":this.props.user.id,
                            "phone_number":this.props.user.phone_number,
                            "username":this.props.user.username,
                            "work_in":this.props.user.work_in,
                            "Genre":this.props.user.Genre,
                            "messageCount":0,
                            "Is_vip":this.props.user.Is_vip
                         })
                    }
        }
    }

    async GetDataVideo() {
        this.setState({loadingVideo:true})
        try {
         const details = {
             "admintoken": this.props.global.adminToken,
         };
         console.log(details)
         let result = await Connect.sendPost(this.props.global.baseApiUrl + '/user/get_advertise_video', details);
         if (result.success === true) {
            console.log(result)
            await this.setState({Videos: result.data,loadingVideo:false})
         // console.log(this.state.ListCat)
         } else {
             this.setState({loadingVideo:false});
         }
        } catch (error) {
         this.setState({loadingVideo:false});
        }
     }
    async subCat(item, index,) {
        TextCat = item.category_name;
        let ListSubCat = [];
        for (let i = 0; i < this.state.ListCat.length; i++) {
            if (item.id === this.state.ListCat[i].parent) {
                let a = {name: '', image: '', id: null};
                a.name = this.state.ListCat[i].category_name;
                a.image = this.state.ListCat[i].image;
                a.id = this.state.ListCat[i].id;
                ListSubCat.push(a)
            }
        }
        await this.setState({ListSubCat, changed: this.state.changed + 1, setColor: index})
    }

    ListCatEmpty() {
        if(this.state.loading)
        {
            return(
                <ActivityIndicator  color={this.props.global.grColorTwo} size='large' />
            );
        }
        else
        {
            return(
             <View style={style.viewNotFound}>
                <Text style={style.txtNotFound}>دسته ای وجود
                    ندارد</Text>
            </View>
            );
        }   
    }

    renderListCat({item, index}) {
    
        return (
            <View style={style.viewContainerHeaderFlatList}>
                {/* {item.parent !== 0 && item.category_name !== 'دسته بندی نشده' && */}
                <TouchableOpacity
                    style={[style.viewCatHeader, {backgroundColor: this.state.setColor === index ? '#652d9c' : '#fff'}]}
                    onPress={() => this.subCat(item, index)}>
                    {/*<Text style={[style.txt_cat_header,{color: this.state.setColor == index ?'#fff':'#000'}]}>{item.category_name}</Text>*/}
                    <Image source={{uri: item.image}} style={[style.img_cat_header,]}/>
                </TouchableOpacity>
                {/* } */}
            </View>
        )
    }
    sample_p1(){
        if(this.state.Videos.length >= 3){
            if(this.state.Videos[0].position == "1"){
                return(
                <TouchableOpacity activeOpacity={.7} style={{marginLeft:5}} onPress={()=>Actions.push('MainVideoPlayer',{urlVideo:this.state.Videos[0].video_url})}>
                    <Image source={{uri:this.state.Videos[0].image_url}} style={[style.tablighat,{width: width * .44}]}/>
                    <View style={{position:'absolute',left:'30%',top:'15%',backgroundColor:'#000',justifyContent:'center',alignItems:'center',width:50,height:50,borderRadius:25}}>
                        <Image source={require('../../assets/images/play-button.png')} style={{width:20,height:20}}/>
                    </View>
                    <Text style={{fontFamily: 'IRANSansMobile', textAlign:'center',fontSize:12, color:'black',paddingVertical:5}}>{this.state.Videos[0].title}</Text>
                </TouchableOpacity>
                )
            }else if(this.state.Videos[1].position == "1"){
                return(
                    <TouchableOpacity activeOpacity={.7} style={{marginLeft:5}} onPress={()=>Actions.push('MainVideoPlayer',{urlVideo:this.state.Videos[1].video_url})}>
                        <Image source={{uri:this.state.Videos[1].image_url}} style={[style.tablighat,{width: width * .44}]}/>
                        <View style={{position:'absolute',left:'30%',top:'15%',backgroundColor:'#000',justifyContent:'center',alignItems:'center',width:50,height:50,borderRadius:25}}>
                            <Image source={require('../../assets/images/play-button.png')} style={{width:20,height:20}}/>
                        </View>
                        <Text style={{fontFamily: 'IRANSansMobile', textAlign:'center',fontSize:12, color:'black',paddingVertical:5}}>{this.state.Videos[1].title}</Text>
                    </TouchableOpacity>
                    )
            }
            else if(this.state.Videos[2].position == "1"){
                return(
                    <TouchableOpacity activeOpacity={.7} style={{marginLeft:5}} onPress={()=>Actions.push('MainVideoPlayer',{urlVideo:this.state.Videos[2].video_url})}>
                        <Image source={{uri:this.state.Videos[2].image_url}} style={[style.tablighat,{width: width * .44}]}/>
                        <View style={{position:'absolute',left:'30%',top:'15%',backgroundColor:'#000',justifyContent:'center',alignItems:'center',width:50,height:50,borderRadius:25}}>
                            <Image source={require('../../assets/images/play-button.png')} style={{width:20,height:20}}/>
                        </View>
                        <Text style={{fontFamily: 'IRANSansMobile', textAlign:'center',fontSize:12, color:'black',paddingVertical:5}}>{this.state.Videos[2].title}</Text>
                    </TouchableOpacity>
                    )
            }
            else{
                return <Image source={require('../../assets/images/temp.jpg')} style={{width:width * .44,height: height * .15,borderRadius:20,marginLeft:10}} />
            }
        }
        else if(this.state.Videos.length >= 2){

            if(this.state.Videos[0].position == "1"){
                return(
                <TouchableOpacity activeOpacity={.7} style={{marginLeft:5}} onPress={()=>Actions.push('MainVideoPlayer',{urlVideo:this.state.Videos[0].video_url})}>
                    <Image source={{uri:this.state.Videos[0].image_url}} style={[style.tablighat,{width: width * .44}]}/>
                    <View style={{position:'absolute',left:'30%',top:'15%',backgroundColor:'#000',justifyContent:'center',alignItems:'center',width:50,height:50,borderRadius:25}}>
                        <Image source={require('../../assets/images/play-button.png')} style={{width:20,height:20}}/>
                    </View>
                    <Text style={{fontFamily: 'IRANSansMobile', textAlign:'center',fontSize:12, color:'black',paddingVertical:5}}>{this.state.Videos[0].title}</Text>
                </TouchableOpacity>
                )
            }else if(this.state.Videos[1].position == "1"){
                return(
                    <TouchableOpacity activeOpacity={.7} style={{marginLeft:5}} onPress={()=>Actions.push('MainVideoPlayer',{urlVideo:this.state.Videos[1].video_url})}>
                        <Image source={{uri:this.state.Videos[1].image_url}} style={[style.tablighat,{width: width * .44}]}/>
                        <View style={{position:'absolute',left:'30%',top:'15%',backgroundColor:'#000',justifyContent:'center',alignItems:'center',width:50,height:50,borderRadius:25}}>
                            <Image source={require('../../assets/images/play-button.png')} style={{width:20,height:20}}/>
                        </View>
                        <Text style={{fontFamily: 'IRANSansMobile', textAlign:'center',fontSize:12, color:'black',paddingVertical:5}}>{this.state.Videos[1].title}</Text>
                    </TouchableOpacity>
                    )
            }
            else{
                return <Image source={require('../../assets/images/temp.jpg')} style={{width:width * .44,height: height * .15,borderRadius:20,marginLeft:10}} />
            }
        }
        else if(this.state.Videos.length >= 1){

            if(this.state.Videos[0].position == "1"){
                return(
                <TouchableOpacity activeOpacity={.7} style={{marginLeft:5}} onPress={()=>Actions.push('MainVideoPlayer',{urlVideo:this.state.Videos[0].video_url})}>
                    <Image source={{uri:this.state.Videos[0].image_url}} style={[style.tablighat,{width: width * .44}]}/>
                    <View style={{position:'absolute',left:'30%',top:'15%',backgroundColor:'#000',justifyContent:'center',alignItems:'center',width:50,height:50,borderRadius:25}}>
                        <Image source={require('../../assets/images/play-button.png')} style={{width:20,height:20}}/>
                    </View>
                    <Text style={{fontFamily: 'IRANSansMobile', textAlign:'center',fontSize:12, color:'black',paddingVertical:5}}>{this.state.Videos[0].title}</Text>
                </TouchableOpacity>
                )
            }
            else{
                return <Image source={require('../../assets/images/temp.jpg')} style={{width:width * .44,height: height * .15,borderRadius:20,marginLeft:10}} />
            }
        }
        else{
            return <Image source={require('../../assets/images/temp.jpg')} style={{width:width * .44,height: height * .15,borderRadius:20,marginLeft:10}} />
        }          
    }
    sample_p2(){

        if(this.state.Videos.length >= 3){
                if(this.state.Videos[0].position == "2"){
                    return(
                    <TouchableOpacity activeOpacity={.7} style={{marginLeft:5}} onPress={()=>Actions.push('MainVideoPlayer',{urlVideo:this.state.Videos[0].video_url})}>
                        <Image source={{uri:this.state.Videos[0].image_url}} style={[style.tablighat,{width: width * .44}]}/>
                        <View style={{position:'absolute',left:'30%',top:'15%',backgroundColor:'#000',justifyContent:'center',alignItems:'center',width:50,height:50,borderRadius:25}}>
                            <Image source={require('../../assets/images/play-button.png')} style={{width:20,height:20}}/>
                        </View>
                        <Text style={{fontFamily: 'IRANSansMobile', textAlign:'center',fontSize:12, color:'black',paddingVertical:5}}>{this.state.Videos[0].title}</Text>
                    </TouchableOpacity>
                    )
                }else if(this.state.Videos[1].position == "2"){
                    return(
                        <TouchableOpacity activeOpacity={.7} style={{marginLeft:5}} onPress={()=>Actions.push('MainVideoPlayer',{urlVideo:this.state.Videos[1].video_url})}>
                            <Image source={{uri:this.state.Videos[1].image_url}} style={[style.tablighat,{width: width * .44}]}/>
                            <View style={{position:'absolute',left:'30%',top:'15%',backgroundColor:'#000',justifyContent:'center',alignItems:'center',width:50,height:50,borderRadius:25}}>
                                <Image source={require('../../assets/images/play-button.png')} style={{width:20,height:20}}/>
                            </View>
                            <Text style={{fontFamily: 'IRANSansMobile', textAlign:'center',fontSize:12, color:'black',paddingVertical:5}}>{this.state.Videos[1].title}</Text>
                        </TouchableOpacity>
                        )
                }
                else if(this.state.Videos[2].position == "2"){
                    return(
                        <TouchableOpacity activeOpacity={.7} style={{marginLeft:5}} onPress={()=>Actions.push('MainVideoPlayer',{urlVideo:this.state.Videos[2].video_url})}>
                            <Image source={{uri:this.state.Videos[2].image_url}} style={[style.tablighat,{width: width * .44}]}/>
                            <View style={{position:'absolute',left:'30%',top:'15%',backgroundColor:'#000',justifyContent:'center',alignItems:'center',width:50,height:50,borderRadius:25}}>
                                <Image source={require('../../assets/images/play-button.png')} style={{width:20,height:20}}/>
                            </View>
                            <Text style={{fontFamily: 'IRANSansMobile', textAlign:'center',fontSize:12, color:'black',paddingVertical:5}}>{this.state.Videos[2].title}</Text>
                        </TouchableOpacity>
                        )
                }
                else{
                    return <Image source={require('../../assets/images/temp.jpg')} style={{width:width * .44,height: height * .15,borderRadius:20,marginLeft:10}} />
                }
        }
        else if(this.state.Videos.length >= 2){

            if(this.state.Videos[0].position == "2"){
                return(
                <TouchableOpacity activeOpacity={.7} style={{marginLeft:5}} onPress={()=>Actions.push('MainVideoPlayer',{urlVideo:this.state.Videos[0].video_url})}>
                    <Image source={{uri:this.state.Videos[0].image_url}} style={[style.tablighat,{width: width * .44}]}/>
                    <View style={{position:'absolute',left:'30%',top:'15%',backgroundColor:'#000',justifyContent:'center',alignItems:'center',width:50,height:50,borderRadius:25}}>
                        <Image source={require('../../assets/images/play-button.png')} style={{width:20,height:20}}/>
                    </View>
                    <Text style={{fontFamily: 'IRANSansMobile', textAlign:'center',fontSize:12, color:'black',paddingVertical:5}}>{this.state.Videos[0].title}</Text>
                </TouchableOpacity>
                )
            }else if(this.state.Videos[1].position == "2"){
                return(
                    <TouchableOpacity activeOpacity={.7} style={{marginLeft:5}} onPress={()=>Actions.push('MainVideoPlayer',{urlVideo:this.state.Videos[1].video_url})}>
                        <Image source={{uri:this.state.Videos[1].image_url}} style={[style.tablighat,{width: width * .44}]}/>
                        <View style={{position:'absolute',left:'30%',top:'15%',backgroundColor:'#000',justifyContent:'center',alignItems:'center',width:50,height:50,borderRadius:25}}>
                            <Image source={require('../../assets/images/play-button.png')} style={{width:20,height:20}}/>
                        </View>
                        <Text style={{fontFamily: 'IRANSansMobile', textAlign:'center',fontSize:12, color:'black',paddingVertical:5}}>{this.state.Videos[1].title}</Text>
                    </TouchableOpacity>
                    )
            }
            else{
                return <Image source={require('../../assets/images/temp.jpg')} style={{width:width * .44,height: height * .15,borderRadius:20,marginLeft:10}} />
            }
        }
        else if(this.state.Videos.length >= 1){

            if(this.state.Videos[0].position == "2"){
                return(
                <TouchableOpacity activeOpacity={.7} style={{marginLeft:5}} onPress={()=>Actions.push('MainVideoPlayer',{urlVideo:this.state.Videos[0].video_url})}>
                    <Image source={{uri:this.state.Videos[0].image_url}} style={[style.tablighat,{width: width * .44}]}/>
                    <View style={{position:'absolute',left:'30%',top:'15%',backgroundColor:'#000',justifyContent:'center',alignItems:'center',width:50,height:50,borderRadius:25}}>
                        <Image source={require('../../assets/images/play-button.png')} style={{width:20,height:20}}/>
                    </View>
                    <Text style={{fontFamily: 'IRANSansMobile', textAlign:'center',fontSize:12, color:'black',paddingVertical:5}}>{this.state.Videos[0].title}</Text>
                </TouchableOpacity>
                )
            }
            else{
                return <Image source={require('../../assets/images/temp.jpg')} style={{width:width * .44,height: height * .15,borderRadius:20,marginLeft:10}} />
            }
        }
        else{
            return <Image source={require('../../assets/images/temp.jpg')} style={{width:width * .44,height: height * .15,borderRadius:20,marginLeft:10}} />
        }    
    }
    sample_p3(){
        if(this.state.Videos.length >= 3){
            if(this.state.Videos[0].position == "3"){
                return(
                    <TouchableOpacity activeOpacity={.7} style={{flex:1,marginTop:width*.02}} onPress={()=>Actions.push('MainVideoPlayer',{urlVideo:this.state.Videos[0].video_url})}>
                        <Image source={{uri:this.state.Videos[0].image_url}} style={[style.tablighat]}/>
                        <View style={{position:'absolute',left:width/2.6,top:'15%',backgroundColor:'#000',justifyContent:'center',alignItems:'center',width:50,height:50,borderRadius:25}}>
                            <Image source={require('../../assets/images/play-button.png')} style={{width:20,height:20}}/>
                        </View>
                        <Text style={{fontFamily: 'IRANSansMobile', textAlign:'center',fontSize:12, color:'black',paddingVertical:5}}>{this.state.Videos[0].title}</Text>
                    </TouchableOpacity>
                )
            }else if(this.state.Videos[1].position == "3"){
                return(
                    <TouchableOpacity activeOpacity={.7} style={{flex:1,marginTop:width*.02}} onPress={()=>Actions.push('MainVideoPlayer',{urlVideo:this.state.Videos[1].video_url})}>
                        <Image source={{uri:this.state.Videos[1].image_url}} style={[style.tablighat]}/>
                        <View style={{position:'absolute',left:width/2.6,top:'15%',backgroundColor:'#000',justifyContent:'center',alignItems:'center',width:50,height:50,borderRadius:25}}>
                            <Image source={require('../../assets/images/play-button.png')} style={{width:20,height:20}}/>
                        </View>
                        <Text style={{fontFamily: 'IRANSansMobile', textAlign:'center',fontSize:12, color:'black',paddingVertical:5}}>{this.state.Videos[1].title}</Text>
                    </TouchableOpacity>
                    )
            }
            else if(this.state.Videos[2].position == "3"){
                return(
                    <TouchableOpacity activeOpacity={.7} style={{flex:1,marginTop:width*.02}} onPress={()=>Actions.push('MainVideoPlayer',{urlVideo:this.state.Videos[2].video_url})}>
                        <Image source={{uri:this.state.Videos[2].image_url}} style={[style.tablighat]}/>
                        <View style={{position:'absolute',left:width/2.6,top:'15%',backgroundColor:'#000',justifyContent:'center',alignItems:'center',width:50,height:50,borderRadius:25}}>
                            <Image source={require('../../assets/images/play-button.png')} style={{width:20,height:20}}/>
                        </View>
                        <Text style={{fontFamily: 'IRANSansMobile', textAlign:'center',fontSize:12, color:'black',paddingVertical:5}}>{this.state.Videos[2].title}</Text>
                    </TouchableOpacity>
                    )
            }
            else{
                return <Image resizeMode='stretch' source={require('../../assets/images/temp.jpg')} style={[style.tablighat,{borderRadius:10,marginTop:10}]} />
            }
        }
        else if(this.state.Videos.length >= 2){

            if(this.state.Videos[0].position == "3"){
                return(
                    <TouchableOpacity activeOpacity={.7} style={{flex:1,marginTop:width*.02}} onPress={()=>Actions.push('MainVideoPlayer',{urlVideo:this.state.Videos[0].video_url})}>
                        <Image source={{uri:this.state.Videos[0].image_url}} style={[style.tablighat]}/>
                        <View style={{position:'absolute',left:width/2.6,top:'15%',backgroundColor:'#000',justifyContent:'center',alignItems:'center',width:50,height:50,borderRadius:25}}>
                            <Image source={require('../../assets/images/play-button.png')} style={{width:20,height:20}}/>
                        </View>
                        <Text style={{fontFamily: 'IRANSansMobile', textAlign:'center',fontSize:12, color:'black',paddingVertical:5}}>{this.state.Videos[0].title}</Text>
                    </TouchableOpacity>
                )
            }else if(this.state.Videos[1].position == "3"){
                return(
                    <TouchableOpacity activeOpacity={.7} style={{flex:1,marginTop:width*.02}} onPress={()=>Actions.push('MainVideoPlayer',{urlVideo:this.state.Videos[1].video_url})}>
                        <Image source={{uri:this.state.Videos[1].image_url}} style={[style.tablighat]}/>
                        <View style={{position:'absolute',left:width/2.6,top:'15%',backgroundColor:'#000',justifyContent:'center',alignItems:'center',width:50,height:50,borderRadius:25}}>
                            <Image source={require('../../assets/images/play-button.png')} style={{width:20,height:20}}/>
                        </View>
                        <Text style={{fontFamily: 'IRANSansMobile', textAlign:'center',fontSize:12, color:'black',paddingVertical:5}}>{this.state.Videos[1].title}</Text>
                    </TouchableOpacity>
                    )
            }
            else{
              return <Image resizeMode='stretch' source={require('../../assets/images/temp.jpg')} style={[style.tablighat,{borderRadius:10,marginTop:10}]} />
            }
        }
        else if(this.state.Videos.length >= 1){

            if(this.state.Videos[0].position == "3"){
                return(
                    <TouchableOpacity activeOpacity={.7} style={{flex:1,marginTop:width*.02}} onPress={()=>Actions.push('MainVideoPlayer',{urlVideo:this.state.Videos[0].video_url})}>
                        <Image source={{uri:this.state.Videos[0].image_url}} style={[style.tablighat]}/>
                        <View style={{position:'absolute',left:width/2.6,top:'15%',backgroundColor:'#000',justifyContent:'center',alignItems:'center',width:50,height:50,borderRadius:25}}>
                            <Image source={require('../../assets/images/play-button.png')} style={{width:20,height:20}}/>
                        </View>
                        <Text style={{fontFamily: 'IRANSansMobile', textAlign:'center',fontSize:12, color:'black',paddingVertical:5}}>{this.state.Videos[0].title}</Text>
                    </TouchableOpacity>
                )
            }
            else{
                return <Image resizeMode='stretch' source={require('../../assets/images/temp.jpg')} style={[style.tablighat,{borderRadius:10,marginTop:10}]} />
            }
        }
        else{
            return <Image resizeMode='stretch' source={require('../../assets/images/temp.jpg')} style={[style.tablighat,{borderRadius:10,marginTop:10}]} />
        }    
    }

    async GetDataCategory() {
       try {
        const details = {
            "admintoken": this.props.global.adminToken,
        };
        let result = await Connect.sendPost(this.props.global.baseApiUrl + '/post/getpostcats', details);
        console.log(result)
        if (result.success == true) {
            let item1 = result.data.filter(a => a.parent === 0 && a.category_name !== 'دسته بندی نشده');
            await this.setState({ListCat: result.data,ListCat1:item1,loading:false})
        // console.log(this.state.ListCat)
        } else {
            this.setState({loading:false});
        }
       } catch (error) {
        this.setState({loading:false});
       }
    }
   getToPostsOrCategory(item){
       if(item.name === 'سرگرمی' || item.name === 'آموزش'){
            Actions.push('Categories', {ItemId: item.id,AllCat:this.state.ListCat,itemName:item.name})
            }else{
                Actions.push('Posts', {idCat: item.id,itemName:item.name})
            }
   }

    renderItemSubCat({item, index}) {
        return (
            <TouchableOpacity activeOpacity={.7} style={style.viewSubCat}
                              onPress={this.getToPostsOrCategory.bind(this,item)}>
                <Image source={{uri: item.image}}
                       style={style.imageIconSubCat} resizeMode={'contain'}/>
                <Text style={style.txt_cat_header}>{item.name}</Text>
            </TouchableOpacity>
        );
    }

    render() {
        return (
            <View style={style.containerHome}>
             
                
                    {/*<LinearGradient style={[style.view_header_user]} start={{x: 0, y: 0}} end={{x: 1, y: 0}}*/}
                    {/*                colors={['#8855ba', '#69319f', '#662e9d']}>*/}
                    <View style={[style.view_header_user,{height:1}]}>
           
                        <Header transparent style={style.view_top_header}>
                            <StatusBar backgroundColor="transparent" translucent={true} barStyle="dark-content"/>

                            <Left style={{flex: 1,}}/>
                            {/*    <Button transparent>*/}
                            {/*        <TouchableOpacity activeOpacity={.5}>*/}
                            {/*            <Icon name="arrow-left" color={"#ffffff"} size={width * .07}/>*/}
                            {/*        </TouchableOpacity>*/}

                            {/*    </Button>*/}
                            {/*</Left>*/}
                            <Body style={{flex: 1, alignItems: 'center'}}>
                                <Title style={style.txt_top_header}>تیتراَپ</Title>
                            </Body>
                            <Right style={{flex: 1}}>
                                <Button transparent onPress={() => Actions.drawerOpen()}>
                                    <Icon name="bars" color={"#000"} size={width * .07}/>
                                </Button>
                            </Right>
                        </Header>
                    </View>
                    {/*//</LinearGradient>*/}
                    <View style={style.viewTopHeaderHome}>
                        {/*<Image source={require('../assets/images/topHeaderHome.png')} style={style.topHeaderHome}*/}
                        {/*       resizeMode={'stretch'}/>*/}
                    </View>
                    <View style={style.topHome}>
               
                    {(!this.state.loading) &&
                        <FlatList
                            horizontal
                            showsHorizontalScrollIndicator={false}
                            keyExtractor={(item, index) => index.toString()}
                            data={this.state.ListCat1}
                            renderItem={this.renderListCat}
                            extraData={this.state.changed}
                            ListEmptyComponent={this.ListCatEmpty}
                        />
                        }
                    {(this.state.loading) &&
                    <View style={style.viewNotFound}>
                        <ActivityIndicator  color={this.props.global.grColorTwo}  />
                    </View>
                    }
                    </View>
                
                
                <ScrollView>
                    <View style={style.centerView}>
                        <View style={style.viewSubCatContainer}>
                            <View style={style.viewTopSubCat}>
                                {/*<View style={style.viewShowAll}>*/}
                                {/*    <Image source={require('../assets/images/transection_dot.png')}*/}
                                {/*           style={style.transectionDot} resizeMode={'contain'}/>*/}
                                {/*    <Text>{'مشاهده همه'}</Text>*/}
                                {/*</View>*/}
                                <Text style={style.txtNameCategory}>{TextCat}</Text>
                            </View>
                            <View style={style.viewContainerCenterHome}>
                                <View style={style.topContainerCenterHome}>
                                    <FlatList
                                        keyExtractor={(item, index) => index.toString()}
                                        data={this.state.ListSubCat}
                                        numColumns={3}
                                        renderItem={this.renderItemSubCat}/>
                                </View>
                                <TouchableOpacity style={style.viewSubCatRequest}
                                                  onPress={() => Actions.push('Search1')}>
                                    <Image source={require('../../assets/images/technical-support.png')}
                                           style={style.imageIconSubCat}/>
                                    <View>
                                        <Text style={style.txt_cat_header}>{'درخواست خدمات'}</Text>
                                        <Text
                                            style={style.txt_cat_header1}>{'از این بخش درخواست خود را ارسال کنید!'}</Text>
                                    </View>
                                </TouchableOpacity>
                            </View>

                        </View>
                        {!this.state.loadingVideo && 
                        <View style={style.viewImageTablight}>
                            <View style={{flex:1,flexDirection:'row'}}>
                            {this.sample_p1()}
                            {this.sample_p2()}
                            </View>
                            {this.sample_p3()}
                        </View>
                    }
                    {this.state.loadingVideo && 
                    <ActivityIndicator  color={this.props.global.grColorTwo}/>
                    }
                    </View>
                
                </ScrollView>

                <Modal isVisible={this.state.visibleComplateProfile}
                       deviceWidth={width}
                       deviceHeight={height}
                       animationType={"slide"}
                       hasBackdrop={true}
                       coverScreen={false}
                       backdropColor={'#000000'}
                       backdropOpacity={0.70}
                       transparent={false}
                       onRequestClose={() => {
                           alert("Modal has been closed.")
                       }}
                    // onBackdropPress={() => this.setState({isModalSupport: !this.state.isModalSupport})}
                       style={style.ModalComplateProfile}
                >
                    <View style={style.viewContainerSupport}>
                        {/* <View style={style.viewModal}> */}
                            <View style={style.viewTextModalComplateProfile}>
                               <Text style={style.txt_modal_complateProfile}>
                                   با سلام به اپلیکیشن تیتراَپ خوش آمدید!
                                   لطفا برای استفاده از تمام امکانات اپلیکیشن به پخش ویرایش پروفایل بروید
                                   و پروفایل خودتان را تکمیل نمایید
                                </Text>
                            </View>
                            {/* <View style={style.view_imgModalComplateProfile}>
                                     <Image source={require('../../assets/images/back_login.png')} style={style.imgModalComplateProfile}
                               resizeMode={'center'}/>
                            </View> */}
                        {/* </View> */}
                        <TouchableOpacity style={style.btnSupport1} activeOpacity={.7}
                                          onPress={() => this.setState({visibleComplateProfile: false})}>
                            <Image source={require('../../assets/images/close.png')} resizeMode={'contain'} style={{width:width*.05,height:width*.05}}/>
                        </TouchableOpacity>
                        
                    </View>
                </Modal>
              
             {this.state.visibleComplateProfile !== true &&
                <View>
                    <BottomNavigation styleBottom={'home'} color_home={'#48147c'}/>
                </View>
            }
            </View>
        );
    }

    
}
const mapDispatchToProps = dispatch => {
    return {
        setUser: users => {
            dispatch(setUser(users))
        }
    }
};

const mapStateToProps = (state) => {
    return {
        user: state.user,
        global:state.global
    };
};
export default connect(mapStateToProps, mapDispatchToProps)(Home_Screen)

