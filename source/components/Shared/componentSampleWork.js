import React, {Component} from 'react';
import {
    Dimensions,
    View,
    Text, Platform, FlatList, StyleSheet,
    BackHandler, StatusBar, TouchableOpacity, TextInput, KeyboardAvoidingView, Image
} from 'react-native';
import ServerData from '../connection/connect';


const width = Dimensions.get('window').width;
const height = Dimensions.get('window').height;
const Connect = new ServerData();

export default class ComponentSampleWork extends Component {

    constructor(props) {
        super(props);
        this.state = {}
    };

    render() {
        const message = this.props.message;
        return (

            <View style={style.SampleWork_container}>

                <View style={style.view_name}>
                    <Image source={{uri:message.original}} style={style.img_sampleWork} resizeMode={'contain'}/>
                </View>

            </View>
        );

    }
}


const style = StyleSheet.create({
    SampleWork_container: {
        flex: 1,
        flexDirection: 'row',
       marginTop:height*.005,
        justifyContent: "flex-end"
    },
    view_name: {width:width*.33, height: width * .33,alignItems:'center',justifyContent:'center', backgroundColor: '#fff',},
    img_sampleWork:{width:width*.33,height:width*.33}
});