import React, {Component} from 'react';
import {
    Text,
    TouchableOpacity,
    Dimensions,
    View,
    TouchableNativeFeedback,
    Image, 
} from 'react-native';
import AsyncStorage from '@react-native-community/async-storage';
// import BottomNavigation, {FullTab} from 'react-native-material-bottom-navigation';
import Icon from 'react-native-vector-icons/FontAwesome5';
import ServerData from '../connection/connect';
import style from '../../assets/styles/bottomNavigation_css';
import {Actions} from "react-native-router-flux";
import LinearGradient from "react-native-linear-gradient";
import {connect} from "react-redux";
import Helpers from './Helper';
import {setUser}from '../../Redux/Actions'
import { from } from 'jalali-moment';

const width = Dimensions.get('window').width;
const Connect = new ServerData();

class bottomNavigation extends Component {

    constructor(props) {
        super(props);
        this.state = {
            activeTab: '',
            count:0
        }
    };

    render() {
        const {styleBottom, color_group = '#999aa0', color_home = '#999aa0', color_members = '#999aa0', color_prof = '#999aa0'} = this.props;
        let apiToken = this.props.user.apiToken;
        return (
            //money-bill-wave
            <View>
                {styleBottom === 'home' &&
                <View style={[style.view_bottom_user_home,{borderTopColor:'white',paddingVertical:5}]}>
                    <TouchableNativeFeedback  onPress={() => Actions.push('support')}>
                        <View style={style.view_group_navigation_bottom}>
                            <Icon size={width * .05} color={color_group} name={'question-circle'}/>
                            <Text style={[style.txt_navigate_bottom, {color: color_group}]}>پشتیبانی</Text>
                        </View>
                    </TouchableNativeFeedback>
                    <TouchableNativeFeedback onPress={() => Actions.push('Search4',{item:''})}>
                        <View style={style.view_member_navigation_bottom}>
                            <Icon size={width * .05} color={color_members} name={'users'}/>
                            <Text style={[style.txt_navigate_bottom, {color: color_members}]}>اعضا</Text>
                        </View>
                    </TouchableNativeFeedback>
                    <TouchableNativeFeedback onPress={() => Actions.push('subscribe',{item:''})}>
                        <View style={style.view_member_navigation_bottom}>
                            <Icon size={width * .05} color={color_members} name={'money-bill-wave'}/>
                            <Text style={[style.txt_navigate_bottom, {color: color_members}]}>اشتراک</Text>
                        </View>
                    </TouchableNativeFeedback>
                    {/* {this.props.user.apiToken === null &&
                        <TouchableNativeFeedback background={TouchableNativeFeedback.Ripple('rgba(0.0.0.1)')}
                                                 onPress={() => Actions.replace('Home_Screen')}>
                            <View style={style.view_profile_navigation_bottom}>
                                <Icon size={width * .05} color={color_home} name={'home'}/>
                                <Text style={[style.txt_navigate_bottom, {color: color_home}]}>خانه</Text>
                            </View>
                        </TouchableNativeFeedback>
                    } */}
                    {this.props.user.apiToken !== null &&
                        <TouchableNativeFeedback background={TouchableNativeFeedback.Ripple('rgba(0.0.0.1)')}
                                                 onPress={() => Actions.push('MessageInBox')}>
                            <View style={style.view_profile_navigation_bottom}>
                                <Icon size={width * .05} color={color_prof} name={'envelope'}/>
                                <Text style={{width:20,height:20,borderRadius:10,justifyContent:'center',textAlign:'center',alignItems:'center',backgroundColor:'red',fontSize:13,color:'white',position:'absolute',top:0,right:20}}>{Helpers.ToPersianNumber(String(this.props.user.messageCount))}</Text>
                                <Text style={[style.txt_navigate_bottom, {color: color_prof}]}>پیام ها</Text>
                            </View>
                        </TouchableNativeFeedback>
                    }
                        <TouchableNativeFeedback background={TouchableNativeFeedback.Ripple('rgba(0.0.0.1)')}
                                                 onPress={() => Actions.replace('Home_Screen')}>
                            <View style={style.view_profile_navigation_bottom}>
                                <Icon size={width * .05} color={color_prof} name={'home'}/>
                                <Text style={[style.txt_navigate_bottom, {color: color_prof}]}>تیتراَپ</Text>
                            </View>
                        </TouchableNativeFeedback>
                        {this.props.user.apiToken !== null &&
                        <TouchableNativeFeedback background={TouchableNativeFeedback.Ripple('rgba(0.0.0.1)')}
                                                 onPress={() => Actions.push('profile')}>
                            <View style={style.view_profile_navigation_bottom}>
                                <Icon size={width * .05} color={color_prof} name={'user-circle'}/>
                                <Text style={[style.txt_navigate_bottom, {color: color_prof}]}>پروفایل</Text>
                            </View>
                        </TouchableNativeFeedback>
                    }
                </View>
                }
                {styleBottom === 'other' &&
                <LinearGradient style={[style.view_bottom_user,{paddingVertical:5}]} start={{x: 0, y: 0}} end={{x: 1, y: 0}}
                                colors={['#6941d8', '#5e36ca', '#4b24b1']}>
                    <TouchableNativeFeedback onPress={() => Actions.push('support')}>
                        <View style={style.view_group_navigation_bottom}>
                            <Icon size={width * .05} color={color_group} name={'question-circle'}/>
                            <Text style={[style.txt_navigate_bottom, {color: color_group}]}>پشتیبانی</Text>
                        </View>
                    </TouchableNativeFeedback>
                    <TouchableNativeFeedback onPress={() => Actions.push('Search4',{item:''})}>
                        <View style={style.view_member_navigation_bottom}>
                            <Icon size={width * .05} color={color_members} name={'users'}/>
                            <Text style={[style.txt_navigate_bottom, {color: color_members}]}>اعضا</Text>
                        </View>
                    </TouchableNativeFeedback>
                    <TouchableNativeFeedback onPress={() => Actions.push('subscribe',{item:''})}>
                        <View style={style.view_member_navigation_bottom}>
                            <Icon size={width * .05} color={color_members} name={'money-bill-wave'}/>
                            <Text style={[style.txt_navigate_bottom, {color: color_members}]}>اشتراک</Text>
                        </View>
                    </TouchableNativeFeedback>
                    {/* {this.props.user.apiToken === null &&
                        <TouchableNativeFeedback background={TouchableNativeFeedback.Ripple('rgba(0.0.0.1)')}
                                                 onPress={() => Actions.replace('Home_Screen')}>
                            <View style={style.view_profile_navigation_bottom}>
                                <Icon size={width * .05} color={color_home} name={'home'}/>
                                <Text style={[style.txt_navigate_bottom, {color: color_home}]}>خانه</Text>
                            </View>
                        </TouchableNativeFeedback>
                        } */}
                        {this.props.user.apiToken !== null &&
                        <TouchableNativeFeedback background={TouchableNativeFeedback.Ripple('rgba(0.0.0.1)')}
                                                 onPress={() => Actions.push('MessageInBox')}>
                            <View style={style.view_profile_navigation_bottom}>
                                <Icon size={width * .05} color={color_prof} name={'envelope'}/>
                                <Text style={{width:20,height:20,borderRadius:10,justifyContent:'center',textAlign:'center',alignItems:'center',backgroundColor:'red',fontSize:13,color:'white',position:'absolute',top:0,right:20}}>
                                    {this.props.user.messageCount != undefined ? Helpers.ToPersianNumber(String(this.props.user.messageCount)) : '-'}</Text>
                                <Text style={[style.txt_navigate_bottom, {color: color_prof}]}>پیام ها</Text>
                            </View>
                        </TouchableNativeFeedback>
                    }
                        <TouchableNativeFeedback background={TouchableNativeFeedback.Ripple('rgba(0.0.0.1)')}
                                                 onPress={() => Actions.replace('Home_Screen')}>
                            <View style={style.view_profile_navigation_bottom}>
                                <Icon size={width * .05} color={color_prof} name={'home'}/>
                                <Text style={[style.txt_navigate_bottom, {color: color_prof}]}>تیتراَپ</Text>
                            </View>
                        </TouchableNativeFeedback>
                        {this.props.user.apiToken !== null &&
                        <TouchableNativeFeedback background={TouchableNativeFeedback.Ripple('rgba(0.0.0.1)')}
                                                 onPress={() => Actions.push('profile')}>
                            <View style={style.view_profile_navigation_bottom}>
                                <Icon size={width * .05} color={color_prof} name={'user-circle'}/>
                                <Text style={[style.txt_navigate_bottom, {color: color_prof}]}>پروفایل</Text>
                            </View>
                        </TouchableNativeFeedback>
                    }

                </LinearGradient>
                }
            </View>
        );

    }
}
const mapDispatchToProps = dispatch => {
    return {
        setUser: users => {
            dispatch(setUser(users))
        }
    }
};
const mapStateToProps = (state) => {
    return {
        user: state.user,
        global: state.global
    };
};
export default connect(mapStateToProps, mapDispatchToProps)(bottomNavigation)


