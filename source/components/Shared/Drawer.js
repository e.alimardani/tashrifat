import React, {Component} from 'react';
import {Actions} from "react-native-router-flux";
import {
    Dimensions, Image, StyleSheet, Text, TouchableOpacity, 
    View,ScrollView,FlatList
} from 'react-native';
import AsyncStorage from '@react-native-community/async-storage';
import {Accordion} from 'native-base';
import Icon from "react-native-vector-icons/FontAwesome5";
import {setUser} from "../../Redux/Actions";
import {connect} from "react-redux";
import LinearGradient from "react-native-linear-gradient";
import NumberFormat from 'react-number-format';
import Helper from '../Shared/Helper'

const width = Dimensions.get('window').width;
const height = Dimensions.get('window').height;
const dataArray = [
    {
        title: "ویرایش پروفایل",
        content: [

            {name: "ویرایش اطلاعات"},
            // {name: "ویرایش موقعیت مکانی"},
            {name: "ویرایش شبکه های اجتماعی"},

        ],
        image_icon: require('../../assets/images/checked.png')
    },
  ];
class Drawer extends Component {

    constructor(props) {
        super(props);
        this.state = {
            moneyBugs: '0',
         
        };

        this.exitAccount = this.exitAccount.bind(this);
        this.ClickLogin = this.ClickLogin.bind(this);
        this._renderHeader = this._renderHeader.bind(this)
    };

    async componentDidMount(){
        this.getDataMoneyBugs();
    }
   async getDataMoneyBugs(){
        try{
          const details = {
              "admintoken": this.props.global.adminToken,
              "usertoken": this.props.user.apiToken,
          };
          let result = await Connect.sendPost(this.props.global.baseApiUrl+ '/user/getUserCharge', details);
          if (result.success === true) {
             this.setState({moneyBugs:result.data})
              }else {
                
          }
          }catch(err){
              await this.setState({progressVisible:false});
          }
    }
    async exitAccount() {
        await AsyncStorage.removeItem('Token');
        await this.props.setUser({
            id: null,
            username: null,
            email: null,
            full_name: null,
            work_in: null,
            phone_number: null,
            friends_count:null,
            Groups_count: null,
            Avatar: null,
            apiToken: null,
            instegram: null,
            telegram: null,
            whatsapp: null
        });

        Actions.replace('Home_Screen')
    };

     ClickLogin() {
        Actions.push('login');
    }
    _renderHeader(item, expanded) {
        return (
          <View style={style.view_menu_drawer}>
          <Text style={style.txt_menu}>
              {" "}{item.title}
            </Text>
            <Icon name='edit' color={'#000'} size={width * .07}
                 style={{paddingHorizontal: width * .03}}/>
          </View>
        );
      }
    _renderContent(item) {
        return item.content.map((data,index) => {
            return (
              <TouchableOpacity activeOpacity={.7} style={style.view_menu_drawer} onPress={()=> index === 0 ? Actions.push('Edit_profile'):Actions.push('insertSocial')}>
                <Text style={[style.txt_menu,{marginRight:'6%'}]}>{data.name}</Text>
                <Icon name='chevron-left' color={'#000'} size={width * .07}
                    style={{paddingHorizontal: width * .02,marginRight:'10%'}}/>
              </TouchableOpacity>
            )
          })
      }
    render() {
        return (
            <View style={[style.drawer_container]}>
                {this.props.user.apiToken === null &&
                <LinearGradient style={[style.top_drawer]} start={{x: 0, y: 0}} end={{x: 1, y: 0}}
                                colors={['#627efb', '#8b68fc', '#a95bfe']}>
                    <Image source={require('../../assets/images/man.png')} resizeMode={'cover'}
                           style={style.img_header_drawer}/>
                    <Text style={style.txt_emailHeaderDrawer}>{'کاربر مهمان'}</Text>
                </LinearGradient>
                }
                {this.props.user.apiToken !== null &&
                <LinearGradient style={[style.top_drawer,{paddingTop:20}]} start={{x: 0, y: 0}} end={{x: 1, y: 0}}
                                colors={['#627efb', '#8b68fc', '#a95bfe']}>
                        {this.props.user.Avatar !== '' &&
                             <Image source={{uri: this.props.user.Avatar}} resizeMode={'cover'}
                           style={style.img_header_drawer}/>
                        }
                        {this.props.user.Avatar === '' &&
                            <Image source={require('../../assets/images/man.png')} resizeMode={'cover'}
                           style={style.img_header_drawer}/>
                        }
                    {/* <Text style={style.txt_emailHeaderDrawer}>{this.props.user.email}</Text> */}
                    <Text style={[style.txt_phoneHeaderDrawer,{paddingTop:10}]}>{Helper.ToPersianNumber(String(this.props.user.phone_number))}</Text>
                </LinearGradient>
                }
                {this.props.user.apiToken !== null &&
                <View style={{flex:1,flexDirection:'row',backgroundColor:'#111', justifyContent:'center',alignItems:'center'}}>
                <NumberFormat
                    value={parseInt(this.state.moneyBugs)}
                    displayType={'text'}
                    thousandSeparator={true}
                    renderText={value =>
                    <Text style={[style.txt_emailHeaderDrawer,{marginVertical:0}]}>{Helper.ToPersianNumber(String(value))} تومان</Text>}/>
                    <Text style={[style.txt_emailHeaderDrawer,{marginVertical:0}]}>کیف پول :  </Text>
                </View>
                }
                <View style={style.center_drawer}>
                <ScrollView>
                    <TouchableOpacity activeOpacity={.6} style={style.view_menu_drawer}
                                      onPress={() => Actions.push('Home_Screen')}>
                        <Text style={style.txt_menu}>تیتراَپ</Text>
                        <Icon name='home' color={'#000'} size={width * .07}
                              style={{paddingHorizontal: width * .03}}/>
                    </TouchableOpacity>
                    <View style={style.borderBottomDrawer}/>
                    {this.props.user.apiToken !== null &&
                    <View style={style.viewBorderContainer}>
                        <TouchableOpacity activeOpacity={.6} style={style.view_menu_drawer}
                                          onPress={() => Actions.push('profile')}>
                            <Text style={style.txt_menu}>پروفایل کاربری</Text>
                            <Icon name='id-card' color={'#000'} size={width * .07}
                                  style={{paddingHorizontal: width * .03}}/>
                        </TouchableOpacity>
                        <View style={style.borderBottomDrawer}/>
                    </View>
                  }
                  {/* <TouchableOpacity activeOpacity={.6} style={style.view_menu_drawer}
                                      onPress={() => Actions.push('groups')}>
                        <Text style={style.txt_menu}>گروه ها</Text>
                        <Icon name='users' color={'#000'} size={width * .07}
                              style={{paddingHorizontal: width * .03}}/>
                    </TouchableOpacity> */}
                    <View style={style.borderBottomDrawer}/>
                    <TouchableOpacity activeOpacity={.6} style={style.view_menu_drawer}
                                      onPress={() => Actions.push('Search4',{item:''})}>
                        <Text style={style.txt_menu}>اعضاء</Text>
                        <Icon name='users' color={'#000'} size={width * .07} style={{paddingHorizontal: width * .03}}/>
                    </TouchableOpacity>
                    <View style={style.borderBottomDrawer}/>
                    {this.props.user.apiToken !== null &&
                    <Accordion
                    style={{borderColor:'transparent'}}
                        dataArray={dataArray}
                        animation={true}
                        expanded={true}
                        renderHeader={this._renderHeader}
                        renderContent={this._renderContent}
                    />
                    }
                    {/* {this.props.user.apiToken !== null &&
                    <View style={style.viewBorderContainer}>
                        <TouchableOpacity activeOpacity={.6} style={style.view_menu_drawer}
                                          onPress={() => Actions.push('RequestsFollowList')}>
                            <Text style={style.txt_menu}>درخواست های دوستی</Text>
                            <Icon name='heart' color={'#000'} size={width * .07}
                                  style={{paddingHorizontal: width * .03}}/>
                        </TouchableOpacity>
                        <View style={style.borderBottomDrawer}/>
                    </View>
                    } */}
                    {this.props.user.apiToken === null &&
                    <View style={style.viewBorderContainer}>
                        <TouchableOpacity activeOpacity={.6} style={style.view_menu_drawer}
                                          onPress={this.ClickLogin}>
                            <Text style={style.txt_menu}>ثبت نام و ورود</Text>
                            <Icon name='times-circle' color={'#000'} size={width * .07}
                                  style={{paddingHorizontal: width * .03}}/>
                        </TouchableOpacity>
                        < View style={style.borderBottomDrawer}/>
                    </View>
                    }
                    {/* {this.props.user.apiToken !== null &&
                    <View style={style.viewBorderContainer}>
                        <TouchableOpacity activeOpacity={.6} style={style.view_menu_drawer}
                                          onPress={() => Actions.push('support')}>
                            <Text style={style.txt_menu}>پشتیبانی</Text>
                            <Icon name='question-circle' color={'#000'} size={width * .07}
                                  style={{paddingHorizontal: width * .03}}/>
                        </TouchableOpacity>
                        < View style={style.borderBottomDrawer}/>
                    </View>
                    } */}
                                        <View style={style.borderBottomDrawer}/>
                    <TouchableOpacity activeOpacity={.6} style={style.view_menu_drawer}
                                      onPress={() => Actions.push('support')}>
                        <Text style={style.txt_menu}>پشتیبانی</Text>
                        <Icon name='question-circle' color={'#000'} size={width * .07} style={{paddingHorizontal: width * .03}}/>
                    </TouchableOpacity>
                    <View style={style.borderBottomDrawer}/>
                    {this.props.user.apiToken !== null &&
                    <TouchableOpacity style={style.view_menu_drawer} activeOpacity={.6} onPress={this.exitAccount}>
                        <Text style={style.txt_menu}>خروج از حساب کاربری</Text>
                        <Icon name='sign-out-alt' color={'#000'} size={width * .07}
                              style={{paddingHorizontal: width * .03}}/>
                    </TouchableOpacity>
                    }
                    </ScrollView>
                </View>
            </View>
        )
    }
}

const mapDispatchToProps1 = dispatch => {
    return {
        setUser: user => {
            dispatch(setUser(user))
        }
    }
};
const mapStateToProps = (state) => {
    return {
        user: state.user,
        global: state.global
    };
};
export default connect(mapStateToProps, mapDispatchToProps1)(Drawer)
const style = StyleSheet.create({
    view_menu_drawer: {
        width: width * .80,
        height: height * .08,
        justifyContent: 'flex-end',
        alignItems: 'center',
        marginTop: height * .001,
        backgroundColor: '#fff',
        flexDirection: 'row'
    },
    viewBorderContainer: {justifyContent: 'center', alignItems: 'center'},
    borderBottomDrawer: { width: width * .70},
    exitBtn: {justifyContent: 'center', alignItems: 'center'},
    img_header_drawer: {
        width: width * .26,
        height: width * .26,
        borderWidth: width * .01,
        borderColor: '#fff',
        borderRadius: width * .13
    },
    drawer_container: {flex: 10},
    top_drawer: {flex: 3, justifyContent: 'center', alignItems: 'center', backgroundColor: '#848484'},
    center_drawer: {flex: 7, justifyContent: 'flex-start', alignItems: 'center'},
    left_bottom_drawer: {flex: 1, justifyContent: 'center', alignItems: 'center'},
    center_bottom_drawer: {flex: 1, justifyContent: 'center', alignItems: 'center'},
    right_bottom_drawer: {flex: 1, justifyContent: 'center', alignItems: 'center'},
    img_instegram: {width: width * .17, height: height * .04},
    img_instegram1: {width: width * .17, height: height * .04, marginTop: height * .04},
    txt_menu: {fontSize: width / 25, color: '#8b68fc', fontFamily: 'IRANSansMobile',},
    txt_emailHeaderDrawer: {
        color: '#fff',
        fontFamily: 'IRANSansMobile',
        fontSize: width / 28,
        marginVertical: height * .01
    },
    txt_phoneHeaderDrawer: {color: '#fff', fontFamily: 'IRANSansMobile', fontSize: width / 28,}
});
{/* <View style={style.viewBorderContainer}>
<TouchableOpacity activeOpacity={.6} style={style.view_menu_drawer}
                  onPress={() => Actions.push('Edit_profile')}>
    <Text style={style.txt_menu}>ویرایش پروفایل</Text>
    <Icon name='edit' color={'#000'} size={width * .07}
          style={{paddingHorizontal: width * .03}}/>
</TouchableOpacity>
<View style={style.borderBottomDrawer}/>
</View> */}