import React from 'react';
import {StyleSheet, Dimensions, Platform} from 'react-native';


const width = Dimensions.get('window').width;
const height = Dimensions.get('window').height;

export default StyleSheet.create({
    view_container_prof: {
        alignItems: 'center',
        flex:1
    },
    view_header_prof: {
        width: width / 1,
        paddingBottom:15,
        marginBottom:10
        // backgroundColor:'#000'
    },
    view_top_header: {
        marginTop: height * .01,
        // justifyContent:'center',
        // alignItems:'center',
        // flexDirection:'row',
        paddingHorizontal: width * .05
    },
    txt_top_header: {
        // flex:1,
        color: '#fff',
        fontFamily: 'IRANSansMobile',
        textAlign: 'center',
        fontSize: width / 25
    },
    view_center_header: {
        flexDirection: 'row',
        marginTop: height * .05,
        paddingHorizontal: width * .10,
        // backgroundColor: '#000',
        marginBottom: height * .03,
        justifyContent: 'flex-end'
    },
    view_center_headerEditProfile: {
        marginVertical: 0,
        justifyContent: 'center',
        width:width*.20,
        alignSelf:'center',
        alignItems: 'center'
    },
    img_user_prof: {
        width: width * .20,
        height: width * .20,
        borderRadius: width * .10,
        borderWidth: width * .01,
        borderColor: '#323232',
        position: 'absolute',
        bottom: height * .02
    },
    img_user_profEditProfile: {
        width: width * .20,
        height: width * .20,
        borderRadius: width * .10,
        borderWidth: width * .01,
        borderColor: '#fff',
        justifyContent:'center',
        alignItems:'center'
    },
    pic_member: {
        width: width * .18,
        height: width * .18,
        borderRadius: width * .10,

    },

    txt_nameUser_prof_header: {
        ...Platform.select({
            ios: {
                fontFamily: 'IRANSansMobile',
                fontWeight: 'bold'
            },
            android: {
                fontFamily: 'IRANSansMobile_Bold',
            }
        }),
        fontSize: width / 25, color: '#fff'
    },
    view_name_side_prof: {
        marginRight: width * .10,
        justifyContent: 'center'
    },
    txt_post_prof_header: {
        fontFamily: 'IRANSansMobile',
        fontSize: width / 30, color: '#fff'
    },
    view_bottom_header: {
        // backgroundColor:'#4b5aba',
        justifyContent: 'space-around',
        alignItems: 'flex-end',
        flexDirection: 'row'

    },
    view_text_header: {
        flexDirection: 'row',
        justifyContent: 'flex-end',
        alignItems: 'flex-end'
    },
    txt_header_fix: {
        color: '#fff',
        fontSize: width / 30,
        fontFamily: 'IRANSansMobile',
        alignSelf: 'flex-end'
    },
    txt_header: {
        color: '#fff',
        fontSize: width / 30,
        fontFamily: 'IRANSansMobile',
    },
    container_menu_profile: {
        backgroundColor: '#fff', borderRadius: width * .02, width: width * .80, elevation: 2,alignSelf:'center',marginBottom:10

    },
    view_menu_top: {
        width: width * .80,
        flexDirection: 'row',
    },
    tabMenu: {
        flex: 1,
        backgroundColor: '#fbf8ff',
        borderTopLeftRadius: width * .02,
        flexDirection: 'row',
        alignItems: 'center', justifyContent: 'center'
    },
    tabMenu_focus: {
        flex: 1,
        elevation: 2,
        backgroundColor: '#fff',
        borderTopLeftRadius: width * .02,
        flexDirection: 'row',
        alignItems: 'center', justifyContent: 'center'
    },
    tabMenu1: {
        flex: 1,
        backgroundColor: '#fbf8ff',
        borderTopRightRadius: width * .02,
        flexDirection: 'row',
        alignItems: 'center', justifyContent: 'center'
    },
    tabMenu1_focus: {
        flex: 1,
        elevation: 2,
        backgroundColor: '#fff',
        borderTopRightRadius: width * .02,
        flexDirection: 'row',
        alignItems: 'center', justifyContent: 'center'
    },
    txt_tabMenu: {
        fontFamily: 'IRANSansMobile',
        fontSize: width / 25,
        padding:5
    },
    view_bottom_screen: {
        // backgroundColor: "#eeb6b5",
        height: height / 2.6,
        width: width * .85,
        marginTop: height * .07,
        flexDirection: 'row',
        // flex:1
    },
    left_bottom_screen: {
        flex: 1,
        // backgroundColor: "#ee3cbe",
        alignItems: 'center',
        height: height / 2.6,
        justifyContent: 'space-around',
        flexDirection: 'column'
    },
    center_bottom_screen: {
        flex: 1,
        height: height / 2.6,
        justifyContent: 'space-around',
        alignItems: 'center',
        flexDirection: 'column'
    },
    right_bottom_screen: {
        flex: 1,
        height: height / 2.6,
        justifyContent: 'space-around',
        alignItems: 'center',
        flexDirection: 'column'
    },
    txt_icon_bottom: {
        fontFamily: 'IRANSansMobile',
        fontSize: width / 28,
        textAlign: 'center',
        paddingTop: height * .02
    },
    view_icon_name: {
        justifyContent: 'center', alignItems: 'center'
    },

    view_bottom_screen1: {
        backgroundColor: "#eeb6b5",
        height: height / 2.6,
        width: width * .85,
        marginTop: height * .07,
        flexDirection: 'row',
        alignItems: 'flex-start',
        justifyContent: 'center'
        // flex:1
    },
    view_container_form_signUp: {
        marginTop: height * .03,
        // backgroundColor:'#000',
        width: width * .80,
        paddingBottom:20
    },
    label_input: {
        fontFamily: 'IRANSansMobile',
        fontSize: width / 25,
    },
    username_input: {
        color: '#000', fontFamily: 'IRANSansMobile',
        backgroundColor:'rgb(250,239,253)',
        fontSize: width / 28,
        padding:0,
        width: width * .60,
        height:height*.07,
        borderRadius: width * .10,
        textAlign:'center',
        alignItems:'center',
        justifyContent:'center',

    },
    username_inputSkill: {
        color: '#8b68fc', fontFamily: 'IRANSansMobile',
        fontSize: width / 25,
        marginBottom: height * .01,
        textAlign:'center'
    },
    view_input_Item: {marginTop: height * .01,alignSelf:'center'},
    view_input_ItemEditSkill: {marginTop: height * .01, height: height * .07 + 2,
        width: width * .70,alignItems:'center',justifyContent:'center',alignSelf:'center', backgroundColor: '#00000015',},
   
    view_ItemEditSkill: { height: height * .07 + 2,borderBottomWidth:width*.001,borderBottomColor:'#00000020',marginTop:width*.01,
    width: width * .79,alignItems:'flex-end',justifyContent:'center',alignSelf:'center', backgroundColor: 'transparent',},
   
    wrong_userName: {
        color: '#ffed3a', fontFamily: 'IRANSansMobile',
        fontSize: width / 25,
    },
    view_btn_signUp: {
        justifyContent: 'center',
        alignItems: 'center',
        alignSelf: 'center',
        width: width * .30,
        height: height * .07,
        borderRadius: width * .02,
        marginTop: height * .02,
        backgroundColor: '#8b68fc',
        // 1c8bd9
    },
    txt_btn_signUp: {
        color: '#fff',
        fontFamily: 'IRANSansMobile',
        fontSize: width / 25,
    },
    dropdownStyleTextSkill: {
        color: '#2b2b2b',
        // width: width * .25,
        fontSize: width / 25,
        justifyContent: 'center',
        height: height * .07 + 2,
        width: width * .70,
        paddingRight: width * .02,
        backgroundColor: '#fff',
        // borderRadius: width * .10,
        // marginTop: height * .01,
        alignSelf: 'center',
        textAlign: 'center'
    },
    modalTextStyleSkill: {color: '#8b68fc', fontFamily: 'IRANSansMobile', fontSize: width / 25, textAlign: 'center'},
    modalFilterSkill: {
        justifyContent: 'center',
        height: height * .07 + 2,
        width: width * .70,
        paddingRight: width * .02,
        backgroundColor: '#00000015',
        borderRadius: width * .10,
        marginTop: height * .01,
        alignSelf: 'center'
    },
    ModalEditSkillStyle:{
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center',
        margin: 0,
        padding: 0,
        // width:width,
        height:height*70,
        // backgroundColor:'#000',
        borderRadius:20
        // borderTopLeftRadius: width * .15,
        // borderTopRightRadius: width * .15,
    },
    viewModalEditSkillState:{
        backgroundColor:'#8b68fc',
        height: height * .7,
        // width: width * .75,
        // borderTopLeftRadius: width * .12,
        // borderTopRightRadius: width * .12,
        borderRadius:20,
        padding:15,

        justifyContent: 'center', alignItems: 'center',
    },
    itemOptionModal:{width:width*.80,height:height*.06,marginTop:height*.003,justifyContent:'center',alignItems:'center'},
    txt_itemOptionModal:{fontSize:width/25,fontFamily:'IRANSansMobile',color:'#fff'},
    btnExitModal:{backgroundColor:'#00000080',width:width*.10,height:width*.10,position:'absolute',borderRadius:width*.05,zIndex:10,left:0,top:0,justifyContent:'center',alignItems:'center'}

});

