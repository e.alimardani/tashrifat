import React from 'react';
import {StyleSheet, Dimensions, Platform} from 'react-native';


const width = Dimensions.get('window').width;
const height = Dimensions.get('window').height;

export default StyleSheet.create({
    view_container_user: {
        alignItems: 'center',
        flex: 1
    },
    view_top: {justifyContent: 'center', alignItems: 'center'},
    view_header_group: {
        height: height / 4,
        width: width / 1,
        // backgroundColor:'#000'
    },
    view_top_header: {
        marginTop: height * .01,
        paddingHorizontal: width * .05,
        width: width,
        // flex:1
    },
    txt_top_header: {
        // flex:1,
        color: '#fff',
        fontFamily: 'IRANSansMobile',
        textAlign: 'center',
        fontSize: width / 25
    },
    view_search: {
        height: height * .08,
        width: width * .85,
        marginTop: height * .01,
        flexDirection: 'row',
        borderRadius: width * .07,
        backgroundColor: 'rgba(0,0,0,0.1)',
        borderColor: '#fff',
        borderWidth: width * .001,
        alignSelf: 'center'
    },
    input_search: {
        height: height * .08,
        width: width * .70,
        borderBottomRightRadius: width * .07,
        borderTopRightRadius: width * .07,
        // backgroundColor:'#000'
    },
    textinput_search: {
        textAlign: 'right',
        fontSize: width / 28,
        fontFamily: 'IRANSansMobile',
        paddingHorizontal: width * .03,
        height: height * .08,
        width: width * .70,
        color: '#e8e8e8',
        borderBottomRightRadius: width * .07,
        borderTopRightRadius: width * .07,
        // backgroundColor: '#000'
    },
    icon_search: {
        alignItems: 'center',
        justifyContent: 'center',
        height: height * .08,
        width: width * .10,
        // backgroundColor:'#ff31fe',
        borderBottomLeftRadius: width * .07,
        borderTopLeftRadius: width * .07
    },
    center_page_groups: {flex: 1,},
    view_user: {
        flexDirection: 'column',
        width: width / 2.3,
        height: height * .28,
        marginLeft: width * .02,
        marginTop: height * .02,
        backgroundColor: '#fff',
        elevation: 2,
        alignItems:'center',
        justifyContent:'space-around',
        borderRadius: width * .03
    },
    view_user_icon: {alignItems: 'center', flexDirection: 'row', justifyContent: 'center',
        // backgroundColor: '#fb9a78'
    },
    view_user_txt: {flex: 1, justifyContent: 'center', alignItems: 'flex-end', marginRight: width * .02},
    txt_name_user: {fontSize: width / 27, fontFamily: 'IRANSansMobile',},
    view_img: {justifyContent: 'center', alignItems: 'center',borderRadius:width*.10,width: width * .20,
        height: width * .20,borderColor:'#e5e5e5',borderWidth:width*.01,backgroundColor:'#dedede'},
    img_groups:{fontSize: width / 20, fontFamily: 'IRANSansMobile',color:'#000'},
    pic_user: {
        width: width * .16,
        height: width * .16,
        borderRadius: width * .08,
        // marginTop: height * .02
    },
    view_folow: {
        justifyContent: 'center',
        alignItems: 'center',
        width: width * .20,
        height: height * .04,
        borderRadius: width * .02,
        backgroundColor: '#8b68fc',
    },
    txt_flow: {
        color: '#fff',
        fontFamily: 'IRANSansMobile',
        fontSize: width / 35,
    }

});

