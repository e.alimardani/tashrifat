import React from 'react';
import {StyleSheet, Dimensions, Platform} from 'react-native';


const width = Dimensions.get('window').width;
const height = Dimensions.get('window').height;

export default StyleSheet.create({
    container_member: {flexDirection: 'column', flex: 10, alignItems: 'center', backgroundColor: '#fff'},
    view_appBar: {
        height: height * .08,
        width: width / 1,
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'center',
        marginTop: height * .03,
    },
    txt_AppBar_infoOrder: {
        flex: 1,
        fontSize: width / 20,
        color: '#000',
        textAlign: 'center',
        ...Platform.select({
            ios:{
                fontFamily: 'IRANSansMobile',
                fontWeight: 'bold'
            },
            android:{
                fontFamily: 'IRANSansMobile_Bold',
            }
        }),
    },
    view_search: {
        height: height * .08,
        width: width * .90,
        marginTop: height * .01,
        flexDirection: 'row',
        borderRadius: width * .05
    },
    input_search: {
        height: height * .08,
        width: width * .77,
        borderBottomRightRadius: width * .05,
        borderTopRightRadius: width * .05
    },
    textinput_search: {
        textAlign: 'right',
        fontSize: width / 28,
        fontFamily: 'IRANSansMobile',
        paddingHorizontal: width * .03,
        height: height * .08,
        width: width * .80,
        borderBottomRightRadius: width * .05,
        borderTopRightRadius: width * .05,
        // backgroundColor: '#000'
    },
    icon_search: {
        alignItems: 'center',
        justifyContent: 'center',
        height: height * .08,
        width: width * .10,
        borderBottomLeftRadius: width * .05,
        borderTopLeftRadius: width * .05
    },
    txt_head_memeber: {
        fontSize: width / 18,
        color: '#000',
        alignSelf: 'flex-end',
        marginRight: width * .08,
        ...Platform.select({
            ios:{
                fontFamily: 'IRANSansMobile',
                fontWeight: 'bold'
            },
            android:{
                fontFamily: 'IRANSansMobile_Bold',
            }
        }),

    },
    body_member: {width: width * .90, flex: 9},
    view_member_unclick: {
        width: width * .43,
        height: height * .33,
        alignItems: 'center',
        borderRadius: width * .05,
        marginTop: height * .02,
        marginHorizontal: width * .01,
        elevation: 1

    },
    icon_menu_list: {alignSelf: 'flex-end', marginRight: width * .05, marginTop: height * .02},
    pic_member: {
        width: width * .20,
        height: width * .20,
        borderRadius: width * .10,
        marginTop: height * .01
    },
    txt_name_member: {fontSize: width / 25, color: '#fff', marginTop: height * .01, fontFamily: 'IRANSansMobile'},
    view_social_member: {flexDirection: 'row', justifyContent: 'flex-end', marginTop: height * .04,},
    icon_social: {paddingHorizontal: width * .04},
    bottom_navigate_member: {flex: 1},


});

