import React from 'react';
import {StyleSheet, Dimensions, Platform} from 'react-native';


const width = Dimensions.get('window').width;
const height = Dimensions.get('window').height;

export default StyleSheet.create({
    view_header_user: {
        height: height / 9,
        width: width / 1,
        // backgroundColor:'#000'
    },
    containerHome: {
        flex: 1,
        backgroundColor: '#fff',
        // justifyContent:'center',
        // alignItems:'center'
        margin: 0
    },
    ModalComplete_profile:{
       flex:1,
       alignItems:'center',
       justifyContent:'flex-end'
        // backgroundColor:'#000'
    },
    viewContainerComplete_profile:{
        width:width,
        height:width,
        position:'absolute',
        bottom:0,
        borderTopRightRadius:width*.10,
        borderTopLeftRadius:width*.10,
        backgroundColor:'#fff',
        alignSelf:'center'
    },
    ModalComplateProfile: {
        alignItems: 'center',
        justifyContent: 'flex-end',
        margin: 0,
        padding: 0,
        borderTopLeftRadius: width * .15,
        borderTopRightRadius: width * .15,
    },
    viewNotFound:{width: width , height: height * .60, justifyContent: 'center', alignItems: 'center',},
    txtNotFound:{fontSize: width / 20, color: '#000', fontFamily: 'IRANSansMobile',},
    viewImageTablight: {
        flex:1,
        alignItems: 'center',
        justifyContent: 'flex-end',
        marginVertical: height * .02,
        elevation: 2
    },
    tablighat: {width: width * .90, height: height * .15, borderRadius: width * .07},
    topHome: {
        // backgroundColor:'#000000',
        backgroundColor: 'transparent',
        width: width,
        paddingVertical:10,
        alignItems: 'flex-end',
        justifyContent: 'center'
    },
    viewContainerHeaderFlatList: {},
    viewCatHeader: {
        width: 70,
        height: 70,
        borderRadius: 35,
        // borderWidth: width * .005,
        elevation: 4,
        marginVertical:5,
        justifyContent: 'center',
        alignItems: 'center',
        marginHorizontal: width * .02
    },
    txt_top_header: {
        // flex:1,
        color: '#000',
        fontFamily: 'IRANSansMobile',
        textAlign: 'center',
        fontSize: width / 25
    },
    centerView: {flex: 1, justifyContent: 'space-around', alignItems: 'center', marginTop: 10},
    txt_cat_header: {fontFamily: 'IRANSansMobile', fontSize: width / 28},
    txt_modal_complateProfile: {fontFamily: 'IRANSansMobile', fontSize: width / 25,color:'#000',padding:width*.05,marginTop:width*.05,lineHeight:width*.10},
    img_cat_header: {width: width * .10, height: width * .10},
    txt_cat_header1: {fontFamily: 'IRANSansMobile', fontSize: width / 35, color: '#b1acb5'},
    viewSubCatContainer: {
        elevation: 2,
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: '#fff',
        width: width * .90,
        borderRadius: width * .05
    },
    viewTopSubCat: {
        width: width * .90,
        height: height * .08,
        borderTopRightRadius: width * .05,
        borderTopLeftRadius: width * .05,
        backgroundColor: '#f6f1fa',
        justifyContent: 'flex-end',
        paddingHorizontal: width * .03,
        alignItems: 'center',
        flexDirection: 'row'
    },
    viewShowAll: {flexDirection: 'row', width: width * .20, justifyContent: 'space-between', alignItems: 'center',},
    transectionDot: {width: width * .04, height: height * .02},
    imageIconSubCat: {width: width * .15, height: width * .15},
    topContainerCenterHome: {flexDirection: 'row', alignItems:'center',justifyContent: 'center', marginBottom: height * .02},
    viewContainerCenterHome: {marginTop: height * .01, flex: 1, flexDirection: 'column',},
    viewSubCat: {
        width: width *.27,
        height: height * .17,
        borderRadius: width * .03,
        marginBottom: height * .01,
        marginHorizontal: width * .01,
        backgroundColor: '#f6f0fc',
        elevation: 1,
        alignSelf: 'flex-start',
        justifyContent: 'center',
        alignItems: 'center'
    },
    viewSubCatRequest: {
        width: width * .85,
        height: height * .12,
        borderRadius: width * .03,
        backgroundColor: '#f6f0fc',
        elevation: 1,
        // marginTop:height*.16,
        marginBottom: height * .02,
        alignSelf: 'center',
        flexDirection: 'row',
        justifyContent: 'space-around',
        alignItems: 'center'
    },
    viewTopHeaderHome: {
        width: width / 1, height: height * .10, margin: 0,
        // backgroundColor:'#662e9d'
    },
    viewContainerSupport: {
        backgroundColor: "#fff",
        borderTopLeftRadius: width * .15,
        borderTopRightRadius: width * .15,
        width: width ,
        height: width,
        // marginBottom: height * .20,
        alignItems: 'center'
    },
    viewModal:{width:width,flexDirection:'row',height:width,backgroundColor:'transparent',borderTopLeftRadius:width*.15,borderTopRightRadius:width*.15},
    view_imgModalComplateProfile:{backgroundColor:'#fff',width:width*.40,height:width,borderTopRightRadius:width*.15,},
    imgModalComplateProfile:{width:width*.30,height:height*.50},
    viewTextModalComplateProfile:{width:width*.80,height:width,borderTopLeftRadius:width*.15,},
    btnSupport1: {
        justifyContent: 'center',
        alignItems: 'center',
        width: width * .10,
        height: width * .10,
        backgroundColor: '#f0f6fc',
        borderRadius: width * .05,
        position:'absolute',
        top:width* -(.05),
        elevation: 1},
    topHeaderHome: {
        width: width / 1,
        height: height * .13,
        margin: 0,
        position: 'absolute',
        right: width * (-.0012),
        top: height * (-.01)
    },
    txtNameCategory: {
        fontSize: width / 28,
        ...Platform.select({
            ios: {
                fontFamily: 'IRANSansMobile',
                fontWeight: 'bold'
            },
            android: {
                fontFamily: 'IRANSansMobile_Bold',
            }
        }),
    }
});

