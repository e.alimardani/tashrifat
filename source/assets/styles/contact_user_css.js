import React from 'react';
import {StyleSheet, Dimensions, Platform} from 'react-native';


const width = Dimensions.get('window').width;
const height = Dimensions.get('window').height;

export default StyleSheet.create({
    view_container_user: {
        alignItems: 'center',
        flex: 1
    },
    view_top: {justifyContent: 'center', alignItems: 'center'},
    view_header_user: {
        height: height / 6,
        width: width / 1,
        // backgroundColor:'#000'
    },
    center_page_users: {flex: 1,width:'90%'},
    txt_top_header: {
        // flex:1,
        color: '#fff',
        fontFamily: 'IRANSansMobile',
        textAlign: 'center',
        fontSize: width / 25
    },
    view_top_header: {
        marginTop: height * .01,
        paddingHorizontal: width * .05,
        width: width,
        // flex:1
    },
    view_send_container: {
        elevation: 1,
        backgroundColor: '#fff',
        borderRadius: width * .03,
        marginTop: height * .02,
        padding:15
    },
    view_subject_send_message: {
        flex:1,
        flexDirection: 'row',
        justifyContent: 'space-around',
        alignItems: 'center'
    },
    textinput_subject: {
        borderRadius: width * .03,
        borderWidth: width * .001,
        borderColor: '#999',
        textAlign: 'right',
        flex:1,
        fontFamily: 'IRANSansMobile'

    },
    textInputMessage: {
        flex:1,
        borderRadius: 15,
        borderColor: '#999',
        textAlignVertical: 'top',
        justifyContent: 'flex-start',
        borderWidth: width * .001,
        textAlign: 'right',
        height: 200,
        padding:10,
        fontFamily: 'IRANSansMobile'
    },
    txt_message_user: {fontFamily: 'IRANSansMobile',fontSize: 16, marginBottom: height * .01},
    txt_message_user1: {fontFamily: 'IRANSansMobile',fontSize: 16},
    txt_message_user2: {fontFamily: 'IRANSansMobile',fontSize: 16, marginVertical: height * .01},
    btnSendMessage: {marginTop: height * .03,paddingVertical:10,paddingHorizontal:30, backgroundColor: '#8b68fc',borderRadius: width * .03, elevation: 1, justifyContent: 'center', alignItems: 'center'
    }

});

