import React from 'react';
import {StyleSheet, Dimensions, Platform} from 'react-native';


const width = Dimensions.get('window').width;
const height = Dimensions.get('window').height;

export default StyleSheet.create({
    view_container_messageRecive: {
        flex: 1,
        // backgroundColor: '#000'
    },
    view_header_prof: {
        width: width / 1,
        // backgroundColor:'#000'
    },
    view_center_header: {
        marginTop: height * .02,
        marginBottom: height * .03,
        justifyContent: 'center',
        alignItems: 'center'
    },
    viewNotFound:{width: width, height: height * .40, justifyContent: 'center', alignItems: 'center',},
    txtNotFound:{fontSize: width / 20, color: '#000', fontFamily: 'IRANSansMobile',},
    
    img_user_prof: {
        width: width * .20,
        height: width * .20,
        borderRadius: width * .10,
        borderWidth: width * .01,
        borderColor: '#fff',
        justifyContent:'center',
        alignItems:'center'
    },
    pic_member: {
        width: width * .18,
        height: width * .18,
        borderRadius: width * .09,
    },
    view_name_side_prof: {
        // marginRight: width * .10,
        justifyContent: 'center',
        alignItems: 'center',
        flexDirection: 'row'
        // backgroundColor: '#000'
    },
    txt_post_prof_header: {
        fontFamily: 'IRANSansMobile',
        fontSize: width / 30, color: '#fff'
    },
    txt_nameUser_prof_header: {
        ...Platform.select({
            ios: {
                fontFamily: 'IRANSansMobile',
                fontWeight: 'bold'
            },
            android: {
                fontFamily: 'IRANSansMobile_Bold',
            }
        }),
        fontSize: width / 25, color: '#fff', marginLeft: width * .01
    },
    view_count_group_users: {flexDirection: 'row', marginHorizontal: width * .08, justifyContent: 'space-around',},
    view_bottom_header: {
        borderColor: '#fff',
        borderWidth: width * .002,
        flexDirection: 'row',
        justifyContent: 'space-around',
        alignItems: 'center',
        width: width * .30,
        height: height * .05,
        borderRadius: width * .02

    },
    view_count_txt:{
        width:width*.06,
        height:width*.06,
        borderRadius:width*.01,
        backgroundColor:'#fff',
        justifyContent:'center',
        alignItems:'center'
    },
    txt_header: {
        // backgroundColor:'#fff',
        color: '#404040',
        fontSize: width /25,
        fontFamily: 'IRANSansMobile',
    },
    txt_header1: {
        color: '#fff',
        fontSize: width / 30,
        fontFamily: 'IRANSansMobile',
    },
    view_header_user: {
        height: height / 3,
        width: width / 1,
        // backgroundColor:'#000'
    },
    view_top_header: {
        marginTop: height * .01,
        paddingHorizontal: width * .05,
        width: width,
    },
    txt_top_header: {
        color: '#fff',
        fontFamily: 'IRANSansMobile',
        textAlign: 'center',
        fontSize: width / 25
    },
    center_messageRecive: {flex: 1,alignItems:'flex-end'},
    ModalSearchDriver: {
        alignItems: 'center',
        justifyContent: 'flex-end',
        margin: 0,
        padding: 0,
        borderTopLeftRadius: width * .15,
        borderTopRightRadius: width * .15,
    },
    viewContainerSupport: {
        backgroundColor: "#fff",
        borderRadius: width * .05,
        width: width * .80,
        padding:50,
        elevation:3,
        marginBottom: width * .50,
        alignItems: 'center',
        justifyContent:'center',
    },
    btnSupport1: {
        justifyContent: 'center',
        alignItems: 'center',
        width: width * .10,
        height: width * .10,
        backgroundColor: '#fff',
        borderRadius: width * .05,
        position:'absolute',
        right:width*(-.04),
        top:height*(-.02),
        elevation: 1,
        overflow:'hidden'
    },
    Image_close:{width:width*.05,height:width*.05},
    view_bottom_support: {
        width: width * .60,
        justifyContent: 'space-around',
        alignItems: 'center',
        height: height * .10,
        flexDirection: 'row'
    },
    imgModalSampleWork:{
        width:width*.6,
        height:width*.6,
        // : width * .02,
        // borderTopLeftRadius:width*.05,
        // borderTopRightRadius:width*.05,
        // borderBottomLeftRadius:width*.03,
        // borderBottomRightRadius:width*.03,

    },
    SampleWork_container: {
        // flex: 1,
        width:width*.33, height: width * .33,
        flexDirection: 'row',
        marginTop:height*.005,
        marginHorizontal:width*.003,
        // justifyContent: "center",
        // alignItems: "center",
    },
    view_name: {width:width*.33, height: width * .33,alignItems:'center',justifyContent:'center', backgroundColor: '#fff',},
    img_sampleWork:{width:width*.33,height:width*.33}


});

