import React from 'react';
import {StyleSheet, Dimensions, Platform} from 'react-native';


const width = Dimensions.get('window').width;
const height = Dimensions.get('window').height;

export default StyleSheet.create({
    view_container_user: {
        alignItems: 'center',
        flex: 1
    },
    view_item_search: {
        justifyContent: 'center',
        alignItems: 'center',
        width: width * .70,
        height: height * .07,
        backgroundColor: '#fff',
        marginVertical: height * .003,
        borderTopRightRadius: width * .03,
        borderBottomLeftRadius: width * .03
    },
    txt_item_search: {
        fontFamily: 'IRANSansMobile',
        color: '#8b68fc',
        fontSize: width / 25
    },
    viewNotFound:{width: width, height: height * .40, justifyContent: 'center', alignItems: 'center',},
    txtNotFound:{fontSize: width / 20, color: '#000', fontFamily: 'IRANSansMobile',},
    
    view_top: {justifyContent: 'center', alignItems: 'center'},
    view_header_user: {
        height: height / 4,
        width: width / 1,
        // backgroundColor:'#000'
    },
    view_top_header: {
        marginTop: height * .01,
        paddingHorizontal: width * .05,
        width: width,
        // flex:1
    },
    txt_top_header: {
        // flex:1,
        color: '#fff',
        fontFamily: 'IRANSansMobile',
        textAlign: 'center',
        fontSize: width / 25
    },
    view_search: {
        height: height * .08,
        width: width * .85,
        marginTop: height * .01,
        flexDirection: 'row',
        borderRadius: width * .07,
        backgroundColor: 'rgba(0,0,0,0.1)',
        borderColor: '#fff',
        borderWidth: width * .001,
        alignSelf: 'center'
    },
    input_search: {
        height: height * .08,
        width: width * .50,
        justifyContent: 'center'
        // borderBottomRightRadius: width * .07,
        // borderTopRightRadius: width * .07,
        // backgroundColor:'#000'
    },
    textinput_search: {
        textAlign: 'right',
        fontSize: width / 28,
        fontFamily: 'IRANSansMobile',
        paddingHorizontal: width * .03,
        height: height * .07,
        width: width * .70,
        color: '#e8e8e8',
        // borderBottomRightRadius: width * .07,
        // borderTopRightRadius: width * .07,
        // backgroundColor: '#5b3862'
    },
    modalFilter: {
        justifyContent: 'center',
        height: height * .07 + 2,
        width: width * .25,
        paddingRight: width * .02,
        backgroundColor: '#00000050',
        borderBottomRightRadius: width * .07,
        borderTopRightRadius: width * .07,
        alignSelf: 'center'
    },
    modalTextStyle: {color: '#fff', fontFamily: 'IRANSansMobile', fontSize: width / 30, textAlign: 'center'},
    dropdownStyleText: {
        color: '#2b2b2b',
        width: width * .25,
        fontSize: width / 25,
    },
    icon_search: {
        alignItems: 'center',
        justifyContent: 'center',
        height: height * .08,
        width: width * .10,
        // backgroundColor:'#ff31fe',
        borderBottomLeftRadius: width * .07,
        borderTopLeftRadius: width * .07
    },
    center_page_users: {flex: 1,},
    ModalSearchUserStyle: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'flex-end',
        margin: 0,
        padding: 0,
        // width:width,
        height: height * 65,
        // backgroundColor:'#000',
        borderTopLeftRadius: width * .15,
        borderTopRightRadius: width * .15,
    },
    viewContainerModalSearch: {
        backgroundColor: '#8b68fc',
        height: height * .65,
        width: width * .90,
        // paddingVertical: height * .02,
        borderTopLeftRadius: width * .12,
        borderTopRightRadius: width * .12,
        justifyContent: 'flex-end', alignItems: 'center',
    },
    viewHeaderModalSearch: {
        width: width * .90,
        height: height * .06,
        elevation: 2,
        backgroundColor: '#fff',
        borderTopLeftRadius: width * .15,
        borderTopRightRadius: width * .15,
        // justifyContent: 'center',
        alignItems: 'center',
        flexDirection:'row'
    },
    txtHeaderModalSearch: {fontSize: width / 27,flex:1,textAlign:'center', fontFamily: 'IRANSansMobile',},
    imgIconModal:{width:width*.05,height:width*.05},
    btnExitModal: {
        // backgroundColor: '#00000090',
        width: width * .08,
        height: width * .08,
        // position: 'absolute',
        borderRadius: width * .04,
        justifyContent: 'center',
        alignItems: 'center',
        marginHorizontal:width*.05
    },
    btnBackModal: {
        backgroundColor: '#00000090',
        width: width * .08,
        height: width * .08,
        position: 'absolute',
        borderRadius: width * .04,
        zIndex: 10,
        left: 0,
        top: 0,
        justifyContent: 'center',
        alignItems: 'center'
    },
    view_user: {
        flexDirection: 'row',
        width: width * .90,
        height: height * .12,
        paddingHorizontal: width * .03,
        marginTop: height * .02,
        backgroundColor: '#fff',
        justifyContent: 'center',
        alignItems: 'center',
        borderRadius: width * .10
    },
    view_user_icon: {alignItems: 'center', flexDirection: 'row', justifyContent: 'center'},
    view_user_txt: {flex: 1, justifyContent: 'center', alignItems: 'flex-end', marginRight: width * .02},
    txt_name_user: {fontSize: width / 27, fontFamily: 'IRANSansMobile',},
    view_img: {
        justifyContent: 'flex-end',
        alignItems: 'center',
        borderWidth: width * .01,
        borderRadius: width * .07,
        borderColor: '#d1d1d1'
    },
    pic_user: {
        width: width * .12,
        height: width * .12,
        borderRadius: width * .06,
        // marginTop: height * .08
    },
    view_folow: {
        justifyContent: 'center',
        alignItems: 'center',
        width: width * .10,
        height: width * .10,
        borderRadius: width * .05,
        backgroundColor: '#8b68fc',
        marginRight: width * .02
    },
    txt_flow: {
        color: '#7e7e7e',
        fontFamily: 'IRANSansMobile',
        fontSize: width / 30,
    }

});

