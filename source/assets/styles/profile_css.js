import React from 'react';
import {StyleSheet, Dimensions, Platform} from 'react-native';


const width = Dimensions.get('window').width;
const height = Dimensions.get('window').height;

export default StyleSheet.create({
    container_menu_profile: {
        flex:1,
        borderRadius: width * .02,
        marginTop:10,
        width: width * .80,
        elevation: 2,
        marginBottom:10
    },
    view_shareLink:{
        width:width*.30,
        height:width*.10,
        backgroundColor:'#fff',
        justifyContent:'center',
        alignItems:'center',
    alignSelf:'center',
    marginTop:width*.03,
    borderRadius:width*.02
    },
    viewSocialContainer:{flexDirection:'row',justifyContent:'space-around',marginTop:'4%',width:width*.70,alignSelf:'center'},
    container: {
        // borderRadius: width * .02,
        // backgroundColor: 'rgba(105,253,122,1)',
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent:'flex-end',
        marginHorizontal: width * .02,
        // marginBottom: width * .02,
       
    },
    viewNotFound:{width: width, height: height * .40, justifyContent: 'center', alignItems: 'center',},
    txtNotFound:{fontSize: width / 25, color: '#000', fontFamily: 'IRANSansMobile',},
    txt_takhasos: {
        fontSize: width / 25,
        fontFamily: 'IRANSansMobile',
        textAlign:'right',
        marginRight: width * .05
    },
    txt_takhasos1: {
        fontSize: width / 25,
        fontFamily: 'IRANSansMobile',
        color:'#000'
    },
    view_container_prof: {
        alignItems: 'center'
    },
    view_header_prof: {
        width: width / 1,
        paddingBottom:15
        // backgroundColor:'#000'
    },
    view_top_header: {
        marginTop: height * .01,
        // justifyContent:'center',
        // alignItems:'center',
        // flexDirection:'row',
        paddingHorizontal: width * .05
    },
    txt_top_header: {
        // flex:1,
        color: '#fff',
        fontFamily: 'IRANSansMobile',
        textAlign: 'center',
        fontSize: width / 25
    },
    view_center_header: {
        marginTop: height * .02,
        marginBottom: height * .03,
        justifyContent: 'center',
        alignItems: 'center'
    },
    img_user_prof: {
        width: width * .20,
        height: width * .20,
        borderRadius: width * .10,
        borderWidth: width * .01,
        borderColor: '#fff',
        justifyContent:'center',
        alignItems:'center'
    },
    pic_member: {
        width: width * .18,
        height: width * .18,
        borderRadius: width * .09,
        
    },
    viewContainerLocation:{alignSelf:'flex-start',flexDirection:'row',alignItems:'center',justifyContent:'center'},
    img_location:{width:20,height:20},
    txt_nameUser_prof_header: {
        ...Platform.select({
            ios: {
                fontFamily: 'IRANSansMobile',
                fontWeight: 'bold'
            },
            android: {
                fontFamily: 'IRANSansMobile_Bold',
            }
        }),
        fontSize: 14, color: '#fff', marginLeft: width * .01
    },
    view_name_side_prof: {
        // marginRight: width * .10,
        justifyContent: 'center',
        alignItems: 'center',
        flexDirection: 'row'
        // backgroundColor: '#000'
    },
    txt_post_prof_header: {
        fontFamily: 'IRANSansMobile',
        fontSize: width / 30, color: '#fff'
    },
    view_count_group_users: {
        flexDirection: 'row',
         marginHorizontal: width * .08,
          justifyContent: 'space-around',
        },
    view_bottom_header_follow: {
        borderColor: '#fff',
        borderWidth: width * .002,
        flexDirection: 'row',
        justifyContent: 'space-around',
        alignItems: 'center',
        width: width * .30,
        height: height * .05,
        borderRadius: width * .05

    },
    view_bottom_header: {
        borderColor: '#fff',
        borderWidth: width * .002,
        flexDirection: 'row',
        justifyContent: 'space-around',
        alignItems: 'center',
        flex:1,
        marginHorizontal:20,
        paddingVertical:5,
        // width: width * .30,
        // height: height * .05,
        borderRadius: width * .02

    },
    txt_header_fix: {
        color: '#fff',
        fontSize: width / 30,
        fontFamily: 'IRANSansMobile',
        alignSelf: 'flex-end'
    },
    view_count_txt:{
        width:20,
        height:20,
        borderRadius:2,
        backgroundColor:'#fff',
        justifyContent:'center',
        alignItems:'center'
    },
    txt_header: {
        // backgroundColor:'#fff',
        color: '#404040',
        fontSize: width / 30,
        fontFamily: 'IRANSansMobile',
    },
    txt_header1: {
        color: '#fff',
        fontSize: width / 30,
        fontFamily: 'IRANSansMobile',
    },
    view_menu_top: {
        width: width * .80,
        flexDirection: 'row',
    },
    tabMenu: {
        flex: 1,
        backgroundColor: '#fbf8ff',
        borderTopLeftRadius: width * .02,
        flexDirection: 'row',
        alignItems: 'center', justifyContent: 'center'
    },
    tabMenu_focusProfileUser: {
        flex: 1,
        width:width*.80,
        elevation: 2,
        backgroundColor: '#fff',
        borderTopLeftRadius: width * .02,
        borderTopRightRadius: width * .02,
        flexDirection: 'row',
        alignItems: 'center', justifyContent: 'center'
    },
    tabMenu_focus: {
        flex: 1,
        elevation: 2,
        backgroundColor: '#fff',
        borderTopLeftRadius: width * .02,
        flexDirection: 'row',
        alignItems: 'center', justifyContent: 'center'
    },
    tabMenu1: {
        flex: 1,
        backgroundColor: '#fbf8ff',
        borderTopRightRadius: width * .02,
        flexDirection: 'row',
        alignItems: 'center', justifyContent: 'center'
    },
    tabMenu1_focus: {
        flex: 1,
        elevation: 2,
        backgroundColor: '#fff',
        borderTopRightRadius: width * .02,
        flexDirection: 'row',
        alignItems: 'center', justifyContent: 'center'
    },
    txt_tabMenu: {
        padding:5,
        fontFamily: 'IRANSansMobile',
        fontSize: width / 25
    },
    view_menu2: {
        // backgroundColor: '#b0ff7c',
        borderBottomRightRadius: width * .02,
        borderBottomLeftRadius: width * .02,
        justifyContent: 'center',
        alignItems: 'center'
    },
    view_menu3: {
        // backgroundColor: '#ff513e',
        height: height * .39,
        width: width * .80,
        marginTop:width*(-.06),
        borderBottomRightRadius: width * .02,
        borderBottomLeftRadius: width * .02,
        justifyContent: 'center',
        alignItems: 'flex-start',
        padding:10
    },
    view_bottom_screen: {
        //backgroundColor: "#eeb6b5",
        width: width * .80,
        marginTop: 10,
        flexDirection: 'column',
        padding:10,
    },
    view_bottom_screen1: {
        // backgroundColor: "#726aff",
        // height: height / 2.8,
        width: width * .80,
        marginTop: 15,
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'center'
        // flex:1
    },
    left_bottom_screen: {
        flex: 1,
        //backgroundColor: "#ee3cbe",
        alignItems: 'center',
        // height: height / 70,
        justifyContent: 'center',
        flexDirection: 'row'
    },
    center_bottom_screen: {
        //backgroundColor: "#777aee",
        marginTop:10,
        justifyContent: 'center',
        alignItems: 'center',
        flexDirection: 'row'
    },
    right_bottom_screen: {
        flex: 1,
        height: height / 2.6,
        justifyContent: 'space-around',
        alignItems: 'center',
        flexDirection: 'column'
    },
    txt_icon_bottom: {
        fontFamily: 'IRANSansMobile',
        fontSize: width / 28,
        textAlign: 'center',
        paddingTop: height * .02
    },
    view_icon_name: {
        justifyContent: 'center', alignItems: 'center',flex:1
    },


});

