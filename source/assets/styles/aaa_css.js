import React from 'react';
import {StyleSheet, Dimensions, Platform} from 'react-native';


const width = Dimensions.get('window').width;
const height = Dimensions.get('window').height;

export default StyleSheet.create({
    container_prof: {flexDirection: 'column', flex: 10, alignItems: 'center'},
    header_prof: {height: height * .43,alignItems:'center'},
    img_prof: {width: width / 1, height: height * .30},
    view_img_user: {
        position: 'absolute',
        top: height * .24,
        alignItems: 'center'},
    img_user: {width: width * .20, height: width * .20, borderRadius: width * .10},
    name_user: {
        fontFamily: 'IRANSansMobile',
        fontSize: width / 25,
        color: '#000',
        fontWeight: 'bold',
        paddingTop: height * .01
    },
    body_prof: {flex: 6.2, width: width * .90,},
    view_topBody: {flexDirection: 'row', justifyContent: 'space-around'},
    view_count_message: {width: width * .20, alignItems: 'center'},
    txt_count_message: {fontFamily: 'IRANSansMobile', fontSize: width / 30, color: '#000', fontWeight: 'bold'},
    txt_count_message_fix: {fontFamily: 'IRANSansMobile', fontSize: width / 30, color: '#494949'},
    view_btn_rezome: {
        backgroundColor: '#ee745f',
        width: width * .20,
        height: height * .05,
        borderRadius: width * .05,
        alignItems: 'center',
        flexDirection: 'row',
        justifyContent: 'center'
    },
    view_btn_follow: {
        backgroundColor: '#ee745f',
        width: width * .22,
        height: height * .05,
        borderRadius: width * .05,
        alignItems: 'center',
        flexDirection: 'row',
        justifyContent: 'center'
    },
    txt_rezome: {fontFamily: 'IRANSansMobile', fontSize: width / 30, color: '#fff', paddingRight: width * .01},
    txt_follow: {fontFamily: 'IRANSansMobile', fontSize: width / 30, color: '#fff', paddingRight: width * .01},
    information_user: {},
    view_infoUser: {
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'flex-end',
        paddingTop: height * .03,
        paddingBottom: height * .02
    },
    txt_infoUser_fix: {fontFamily: 'IRANSansMobile', fontSize: width / 25, paddingRight: width * .02,},
    txt_infoUser: {fontFamily: 'IRANSansMobile', fontSize: width / 25, color: '#000', paddingBottom: height * .01},
    bottom_navigate_prof: {flex: 1},
    view_infoUserFlat: {paddingRight: width * .02}

});

