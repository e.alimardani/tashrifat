import React from 'react';
import {StyleSheet, Dimensions, Platform} from 'react-native';


const width = Dimensions.get('window').width;
const height = Dimensions.get('window').height;

export default StyleSheet.create({

    view_bottom_user:{
        height:height*.10,
        borderTopRightRadius:width*.05,
        borderTopLeftRadius:width*.05,
        width:width,
        flexDirection:'row',
        alignItems:'center'
    },
    view_bottom_user_home:{
        borderTopRightRadius:width*.07,
        borderTopLeftRadius:width*.07,
        width:width,
        flexDirection:'row',
        alignItems:'center',
        backgroundColor:'#fff',
        elevation: 10,
        borderTopWidth:width*.003,
        borderTopColor:'#6c1ebb'
    },
    view_search_navigation_bottom:{
        flex: 1,
        height:height*.08,
        // backgroundColor: '#000',
        alignItems:'center',
        justifyContent:'center'
    },view_group_navigation_bottom:{
        flex: 1,
        height:height*.08,
        // backgroundColor: '#4b5aba',
        alignItems:'center',
        justifyContent:'center'
    },view_member_navigation_bottom:{
        flex: 1,
        height:height*.08,
        // backgroundColor: '#ba4933',
        alignItems:'center',
        justifyContent:'center'
    },view_profile_navigation_bottom:{
        flex: 1,
        height:height*.08,
        // backgroundColor: '#3cba43',
        alignItems:'center',
        justifyContent:'center'
    },
    txt_navigate_bottom:{fontSize:13,fontFamily: 'IRANSansMobile',color:'#000'}

});

