import React, {Component} from 'react';
import {Router, Stack, Scene, Drawer, Actions} from 'react-native-router-flux';
import {BackHandler, Dimensions,ToastAndroid} from 'react-native';
import Toast from 'react-native-simple-toast';
import {connect, Provider} from 'react-redux';
import {createStore} from 'redux';
import reducer from './Redux/Reducers'
import login from './components/Intro/login';
import signUp from './components/Intro/signup';
import profile from './components/Profile/profile';
import users from './components/User/users';

import Edit_profile from "./components/Profile/Edit_profile";
import Splash_Screen from "./components/Other/Splash_Screen";
import DrawerMenu from "./components/Shared/Drawer";
import groups from "./components/User/groups";
import store from "./Redux/Store";
import Profile_user from "./components/Profile/Profile_user";
import contact_user from "./components/User/contact_user";

import sampleWork from "./components/Profile/sampleWork";
import sampleWorkUser from "./components/User/sampleWorkUser";
import Home_Screen from "./components/Home/Home_Screen";
import MessageInBox from './components/User/Messages/MessageInBox';
import MessageSendBox from "./components/User/Messages/MessageSendBox";
import ListUserInBox from "./components/User/Messages/ListUserInBox";
import ListUserSendBox from "./components/User/Messages/ListUserSendBox";
import User_Friend from "./components/User/User_Friend";
import Search1 from "./components/SearchAdvanced/Search1";
import Search2 from "./components/SearchAdvanced/Search2";
import Search3 from "./components/SearchAdvanced/Search3";
import Search4 from "./components/SearchAdvanced/Search4";
import Posts from "./components/Home/Posts";
import Post from "./components/Home/Post";
import RequestsFollowList from './components/User/RequestsFollowList';
import ticketSend from './components/User/Ticket/Ticket_Send';
import myTicketList from './components/User/Ticket/MyTicketList';
import ReplayTicket from './components/User/Ticket/ReplayTicket';
import Maps from './components/Other/Maps';
import InsertSocial from './components/Profile/InsertSocial';
import Categories from './components/Home/Categories';
import MainVideoPlayer from './components/Other/VideoPlayer';
import Support from './components/User/Ticket/Support';
import ContactUs from './components/User/Ticket/ContactUs'
import AboutUs from './components/User/Ticket/AboutUs'
import AddCode from './components/Intro/AddCode'
import Reply from './components/User/Messages/Reply'
import Orientation from 'react-native-orientation';
import Subscribe from './components/User/Subscribe'

const width = Dimensions.get('window').width;
let backButtonPressedOnceToExit = false;
export default class App extends Component {

constructor(props){
    super(props);
}

onBackPress() {
    console.log(Actions.currentScene)
    if (backButtonPressedOnceToExit) {
        BackHandler.exitApp()
    } else {

        if (Actions.currentScene !== 'Home_Screen' && Actions.currentScene !== 'Splash') {
            Actions.pop();
            Orientation.lockToPortrait();
            return true;
        }
        else {
            backButtonPressedOnceToExit = true;
            ToastAndroid.show("برای خروج از برنامه دوباره دکمه بازگشت را بفشارید.",ToastAndroid.SHORT);
            //setting timeout is optional
            setTimeout( () => { backButtonPressedOnceToExit = false }, 2000);
            return true;

        }
    }
}


    store = createStore(reducer);

    render() {
        const RouterWithRedux = connect()(Router);
        return (
            <Provider store={store}>
                <RouterWithRedux backAndroidHandler={this.onBackPress.bind(this)}>
                    <Stack key="root" hideNavBar>
                        <Scene key="Splash" component={Splash_Screen}/>
                        <Scene key="login" component={login} />
                        <Scene key="signUp" component={signUp} />
                        <Drawer
                            drawerPosition={'right'}
                            drawerWidth={width * .80}
                            key='drawer'
                            contentComponent={DrawerMenu}>
                            <Scene key="drawer_root" hideNavBar>
                                <Scene key="Home_Screen" component={Home_Screen} type='reset'/>
                                <Scene key="profile" component={profile}/>
                                <Scene key="sampleWork" component={sampleWork}/>
                                <Scene key="sampleWorkUser" component={sampleWorkUser}/>
                                <Scene key="Profile_user" component={Profile_user}/>
                                <Scene key="contact_user" component={contact_user}/>
                                <Scene key="users" component={users} />
                                <Scene key="User_Friend" component={User_Friend} />
                                <Scene key="Edit_profile" component={Edit_profile}/>
                                <Scene key="groups" component={groups}/>
                                <Scene key="MessageInBox" component={MessageInBox} />
                                <Scene key="MessageSendBox" component={MessageSendBox} />
                                <Scene key="ListUserSendBox" component={ListUserSendBox} />
                                <Scene key="ListUserInBox" component={ListUserInBox} />
                                <Scene key="Search1" component={Search1} />
                                <Scene key="Search2" component={Search2} />
                                <Scene key="Search3" component={Search3} />
                                <Scene key="Search4" component={Search4} />
                                <Scene key="Categories" component={Categories}/>
                                <Scene key="Posts" component={Posts}/>
                                <Scene key="Post" component={Post}/>
                                <Scene key="RequestsFollowList" component={RequestsFollowList}/>
                                <Scene key="ticketSend" component={ticketSend}/>
                                <Scene key="myTicketList" component={myTicketList}/>
                                <Scene key="ReplayTicket" component={ReplayTicket}/>
                                <Scene key="insertSocial" component={InsertSocial}/>
                                <Scene key="maps" component={Maps}  />
                                <Scene key="MainVideoPlayer" component={MainVideoPlayer}  />
                                <Scene key="support" component={Support}/>
                                <Scene key="contactUs" component={ContactUs}/>
                                <Scene key="subscribe" component={Subscribe} />
                                <Scene key="aboutUs" component={AboutUs}/>
                                <Scene key="addCode" component={AddCode} type='reset'/>
                                <Scene key="reply" component={Reply}/>
                            </Scene>
                        </Drawer>
                    </Stack>
                </RouterWithRedux>
            </Provider>
        );
    }
}