import {SET_SKILL_USER} from '../Actions/Type';

const initialState = {
    skillUser: null,

};

let skillUser;
export default skillUser = (state = initialState,action = {}) => {
    switch (action.type) {
        case SET_SKILL_USER :
            const {skill_user} = action;
            return {
                skillUser: skill_user.skill_user,
            };
            break;
        default : return state;
    }

}