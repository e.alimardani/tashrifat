import {SET_USER} from '../Actions/Type';

const initialState = {
    id: null,
    username: null,
    email: null,
    full_name: null,
    work_in: null,
    phone_number: null,
    friends_count:null,
    Groups_count: null,
    Avatar: null,
    apiToken: null,
    instegram: null,
    telegram: null,
    whatsapp: null,
    Genre:null,
    messageCount:0,
    Is_vip:false,
};

let InfoUserProfile;
export default InfoUserProfile = (state = initialState, action = {}) => {
    switch (action.type) {
        case SET_USER :
            const {user} = action;
            return {
                id: user.id,
                username: user.username,
                email: user.email,
                full_name: user.full_name,
                work_in: user.work_in,
                phone_number: user.phone_number,
                friends_count:user.friends_count,
                Groups_count: user.Groups_count,
                Avatar: user.Avatar,
                apiToken: user.apiToken,
                instegram: user.instegram,
                telegram: user.telegram,
                whatsapp: user.whatsapp,
                Genre:user.Genre,
                messageCount:user.messageCount,
                Is_vip:user.Is_vip
            };
            break;
        default : return state;
    }

}