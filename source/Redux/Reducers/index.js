import {combineReducers} from 'redux';
import user from './InfoUserProfile';
import skillUser from './Skill_User';
import global from './globalReducer';



export default combineReducers({
    user,
    global,
    skillUser
});



