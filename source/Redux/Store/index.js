import {createStore, applyMiddleware, compose} from 'redux';
// import {AsyncStorage} from 'react-native';
import AsyncStorage from '@react-native-community/async-storage';
import thunk from "redux-thunk";
import {createLogger} from "redux-logger";
import reducers from '../Reducers/index';
import {persistStore} from 'redux-persist'

const middleware = [thunk];

middleware.push(createLogger());

const store = createStore(
    reducers,
    undefined,
    compose(
        applyMiddleware(...middleware)
    )
);

// persistStore(store,{storage : AsyncStorage});

export default store;