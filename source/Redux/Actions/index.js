import * as types from './Type'

export const setUser = (user) => ({
    type: types.SET_USER,
    user
});
export const setSkillUser = (skill_user) => ({
    type: types.SET_SKILL_USER,
    skill_user
});
